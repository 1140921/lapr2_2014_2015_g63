/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Login;
import eventoscientificos.domain.Utilizador;
import java.io.FileNotFoundException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class AlterarUtilizadorControllerTest {

    public AlterarUtilizadorControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of alteraDados method, of class AlterarUtilizadorController.
     */
    @Test
    public void testAlteraDados() throws FileNotFoundException {
        System.out.println("alteraDados");
        String strNome = "Hulk Smash";
        String strUsername = "Mulk";
        String strPwd = "Hulk.Don't.know";
        String strEmail = "hulk@foget.email";
        Empresa m_empresa = new Empresa();
        Utilizador u = new Utilizador();
        u.setNome("Hulk");
        u.setUsername("Mudar");
        u.setEmail("Email");
        u.setPassword("123");
        Login log = new Login(u.getUsername(), u.getPwd());
        LoginController loginController = new LoginController(m_empresa);
        loginController.setLogin(log);
       
        m_empresa.getRegistoUtilizadores().addUtilizador(u);
        Utilizador m_user = m_empresa.getRegistoUtilizadores().getUtilizadorByID("Hulk");

        AlterarUtilizadorController instance = new AlterarUtilizadorController(m_empresa);
        instance.alteraDados(strNome, strUsername, strPwd, strEmail);
        boolean expResult = true;
        boolean result = instance.alteraDados(strNome, strUsername, strPwd, strEmail);
        assertEquals(expResult, result);
    }
    
    
    
     /**
     * Test of alteraDados method, of class AlterarUtilizadorController.
     */
    @Test
    public void testAlteraDadosDois() throws FileNotFoundException {
        System.out.println("alteraDados");
        String strNome = "Hulk Smash";
        String strUsername = "Mulk";
        String strPwd = "Hulk.Don't.know";
        String strEmail = "hulk@foget.email";
        Empresa m_empresa = new Empresa();
        Utilizador u = new Utilizador();
        u.setNome("Hulk");
        u.setUsername("Mudar");
        u.setEmail("Email");
        u.setPassword("123");
        Login log = new Login(u.getUsername(), u.getPwd());
        LoginController loginController = new LoginController(m_empresa);
        loginController.setLogin(log);
       
        m_empresa.getRegistoUtilizadores().addUtilizador(u);
//        Utilizador m_user = m_empresa.getRegistoUtilizadores().getUtilizadorByID("Hulk");

        AlterarUtilizadorController instance = new AlterarUtilizadorController(m_empresa);
        instance.alteraDados(strNome, strUsername, strPwd, strEmail);
        boolean expResult = true;
        boolean result = instance.alteraDados(strNome, strUsername, strPwd, strEmail);
        assertEquals(expResult, result);
    }

}
