/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Login;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.domain.Utilizador;
import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class SubmeterArtigoControllerTest {

    public SubmeterArtigoControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isVazioAutores method, of class SubmeterArtigoController.
     */
    @Test
    public void testIsVazioAutores() {
        System.out.println("isVazioAutores");
        Empresa empresa = new Empresa();
        Utilizador u = new Utilizador("test", "test", "test", "test@test.com");
        empresa.getRegistoUtilizadores().addUtilizador(u);
        Login login = new Login(u.getUsername(), u.getPwd());
        empresa.setLogin(login);

        CriarEventoController controllerEvento = new CriarEventoController(empresa);
        controllerEvento.novoEvento();
        controllerEvento.setTitulo("Titulo");

        controllerEvento.setDescricao("Descrção");
        controllerEvento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        controllerEvento.setDataInicio(d1);
        controllerEvento.setDataFim(d1);
        controllerEvento.setDataInicioSubmissao(d1);
        controllerEvento.setDataFimSubmissao(d1);
        controllerEvento.setDataInicioDistribuicao(d1);
        controllerEvento.addOrganizador(u.getUsername());

        controllerEvento.registaEvento();

        CriarSessaoTematicaController controllerSessaoT = new CriarSessaoTematicaController(empresa);
        controllerSessaoT.getListaEventosRegistadosDoUtilizador(login.getUsername());
        List<Evento> evs = empresa.getRegistoEventos().getListaEventosRegistadosDoUtilizador(login.getUsername());
        System.out.println(evs.get(0));
        controllerSessaoT.setEvento(evs.get(0));
        controllerSessaoT.setDados("descr", "descr", d1, d1, d1,d1,d1);
        controllerSessaoT.addProponente(u.getUsername());
        controllerSessaoT.registaProponente();
        controllerSessaoT.registaSessaoTematica();

        DefinirCPController controllerCP = new DefinirCPController(empresa);
        controllerCP.getListaCPDefiniveisEmDefinicao(u.getUsername());

        empresa.getRegistoEventos().registaEvento(evs.get(0));
        controllerCP.novaCP(evs.get(0));
        controllerCP.novoMembroCP(login.getUsername());
        controllerCP.registaCP();

        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);

        instance.selectSubmissivel(evs.get(0));

        instance.setDados("Titulo", "Descrição","sx");

        Autor a = instance.novoAutor("",login.getUsername(),u.getEmail() ,"test");
        instance.addAutor(a);

        boolean expResult = false;
        boolean result = instance.isVazioAutores();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of isAutorCorrespondente method, of class SubmeterArtigoController.
     */
    @Test
    public void testIsAutorCorrespondente() {
        System.out.println("isAutorCorrespondente");
        Empresa empresa = new Empresa();
        Utilizador u = new Utilizador("test", "test", "test", "test@test.com");
        empresa.getRegistoUtilizadores().addUtilizador(u);
        Login login = new Login(u.getUsername(), u.getPwd());
        empresa.setLogin(login);

        CriarEventoController controllerEvento = new CriarEventoController(empresa);
        controllerEvento.novoEvento();
        controllerEvento.setTitulo("Titulo");

        controllerEvento.setDescricao("Descrção");
        controllerEvento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        controllerEvento.setDataInicio(d1);
        controllerEvento.setDataFim(d1);
        controllerEvento.setDataInicioSubmissao(d1);
        controllerEvento.setDataFimSubmissao(d1);
        controllerEvento.setDataInicioDistribuicao(d1);
        controllerEvento.addOrganizador(u.getUsername());

        controllerEvento.registaEvento();

        CriarSessaoTematicaController controllerSessaoT = new CriarSessaoTematicaController(empresa);
        controllerSessaoT.getListaEventosRegistadosDoUtilizador(login.getUsername());
        List<Evento> evs = empresa.getRegistoEventos().getListaEventosRegistadosDoUtilizador(login.getUsername());
        System.out.println(evs.get(0));
        controllerSessaoT.setEvento(evs.get(0));
        controllerSessaoT.setDados("descr", "descr", d1, d1, d1,d1,d1);
        controllerSessaoT.addProponente(u.getUsername());
        controllerSessaoT.registaProponente();
        controllerSessaoT.registaSessaoTematica();

        DefinirCPController controllerCP = new DefinirCPController(empresa);
        controllerCP.getListaCPDefiniveisEmDefinicao(u.getUsername());

        empresa.getRegistoEventos().registaEvento(evs.get(0));
        controllerCP.novaCP(evs.get(0));
        controllerCP.novoMembroCP(login.getUsername());
        controllerCP.registaCP();

        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);

        instance.selectSubmissivel(evs.get(0));

        instance.setDados("Titulo", "Descrição","chave");

        Autor a = instance.novoAutor("",login.getUsername(),u.getEmail() ,"test");
        instance.addAutor(a);
        instance.setCorrespondente(a);

        boolean expResult = true;
        boolean result = instance.isAutorCorrespondente();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of isVazioPdf method, of class SubmeterArtigoController.
     */
    @Test
    public void testIsVazioPdf() {
        System.out.println("isVazioPdf");

        Empresa empresa = new Empresa();
        Utilizador u = new Utilizador("test", "test", "test", "test@test.com");
        empresa.getRegistoUtilizadores().addUtilizador(u);
        Login login = new Login(u.getUsername(), u.getPwd());
        empresa.setLogin(login);

        CriarEventoController controllerEvento = new CriarEventoController(empresa);
        controllerEvento.novoEvento();
        controllerEvento.setTitulo("Titulo");

        controllerEvento.setDescricao("Descrção");
        controllerEvento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        controllerEvento.setDataInicio(d1);
        controllerEvento.setDataFim(d1);
        controllerEvento.setDataInicioSubmissao(d1);
        controllerEvento.setDataFimSubmissao(d1);
        controllerEvento.setDataInicioDistribuicao(d1);
        controllerEvento.addOrganizador(u.getUsername());

        controllerEvento.registaEvento();

        CriarSessaoTematicaController controllerSessaoT = new CriarSessaoTematicaController(empresa);
        controllerSessaoT.getListaEventosRegistadosDoUtilizador(login.getUsername());
        List<Evento> evs = empresa.getRegistoEventos().getListaEventosRegistadosDoUtilizador(login.getUsername());
        System.out.println(evs.get(0));
        controllerSessaoT.setEvento(evs.get(0));
        controllerSessaoT.setDados("descr", "descr", d1, d1, d1,d1,d1);
        controllerSessaoT.addProponente(u.getUsername());
        controllerSessaoT.registaProponente();
        controllerSessaoT.registaSessaoTematica();

        DefinirCPController controllerCP = new DefinirCPController(empresa);
        controllerCP.getListaCPDefiniveisEmDefinicao(u.getUsername());

        empresa.getRegistoEventos().registaEvento(evs.get(0));
        controllerCP.novaCP(evs.get(0));
        controllerCP.novoMembroCP(login.getUsername());
        controllerCP.registaCP();

        SubmeterArtigoController instance = new SubmeterArtigoController(empresa);

        instance.selectSubmissivel(evs.get(0));

        instance.setDados("Titulo", "Descrição","chave");

        Autor a = instance.novoAutor("",login.getUsername(), u.getEmail(),"test");
        instance.addAutor(a);
        instance.setCorrespondente(a);
        instance.setFicheiro("ficheiro PDF");

        boolean expResult = false;
        boolean result = instance.isVazioPdf();
        assertEquals(expResult, result);

    }

}
