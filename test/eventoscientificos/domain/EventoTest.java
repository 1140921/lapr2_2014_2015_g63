/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.state.EventoState;
import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class EventoTest {

    public EventoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of isInEmSubmissao method, of class Evento.
     */
    @Test
    public void testIsInEmSubmissao() {
        System.out.println("isInEmSubmissao");
        Evento instance = new Evento();
        instance.setCP(null);
        boolean expResult = false;
        boolean result = instance.isInEmSubmissao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    @Test
    public void testIsInEmSubmissao2() {
        System.out.println("isInEmSubmissao");
     
        Empresa empresa = new Empresa();
        Utilizador u = new Utilizador("test", "test", "test", "test@test.com");
        empresa.getRegistoUtilizadores().addUtilizador(u);
        Login login = new Login(u.getUsername(),u.getPwd());
        empresa.setLogin(login);
        
       
        
        CriarEventoController controllerEvento = new CriarEventoController(empresa);
        controllerEvento.novoEvento();
        controllerEvento.setTitulo("Titulo");

        controllerEvento.setDescricao("Descrção");
        controllerEvento.setLocal("local");
        Date d1 = new Date(2018,8,9);
        controllerEvento.setDataInicio(d1);
        controllerEvento.setDataFim(d1);
        controllerEvento.setDataInicioSubmissao(d1);
        controllerEvento.setDataFimSubmissao(d1);
        controllerEvento.setDataInicioDistribuicao(d1);
        controllerEvento.addOrganizador(u.getUsername());
        
        controllerEvento.registaEvento();
//        
//        for(Evento ev : empresa.getRegistoEventos().getListaEventosRegistadosDoUtilizador(u.getUsername())){
//            System.out.println("--->"+ev);
//        }
//        
        CriarSessaoTematicaController controllerSessaoT = new CriarSessaoTematicaController(empresa);
        controllerSessaoT.getListaEventosRegistadosDoUtilizador(login.getUsername());
         List<Evento> evs = empresa.getRegistoEventos().getListaEventosRegistadosDoUtilizador(login.getUsername());
        System.out.println(evs.get(0));
        controllerSessaoT.setEvento(evs.get(0));
        controllerSessaoT.setDados("descr","descr", d1, d1, d1,d1,d1);
        controllerSessaoT.addProponente(u.getUsername());
        controllerSessaoT.registaProponente();
        controllerSessaoT.registaSessaoTematica();
        
       
        
        DefinirCPController controllerCP = new DefinirCPController(empresa);
        controllerCP.getListaCPDefiniveisEmDefinicao(u.getUsername());

        empresa.getRegistoEventos().registaEvento(evs.get(0));
        controllerCP.novaCP(evs.get(0));
        controllerCP.novoMembroCP(login.getUsername());
        controllerCP.registaCP();
        Revisor r = new Revisor(u);
        
        Evento instance = controllerEvento.getEvento();

        boolean expResult = true;
        boolean result = instance.isInEmSubmissao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }



    /**
     * Test of hasOrganizador method, of class Evento.
     */
    @Test
    public void testHasOrganizador() {
        System.out.println("hasOrganizador");
        String strID = "Este";
        Utilizador u = new Utilizador("N e este", "Este", "tb n", "achas");
        Evento instance = new Evento();
        instance.getListaOrganizadores().addOrganizador(u);
        boolean expResult = true;
        boolean result = instance.hasOrganizador(strID);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }


}
