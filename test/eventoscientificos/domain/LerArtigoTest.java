/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class LerArtigoTest {
    
    public LerArtigoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of lercsv method, of class LerArtigo.
     */
    @Test
    public void testLercsv() throws Exception {
        System.out.println("lercsv");
        String FILE_TXT = "Ficheiro_UC18_v03.csv";
        Empresa m_empresa = new Empresa();
        
        LerArtigo instance = new LerArtigo(m_empresa, FILE_TXT);
        String[][] expResult = null;
        String[][] result = instance.lercsv(FILE_TXT);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of numeroLinhasFicheiro method, of class LerArtigo.
     */
    @Test
    public void testNumeroLinhasFicheiro() throws Exception {
        System.out.println("numeroLinhasFicheiro");
        String FILE_TXT = "Ficheiro_UC18_v03.csv";
        int expResult = 2;
        int result = LerArtigo.numeroLinhasFicheiro(FILE_TXT);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of numeroColunasFicheiro method, of class LerArtigo.
     */
    @Test
    public void testNumeroColunasFicheiro() throws Exception {
        System.out.println("numeroColunasFicheiro");
        String FILE_TXT = "Ficheiro_UC18_v03.csv";
        
        int expResult = 11;
        int result = LerArtigo.numeroColunasFicheiro(FILE_TXT);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
