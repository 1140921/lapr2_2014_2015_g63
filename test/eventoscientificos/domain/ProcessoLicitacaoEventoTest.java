/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class ProcessoLicitacaoEventoTest {
    
    public ProcessoLicitacaoEventoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
//
//    /**
//     * Test of novaLicitação method, of class ProcessoLicitacaoEvento.
//     */
//    @Test
//    public void testNovaLicitação() {
//        System.out.println("novaLicita\u00e7\u00e3o");
//        Revisor r = null;
//        Submissao s = null;
//        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
//        Licitacao expResult = null;
//        Licitacao result = instance.novaLicitação(r, s);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addLicitacao method, of class ProcessoLicitacaoEvento.
//     */
//    @Test
//    public void testAddLicitacao() {
//        System.out.println("addLicitacao");
//        Licitacao l = null;
//        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
//        boolean expResult = false;
//        boolean result = instance.addLicitacao(l);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of valida method, of class ProcessoLicitacaoEvento.
//     */
//    @Test
//    public void testValida() {
//        System.out.println("valida");
//        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
//        boolean expResult = false;
//        boolean result = instance.valida();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getLicitacoesUtilizador method, of class ProcessoLicitacaoEvento.
//     */
    @Test
    public void testGetLicitacoesUtilizador() {
        
        
        Empresa m_empresa = new Empresa();
        
        System.out.println("getLicitacoesUtilizador");
        Evento e = new Evento();
        e.setTitulo("ASAD");
        e.setDescricao("bla bla");
        e.setLocal("Porto");
        Date a = new Date(2018,06,25);
        e.setDataInicio(a);
        e.setDataFim(a);
        e.setDataInicioSubmissao(a);
        e.setDataFimSubmissao(a);
        e.setDataInicioDistribuicao(a);
        e.setDataLimiteRevisao(a);
        e.setDataLmiteSubmissaoFinal(a);
        
        Utilizador u = new Utilizador("Andre","Bot","1234","bot@isep.ipp.pt");
        Revisor rev = new Revisor(u);
        Autor autor = new Autor("Jose");
        
        Artigo c = new Artigo();
        c.setTitulo("asdasd");
        c.setResumo("bla bla");
        c.setPalavrasChave("SAD");
        c.setAutorCorrespondente(autor);
        
        Submissao sub = new Submissao(c);
        sub.setArtigo(c);
        
        
        Licitacao lici = new Licitacao();
        lici.setInterresse(2);
        lici.setRevisor(rev);
        lici.setSubmissao(sub);
        
        List<Licitacao> ls = new ArrayList<>();
        ls.add(lici);
        
        
        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
        instance.addLicitacao(lici);
        List<Licitacao> expResult = null;
        expResult.add(lici);
        
        List<Licitacao> result = instance.getLicitacoesUtilizador(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of validaLicitacao method, of class ProcessoLicitacaoEvento.
     */
    @Test
    public void testValidaLicitacao() {
        System.out.println("validaLicitacao");
        Licitacao l = null;
        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
        boolean expResult = false;
        boolean result = instance.validaLicitacao(l);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of registaLicitacao method, of class ProcessoLicitacaoEvento.
     */
    @Test
    public void testRegistaLicitacao() {
        System.out.println("registaLicitacao");
        
        Licitacao licitacao = new Licitacao();
        Utilizador u = new Utilizador("Andre","Bot","1234","bot@isep.ipp.pt");
        Revisor rev = new Revisor(u);
        
        Autor autor = new Autor();
        autor.setNome("Jose");
        autor.setStrEmail("jose@isep.ipp.pt");
        autor.setUsername("yes");
        autor.setAfiliacao("asdjiasf");
        
        Artigo artigo = new Artigo();
        artigo.setTitulo("yes");
        artigo.setAutorCorrespondente(autor);
        artigo.setResumo("asdasdasd");
        artigo.setPalavrasChave("sadasdf");
        artigo.setFicheiro("asdaffgg");
        
        Utilizador ut = new Utilizador("joao", "lol", "lol", "joao@isep.ipp.pt");
        
        Submissao a = new Submissao(artigo);
        licitacao.setRevisor(rev);
        licitacao.setInterresse(3);
        licitacao.setSubmissao(a);
        licitacao.setTipoConflitoDetetado(null);
        
        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
        
        instance.registaLicitacao(licitacao);
        
        
        assertTrue(instance.validaLicitacao(licitacao));
        
    }

    /**
     * Test of novaLicitação method, of class ProcessoLicitacaoEvento.
     */
    @Test
    public void testNovaLicitação() {
        System.out.println("novaLicita\u00e7\u00e3o");
        Revisor r = null;
        Submissao s = null;
        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
        Licitacao expResult = null;
        Licitacao result = instance.novaLicitação(r, s);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addLicitacao method, of class ProcessoLicitacaoEvento.
     */
    @Test
    public void testAddLicitacao() {
        System.out.println("addLicitacao");
        Licitacao l = null;
        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
        boolean expResult = false;
        boolean result = instance.addLicitacao(l);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of valida method, of class ProcessoLicitacaoEvento.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        ProcessoLicitacaoEvento instance = new ProcessoLicitacaoEvento();
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
