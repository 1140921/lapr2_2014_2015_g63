/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.state.STState;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paisana
 */
public class SessaoTematicaTest {

    public SessaoTematicaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getProcessoLicitacao method, of class SessaoTematica.
     */
//    @Test
//    public void testGetProcessoLicitacao() {
//        System.out.println("getProcessoLicitacao");
//        SessaoTematica instance = null;
//        ProcessoLicitacao expResult = null;
//        ProcessoLicitacao result = instance.getProcessoLicitacao();
//        assertEquals(expResult, result);
//        
//    }
    /**
     * Test of hasRevisor method, of class SessaoTematica.
     */
    @Test
    public void testHasRevisor() {
        System.out.println("hasRevisor");
        String strID = "Jose";
        Empresa m_empresa = new Empresa();
        Utilizador u = new Utilizador();
        u.setNome("Jose");
        u.setUsername("Jose");
        u.setEmail("Email");
        u.setPassword("123");
        Autor autor = new Autor("Jose");
        autor.setNome("Jose");

        Utilizador u1 = new Utilizador("Anzonio", "Anzonio", "Anzonio", "Anzonio");
        Autor autor1 = new Autor();
        autor1.setUsername(u1.getUsername());

        Evento m_evento = new Evento();

        m_empresa.getRegistoEventos().novoEvento();

        m_evento.setTitulo("Titulo");

        m_evento.setDescricao("Descrção");
        m_evento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        m_evento.setDataInicio(d1);
        m_evento.setDataFim(d1);
        m_evento.setDataInicioSubmissao(d1);
        m_evento.setDataFimSubmissao(d1);
        m_evento.setDataInicioDistribuicao(d1);
        m_evento.getListaOrganizadores().addOrganizador(u);
        m_empresa.getRegistoEventos().registaEvento(m_evento);

        SessaoTematica instance = new SessaoTematica(strID, "descrição", d1, d1, d1, d1, d1, m_evento);

        Proponente p = new Proponente(u);
        Revisor r = new Revisor(u);

        instance.getListaProponentes().getListProponentes().add(p);

        instance.novaCP();
        instance.get_cp().addMembroCP(r);

        m_empresa.getRegistoEventos().getListEventos().get(0).getListaDeSessõesTemáticas().registaSessaoTematica(instance);

        List<Revisor> revisores = new ArrayList<>();
        Revisor re = new Revisor(u);
        revisores.add(re);
        boolean expResult = true;
        boolean result = instance.hasRevisor("Jose");
        assertEquals(expResult, result);

    }

    /* 
     Test of getLicitacoesUtilizador method, of class ProcessoLicitacaoEvento.
     */
//    @Test
//    public void testGetLicitacoesUtilizador() {
//
//        Empresa m_empresa = new Empresa();
//
//        System.out.println("getLicitacoesUtilizador");
//        Evento e = new Evento();
//        e.setTitulo("ASAD");
//        e.setDescricao("bla bla");
//        e.setLocal("Porto");
//        Date a = new Date(2018, 06, 25);
//        e.setDataInicio(a);
//        e.setDataFim(a);
//        e.setDataInicioSubmissao(a);
//        e.setDataFimSubmissao(a);
//        e.setDataInicioDistribuicao(a);
//        e.setDataLimiteRevisao(a);
//        e.setDataLmiteSubmissaoFinal(a);
//
//        Utilizador u = new Utilizador("Andre", "Bot", "1234", "bot@isep.ipp.pt");
//        Revisor rev = new Revisor(u);
//        Autor autor = new Autor("Jose");
//
//        Artigo c = new Artigo();
//        c.setTitulo("asdasd");
//        c.setResumo("bla bla");
//        c.setPalavrasChave("SAD");
//        c.setAutorCorrespondente(autor);
//
//        Submissao sub = new Submissao(c);
//        sub.setArtigo(c);
//
//        Licitacao lici = new Licitacao();
//        lici.setInterresse(2);
//        lici.setRevisor(rev);
//        lici.setSubmissao(sub);
//
//        List<Licitacao> ls = new ArrayList<>();
//        ls.add(lici);
//
//        eventoscientificos.domain.ProcessoLicitacaoEvento instance = new eventoscientificos.domain.ProcessoLicitacaoEvento();
//        instance.addLicitacao(lici);
//        List<Licitacao> expResult = null;
//        expResult.add(lici);
//
//        List<Licitacao> result = instance.getLicitacoesUtilizador(u);
//        assertEquals(expResult, result);
//    }

    /**
     * Test of registaLicitacao method, of class ProcessoLicitacaoEvento.
     */
//    @Test
//    public void testRegistaLicitacao() {
//        System.out.println("registaLicitacao");
//
//        Licitacao licitacao = new Licitacao();
//        Utilizador u = new Utilizador("Andre", "Bot", "1234", "bot@isep.ipp.pt");
//        Revisor rev = new Revisor(u);
//
//        Autor autor = new Autor();
//        autor.setNome("Jose");
//        autor.setStrEmail("jose@isep.ipp.pt");
//        autor.setUsername("yes");
//        autor.setAfiliacao("asdjiasf");
//
//        Artigo artigo = new Artigo();
//        artigo.setTitulo("yes");
//        artigo.setAutorCorrespondente(autor);
//        artigo.setResumo("asdasdasd");
//        artigo.setPalavrasChave("sadasdf");
//        artigo.setFicheiro("asdaffgg");
//
//        Utilizador ut = new Utilizador("joao", "lol", "lol", "joao@isep.ipp.pt");
//        Revisor re = new Revisor(ut);
//        Submissao a = new Submissao(artigo);
//        licitacao.setRevisor(re);
//        licitacao.setInterresse(3);
//        licitacao.setSubmissao(a);
//        licitacao.setTipoConflitoDetetado(null);
//
//        eventoscientificos.domain.ProcessoLicitacaoEvento instance = new eventoscientificos.domain.ProcessoLicitacaoEvento();
//
//        instance.registaLicitacao(licitacao);
//
//        assertTrue(instance.validaLicitacao(licitacao));
//
//    }

}
