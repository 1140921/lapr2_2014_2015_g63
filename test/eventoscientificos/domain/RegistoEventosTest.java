/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class RegistoEventosTest {

    public RegistoEventosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of registaEvento method, of class RegistoEventos.
     */
    @Test
    public void testRegistaEvento() {
        System.out.println("registaEvento");

        RegistoEventos instance = new RegistoEventos();
        Evento evento = new Evento();
        instance.novoEvento();
        evento.setTitulo("Titulo");
        evento.setDescricao("Descrição");
        evento.setLocal("Local");
        Date d = new Date(2018, 8, 9);
        evento.setDataInicio(d);
        evento.setDataFim(d);
        evento.setDataInicioSubmissao(d);
        evento.setDataFimSubmissao(d);
        evento.setDataInicioDistribuicao(d);
        instance.registaEvento(evento);

        Evento evento1 = new Evento();
        instance.novoEvento();
        evento1.setTitulo("Titulo");
        evento1.setDescricao("Descrição");
        evento1.setLocal("Local");

        evento1.setDataInicio(d);
        evento1.setDataFim(d);
        evento1.setDataInicioSubmissao(d);
        evento1.setDataFimSubmissao(d);
        evento1.setDataInicioDistribuicao(d);
        instance.registaEvento(evento1);
        boolean expResult = false;
        boolean result = instance.registaEvento(evento1);
        assertEquals(expResult, result);
      
    }

}
