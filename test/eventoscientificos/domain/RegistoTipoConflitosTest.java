/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paisana
 */
public class RegistoTipoConflitosTest {

    public RegistoTipoConflitosTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of registaTipoConflito method, of class RegistoTipoConflitos.
     */
    @Test
    public void testRegistaTipoConflito() {
        System.out.println("registaTipoConflito");
        TipoConflito tpConflito = new TipoConflito();
        tpConflito.setDescricao("Uma boa descricao");
        RegistoTipoConflitos instance = new RegistoTipoConflitos();
        boolean expResult = true;
        boolean result = instance.registaTipoConflito(tpConflito);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaTipoConflito method, of class RegistoTipoConflitos.
     */
    @Test
    public void testRegistaTipoConflitoNot() {
        System.out.println("registaTipoConflitoNot");
        TipoConflito tpConflito1 = new TipoConflito();
        tpConflito1.setDescricao("Uma boa descricao");
        TipoConflito tpConflito2 = new TipoConflito();
        tpConflito2.setDescricao("Uma boa descricao");
        RegistoTipoConflitos instance = new RegistoTipoConflitos();
        instance.registaTipoConflito(tpConflito1);
        boolean expResult = false;
        boolean result = instance.registaTipoConflito(tpConflito2);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaTipoConflito method, of class RegistoTipoConflitos.
     */
    @Test
    public void testRegistaTipoConflitoNull() {
        System.out.println("registaTipoConflitoNull");
        TipoConflito tpConflito1 = new TipoConflito();
        tpConflito1.setDescricao("Uma boa descricao");
        tpConflito1 = null;
        RegistoTipoConflitos instance = new RegistoTipoConflitos();
        boolean expResult = false;
        boolean result = instance.registaTipoConflito(tpConflito1);
        assertEquals(expResult, result);
    }

}
