/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class GerarEstatisticasTopicosTest {
    
    public GerarEstatisticasTopicosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

 






    /**
     * Test of numeroPalavrasChaveEvento method, of class GerarEstatisticasTopicos.
     */
    @Test
    public void testNumeroPalavrasChaveEvento() {
        System.out.println("numeroPalavrasChaveEvento");
        
        Empresa m_empresa = new  Empresa ();
        Evento m_evento = new Evento();
        Utilizador u = new Utilizador();
        u.setNome("Jose");
        u.setUsername("Jose");
        u.setEmail("Email");
        u.setPassword("123");
        m_empresa.getRegistoEventos().novoEvento();

        m_evento.setTitulo("Titulo");

        m_evento.setDescricao("Descrção");
        m_evento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        m_evento.setDataInicio(d1);
        m_evento.setDataFim(d1);
        m_evento.setDataInicioSubmissao(d1);
        m_evento.setDataFimSubmissao(d1);
        m_evento.setDataInicioDistribuicao(d1);
        m_evento.getListaOrganizadores().addOrganizador(u);
        m_empresa.getRegistoEventos().registaEvento(m_evento);
        
        System.err.println(m_empresa.getRegistoEventos().getListEventos().get(0));
        
        Revisor r = new Revisor(u);
        m_evento.novaCP();

        m_evento.get_cp().addMembroCP(r);

        Autor a = new Autor(u.getNome());
        Artigo artigo = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave;duas");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        
        Submissao sub = new Submissao(artigo);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub);
        
        GerarEstatisticasTopicos instance = new GerarEstatisticasTopicos(m_empresa);
        int expResult = 2;
        int result = instance.numeroPalavrasChaveEvento();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }



}
