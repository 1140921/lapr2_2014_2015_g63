/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paisana
 */
public class CPEventoTest {
    
    public CPEventoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addMembroCP method, of class CPEvento.
     */
    @Test
    public void testAddMembroCP() {
        System.out.println("addMembroCP");
        Utilizador u = new Utilizador("Joao","15","lol","joao@isep.ipp.pt");
        Revisor r = new Revisor(u);
        CPEvento instance = new CPEvento();
        boolean expResult = true;
        boolean result = instance.addMembroCP(r);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of addMembroCP method, of class CPEvento.
     */
    @Test
    public void testAddMembroCPNot() {
        System.out.println("AddMembroCPNot");
        Utilizador u1 = new Utilizador("Joao","15","lol","joao@isep.ipp.pt");
        Revisor r1 = new Revisor(u1);
        Utilizador u2 = new Utilizador("Joao","15","lol","joao@isep.ipp.pt");
        Revisor r2 = new Revisor(u2);
        CPEvento instance = new CPEvento();
        instance.addMembroCP(r1);
        boolean expResult = false;
        boolean result = instance.addMembroCP(r2);
        assertEquals(expResult, result);
        
    }
    
    /**
     * Test of addMembroCP method, of class CPEvento.
     */
    @Test
    public void testAddMembroCPNull() {
        System.out.println("addMembroCPNull");
        Utilizador u = new Utilizador("Joao","15","lol","joao@isep.ipp.pt");
        Revisor r = new Revisor(u);
        r=null;
        CPEvento instance = new CPEvento();
        boolean expResult = false;
        boolean result = instance.addMembroCP(r);
        assertEquals(expResult, result);
        
    }
    
}
