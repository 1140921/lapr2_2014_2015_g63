/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class ArtigoTest {
    
    public ArtigoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

  
    /**
     * Test of setPalavrasChave method, of class Artigo.
     */
    @Test
    public void testSetPalavrasChave() {
        System.out.println("setPalavrasChave");
        String m_strPalavrasChave = "palavra;chave";
        Artigo instance = new Artigo();
        
        instance.setTitulo("titulo");
        instance.setResumo("Resuno");
        instance.setFicheiro("qualquer lado");
//        instance.setAutorCorrespondente(null);
        instance.setPalavrasChave(m_strPalavrasChave);
        // TODO review the generated test code and remove the default call to fail.
    }
    @Test
    public void testSetPalavrasChaveFail() {
        System.out.println("setPalavrasChave");
        String m_strPalavrasChave = "palavra;chave;tem;mais;que;cinco";
        Artigo instance = new Artigo();
        
        instance.setTitulo("titulo");
        instance.setResumo("Resuno");
        instance.setFicheiro("qualquer lado");
//        instance.setAutorCorrespondente(null);
        instance.setPalavrasChave(m_strPalavrasChave);
        // TODO review the generated test code and remove the default call to fail.
    }

    
}
