/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class ListaRevisoesTest {
    
    public ListaRevisoesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getRevisoesRevisor method, of class ListaRevisoes.
     */
    @Test
    public void testGetRevisoesRevisor() {
        System.out.println("getRevisoesRevisor");
        String id = "doris";
        Empresa empresa = new Empresa();
        Utilizador u = new Utilizador();
        u.setNome(id);
        u.setEmail("id@doris.com");
        u.setPassword(id);
        u.setUsername(id);
        empresa.getRegistoUtilizadores().addUtilizador(u);
        Evento e = new Evento();
        e.setTitulo("Titulo");

        e.setDescricao("Descrção");
        e.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        e.setDataInicio(d1);
        e.setDataFim(d1);
        e.setDataInicioSubmissao(d1);
        e.setDataFimSubmissao(d1);
        e.setDataInicioDistribuicao(d1);
        e.getListaOrganizadores().addOrganizador(u);
        Revisor r = new Revisor(u);
        ListaRevisoes instance = new ListaRevisoes();
        List<Revisao> expResult = instance.getRevisoesRevisor(r.getStrUsername());
        List<Revisao> result = instance.getRevisoesRevisor(id);
        assertEquals(expResult, result);
    
    }

   



    /**
     * Test of isEmpty method, of class ListaRevisoes.
     */
    @Test
    public void testIsEmpty() {
        System.out.println("isEmpty");
        ListaRevisoes instance = new ListaRevisoes();
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    
    
}
