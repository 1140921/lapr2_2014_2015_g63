/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class GerarEstatisticasTest {

    public GerarEstatisticasTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of somatorioAtributosClassificacoes method, of class
     * GerarEstatisticas.
     */
    @Test
    public void testSomatorioAtributosClassificacoes() {
        System.out.println("somatorioAtributosClassificacoes");

        Empresa m_empresa = new Empresa();
        Evento m_evento = new Evento();
        Utilizador u = new Utilizador();
        u.setNome("Jose");
        u.setUsername("Jose");
        u.setEmail("Email");
        u.setPassword("123");
        m_empresa.getRegistoEventos().novoEvento();

        m_evento.setTitulo("Titulo");

        m_evento.setDescricao("Descrção");
        m_evento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        m_evento.setDataInicio(d1);
        m_evento.setDataFim(d1);
        m_evento.setDataInicioSubmissao(d1);
        m_evento.setDataFimSubmissao(d1);
        m_evento.setDataInicioDistribuicao(d1);
        m_evento.getListaOrganizadores().addOrganizador(u);
        m_empresa.getRegistoEventos().registaEvento(m_evento);

        Revisor r = new Revisor(u);
        m_evento.novaCP();

        m_evento.get_cp().addMembroCP(r);

        Autor a = new Autor(u.getNome());
        Artigo artigo = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
//        Submissao sub = new Submissao(artigo);
        Revisao rev = new Revisao(artigo);
        
        rev.setRevisor(r);
        
        rev.setM_decisao("Decisao");
        rev.setM_Confianca(5);
        rev.setM_Adequacao(4);
        rev.setM_Originalidade(4);
        rev.setM_Apresentacao(3);
        rev.setM_Recomendacao(2);
        rev.setJustificacao("bla bla bla");
        rev.setArtigo(artigo);
        r.getLstRevisoes().addRevisao(rev);
        
        
//        m_evento.getListaSubmissoes().addSubmissao(sub);
        
        

        int indexEvento = 0;
        int indexRevisor = 0;
        GerarEstatisticas instance = new GerarEstatisticas(m_empresa);
        

        int expResult = 17;
        int result = instance.somatorioAtributosClassificacoes(indexEvento, indexRevisor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of ClassificacaoFinalDeCadaRevisao method, of class
     * GerarEstatisticas.
     */
//    @Test
//    public void testClassificacaoFinalDeCadaRevisao() {
//        System.out.println("ClassificacaoFinalDeCadaRevisao");
//        int indexEvento = 0;
//        int indexRevisor = 0;
//        GerarEstatisticas instance = null;
//        float[] expResult = null;
//        float[] result = instance.ClassificacaoFinalDeCadaRevisao(indexEvento, indexRevisor);
//        assertArrayEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of mediaDoSomatorioClassificacoes method, of class
     * GerarEstatisticas.
     */
    @Test
    public void testMediaDoSomatorioClassificacoes() {
        System.out.println("mediaDoSomatorioClassificacoes");
              Empresa m_empresa = new Empresa();
        Evento m_evento = new Evento();
        Utilizador u = new Utilizador();
        u.setNome("Jose");
        u.setUsername("Jose");
        u.setEmail("Email");
        u.setPassword("123");
        m_empresa.getRegistoEventos().novoEvento();

        m_evento.setTitulo("Titulo");

        m_evento.setDescricao("Descrção");
        m_evento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        m_evento.setDataInicio(d1);
        m_evento.setDataFim(d1);
        m_evento.setDataInicioSubmissao(d1);
        m_evento.setDataFimSubmissao(d1);
        m_evento.setDataInicioDistribuicao(d1);
        m_evento.getListaOrganizadores().addOrganizador(u);
        m_empresa.getRegistoEventos().registaEvento(m_evento);

        Revisor r = new Revisor(u);
        m_evento.novaCP();

        m_evento.get_cp().addMembroCP(r);

        Autor a = new Autor(u.getNome());
        Artigo artigo = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Artigo artigo2 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Revisao rev = new Revisao(artigo);
        Revisao rev2 = new Revisao(artigo2);
        rev2.setRevisor(r);
        
        rev.setRevisor(r);
        
        rev.setM_decisao("Decisao");
        rev.setM_Confianca(5);
        rev.setM_Adequacao(4);
        rev.setM_Originalidade(4);
        rev.setM_Apresentacao(3);
        rev.setM_Recomendacao(2);
        rev.setJustificacao("bla bla bla");
        rev.setArtigo(artigo);
        r.getLstRevisoes().addRevisao(rev);
        
        rev2.setM_decisao("Decisao");
        rev2.setM_Confianca(5);
        rev2.setM_Adequacao(2);
        rev2.setM_Originalidade(4);
        rev2.setM_Apresentacao(3);
        rev2.setM_Recomendacao(2);
        rev2.setJustificacao("bla bla bla");
        rev2.setArtigo(artigo);
        r.getLstRevisoes().addRevisao(rev2);
        
        
        
        int indexEvento = 0;
        int indexRevisor = 0;
        GerarEstatisticas instance = new GerarEstatisticas(m_empresa);
        float expResult = 15;
        float result = instance.mediaDoSomatorioClassificacoes(indexEvento, indexRevisor);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

 


    /**
     * Test of TesteHipoteses method, of class GerarEstatisticas.
     */
    @Test
    public void testTesteHipoteses() {
        System.out.println("TesteHipoteses");
//        Empresa m_empresa = new Empresa();
//        Evento m_evento = new Evento();
//        Utilizador u = new Utilizador();
//        u.setNome("Jose");
//        u.setUsername("Jose");
//        u.setEmail("Email");
//        u.setPassword("123");
//        m_empresa.getRegistoEventos().novoEvento();
//
//        m_evento.setTitulo("Titulo");
//
//        m_evento.setDescricao("Descrção");
//        m_evento.setLocal("local");
//        Date d1 = new Date(2018, 8, 9);
//        m_evento.setDataInicio(d1);
//        m_evento.setDataFim(d1);
//        m_evento.setDataInicioSubmissao(d1);
//        m_evento.setDataFimSubmissao(d1);
//        m_evento.setDataInicioDistribuicao(d1);
//        m_evento.getListaOrganizadores().addOrganizador(u);
//        m_empresa.getRegistoEventos().registaEvento(m_evento);
//
//        Revisor r = new Revisor(u);
//        m_evento.novaCP();
//
//        m_evento.get_cp().addMembroCP(r);
//
//        Autor a = new Autor(u.getNome());
//        
//        Artigo artigo = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Submissao sub = new Submissao(artigo);
//        
//        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub);
//        
//        Artigo artigo2 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Submissao sub1 = new Submissao(artigo2);
//        
//        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub1);
//        
//        Artigo artigo3 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Submissao sub3 = new Submissao(artigo3);
//        
//        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub1);
//        
//        Artigo artigo4 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo5 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo6 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo7 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo8 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo9 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo10 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo11 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo12 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo13 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo14 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo15 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo16 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo17 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo18 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo19 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo20 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo21 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo22 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo23 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo24 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo25 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo26 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo27 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo28 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo29 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo30 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo31 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        Artigo artigo32 = new Artigo();
//        artigo.setTitulo("Qualquer titulo");
//        artigo.setResumo("Resumo");
//        artigo.setPalavrasChave("palavraChave");
//        artigo.setFicheiro("Ficheiro");
//        artigo.setAutorCorrespondente(a);
//        
//        
//        Revisao rev = new Revisao(artigo);
//        Revisao rev2 = new Revisao(artigo2);
//        Revisao rev3 = new Revisao(artigo3);
//        Revisao rev4 = new Revisao(artigo4);
//        Revisao rev5 = new Revisao(artigo5);
//        Revisao rev6 = new Revisao(artigo6);
//        Revisao rev7 = new Revisao(artigo7);
//        Revisao rev8 = new Revisao(artigo8);
//        Revisao rev9 = new Revisao(artigo9);
//        Revisao rev10 = new Revisao(artigo10);
//        Revisao rev11 = new Revisao(artigo11);
//        Revisao rev12 = new Revisao(artigo12);
//        Revisao rev13 = new Revisao(artigo13);
//        Revisao rev14 = new Revisao(artigo14);
//        Revisao rev15 = new Revisao(artigo15);
//        Revisao rev16 = new Revisao(artigo16);
//        Revisao rev17 = new Revisao(artigo17);
//        Revisao rev18 = new Revisao(artigo18);
//        Revisao rev19 = new Revisao(artigo19);
//        Revisao rev20 = new Revisao(artigo20);
//        Revisao rev21 = new Revisao(artigo21);
//        Revisao rev22 = new Revisao(artigo22);
//        Revisao rev23 = new Revisao(artigo23);
//        Revisao rev24 = new Revisao(artigo24);
//        Revisao rev25 = new Revisao(artigo25);
//        Revisao rev26 = new Revisao(artigo26);
//        Revisao rev27 = new Revisao(artigo27);
//        Revisao rev28 = new Revisao(artigo28);
//        Revisao rev29 = new Revisao(artigo29);
//        Revisao rev30 = new Revisao(artigo30);
//        Revisao rev31 = new Revisao(artigo31);
//        Revisao rev32 = new Revisao(artigo32);
//       
//      
//        
//        
//        rev.setRevisor(r);
//        rev2.setRevisor(r);
//        rev3.setRevisor(r);
//        rev4.setRevisor(r);
//        rev5.setRevisor(r);
//        rev6.setRevisor(r);
//        rev7.setRevisor(r);
//        rev8.setRevisor(r);
//        rev9.setRevisor(r);
//        rev10.setRevisor(r);
//        rev11.setRevisor(r);
//        rev12.setRevisor(r);
//        rev13.setRevisor(r);
//        rev14.setRevisor(r);
//        rev15.setRevisor(r);
//        rev16.setRevisor(r);
//        rev17.setRevisor(r);
//        rev18.setRevisor(r);
//        rev19.setRevisor(r);
//        rev20.setRevisor(r);
//        rev21.setRevisor(r);
//        rev22.setRevisor(r);
//        rev23.setRevisor(r);
//        rev24.setRevisor(r);
//        rev25.setRevisor(r);
//        rev26.setRevisor(r);
//        rev27.setRevisor(r);
//        rev28.setRevisor(r);
//        rev29.setRevisor(r);
//        rev30.setRevisor(r);
//        rev31.setRevisor(r);
//        rev32.setRevisor(r);
//      
//        
//        
//        
//        rev.setM_decisao("Decisao");
//        rev.setM_Confianca(5);
//        rev.setM_Adequacao(4);
//        rev.setM_Originalidade(4);
//        rev.setM_Apresentacao(3);
//        rev.setM_Recomendacao(2);
//        rev.setJustificacao("bla bla bla");
//        rev.setArtigo(artigo);
//        r.getLstRevisoes().addRevisao(rev);
//        
//        rev3.setM_decisao("Decisao");
//        rev3.setM_Confianca(5);
//        rev3.setM_Adequacao(2);
//        rev3.setM_Originalidade(4);
//        rev3.setM_Apresentacao(3);
//        rev3.setM_Recomendacao(2);
//        rev3.setJustificacao("bla bla bla");
//        rev3.setArtigo(artigo2);
//        r.getLstRevisoes().addRevisao(rev3);
//        
//        rev4.setM_decisao("Decisao");
//        rev4.setM_Confianca(5);
//        rev4.setM_Adequacao(2);
//        rev4.setM_Originalidade(4);
//        rev4.setM_Apresentacao(3);
//        rev4.setM_Recomendacao(2);
//        rev4.setJustificacao("bla bla bla");
//        rev4.setArtigo(artigo3);
//        r.getLstRevisoes().addRevisao(rev4);
//        
//        rev5.setM_decisao("Decisao");
//        rev5.setM_Confianca(5);
//        rev5.setM_Adequacao(2);
//        rev5.setM_Originalidade(4);
//        rev5.setM_Apresentacao(3);
//        rev5.setM_Recomendacao(2);
//        rev5.setJustificacao("bla bla bla");
//        rev5.setArtigo(artigo4);
//        r.getLstRevisoes().addRevisao(rev5);
//        
//        rev6.setM_decisao("Decisao");
//        rev6.setM_Confianca(5);
//        rev6.setM_Adequacao(2);
//        rev6.setM_Originalidade(4);
//        rev6.setM_Apresentacao(3);
//        rev6.setM_Recomendacao(2);
//        rev6.setJustificacao("bla bla bla");
//        rev6.setArtigo(artigo5);
//        r.getLstRevisoes().addRevisao(rev6);
//        
//        rev7.setM_decisao("Decisao");
//        rev7.setM_Confianca(5);
//        rev7.setM_Adequacao(2);
//        rev7.setM_Originalidade(4);
//        rev7.setM_Apresentacao(3);
//        rev7.setM_Recomendacao(2);
//        rev7.setJustificacao("bla bla bla");
//        rev7.setArtigo(artigo6);
//        r.getLstRevisoes().addRevisao(rev7);
//        
//        rev8.setM_decisao("Decisao");
//        rev8.setM_Confianca(5);
//        rev8.setM_Adequacao(2);
//        rev8.setM_Originalidade(4);
//        rev8.setM_Apresentacao(3);
//        rev8.setM_Recomendacao(2);
//        rev8.setJustificacao("bla bla bla");
//        rev8.setArtigo(artigo7);
//        r.getLstRevisoes().addRevisao(rev8);
//        
//        rev9.setM_decisao("Decisao");
//        rev9.setM_Confianca(5);
//        rev9.setM_Adequacao(2);
//        rev9.setM_Originalidade(4);
//        rev9.setM_Apresentacao(3);
//        rev9.setM_Recomendacao(2);
//        rev9.setJustificacao("bla bla bla");
//        rev9.setArtigo(artigo8);
//        r.getLstRevisoes().addRevisao(rev9);
//        
//        rev10.setM_decisao("Decisao");
//        rev10.setM_Confianca(5);
//        rev10.setM_Adequacao(2);
//        rev10.setM_Originalidade(4);
//        rev10.setM_Apresentacao(3);
//        rev10.setM_Recomendacao(2);
//        rev10.setJustificacao("bla bla bla");
//        rev10.setArtigo(artigo9);
//        r.getLstRevisoes().addRevisao(rev10);
//        
//        rev11.setM_decisao("Decisao");
//        rev11.setM_Confianca(5);
//        rev11.setM_Adequacao(2);
//        rev11.setM_Originalidade(4);
//        rev11.setM_Apresentacao(3);
//        rev11.setM_Recomendacao(2);
//        rev11.setJustificacao("bla bla bla");
//        rev11.setArtigo(artigo10);
//        r.getLstRevisoes().addRevisao(rev11);
//        
//        rev12.setM_decisao("Decisao");
//        rev12.setM_Confianca(5);
//        rev12.setM_Adequacao(2);
//        rev2.setM_Originalidade(4);
//        rev12.setM_Apresentacao(3);
//        rev12.setM_Recomendacao(2);
//        rev12.setJustificacao("bla bla bla");
//        rev12.setArtigo(artigo11);
//        r.getLstRevisoes().addRevisao(rev12);
//        
//        rev13.setM_decisao("Decisao");
//        rev13.setM_Confianca(5);
//        rev13.setM_Adequacao(2);
//        rev13.setM_Originalidade(4);
//        rev13.setM_Apresentacao(3);
//        rev13.setM_Recomendacao(2);
//        rev13.setJustificacao("bla bla bla");
//        rev13.setArtigo(artigo12);
//        r.getLstRevisoes().addRevisao(rev13);
//
//        rev14.setM_decisao("Decisao");
//        rev14.setM_Confianca(5);
//        rev14.setM_Adequacao(2);
//        rev14.setM_Originalidade(4);
//        rev14.setM_Apresentacao(3);
//        rev14.setM_Recomendacao(2);
//        rev14.setJustificacao("bla bla bla");
//        rev14.setArtigo(artigo13);
//        r.getLstRevisoes().addRevisao(rev14);
//        
//        rev15.setM_decisao("Decisao");
//        rev15.setM_Confianca(5);
//        rev15.setM_Adequacao(2);
//        rev15.setM_Originalidade(4);
//        rev15.setM_Apresentacao(3);
//        rev15.setM_Recomendacao(2);
//        rev15.setJustificacao("bla bla bla");
//        rev15.setArtigo(artigo14);
//        r.getLstRevisoes().addRevisao(rev15);
//        
//        rev16.setM_decisao("Decisao");
//        rev16.setM_Confianca(5);
//        rev16.setM_Adequacao(2);
//        rev16.setM_Originalidade(4);
//        rev16.setM_Apresentacao(3);
//        rev16.setM_Recomendacao(2);
//        rev16.setJustificacao("bla bla bla");
//        rev16.setArtigo(artigo15);
//        r.getLstRevisoes().addRevisao(rev16);
//        
//        rev17.setM_decisao("Decisao");
//        rev17.setM_Confianca(5);
//        rev17.setM_Adequacao(2);
//        rev17.setM_Originalidade(4);
//        rev17.setM_Apresentacao(3);
//        rev17.setM_Recomendacao(2);
//        rev17.setJustificacao("bla bla bla");
//        rev17.setArtigo(artigo16);
//        r.getLstRevisoes().addRevisao(rev17);
//        
//        rev18.setM_decisao("Decisao");
//        rev18.setM_Confianca(5);
//        rev18.setM_Adequacao(2);
//        rev18.setM_Originalidade(4);
//        rev18.setM_Apresentacao(3);
//        rev18.setM_Recomendacao(2);
//        rev18.setJustificacao("bla bla bla");
//        rev18.setArtigo(artigo17);
//        r.getLstRevisoes().addRevisao(rev18);
//        
//        rev19.setM_decisao("Decisao");
//        rev19.setM_Confianca(5);
//        rev19.setM_Adequacao(2);
//        rev19.setM_Originalidade(4);
//        rev19.setM_Apresentacao(3);
//        rev19.setM_Recomendacao(2);
//        rev19.setJustificacao("bla bla bla");
//        rev19.setArtigo(artigo18);
//        r.getLstRevisoes().addRevisao(rev19);
//        rev20.setM_decisao("Decisao");
//        rev20.setM_Confianca(5);
//        rev20.setM_Adequacao(2);
//        rev20.setM_Originalidade(4);
//        rev20.setM_Apresentacao(3);
//        rev20.setM_Recomendacao(2);
//        rev20.setJustificacao("bla bla bla");
//        rev20.setArtigo(artigo19);
//        r.getLstRevisoes().addRevisao(rev20);
//
//        rev21.setM_decisao("Decisao");
//        rev21.setM_Confianca(5);
//        rev21.setM_Adequacao(2);
//        rev21.setM_Originalidade(4);
//        rev21.setM_Apresentacao(3);
//        rev21.setM_Recomendacao(2);
//        rev21.setJustificacao("bla bla bla");
//        rev21.setArtigo(artigo20);
//        r.getLstRevisoes().addRevisao(rev21);
//
//        rev22.setM_decisao("Decisao");
//        rev22.setM_Confianca(5);
//        rev22.setM_Adequacao(2);
//        rev22.setM_Originalidade(4);
//        rev22.setM_Apresentacao(3);
//        rev22.setM_Recomendacao(2);
//        rev22.setJustificacao("bla bla bla");
//        rev22.setArtigo(artigo21);
//        r.getLstRevisoes().addRevisao(rev22);
//        
//        rev23.setM_decisao("Decisao");
//        rev23.setM_Confianca(5);
//        rev23.setM_Adequacao(2);
//        rev23.setM_Originalidade(4);
//        rev23.setM_Apresentacao(3);
//        rev23.setM_Recomendacao(2);
//        rev23.setJustificacao("bla bla bla");
//        rev23.setArtigo(artigo22);
//        r.getLstRevisoes().addRevisao(rev23);
//
//        rev24.setM_decisao("Decisao");
//        rev24.setM_Confianca(5);
//        rev24.setM_Adequacao(2);
//        rev24.setM_Originalidade(4);
//        rev24.setM_Apresentacao(3);
//        rev24.setM_Recomendacao(2);
//        rev24.setJustificacao("bla bla bla");
//        rev24.setArtigo(artigo23);
//        r.getLstRevisoes().addRevisao(rev24);
//
//        rev25.setM_decisao("Decisao");
//        rev25.setM_Confianca(5);
//        rev25.setM_Adequacao(2);
//        rev25.setM_Originalidade(4);
//        rev25.setM_Apresentacao(3);
//        rev25.setM_Recomendacao(2);
//        rev25.setJustificacao("bla bla bla");
//        rev25.setArtigo(artigo24);
//        r.getLstRevisoes().addRevisao(rev25);
//        
//        rev26.setM_decisao("Decisao");
//        rev26.setM_Confianca(5);
//        rev26.setM_Adequacao(2);
//        rev26.setM_Originalidade(4);
//        rev26.setM_Apresentacao(3);
//        rev26.setM_Recomendacao(2);
//        rev26.setJustificacao("bla bla bla");
//        rev26.setArtigo(artigo25);
//        r.getLstRevisoes().addRevisao(rev26);
//        
//        rev27.setM_decisao("Decisao");
//        rev27.setM_Confianca(5);
//        rev27.setM_Adequacao(2);
//        rev27.setM_Originalidade(4);
//        rev27.setM_Apresentacao(3);
//        rev27.setM_Recomendacao(2);
//        rev27.setJustificacao("bla bla bla");
//        rev27.setArtigo(artigo26);
//        
//        r.getLstRevisoes().addRevisao(rev27);
//        rev28.setM_decisao("Decisao");
//        rev28.setM_Confianca(5);
//        rev28.setM_Adequacao(2);
//        rev28.setM_Originalidade(4);
//        rev28.setM_Apresentacao(3);
//        rev28.setM_Recomendacao(2);
//        rev28.setJustificacao("bla bla bla");
//        rev28.setArtigo(artigo28);
//        
//        r.getLstRevisoes().addRevisao(rev28);
//        rev29.setM_decisao("Decisao");
//        rev29.setM_Confianca(5);
//        rev29.setM_Adequacao(2);
//        rev29.setM_Originalidade(4);
//        rev29.setM_Apresentacao(3);
//        rev29.setM_Recomendacao(2);
//        rev29.setJustificacao("bla bla bla");
//        rev29.setArtigo(artigo28);
//        r.getLstRevisoes().addRevisao(rev29);
//        
//        rev30.setM_decisao("Decisao");
//        rev30.setM_Confianca(5);
//        rev30.setM_Adequacao(2);
//        rev30.setM_Originalidade(4);
//        rev30.setM_Apresentacao(3);
//        rev30.setM_Recomendacao(2);
//        rev30.setJustificacao("bla bla bla");
//        rev30.setArtigo(artigo29);
//        r.getLstRevisoes().addRevisao(rev30);
//        
//        rev31.setM_decisao("Decisao");
//        rev31.setM_Confianca(5);
//        rev31.setM_Adequacao(2);
//        rev31.setM_Originalidade(4);
//        rev31.setM_Apresentacao(3);
//        rev31.setM_Recomendacao(2);
//        rev31.setJustificacao("bla bla bla");
//        rev31.setArtigo(artigo30);
//        r.getLstRevisoes().addRevisao(rev31);
//////        
        Empresa m_empresa = new Empresa();
        Evento m_evento = new Evento();
        Utilizador u = new Utilizador();
        u.setNome("Jose");
        u.setUsername("Jose");
        u.setEmail("Email");
        u.setPassword("123");
        m_empresa.getRegistoEventos().novoEvento();

        m_evento.setTitulo("Titulo");

        m_evento.setDescricao("Descrção");
        m_evento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        m_evento.setDataInicio(d1);
        m_evento.setDataFim(d1);
        m_evento.setDataInicioSubmissao(d1);
        m_evento.setDataFimSubmissao(d1);
        m_evento.setDataInicioDistribuicao(d1);
        m_evento.getListaOrganizadores().addOrganizador(u);
        m_empresa.getRegistoEventos().registaEvento(m_evento);

        Revisor r = new Revisor(u);
        m_evento.novaCP();

        m_evento.get_cp().addMembroCP(r);

        Autor a = new Autor(u.getNome());
        Artigo artigo = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub = new Submissao(artigo);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub);
        
        Artigo artigo2 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub2 = new Submissao(artigo2);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub2);
        
        Artigo artigo3 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub3 = new Submissao(artigo3);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub3);
        
        Artigo artigo4 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub4 = new Submissao(artigo4);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub4);
        
        Artigo artigo5 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub5 = new Submissao(artigo5);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub5);
        
        Artigo artigo6 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub6 = new Submissao(artigo6);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub6);
        
        Artigo artigo7 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub7 = new Submissao(artigo7);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub7);
        
        Artigo artigo8 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub8 = new Submissao(artigo8);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub8);
        
        Artigo artigo9 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub9 = new Submissao(artigo9);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub9);
        
        Artigo artigo10 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub10 = new Submissao(artigo10);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub10);
        
        Artigo artigo11 =new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub11 = new Submissao(artigo11);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub11);
        
        Artigo artigo12 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub12 = new Submissao(artigo12);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub12);
        
        Artigo artigo13 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub13 = new Submissao(artigo13);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub13);
        
        Artigo artigo14 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub14 = new Submissao(artigo14);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub14);
        
        Artigo artigo15 =new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub15 = new Submissao(artigo15);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub15);
        
        Artigo artigo16 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub16 = new Submissao(artigo16);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub16);
        
        Artigo artigo17 =new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub17 = new Submissao(artigo17);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub17);
        
        Artigo artigo18 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub18 = new Submissao(artigo18);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub18);
        
        Artigo artigo19 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub19 = new Submissao(artigo19);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub19);
        
        Artigo artigo20=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
             
        Submissao sub20 = new Submissao(artigo20);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub20);
        
        Artigo artigo21 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
       
        Submissao sub21 = new Submissao(artigo21);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub21);
        
        Artigo artigo22 =new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub22 = new Submissao(artigo22);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub22);
        
        Artigo artigo23 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub23 = new Submissao(artigo23);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub23);
        
        Artigo artigo24=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub24 = new Submissao(artigo24);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub24);
        
        Artigo artigo25=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub25 = new Submissao(artigo25);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub25);
        
        Artigo artigo26=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub26 = new Submissao(artigo26);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub26);
        
        Artigo artigo27=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub27 = new Submissao(artigo27);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub27);
        
        Artigo artigo28=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub28 = new Submissao(artigo28);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub28);
        
        Artigo artigo29=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub29 = new Submissao(artigo29);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub29);
        
        Artigo artigo30=new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Submissao sub30 = new Submissao(artigo30);
        
        m_empresa.getRegistoEventos().getListEventos().get(0).getSubmissoes().add(sub30);
        
        Revisao rev = new Revisao(artigo);
        Revisao rev2 = new Revisao(artigo2);
        Revisao rev3 = new Revisao(artigo3);
        Revisao rev4 = new Revisao(artigo4);
        Revisao rev5 = new Revisao(artigo5);
        Revisao rev6 = new Revisao(artigo6);
        Revisao rev7 = new Revisao(artigo7);
        Revisao rev8 = new Revisao(artigo8);
        Revisao rev9 = new Revisao(artigo9);
        Revisao rev10 = new Revisao(artigo10);
        Revisao rev11 = new Revisao(artigo11);
        Revisao rev12 = new Revisao(artigo12);
        Revisao rev13 = new Revisao(artigo13);
        Revisao rev14= new Revisao(artigo14);
        Revisao rev15= new Revisao(artigo15);
        Revisao rev16= new Revisao(artigo16);
        Revisao rev17= new Revisao(artigo17);
        Revisao rev18= new Revisao(artigo18);
        Revisao rev19= new Revisao(artigo19);
        Revisao rev20= new Revisao(artigo20);
        Revisao rev21= new Revisao(artigo21);
        Revisao rev22= new Revisao(artigo22);
        Revisao rev23= new Revisao(artigo23);
        Revisao rev24= new Revisao(artigo24);
        Revisao rev25= new Revisao(artigo25);
        Revisao rev26= new Revisao(artigo26);
        Revisao rev27= new Revisao(artigo27);
        Revisao rev28= new Revisao(artigo28);
        Revisao rev29= new Revisao(artigo29);
        Revisao rev30= new Revisao(artigo30);
        
        
        rev30.setRevisor(r);
        rev29.setRevisor(r);
        rev28.setRevisor(r);
        rev27.setRevisor(r);
        rev26.setRevisor(r);
        rev25.setRevisor(r);
        rev24.setRevisor(r);
        rev23.setRevisor(r);
        rev22.setRevisor(r);
        rev21.setRevisor(r);
        rev20.setRevisor(r);
        rev19.setRevisor(r);
        rev18.setRevisor(r);
        rev17.setRevisor(r);
        rev16.setRevisor(r);
        rev15.setRevisor(r);
        rev14.setRevisor(r);
        rev13.setRevisor(r);
        rev12.setRevisor(r);
        rev11.setRevisor(r);
        rev10.setRevisor(r);
        rev9.setRevisor(r);
        rev8.setRevisor(r);
        rev7.setRevisor(r);
        rev6.setRevisor(r);
        rev5.setRevisor(r);
        rev4.setRevisor(r);
        rev3.setRevisor(r);
        rev2.setRevisor(r);
        
        rev.setRevisor(r);
        
        rev.setM_decisao("Decisao");
        rev.setM_Confianca(5);
        rev.setM_Adequacao(4);
        rev.setM_Originalidade(4);
        rev.setM_Apresentacao(3);
        rev.setM_Recomendacao(2);
        rev.setJustificacao("bla bla bla");
        rev.setArtigo(artigo);
        r.getLstRevisoes().addRevisao(rev);
        
        rev2.setM_decisao("Decisao");
        rev2.setM_Confianca(5);
        rev2.setM_Adequacao(2);
        rev2.setM_Originalidade(4);
        rev2.setM_Apresentacao(3);
        rev2.setM_Recomendacao(2);
        rev2.setJustificacao("bla bla bla");
        rev2.setArtigo(artigo2);
        r.getLstRevisoes().addRevisao(rev2);
        
        rev3.setM_decisao("Decisao");
        rev3.setM_Confianca(5);
        rev3.setM_Adequacao(2);
        rev3.setM_Originalidade(4);
        rev3.setM_Apresentacao(3);
        rev3.setM_Recomendacao(2);
        rev3.setJustificacao("bla bla bla");
        rev3.setArtigo(artigo3);
        r.getLstRevisoes().addRevisao(rev3);
        
        rev4.setM_decisao("Decisao");
        rev4.setM_Confianca(5);
        rev4.setM_Adequacao(2);
        rev4.setM_Originalidade(4);
        rev4.setM_Apresentacao(2);
        rev4.setM_Recomendacao(2);
        rev4.setJustificacao("bla bla bla");
        rev4.setArtigo(artigo4);
        r.getLstRevisoes().addRevisao(rev4);
        
        rev5.setM_decisao("Decisao");
        rev5.setM_Confianca(2);
        rev5.setM_Adequacao(2);
        rev5.setM_Originalidade(1);
        rev5.setM_Apresentacao(3);
        rev5.setM_Recomendacao(2);
        rev5.setJustificacao("bla bla bla");
        rev5.setArtigo(artigo5);
        r.getLstRevisoes().addRevisao(rev5);
        
        rev6.setM_decisao("Decisao");
        rev6.setM_Confianca(4);
        rev6.setM_Adequacao(2);
        rev6.setM_Originalidade(3);
        rev6.setM_Apresentacao(1);
        rev6.setM_Recomendacao(2);
        rev6.setJustificacao("bla bla bla");
        rev6.setArtigo(artigo6);
        r.getLstRevisoes().addRevisao(rev6);
        
        rev7.setM_decisao("Decisao");
        rev7.setM_Confianca(3);
        rev7.setM_Adequacao(2);
        rev7.setM_Originalidade(2);
        rev7.setM_Apresentacao(1);
        rev7.setM_Recomendacao(2);
        rev7.setJustificacao("bla bla bla");
        rev7.setArtigo(artigo7);
        r.getLstRevisoes().addRevisao(rev7);
        
        rev8.setM_decisao("Decisao");
        rev8.setM_Confianca(1);
        rev8.setM_Adequacao(4);
        rev8.setM_Originalidade(2);
        rev8.setM_Apresentacao(3);
        rev8.setM_Recomendacao(2);
        rev8.setJustificacao("bla bla bla");
        rev8.setArtigo(artigo8);
        r.getLstRevisoes().addRevisao(rev8);
        
        rev9.setM_decisao("Decisao");
        rev9.setM_Confianca(2);
        rev9.setM_Adequacao(4);
        rev9.setM_Originalidade(1);
        rev9.setM_Apresentacao(3);
        rev9.setM_Recomendacao(2);
        rev9.setJustificacao("bla bla bla");
        rev9.setArtigo(artigo9);
        r.getLstRevisoes().addRevisao(rev9);
        
        rev10.setM_decisao("Decisao");
        rev10.setM_Confianca(3);
        rev10.setM_Adequacao(4);
        rev10.setM_Originalidade(2);
        rev10.setM_Apresentacao(3);
        rev10.setM_Recomendacao(2);
        rev10.setJustificacao("bla bla bla");
        rev10.setArtigo(artigo10);
        r.getLstRevisoes().addRevisao(rev10);
        
        rev11.setM_decisao("Decisao");
        rev11.setM_Confianca(5);
        rev11.setM_Adequacao(4);
        rev11.setM_Originalidade(4);
        rev11.setM_Apresentacao(3);
        rev11.setM_Recomendacao(2);
        rev11.setJustificacao("bla bla bla");
        rev11.setArtigo(artigo11);
        r.getLstRevisoes().addRevisao(rev11);
        
        rev12.setM_decisao("Decisao");
        rev12.setM_Confianca(5);
        rev12.setM_Adequacao(4);
        rev12.setM_Originalidade(4);
        rev12.setM_Apresentacao(3);
        rev12.setM_Recomendacao(2);
        rev12.setJustificacao("bla bla bla");
        rev12.setArtigo(artigo12);
        r.getLstRevisoes().addRevisao(rev12);
        
        rev13.setM_decisao("Decisao");
        rev13.setM_Confianca(5);
        rev13.setM_Adequacao(4);
        rev13.setM_Originalidade(4);
        rev13.setM_Apresentacao(3);
        rev13.setM_Recomendacao(2);
        rev13.setJustificacao("bla bla bla");
        rev13.setArtigo(artigo13);
        r.getLstRevisoes().addRevisao(rev13);
        
        rev14.setM_decisao("Decisao");
        rev14.setM_Confianca(5);
        rev14.setM_Adequacao(4);
        rev14.setM_Originalidade(4);
        rev14.setM_Apresentacao(3);
        rev14.setM_Recomendacao(2);
        rev14.setJustificacao("bla bla bla");
        rev14.setArtigo(artigo14);
        r.getLstRevisoes().addRevisao(rev14);
        
        rev15.setM_decisao("Decisao");
        rev15.setM_Confianca(5);
        rev15.setM_Adequacao(4);
        rev15.setM_Originalidade(4);
        rev15.setM_Apresentacao(3);
        rev15.setM_Recomendacao(2);
        rev15.setJustificacao("bla bla bla");
        rev15.setArtigo(artigo15);
        r.getLstRevisoes().addRevisao(rev15);
        
        rev16.setM_decisao("Decisao");
        rev16.setM_Confianca(5);
        rev16.setM_Adequacao(4);
        rev16.setM_Originalidade(4);
        rev16.setM_Apresentacao(3);
        rev16.setM_Recomendacao(2);
        rev16.setJustificacao("bla bla bla");
        rev16.setArtigo(artigo16);
        r.getLstRevisoes().addRevisao(rev16);
        
        rev17.setM_decisao("Decisao");
        rev17.setM_Confianca(5);
        rev17.setM_Adequacao(4);
        rev17.setM_Originalidade(4);
        rev17.setM_Apresentacao(3);
        rev17.setM_Recomendacao(2);
        rev17.setJustificacao("bla bla bla");
        rev17.setArtigo(artigo17);
        r.getLstRevisoes().addRevisao(rev17);
        
        rev18.setM_decisao("Decisao");
        rev18.setM_Confianca(5);
        rev18.setM_Adequacao(4);
        rev18.setM_Originalidade(4);
        rev18.setM_Apresentacao(3);
        rev18.setM_Recomendacao(2);
        rev18.setJustificacao("bla bla bla");
        rev18.setArtigo(artigo18);
        r.getLstRevisoes().addRevisao(rev18);
        
        rev19.setM_decisao("Decisao");
        rev19.setM_Confianca(5);
        rev19.setM_Adequacao(4);
        rev19.setM_Originalidade(4);
        rev19.setM_Apresentacao(3);
        rev19.setM_Recomendacao(2);
        rev19.setJustificacao("bla bla bla");
        rev19.setArtigo(artigo19);
        r.getLstRevisoes().addRevisao(rev19);
        
        rev20.setM_decisao("Decisao");
        rev20.setM_Confianca(1);
        rev20.setM_Adequacao(3);
        rev20.setM_Originalidade(2);
        rev20.setM_Apresentacao(5);
        rev20.setM_Recomendacao(2);
        rev20.setJustificacao("bla bla bla");
        rev20.setArtigo(artigo20);
        r.getLstRevisoes().addRevisao(rev20);
        
        rev21.setM_decisao("Decisao");
        rev21.setM_Confianca(3);
        rev21.setM_Adequacao(1);
        rev21.setM_Originalidade(2);
        rev21.setM_Apresentacao(4);
        rev21.setM_Recomendacao(2);
        rev21.setJustificacao("bla bla bla");
        rev21.setArtigo(artigo21);
        r.getLstRevisoes().addRevisao(rev21);

        rev22.setM_decisao("Decisao");
        rev22.setM_Confianca(5);
        rev22.setM_Adequacao(2);
        rev22.setM_Originalidade(1);
        rev22.setM_Apresentacao(5);
        rev22.setM_Recomendacao(2);
        rev22.setJustificacao("bla bla bla");
        rev22.setArtigo(artigo22);
        r.getLstRevisoes().addRevisao(rev22);
        
        rev23.setM_decisao("Decisao");
        rev23.setM_Confianca(1);
        rev23.setM_Adequacao(2);
        rev23.setM_Originalidade(1);
        rev23.setM_Apresentacao(3);
        rev23.setM_Recomendacao(2);
        rev23.setJustificacao("bla bla bla");
        rev23.setArtigo(artigo23);
        r.getLstRevisoes().addRevisao(rev23);
        
        rev24.setM_decisao("Decisao");
        rev24.setM_Confianca(5);
        rev24.setM_Adequacao(2);
        rev24.setM_Originalidade(2);
        rev24.setM_Apresentacao(2);
        rev24.setM_Recomendacao(2);
        rev24.setJustificacao("bla bla bla");
        rev24.setArtigo(artigo24);
        r.getLstRevisoes().addRevisao(rev24);
        
        rev25.setM_decisao("Decisao");
        rev25.setM_Confianca(1);
        rev25.setM_Adequacao(3);
        rev25.setM_Originalidade(3);
        rev25.setM_Apresentacao(2);
        rev25.setM_Recomendacao(2);
        rev25.setJustificacao("bla bla bla");
        rev25.setArtigo(artigo25);
        r.getLstRevisoes().addRevisao(rev25);
        
        rev26.setM_decisao("Decisao");
        rev26.setM_Confianca(2);
        rev26.setM_Adequacao(3);
        rev26.setM_Originalidade(2);
        rev26.setM_Apresentacao(2);
        rev26.setM_Recomendacao(2);
        rev26.setJustificacao("bla bla bla");
        rev26.setArtigo(artigo26);
        r.getLstRevisoes().addRevisao(rev26);
        
        rev27.setM_decisao("Decisao");
        rev27.setM_Confianca(4);
        rev27.setM_Adequacao(1);
        rev27.setM_Originalidade(2);
        rev27.setM_Apresentacao(1);
        rev27.setM_Recomendacao(2);
        rev27.setJustificacao("bla bla bla");
        rev27.setArtigo(artigo27);
        r.getLstRevisoes().addRevisao(rev27);
        
        rev28.setM_decisao("Decisao");
        rev28.setM_Confianca(1);
        rev28.setM_Adequacao(5);
        rev28.setM_Originalidade(4);
        rev28.setM_Apresentacao(1);
        rev28.setM_Recomendacao(2);
        rev28.setJustificacao("bla bla bla");
        rev28.setArtigo(artigo28);
        r.getLstRevisoes().addRevisao(rev28);
        
        rev29.setM_decisao("Decisao");
        rev29.setM_Confianca(3);
        rev29.setM_Adequacao(2);
        rev29.setM_Originalidade(1);
        rev29.setM_Apresentacao(1);
        rev29.setM_Recomendacao(2);
        rev29.setJustificacao("bla bla bla");
        rev29.setArtigo(artigo29);
        r.getLstRevisoes().addRevisao(rev29);
        
        rev30.setM_decisao("Decisao");
        rev30.setM_Confianca(2);
        rev30.setM_Adequacao(4);
        rev30.setM_Originalidade(1);
        rev30.setM_Apresentacao(2);
        rev30.setM_Recomendacao(1);
        rev30.setJustificacao("bla bla bla");
        rev30.setArtigo(artigo30);
        r.getLstRevisoes().addRevisao(rev30);
        
        
        int indexEvento = 0;
        int indexRevisor = 0;
        GerarEstatisticas instance = new GerarEstatisticas(m_empresa);
        
        
        boolean expResult = true;
        boolean result = instance.TesteHipoteses(indexEvento, indexRevisor);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       
    }

    /**
     * Test of SomatorioDi method, of class GerarEstatisticas.
     */
    @Test
    public void testSomatorioDi() {
        System.out.println("SomatorioDi");
                   Empresa m_empresa = new Empresa();
        Evento m_evento = new Evento();
        Utilizador u = new Utilizador();
        u.setNome("Jose");
        u.setUsername("Jose");
        u.setEmail("Email");
        u.setPassword("123");
        m_empresa.getRegistoEventos().novoEvento();

        m_evento.setTitulo("Titulo");

        m_evento.setDescricao("Descrção");
        m_evento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        m_evento.setDataInicio(d1);
        m_evento.setDataFim(d1);
        m_evento.setDataInicioSubmissao(d1);
        m_evento.setDataFimSubmissao(d1);
        m_evento.setDataInicioDistribuicao(d1);
        m_evento.getListaOrganizadores().addOrganizador(u);
        m_empresa.getRegistoEventos().registaEvento(m_evento);

        Revisor r = new Revisor(u);
        m_evento.novaCP();

        m_evento.get_cp().addMembroCP(r);

        Autor a = new Autor(u.getNome());
        Artigo artigo = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Artigo artigo2 = new Artigo();
        artigo.setTitulo("Qualquer titulo");
        artigo.setResumo("Resumo");
        artigo.setPalavrasChave("palavraChave");
        artigo.setFicheiro("Ficheiro");
        artigo.setAutorCorrespondente(a);
        
        Revisao rev = new Revisao(artigo);
        Revisao rev2 = new Revisao(artigo2);
        rev2.setRevisor(r);
        
        rev.setRevisor(r);
        
        rev.setM_decisao("Decisao");
        rev.setM_Confianca(5);
        rev.setM_Adequacao(4);
        rev.setM_Originalidade(4);
        rev.setM_Apresentacao(3);
        rev.setM_Recomendacao(2);
        rev.setJustificacao("bla bla bla");
        rev.setArtigo(artigo);
        r.getLstRevisoes().addRevisao(rev);
        
        rev2.setM_decisao("Decisao");
        rev2.setM_Confianca(5);
        rev2.setM_Adequacao(2);
        rev2.setM_Originalidade(4);
        rev2.setM_Apresentacao(3);
        rev2.setM_Recomendacao(2);
        rev2.setJustificacao("bla bla bla");
        rev2.setArtigo(artigo);
        r.getLstRevisoes().addRevisao(rev2);
        
        int indexEvento = 0;
        int indexRevisor = 0;
        GerarEstatisticas instance = new GerarEstatisticas(m_empresa);
        float expResult = 23;
        float result = instance.SomatorioDi(indexEvento, indexRevisor);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of MediaDi method, of class GerarEstatisticas.
     */
//    @Test
//    public void testMediaDi() {
//        System.out.println("MediaDi");
//        int indexEvento = 0;
//        int indexRevisor = 0;
//        GerarEstatisticas instance = null;
//        float expResult = 0.0F;
//        float result = instance.MediaDi(indexEvento, indexRevisor);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of SomatorioVariancia method, of class GerarEstatisticas.
     */
//    @Test
//    public void testSomatorioVariancia() {
//        System.out.println("SomatorioVariancia");
//        int indexEvento = 0;
//        int indexRevisor = 0;
//        GerarEstatisticas instance = null;
//        float expResult = 0.0F;
//        float result = instance.SomatorioVariancia(indexEvento, indexRevisor);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of Variancia method, of class GerarEstatisticas.
     */
//    @Test
//    public void testVariancia() {
//        System.out.println("Variancia");
//        int indexEvento = 0;
//        int indexRevisor = 0;
//        GerarEstatisticas instance = null;
//        float expResult = 0.0F;
//        float result = instance.Variancia(indexEvento, indexRevisor);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of DesvioPadrao method, of class GerarEstatisticas.
     */
//    @Test
//    public void testDesvioPadrao() {
//        System.out.println("DesvioPadrao");
//        int indexEvento = 0;
//        int indexRevisor = 0;
//        GerarEstatisticas instance = null;
//        float expResult = 0.0F;
//        float result = instance.DesvioPadrao(indexEvento, indexRevisor);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of zObservado method, of class GerarEstatisticas.
     */
//    @Test
//    public void testZObservado() {
//        System.out.println("zObservado");
//        int indexEvento = 0;
//        int indexRevisor = 0;
//        GerarEstatisticas instance = null;
//        float expResult = 0.0F;
//        float result = instance.zObservado(indexEvento, indexRevisor);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

}
