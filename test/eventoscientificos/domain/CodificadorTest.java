/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.io.FileNotFoundException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class CodificadorTest {

    public CodificadorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of codificar method, of class Codificador.
     */
    @Test
    public void testCodificar() throws FileNotFoundException {
        System.out.println("codificar");
        String[][] m = LerCsv.lercsv("Tabela_CA_Pass_v01.csv");

        String s = "cheese";
        int coluna = 1;
        double expResult = 0.0;
        double result = Codificador.codificar(m, s, coluna);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of determinarIndice method, of class Codificador.
     */
    @Test
    public void testDeterminarIndice() {
        System.out.println("determinarIndice");
        String letra = "cheese";
        String[] letrasNaoRepetidas = new String[5];
        int expResult = -1;
        int result = Codificador.determinarIndice(letra, letrasNaoRepetidas);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of ordenaArray method, of class Codificador.
     */
    @Test
    public void testOrdenaArray() {
        System.out.println("ordenaArray");
        String[] arrayLetras = new String[3];
        arrayLetras[0] = "c";
        arrayLetras[1] = "b";
        arrayLetras[2] = "a";

        String[] expResult = new String[3];
        expResult[0] = "a";
        expResult[1] = "b";
        expResult[2] = "c";

        String[] result = Codificador.ordenaArray(arrayLetras);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of amplitude method, of class Codificador.
     */
    @Test
    public void testAmplitude() {
        System.out.println("amplitude");
        double[] intervalo = new double[3];
        intervalo[0] = 1;
        intervalo[1] = 2;
        intervalo[2] = 3;
        double expResult = 2;
        double result = Codificador.amplitude(intervalo);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of charatersNaoRepetidos method, of class Codificador.
     */
    @Test
    public void testCharatersNaoRepetidos() {
        System.out.println("charatersNaoRepetidos");
        String[] charaters = new String[4];
        charaters[0] = "a";
        charaters[1] = "a";
        charaters[2] = "b";
        charaters[3] = "b";
        String[] expResult = new String[2];
        expResult[0] = "a";
        expResult[1] = "b";
        String[] result = Codificador.charatersNaoRepetidos(charaters);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    


}
