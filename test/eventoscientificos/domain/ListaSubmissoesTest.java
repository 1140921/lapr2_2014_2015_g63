/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.Date;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140921
 */
public class ListaSubmissoesTest {
    
    public ListaSubmissoesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }



    /**
     * Test of removerSubmissao method, of class ListaSubmissoes.
     */
    @Test
    public void testRemoverSubmissao() {
        System.out.println("removerSubmissao");
        
         Empresa m_empresa = new Empresa();

        Utilizador u = new Utilizador();
        u.setNome("Jose");
        u.setUsername("Jose");
        u.setEmail("Email");
        u.setPassword("123");
        Autor autor = new Autor("Jose");
        autor.setNome("Jose");

        Utilizador u1 = new Utilizador("Anzonio", "Anzonio", "Anzonio", "Anzonio");
        Autor autor1 = new Autor();
        autor1.setUsername(u1.getUsername());

        Evento m_evento = new Evento();

        m_empresa.getRegistoEventos().novoEvento();

        m_evento.setTitulo("Titulo");

        m_evento.setDescricao("Descrção");
        m_evento.setLocal("local");
        Date d1 = new Date(2018, 8, 9);
        m_evento.setDataInicio(d1);
        m_evento.setDataFim(d1);
        m_evento.setDataInicioSubmissao(d1);
        m_evento.setDataFimSubmissao(d1);
        m_evento.setDataInicioDistribuicao(d1);
        m_evento.getListaOrganizadores().addOrganizador(u);
        m_empresa.getRegistoEventos().registaEvento(m_evento);

        Artigo artigo = new Artigo();
        artigo.setTitulo("O Antonio obrigou-me a fazer isto");
        artigo.setResumo("Fazer um teste Unitário");
        artigo.setFicheiro("Qualquer lado");
        artigo.setPalavrasChave("QualquerPalavra");
        artigo.setAutorCorrespondente(autor1);

        Submissao sub = new Submissao(artigo);

        m_evento.getListaSubmissoes().addSubmissao(sub);
        
        
//        Submissao submissao = new Submissao(sub);
        ListaSubmissoes instance = m_evento.getListaSubmissoes();
        boolean expResult = true;
        boolean result = instance.removerSubmissao(sub);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    
}
