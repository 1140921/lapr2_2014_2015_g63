/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.controllers.RegistarUtilizadorController;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class RegistoUtilizadoresTest {
    
    public RegistoUtilizadoresTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

//    /**
//     * Test of novoUtilizador method, of class RegistoUtilizadores.
//     */
//    @Test
//    public void testNovoUtilizador() {
//        System.out.println("novoUtilizador");
//        RegistoUtilizadores instance = new RegistoUtilizadores();
//        Utilizador expResult = null;
//        Utilizador result = instance.novoUtilizador();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of registaUtilizador method, of class RegistoUtilizadores.
//     */
//    @Test
//    public void testRegistaUtilizador() {
//        System.out.println("registaUtilizador");
//        Utilizador u = null;
//        RegistoUtilizadores instance = new RegistoUtilizadores();
//        boolean expResult = false;
//        boolean result = instance.registaUtilizador(u);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addUtilizador method, of class RegistoUtilizadores.
//     */
//    @Test
//    public void testAddUtilizador() {
//        System.out.println("addUtilizador");
//        Utilizador u = null;
//        RegistoUtilizadores instance = new RegistoUtilizadores();
//        boolean expResult = false;
//        boolean result = instance.addUtilizador(u);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getUtilizadorByID method, of class RegistoUtilizadores.
//     */
//    @Test
//    public void testGetUtilizadorByID() {
//        System.out.println("getUtilizadorByID");
//        String strId = "";
//        RegistoUtilizadores instance = new RegistoUtilizadores();
//        Utilizador expResult = null;
//        Utilizador result = instance.getUtilizadorByID(strId);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getUtilizadorByPassword method, of class RegistoUtilizadores.
//     */
//    @Test
//    public void testGetUtilizadorByPassword() {
//        System.out.println("getUtilizadorByPassword");
//        String pwd = "";
//        RegistoUtilizadores instance = new RegistoUtilizadores();
//        Utilizador expResult = null;
//        Utilizador result = instance.getUtilizadorByPassword(pwd);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getListUtilizador method, of class RegistoUtilizadores.
//     */
//    @Test
//    public void testGetListUtilizador() {
//        System.out.println("getListUtilizador");
//        RegistoUtilizadores instance = new RegistoUtilizadores();
//        List<Utilizador> expResult = null;
//        List<Utilizador> result = instance.getListUtilizador();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getUtilizadorByEmail method, of class RegistoUtilizadores.
//     */
//    @Test
//    public void testGetUtilizadorByEmail() {
//        System.out.println("getUtilizadorByEmail");
//        String strEmail = "";
//        RegistoUtilizadores instance = new RegistoUtilizadores();
//        Utilizador expResult = null;
//        Utilizador result = instance.getUtilizadorByEmail(strEmail);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of alteraUtilizador method, of class RegistoUtilizadores.
     */
    @Test
    public void testAlteraUtilizador() throws FileNotFoundException {
        System.out.println("alteraUtilizador");
        
        RegistoUtilizadores lstUtil = new RegistoUtilizadores();
        Utilizador uOriginal = new Utilizador("Fabio", "Fabio", "Fabio", "Fabio@fabio.fabio");
        lstUtil.addUtilizador(uOriginal);
        Utilizador uClone = uOriginal.clone();
        
        RegistoUtilizadores instance = new RegistoUtilizadores();
        uClone.setNome("FabioTeste");
        boolean expResult = true;
        boolean result = instance.alteraUtilizador(uOriginal, uClone);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    @Test
    public void testAlteraUtilizadorDois() throws FileNotFoundException {
        System.out.println("alteraUtilizador");
        
        RegistoUtilizadores lstUtil = new RegistoUtilizadores();
        Utilizador uOriginal = new Utilizador("Fabio", "Fabio", "Fabio", "Fabio@fabio.fabio");
        lstUtil.addUtilizador(uOriginal);
        Utilizador uClone = uOriginal.clone();
        
        RegistoUtilizadores instance = new RegistoUtilizadores();
        uClone.setNome("FabioTeste");
        boolean expResult = true;
        boolean result = instance.alteraUtilizador(uOriginal, uClone);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }  

   


   

 

  

   

  
}
