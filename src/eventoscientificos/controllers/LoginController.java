package eventoscientificos.controllers;

import eventoscientificos.domain.Codificador;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.LerCsv;
import eventoscientificos.domain.Login;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Utilizador;
import java.io.FileNotFoundException;
import java.util.List;

public class LoginController {

    private Empresa m_empresa;

    private RegistoUtilizadores m_regUtilizadores;
    private Login login;
    private String[][] matrizCodificacao;
    
    public LoginController(Empresa empresa) throws FileNotFoundException {
        m_empresa = empresa;
        m_regUtilizadores = empresa.getRegistoUtilizadores();
        matrizCodificacao=LerCsv.lercsv("Tabela_CA_Pass_v01.csv");
    }

    public boolean validaLogin(String username, String pwd) {
 
        
        
        List<Utilizador> listUtilizadores = m_regUtilizadores.getListUtilizador();

        for (Utilizador u : listUtilizadores) {
            double[]codigo=u.getPasswordCodificada();
            double pwdCodificada=Codificador.codificar(matrizCodificacao, pwd,(int)codigo[0]);
            
            if (u.getUsername().equals(username) && codigo[1]==pwdCodificada) {
                    return true;
            }
         }
        return false;
    }

    public void setLogin(Login login){
        this.login=login;
        m_empresa.setLogin(login);
    }


}
