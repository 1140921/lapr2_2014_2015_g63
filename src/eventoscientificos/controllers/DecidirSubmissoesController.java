 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Decidivel;
import eventoscientificos.domain.Decisao;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.MecanismoDecisao;
import eventoscientificos.domain.ProcessoDecisao;
import eventoscientificos.domain.RegistoEventos;
import java.util.List;

/**
 *
 * @author nunosilva
 */
public class DecidirSubmissoesController
{
    private Empresa m_empresa;
    private RegistoEventos m_regEvento;
    private Decidivel m_decidivel;
    private ProcessoDecisao m_processoDecisao; 

    public DecidirSubmissoesController(Empresa empresa)
    {
        m_empresa = empresa;
        m_regEvento = empresa.getRegistoEventos();
       
    }
    
    public List<Decidivel> getDecisiveis(String id) 
    {
        return m_regEvento.getDecisiveis(id);

    }

	
    public void novoProcessoDecisao(Decidivel d) {
        m_decidivel=d;
        m_processoDecisao = d.novoProcessoDecisao();

    }

    public List<MecanismoDecisao> getMecanismosDecisao() {
	List<MecanismoDecisao> lm = m_empresa.getMecanismosDecisao();
        return lm;
    }

	
    public void setMecanismoDecisao(MecanismoDecisao m) 
    {
	m_processoDecisao.setMecanismoDecisao(m);
        m_processoDecisao.decide();
    }

    public boolean registaPD() 
    {
	m_decidivel.setPD(m_processoDecisao);
        return true;
    }

	
    public boolean notifica() 
    {
	return m_processoDecisao.notifica();
    }

    public List<Decisao> getDecisoes() {
        return m_processoDecisao.getDecisoes();
    }

    public void setAceitacao(Decisao d, String a) 
    {
        d.setAceitacao(a);
    }
}
