/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.ExportarFicheiro;
import eventoscientificos.domain.RegistoUtilizadores;

/**
 * Classe Controller responsável pela interação ExportarFicheiroUI com
 * ExportarFicheiro 
 * @author Andre Ribeiro
 */
public class ExportarFicheiroController {
    
     private ExportarFicheiro m_exportarFicheiro;
     private Empresa m_empresa;
     
     
     public ExportarFicheiroController(Empresa empresa){
         m_empresa= empresa;
         m_exportarFicheiro= new ExportarFicheiro(m_empresa);
     }
     /**
      * Método responsavel por receber o caminho selecionado para guardar 
      * @param pathficheiro caminho do ficheiro
      */
     public void exportarFicheiroUtilizador(String pathficheiro){
        m_exportarFicheiro.exportarFicheiroUtilizador(pathficheiro);
     }
     /**
      * Método responsavel por receber o cmainho selecionado para guardar 
      * @param pathficheiro caminho do ficheiro
      */
     public void exportarFicheiroEvento(String pathficheiro){
         m_exportarFicheiro.exportarFicheiroEvento(pathficheiro);
     }
}
