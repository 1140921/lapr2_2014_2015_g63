package eventoscientificos.controllers;


import eventoscientificos.domain.CP;
import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.Utilizador;
import java.util.List;

/**
 *
 * @author Nuno Silva
 */

public class DefinirCPController
{
    private Empresa m_empresa;
    private RegistoEventos m_regEventos;
    private CPDefinivel m_CPDefinivel;
    private CP m_cp;

   
    public DefinirCPController(Empresa empresa)
    {
        m_empresa = empresa; 
        m_regEventos = empresa.getRegistoEventos();
    }

    public List<CPDefinivel> getListaCPDefiniveisEmDefinicao(String strID)
    {
        return m_regEventos.getListaCPDefiniveisEmDefinicaoDoUtilizador(strID);
    }
    
    public void novaCP(CPDefinivel cpDefinivel)
    {
        this.m_CPDefinivel = cpDefinivel;
        this.m_cp = cpDefinivel.novaCP();
    }
     
    public Revisor novoMembroCP(String strId)
    {
        Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizadorByID(strId);
        
        return m_cp.novoMembroCP(u);
    }
    
    public boolean addMembroCP(Revisor r)
    {
        return m_cp.addMembroCP(r);
    }
    
    public void registaCP()
    {
        m_CPDefinivel.setCP(m_cp); 
    }
   
}

