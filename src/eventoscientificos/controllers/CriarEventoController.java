package eventoscientificos.controllers;

import eventoscientificos.domain.AlterarStateParaEmDistribuicao;
import eventoscientificos.domain.AlterarStateParaEmSubmissao;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Utilizador;
import java.util.Date;

/**
 *
 * @author Nuno Silva
 */
public class CriarEventoController {

    private Empresa m_empresa;
    private RegistoEventos m_regEvento;
    private RegistoUtilizadores m_regUtilizadores;
    private Evento m_evento;

    public CriarEventoController(Empresa empresa) {
        m_empresa = empresa;

        m_regEvento = empresa.getRegistoEventos();

        m_regUtilizadores = empresa.getRegistoUtilizadores();

    }

    public void novoEvento() {
        m_evento = m_regEvento.novoEvento();
    }

    public void setTitulo(String strTitulo) {
        m_evento.setTitulo(strTitulo);
    }

    public void setDescricao(String strDescricao) {
        m_evento.setDescricao(strDescricao);
    }

    public void setLocal(String strLocal) {
        m_evento.setLocal(strLocal);
    }

    public void setDataInicio(Date strDataInicio) {
        m_evento.setDataInicio(strDataInicio);
       
    }

    public void setDataFim(Date strDataFim) {
        m_evento.setDataFim(strDataFim);
    }

    public void setDataInicioSubmissao(Date strDataInicioSubmissao) {
        m_evento.setDataInicioSubmissao(strDataInicioSubmissao);
    }

    public void setDataFimSubmissao(Date strDataFimSubmissao) {
        m_evento.setDataFimSubmissao(strDataFimSubmissao);
    }

    public void setDataInicioDistribuicao(Date strDataInicioDistribuicao) {
        m_evento.setDataInicioDistribuicao(strDataInicioDistribuicao);
    }
    
    public void setDataLimiteRevisao(Date setDataLimiteRevisao){
        m_evento.setDataLimiteRevisao(setDataLimiteRevisao);
    }
    
    public void setDataLmiteSubmissaoFinal(Date m_strDataLimiteSubmissaoFinal){
        m_evento.setDataLmiteSubmissaoFinal(m_strDataLimiteSubmissaoFinal);
    }
            

    public boolean addOrganizador(String strId) {

        Utilizador u = m_regUtilizadores.getUtilizadorByID(strId);

        return m_evento.getListaOrganizadores().addOrganizador(u);

    }

    public Evento getEvento() {
        return m_evento;
    }

    public Evento registaEvento() {

        if (m_regEvento.registaEvento(m_evento)) {
            createTimers();
            return m_evento;
        }
        return null;
    }

    private void createTimers() {
        AlterarStateParaEmSubmissao task1 = new AlterarStateParaEmSubmissao(m_evento);
        this.m_empresa.schedule(task1, m_evento.getDataInicioSubmissao());

        AlterarStateParaEmDistribuicao task2 = new AlterarStateParaEmDistribuicao(m_evento);
        this.m_empresa.schedule(task2, m_evento.getDataInicioDistribuicao());

        DetetarConflitosController task3 = new DetetarConflitosController(m_empresa, m_evento);
        this.m_empresa.schedule(task3, m_evento.getDataFimSubmissao());
    }
}
