
package eventoscientificos.controllers;

import TOCS.iTOCS;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Login;
import eventoscientificos.domain.Utilizador;
import java.io.FileNotFoundException;


public class AlterarUtilizadorController implements iTOCS
{
    private Empresa m_empresa;
    private Utilizador m_user;
    
    //alteracao, adicionei login
    public AlterarUtilizadorController(Empresa empresa)
    {
        m_empresa = empresa;
        m_user=m_empresa.getRegistoUtilizadores().getUtilizadorByID(m_empresa.getLogin().getUsername());    
    }
 
    public Utilizador getUtilizador()
    {       
        return m_user;
    }
    
    public void setLogin(Login login){
        m_empresa.setLogin(login);
    }
    
    public boolean alteraDados(String strNome,String strUsername, String strPwd, String strEmail) throws FileNotFoundException
    {
       Utilizador uClone = m_user.clone();
       uClone.setNome(strNome);
       uClone.setEmail(strEmail);
       uClone.setUsername(strUsername);
       uClone.setPassword(strPwd);
       return m_empresa.getRegistoUtilizadores().alteraUtilizador(m_user,uClone); 
    }

    @Override
    public String showData() {
        return m_user.toString();
    }

    
}
