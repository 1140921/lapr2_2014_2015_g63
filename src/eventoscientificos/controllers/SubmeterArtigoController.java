/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class SubmeterArtigoController {

    private Empresa m_empresa;
    private Submissivel m_submissivel;
    private Submissao m_submissao;
    private Artigo m_artigo;

    public SubmeterArtigoController(Empresa empresa) {
        m_empresa = empresa;
        
    }

    public void selectSubmissivel(Submissivel est) {
        m_submissivel = est;
        this.m_submissao = this.m_submissivel.getListaSubmissoes().novaSubmissao();
        this.m_artigo = this.m_submissao.novoArtigo();
    }

    public void setDados(String strTitulo, String strResumo,String palavrasChave) {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
        this.m_artigo.setPalavrasChave(palavrasChave);
    }

    public Autor novoAutor(String strUsername,String strNome,String email, String strAfiliacao) {
        return this.m_artigo.getListaAutores().novoAutor(strUsername,strNome ,email,strAfiliacao);
    }

    public boolean addAutor(Autor autor) {
      
        return this.m_artigo.getListaAutores().addAutor(autor);
    }

    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.m_artigo.getPossiveisAutoresCorrespondentes();
    }
    
    public boolean isVazioAutores(){
        return this.m_artigo.getPossiveisAutoresCorrespondentes().isEmpty();
    }

    public void setCorrespondente(Autor autor) {
          if(m_empresa.getRegistoUtilizadores().getUtilizadorByID(autor.getStrUsername())==null 
                && m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(autor.getStrEmail())==null ){
             throw new IllegalArgumentException("Autor não é Utilizador");

            
        }else{
              this.m_artigo.setAutorCorrespondente(autor);
          }
        
    }
    public boolean isAutorCorrespondente(){
       if(this.m_artigo.getAutorCorrespondente()!=null){
           return true;
       }
       return false;
    }
    
    public boolean isVazioPdf(){
        if(this.m_artigo.getM_strFicheiro()==null){
            return true;
        }
        return false;
    }
  

    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    public String getInfoResumo() {
        return this.m_submissao.getInfo() + this.m_artigo.getInfo();
    }

    public boolean registarSubmissao() {
        this.m_submissao.setArtigo(m_artigo);
        return this.m_submissivel.getListaSubmissoes().addSubmissao(m_submissao);
    }

    public List<Submissivel> getListaSubmissiveisEmSubmissao() {
        return this.m_empresa.getRegistoEventos().getListaSubmissiveisEmSubmissao();
    }
}
