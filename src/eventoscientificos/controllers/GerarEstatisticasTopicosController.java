/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.GerarEstatisticasTopicos;
import eventoscientificos.domain.SessaoTematica;
import eventoscientificos.domain.Submissao;
import java.util.List;

public class GerarEstatisticasTopicosController {

    private Empresa m_empresa;
    private GerarEstatisticasTopicos m_estatisticas;
    
    public GerarEstatisticasTopicosController(Empresa empresa){
        
        this.m_empresa = empresa;
        this.m_estatisticas = new GerarEstatisticasTopicos(empresa);
    }
    
    public List<Evento> getListaEventos(){
        return m_estatisticas.getListaEventos();
    }
    
    public List<Submissao> getListaSubmissoesEvento(){
        return m_estatisticas.getListaSubmissoesEvento();
    }
    
    public Artigo getArtigoEvento(){
        return m_estatisticas.getArtigoSubmissaoEvento();
    }
    
    public List<SessaoTematica> getListaEventosSessoes(){
        return m_estatisticas.getListaEventosSessoes();
    }
    
    public List<Submissao> getListaSubmissoesSessao(){
        return m_estatisticas.getListaSubmissoesSessao();
    }
    
    public Artigo getArtigoSubmissaoSessao(){
        return m_estatisticas.getArtigoSubmissaoSessao();
    }
    
    public String FrequenciaPalavrasChaveEvento(){
        return m_estatisticas.FrequenciaPalavrasChaveEvento();
    }
    
    public String FrequenciaPalavrasChaveSessao(){
        return m_estatisticas.FrequenciaPalavrasChaveSessao();
    }
    
    public int NumeroPalavrasChaveEvento(){
        return m_estatisticas.numeroPalavrasChaveEvento();
    }
    
    public int NumeroPalavrasChaveSessao(){
        return m_estatisticas.numeroPalavrasChaveSessao();
    }
    public float PercentagemFrequenciaPalavrasChaveEvento(){
        float percentagem = m_estatisticas.numeroPalavrasChaveIguaisEvento() /NumeroPalavrasChaveEvento();
        return percentagem;
    }
    
    public float PercentagemFrequenciaPalavrasChaveSessao(){
        float percentagem = m_estatisticas.numeroPalavrasChaveIguaisEvento() /NumeroPalavrasChaveSessao();
        return percentagem;
    }
}
