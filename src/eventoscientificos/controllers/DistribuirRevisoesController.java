
package eventoscientificos.controllers;

import eventoscientificos.domain.Distribuivel;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.MecanismoDistribuicao;
import eventoscientificos.domain.ProcessoDistribuicao;
import eventoscientificos.domain.RegistoEventos;
import java.util.List;


public class DistribuirRevisoesController{
    
    private Empresa m_empresa;
    private RegistoEventos m_registo;
    private Distribuivel m_distribuivel;
    private ProcessoDistribuicao m_processoDistribuicao; 
    private MecanismoDistribuicao m_mecanismoDistribuicao;
     
    public DistribuirRevisoesController(Empresa empresa){
        m_empresa = empresa;
        m_registo=m_empresa.getRegistoEventos();
     } 
     
     public List<Distribuivel> getDistribuiveisEmDistribuicaoDoUtilizador(String id){
         return m_registo.getDistribuiveisEmDistribuicaoDoUtilizador(id);
     }
     
     public void setEventoSessao(Distribuivel distribuivel){
         m_distribuivel=distribuivel;
         m_processoDistribuicao=m_distribuivel.novaDistribuicao();
     }
     
     public List<MecanismoDistribuicao>getMecanismoDistribuicao(){
           return m_empresa.getListaDeMecanismosDistribuicao();
     }
     
     public void setMecanismoDistribuicao(MecanismoDistribuicao mecanismoDistribuicao){
         m_mecanismoDistribuicao=mecanismoDistribuicao;
         m_processoDistribuicao.setMecanismoDistribuicao(m_mecanismoDistribuicao);
         m_processoDistribuicao.distribui(m_distribuivel);
     }
     
     public void registaDistribuicao(){
          m_distribuivel.setProcessoDistribuicao(m_processoDistribuicao);
     }
             
}
