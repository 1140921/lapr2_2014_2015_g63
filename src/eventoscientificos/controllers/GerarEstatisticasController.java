

package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.GerarEstatisticas;
import eventoscientificos.domain.GerarEstatisticasSessao;
import eventoscientificos.domain.ImportarFicheiro;
import eventoscientificos.domain.ListaRevisoes;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.SessaoTematica;
import java.util.List;

public class GerarEstatisticasController {

    
    private Empresa m_empresa;
    private GerarEstatisticas m_estatisticas;
    private GerarEstatisticasSessao m_estatisticasSessao;
    private List<Revisor> m_lstRevisores;
    private Revisor m_revisor;
    
    public GerarEstatisticasController(Empresa m_empresa){
        this.m_empresa = m_empresa;
        m_estatisticas = new GerarEstatisticas(m_empresa);
        m_estatisticasSessao = new GerarEstatisticasSessao(m_empresa);
    }

    //---------------------------------------------------------- Formulas de calculo para Evento --------------------------------\\
    
    public boolean isTesteHipoteses(int indexEvento, int indexRevisor){
        if (m_estatisticas.ValidaTesteHipoteses(indexEvento, indexRevisor)){
           return true;
        }
        else
            return false;
    }
    
    public List<Revisor> getListaRevisoresEvento(int s){
        return m_estatisticas.getListaRevisoresEve(s);
    }
    
    public List<Evento> getListaEventos(){
        return m_estatisticas.getListaEventos();
    }
    
    public boolean rejeitaAceitaRevisor(int indexEvento, int indexRevisor){
        return m_estatisticas.TesteHipoteses(indexEvento, indexRevisor);
    }
    
    public int getSomatorioAtributosClassificacoes(int indexEvento, int indexRevisor){
        return m_estatisticas.somatorioAtributosClassificacoes(indexEvento, indexRevisor);
    }
    
    public float [] getClassificacaoFinalDeCadaRevisao(int indexEvento, int indexRevisor){
        return m_estatisticas.ClassificacaoFinalDeCadaRevisao(indexEvento, indexRevisor);
    }
    
    public float getMediaDoSomatorioClassificacoes(int indexEvento, int indexRevisor){
        return m_estatisticas.mediaDoSomatorioClassificacoes(indexEvento, indexRevisor);
    }
    
    public float[] getCalculoParaCadaRevisao(int indexEvento, int indexRevisor){
        return m_estatisticas.CalculoParaCadaRevisao(indexEvento, indexRevisor);
    }
    
    public float getSomatorioDi(int indexEvento, int indexRevisor){
        return m_estatisticas.SomatorioDi(indexEvento, indexRevisor);
    }
    
    public float getMediaDi(int indexEvento, int indexRevisor){
        return m_estatisticas.MediaDi(indexEvento, indexRevisor);
    }
    
    public float getSomatorioVariancia(int indexEvento, int indexRevisor){
        return m_estatisticas.SomatorioVariancia(indexEvento, indexRevisor);
    }
    
    public float getVariancia(int indexEvento, int indexRevisor){
        return m_estatisticas.Variancia(indexEvento, indexRevisor);
    }
    
    public float getDesvioPadrao(int indexEvento, int indexRevisor){
        return m_estatisticas.DesvioPadrao(indexEvento, indexRevisor);
    }
    
    public float getZobservado(int indexEvento, int indexRevisor){
        return m_estatisticas.zObservado(indexEvento, indexRevisor);
    }
    
    //---------------------------------------------------------- Formulas de calculo para Sessao --------------------------------\\
    
    public boolean isTesteHipotesesSessao(int indexEvento, int indexSessao, int indexRevisor){
        if (m_estatisticasSessao.ValidaTesteHipoteses(indexEvento, indexSessao, indexRevisor)){
           return true;
        }
        else
            return false;
    }
    
    public List<Revisor> getListaRevisoresSessao(int indexEvento, int indexSessao){
        return m_estatisticasSessao.getListaRevisoresST(indexEvento, indexSessao);
    }
    
    public List<SessaoTematica> getListaSessoes(int s){
        return m_estatisticasSessao.getListaSessoes(s);
    }
   
    public boolean rejeitaAceitaRevisorSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.TesteHipoteses(indexEvento, indexSessao, indexRevisor);
    }
    
    public int getSomatorioAtributosClassificacoesSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.somatorioAtributosClassificacoes(indexEvento, indexSessao, indexRevisor);
    }
    
    public float [] getClassificacaoFinalDeCadaRevisaoSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.ClassificacaoFinalDeCadaRevisao(indexEvento, indexSessao, indexRevisor);
    }
    
    public float getMediaDoSomatorioClassificacoesSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.mediaDoSomatorioClassificacoes(indexEvento, indexSessao, indexRevisor);
    }
    
    public float[] getCalculoParaCadaRevisaoSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.CalculoParaCadaRevisao(indexEvento, indexSessao, indexRevisor);
    }
    
    public float getSomatorioDiSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.SomatorioDi(indexEvento, indexSessao, indexRevisor);
    }
    
    public float getMediaDiSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.MediaDi(indexEvento, indexSessao, indexRevisor);
    }
    
    public float getSomatorioVarianciaSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.SomatorioVariancia(indexEvento, indexSessao, indexRevisor);
    }
    
    public float getVarianciaSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.Variancia(indexEvento, indexSessao, indexRevisor);
    }
    
    public float getDesvioPadraoSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.DesvioPadrao(indexEvento, indexSessao, indexRevisor);
    }
    
    public float getZobservadoSessao(int indexEvento, int indexSessao, int indexRevisor){
        return m_estatisticasSessao.zObservado(indexEvento, indexSessao, indexRevisor);
    }
}
