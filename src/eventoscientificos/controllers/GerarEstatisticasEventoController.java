
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.GerarEstatisticasEvento;
import eventoscientificos.domain.Submissao;
import java.util.List;

public class GerarEstatisticasEventoController {

    private GerarEstatisticasEvento m_estatisticas;
    private Empresa m_empresa;
    
    public GerarEstatisticasEventoController(Empresa empresa){
        this.m_empresa = empresa;
        this.m_estatisticas = new GerarEstatisticasEvento(empresa);
    }
    
    public List<Evento> getListaEventos(){
        return m_estatisticas.getListaEventos();
    }
    
    public List<Submissao> getSubmissoesEventos(int indexEvento, int indexSubmissao){
        return m_estatisticas.getSubmissoesEventos(indexEvento, indexSubmissao);
    } 
    
    public float percentagemAceitaçãoSubmissao(int indexEvento, int indexSubmissao ,int SubmissoesAceites){
        return m_estatisticas.percentagemAceitaçãoSubmissao(indexEvento, indexSubmissao, SubmissoesAceites);
    }
    
    public float percentagemRejeicaoSubmissao(int indexEvento, int indexSubmissao ,int SubmissoesRecusadas){
        return m_estatisticas.percentagemRejeicaoSubmissao(indexEvento, indexSubmissao, SubmissoesRecusadas);
    }
    
    public int getNumeroSubmissoes(int indexEvento, int indexSubmissao){
        return m_estatisticas.getNumeroSubmissoes(indexEvento, indexSubmissao);
    }
}
