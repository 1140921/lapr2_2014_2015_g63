/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.ListaSubmissoes;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.List;


public class AlterarSubmissaoController {

    private Empresa m_empresa;
    private Utilizador u;
    private Submissivel m_submissivel;
    private RegistoEventos m_regEventos;
    private ListaSubmissoes lista;
    private Submissao m_submissao;
    private Evento m_evento;
    private Artigo m_artigo;
    private Autor m_autor;

    public AlterarSubmissaoController(Empresa empresa) {
        m_empresa = empresa;
        m_regEventos = m_empresa.getRegistoEventos();
    }

   

    public void setEvento(Evento e) {

        this.m_evento = e;
        if (e == null) {
            throw new NullPointerException("Evento Selecionado Invalido");
        }
    }

    public List<Evento> getListaEventosRegistadosDoUtilizador(String strID) {
        return m_regEventos.getListaEventosRegistadosDoUtilizador(strID);

    }

    public List<Autor> getListaAutores() {
        return m_submissao.getArtigo().getPossiveisAutoresCorrespondentes();
    }

    public List<Submissivel> getListaSubmissoes() {
        return m_regEventos.getListaSubmissiveisEmSubmissao();
    }

    public void setSubmissao(Submissao s) {
        

        this.m_submissao = s;
        m_submissao = s.clone();
        if (s == null) {
            throw new NullPointerException("Submissao Selecionada Invalida");
        }
    }

    public void setAutor(Autor a) {
        this.m_autor = a;
        if (a == null) {
            throw new NullPointerException("Autor selecionado invalido");
        }
    }
    
    public void alterarAutorCorrespondente(Autor autor){
        if(m_empresa.getRegistoUtilizadores().getUtilizadorByID(autor.getM_strNome())==null 
                && m_empresa.getRegistoUtilizadores().getUtilizadorByEmail(autor.getStrEmail())==null ){
             throw new IllegalArgumentException("Autor não é Utilizador");

            
        }else{
        m_submissao.getArtigo().setAutorCorrespondente(autor);
        }
    }

    public void setSubmissivel(Submissivel s) {
        this.m_submissivel = s;
        if (s == null) {
            throw new NullPointerException("Nenhum evento/sessao selecionado");
        }
    }
    
    public void setFicheiro(String strFicheiro) {
        m_submissao.getArtigo().setFicheiro(strFicheiro);
    }

    public void alterarDadosAutor(String id) {

        m_autor = m_submissao.getArtigo().getAutorByUsername(id);

//       m_submissao.getArtigo().
    }

    public void setDados(String nome, String afiliacao, String email) {
        m_autor.setNome(nome);
        m_autor.setAfiliacao(afiliacao);
        m_autor.setStrEmail(email);
    }

    public void alterarDadosArtigo(String strTitulo, String strResumo) {
        m_submissao.getArtigo().setTitulo(strTitulo);
        m_submissao.getArtigo().setResumo(strResumo);
//       uClone.setUsername(strUsername);
//       uClone.setPassword(strPwd);
//       return m_empresa.getRegistoUtilizadores().alteraUtilizador(m_submissao,suClone); 

    }
}
//    
//    public void setSubmissiveis(Submissivel es){
//        this.es = es;
//    }
//    
//    public List<Submissivel> getListaSubmissiveisAutor(){
//        return this.m_empresa.getRegistoEventos().getListaSubmissiveis();
//    }
//    

