
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Listavel;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Submissao;
import java.util.List;

public class ListarSubmissoesRetiradasController {
    
    private Empresa m_empresa;
    private RegistoEventos m_regEventos;
    private Listavel m_listavel;
    public ListarSubmissoesRetiradasController(Empresa empresa){
        m_empresa=empresa;
        m_regEventos=m_empresa.getRegistoEventos();
    }
    
    public List<Listavel> getListaListaveisUtilizador(String id){
     return m_regEventos.getListaListaveisDoUtilizador(id);
    }
    
    public List <Submissao> selectListavel(Listavel l){
        m_listavel=l;
        if(l==null){
            throw new IllegalArgumentException("Nenhum(a) evento/sessao selecionado(a)");
        }
    
        return m_listavel.getListaSubmissoes().getSubmissoesRetiradas();
    
    }

 


}
    


