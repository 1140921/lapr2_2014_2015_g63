/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Conflito;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Licitacao;
import eventoscientificos.domain.Licitavel;
import eventoscientificos.domain.ListaSessoesTematicas;
import eventoscientificos.domain.ProcessoLicitacao;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.RegistoTipoConflitos;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Revisavel;
import eventoscientificos.domain.TipoConflito;
import eventoscientificos.domain.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nunosilva
 */
public class LicitacaoController {

    private Empresa m_empresa;
    private RegistoUtilizadores m_registoUtilizadores;
    private Utilizador m_utilizador;
    private RegistoEventos m_registoEventos;
    private Licitavel m_licitavel;
    private ListaSessoesTematicas m_listSessoesTematicas;
    private ProcessoLicitacao m_proProcessoLicitacao;
    private Licitacao m_licitacao;
    private RegistoTipoConflitos m_registoTipoConflito;

    public LicitacaoController(Empresa empresa) {
        m_empresa = empresa;
    }
  
    /**
     * Recebe como String o revID (id revisor) e retorna a lista de Eventos e
     * Sessões Temáticas onde o id do revisor inserido pertence a CP e
     *
     * @param revID
     * @return
     */
    public List<Licitavel> getListaEventosSessoes(String revID) {
        List<Licitavel> licitavel = new ArrayList<>();

        m_registoUtilizadores = m_empresa.getRegistoUtilizadores();

        m_utilizador = m_registoUtilizadores.getUtilizadorByID(revID);

        m_registoEventos = m_empresa.getRegistoEventos();

        licitavel = m_registoEventos.getLicitaveisEmLicitacaoDoRevisor(revID);

        return licitavel;

    }

    /**
     * Metodo responsavel por buscar as listas do Evento/Sessao Tematica que se
     * encontram ainda por licitar
     *
     * @param EventoOuSessaoTematicaSelecionado Evento ou ST que o utilizador
     * selecionar
     * @return Lista das Licitações do Evento ou Sessão Temática selecionado
     * pelo Utilizador
     */
    public List<Licitacao> getListaLicitações(List<Licitavel> EventoOuSessaoTematicaSelecionado) {
        List<Licitacao> listaLicitacoes = new ArrayList<>();

        //Vai a lista de Licitações ainda em Processo e vai buscar apelas aquelas que correspondem ao ID do Utilizador (Revisor)
        listaLicitacoes = m_licitavel.getProcessoLicitacao().getLicitacoesUtilizador(m_utilizador);

        return listaLicitacoes;
    }

    public void setInterresse(int i) {
        this.m_licitacao.setInterresse(i);
    }
    public int getInterresse(){
        return this.m_licitacao.getInterresse();
    }

    public List<Conflito> getConflitosDetetados() {
        return m_licitacao.getConflitosDetetados();
    }

    public RegistoTipoConflitos getRegistoConflitos() {
        return m_empresa.getRegistoTipoConflitos();
    }

    public List<TipoConflito> getConflitos() {
        return m_registoTipoConflito.getTipoConflitos();
    }
    
    public void setConflitos(List<Conflito> m_listaConflito) {
        m_licitacao.setConflitos(m_listaConflito);
    }
    public void registaLicitacao(){
       m_proProcessoLicitacao.registaLicitacao(m_licitacao);
    }

}
