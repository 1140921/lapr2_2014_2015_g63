
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Removivel;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.util.ArrayList;
import java.util.List;


public class RemoverSubmissaoController {
    private Empresa m_empresa;
    private RegistoEventos m_regEventos;
    private Removivel m_removivel;
    private Submissao m_submissao;
    
    public RemoverSubmissaoController(Empresa empresa){
        m_empresa=empresa;
       m_regEventos=m_empresa.getRegistoEventos();
    }
    
    
       public List<Removivel> getListaEventoSessoesComSubmissaoAutor(String id) {
        return m_regEventos.getListaEventoSessoesComSubmissaoAutor(id);
       }

    public List<Submissao> selectRemovivel(Removivel r) {
        this.m_removivel = r;
        if (r == null) {
            throw new NullPointerException("Nenhum evento/sessao selecionado");
        }
        return m_removivel.getListaSubmissoes().getSubmissoesDoAutor(m_empresa.getLogin().getUsername());
    
    }
    
    
 
        public boolean removeSubmissao(Submissao s){
               this.m_submissao = s;
        if (s == null) {
            throw new NullPointerException("Submissao Selecionada Invalida");
        }
            
            m_removivel.getListaSubmissoes().addSubmissaoRetirada(m_submissao);
            return m_removivel.getListaSubmissoes().removerSubmissao(m_submissao);
        }
        
        
        
        
}
