
package eventoscientificos.controllers;

import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.ListaRevisoes;
import eventoscientificos.domain.RegistoEventos;
import eventoscientificos.domain.Revisao;
import eventoscientificos.domain.Revisavel;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.Submissao;
import java.util.List;


public class ReverArtigoController
{
    private Empresa m_empresa;
    private RegistoEventos m_regEvento;
    private Revisavel m_revisavel;
    private Revisao m_revisao;
    private ListaRevisoes m_listaRevisoes;
     private Revisor revisor;
    
    public ReverArtigoController(Empresa empresa)
    {
        
        m_empresa = empresa;
        m_regEvento = empresa.getRegistoEventos();
                
    }
    
    public List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String id)
    {
        return m_regEvento.getRevisaveisEmRevisaoDoRevisor(id);
    }
    
    
    public List<Submissao> getListaSubmissoesRevisavel(){
       return  m_revisavel.getListaSubmissoes().getSubmissoes();
    }
    
    
    public void selecionaRevisavel(Revisavel r)
    {
        m_revisavel=r;
        
//        ProcessoDistribuicao pd=r.getProcessoDistribuicao();
//        m_listaRevisoes=pd.getListaDeRevisoes();
        
    }       
    
     public void setRevisor(String id){
      for(Revisor r :m_revisavel.getRevisores()){
          if(r.getStrUsername().equals(id)){
              revisor=r;
              m_revisao.setRevisor(r);
          }
      }
    }
    
    
    public void selecionaArtigo(Artigo artigo){
        m_revisao=new Revisao(artigo);
    
    }         
            
    public void setDadosRevisao(String d,String just,int conf,int ade,int orig,int apre,int rec)
    {
        m_revisao.setM_decisao(d);
        m_revisao.setJustificacao(just);
        m_revisao.setM_Confianca(conf);
        m_revisao.setM_Adequacao(ade);
        m_revisao.setM_Originalidade(orig);
        m_revisao.setM_Apresentacao(apre);
        m_revisao.setM_Recomendacao(rec);
 ////        m_revisao.setDecisao(d);
//        m_revisao.setJustificacao(just);
//        if (m_listaRevisoes.valida(m_revisao))
//         m_revisavel.alteraParaEmDecisão();
        
    }         
    public void registaRevisao(){
        m_listaRevisoes=revisor.getLstRevisoes();
        m_listaRevisoes.addRevisao(m_revisao);
        revisor.setLstRevisoes(m_listaRevisoes);
    }

    
                
}
