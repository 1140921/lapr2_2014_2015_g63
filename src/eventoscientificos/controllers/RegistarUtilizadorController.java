package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Utilizador;
import java.io.FileNotFoundException;

/**
 *
 * @author Nuno Silva
 */
public class RegistarUtilizadorController
{
    private Empresa m_empresa;
    private RegistoUtilizadores m_registo;
    private Utilizador m_utilizador;

    public RegistarUtilizadorController(Empresa empresa)
    {
        m_empresa = empresa;
//        if(m_registo == null){
//        m_registo = new RegistoUtilizadores();
//        }else{
            m_registo = m_empresa.getRegistoUtilizadores();
//        }
//        m_registo =  new RegistoUtilizadores();
    }
    
    public void novoUtilizador()
    {
        m_utilizador = m_registo.novoUtilizador();
    }

    public Utilizador setDados(String strUsername, String strPassword, String strNome, String strEmail) 
            throws FileNotFoundException
    {
        m_utilizador.setUsername(strUsername);
        m_utilizador.setPassword(strPassword);
        m_utilizador.setNome(strNome);
        m_utilizador.setEmail(strEmail);
        m_utilizador.setPasswordCodificada(strPassword);
        if( m_registo.registaUtilizador(m_utilizador))
            return m_utilizador;
        else
            return null;
    }
}

