/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.ImportarFicheiro;


/**
 * Classe Controller responsável pela interação ImportarFicheiroUI com
 * ImportarFicheiro
 *
 * @author Andre Ribeiro
 */
public class ImportarFicheiroController {

    private ImportarFicheiro m_ImportarFicheiro;

    private Empresa m_empresa;
    /**
     * Instanciação da classe ImportarFicheiroController 
     * @param empresa 
     */
    public ImportarFicheiroController(Empresa empresa) {

        m_empresa = empresa;
        m_ImportarFicheiro = new ImportarFicheiro(m_empresa);

    }

    /**
     * Método responsavel por receber o caminho do ficheiro selecionado pelo
     * utilizador e enviar o parametro para a classe ImportarFicheiro
     *
     * @param pathficheiroUtilizador o caminho (path) do ficheiro
     */
    public void importarFicheiroUtilizador(String pathficheiroUtilizador) {
        System.out.println("Caminho do ficheiro :" + pathficheiroUtilizador);
        m_ImportarFicheiro.importarFicheiroUtilizador(pathficheiroUtilizador);
    }

    /**
     * Método responsavel por receber o caminho do ficheiro selecionado pelo
     * utilizador e enviar o parametro para a classe ImportarFicheiro
     *
     * @param pathficheiroUtilizador o caminho (path) do ficheiro
     */
    public void importarFicheiroEvento(String pathficheiroUtilizador) {
        System.out.println("Caminho do ficheiro :" + pathficheiroUtilizador);

    }

    /**
     * Método responsavel por receber o caminho do ficheiro selecionado pelo
     * utilizador e enviar o parametro para a classe ImportarFicheiro
     *
     * @param pathficheiroLocal o caminho (path) do ficheiro
     */
    public void importarFicheiroLocal(String pathficheiroLocal) {
        System.out.println("Caminho do ficheiro :" + pathficheiroLocal);

    }

}
