/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controllers;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.MecanismoDetecaoConflito;
import eventoscientificos.domain.TipoConflito;

/**
 *
 * @author nunosilva
 */
public class DefinirTipoConflitoController
{
    private Empresa m_empresa;

    public DefinirTipoConflitoController(Empresa empresa)
    {
        m_empresa = empresa;
    }
    
    public TipoConflito novoTipoConflito(String strDescricao, MecanismoDetecaoConflito mecanismo)
    {
        return m_empresa.getRegistoTipoConflitos().novoTipoConflito(strDescricao, mecanismo);
    }
    
    public boolean registaTipoConflito(TipoConflito tpConflito)
    {
        return m_empresa.getRegistoTipoConflitos().registaTipoConflito(tpConflito);
    }
}
