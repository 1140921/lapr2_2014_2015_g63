
package eventoscientificos.domain;
public class Revisao {
    private String m_decisao;
    private String m_Justificacao;
    private Integer m_Confianca;
    private Integer m_Adequacao;
    private Integer m_Originalidade;
    private Integer m_Apresentacao;
    private Integer m_Recomendacao;
    private Revisor m_revisor;
    private Artigo m_artigo;  
    
  
    public Revisao(){
        
    }
    
    public Revisao(Artigo artigo){
        this.m_artigo=artigo;
    }
    
    
    public void setJustificacao(String just) 
    {
        setM_Justificacao(just);
    }

    /**
     * @return the m_revisor
     */
    public Revisor getRevisor() {
        return getM_revisor();
    }

    /**
     * @param m_revisor the m_revisor to set
     */
    public void setRevisor(Revisor m_revisor) {
        this.m_revisor = m_revisor;
    }
 
    public void setArtigo(Artigo artigo){
    m_artigo=artigo;
    }

 

    /**
     * @param m_Justificacao the m_Justificacao to set
     */
    public void setM_Justificacao(String m_Justificacao) {
        this.m_Justificacao = m_Justificacao;
        if(m_Justificacao==null || m_Justificacao.trim().isEmpty()){
            throw new NullPointerException("Insira Justificação");
        }
    
    }

    /**
     * @param m_Confianca the m_Confianca to set
     */
    public void setM_Confianca(Integer m_Confianca) {
        this.m_Confianca = m_Confianca;
        if(m_Confianca==null){
            throw new NullPointerException("Insira valor de Confianca");
        }
        if(m_Confianca<0 || m_Confianca>5){
            throw new IllegalArgumentException("Valor inválido de confianca(Escala 0-5)");
        }
    }

    /**
     * @param m_Adequacao the m_Adequacao to set
     */
    public void setM_Adequacao(Integer m_Adequacao) {
        this.m_Adequacao = m_Adequacao;
        if(m_Adequacao==null){
            throw new NullPointerException("Insira valor de Adequacão ao evento");
        }
        if(m_Adequacao<0 || m_Adequacao>5){
            throw new IllegalArgumentException("Valor inválido de Adequacão (Escala 0-5)");
        }
    }

    /**
     * @param m_Originalidade the m_Originalidade to set
     */
    public void setM_Originalidade(Integer m_Originalidade) {
        this.m_Originalidade = m_Originalidade;
        if(m_Originalidade==null){
            throw new NullPointerException("Insira valor de originalida do artigo");
        }
        if(m_Originalidade<0 || m_Originalidade>5){
            throw new IllegalArgumentException("Valor inválido de originalidade(Escala 0-5)");
        }
    }

    /**
     * @param m_Recomendacao the m_Recomendacao to set
     */
    public void setM_Recomendacao(Integer m_Recomendacao) {
        this.m_Recomendacao = m_Recomendacao;
        if(m_Recomendacao==null){
            throw new NullPointerException("Insira valor de Recomendacão");
        }
        if(m_Recomendacao<-2 || m_Recomendacao>2){
            throw new IllegalArgumentException("Valor inválido de recomendacão(Escala [-2,2])");
        }
    }

    /**
     * @return the m_Justificacao
     */
    public String getM_Justificacao() {
        return m_Justificacao;
    }

    /**
     * @return the m_Confianca
     */
    public Integer getM_Confianca() {
        return m_Confianca;

    }

    /**
     * @return the m_Adequacao
     */
    public Integer getM_Adequacao() {
        return m_Adequacao;
    }

    /**
     * @return the m_Originalidade
     */
    public Integer getM_Originalidade() {
        return m_Originalidade;
    }

    /**
     * @return the m_Apresentacao
     */
    public Integer getM_Apresentacao() {
        return m_Apresentacao;
    }

    /**
     * @return the m_Recomendacao
     */
    public Integer getM_Recomendacao() {
        return m_Recomendacao;
    }

    /**
     * @return the m_revisor
     */
    public Revisor getM_revisor() {
        return m_revisor;
    }

    /**
     * @return the m_artigo
     */
    public Artigo getM_artigo() {
        return m_artigo;
    }

    /**
     * @param m_Apresentacao the m_Apresentacao to set
     */
    public void setM_Apresentacao(Integer m_Apresentacao) {
        this.m_Apresentacao = m_Apresentacao;
        if(m_Apresentacao==null){
            throw new NullPointerException("Insira valor de qualidade de apresentacão");
        }
        if(m_Apresentacao<0 || m_Apresentacao>5){
            throw new IllegalArgumentException("Valor inválido (Escala 0-5)");
        }
    }

    /**
     * @return the m_decisao
     */
    public String getM_decisao() {
        return m_decisao;
    }

    /**
     * @param m_decisao the m_decisao to set
     */
    public void setM_decisao(String m_decisao) {
        this.m_decisao = m_decisao;
        if(m_decisao==null || m_decisao.trim().isEmpty()){
            throw new NullPointerException("Insira decisão");
        }
    }
    
    
    
    
    
   
    
    
}

