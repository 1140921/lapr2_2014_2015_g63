/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.List;

/**
 *
 * @author iazevedo
 */
public class Licitacao {

    private Revisor m_rev; // Revisor que efetua a licitação

    private Submissao m_submissao;

    private int m_interresse; // varição de -2 a 2

    private List<Conflito> m_listaConflitosDetetados;
    private List<Conflito> m_listaConflitos;

    public boolean valida() {
        return true;
    }

    public void setRevisor(Revisor r) {
        this.m_rev = r;
    }

    public void setSubmissao(Submissao s) {
        this.m_submissao = s;
    }

    public void setTipoConflitoDetetado(Conflito tc) {

    }

    public List<Conflito> getConflitosDetetados() {
        return this.m_listaConflitosDetetados;
    }

    public void setConflitos(List<Conflito> listaConflitos) {
        this.m_listaConflitos = listaConflitos;
    }

    public void setInterresse(int i) {

        if (i > 2 || i < -2) {
            System.out.println("ERRO :Valor não se encontra dentro dos parâmetros válidos (-2 a 2)");
        } else {

            this.m_interresse = i;
        }
    }

    public int getInterresse() {
        return this.m_interresse;
    }

    public Revisor getRevisor() {
        return m_rev;
    }
/**
 * Verifica se utilizador é revisor
 * @param rev
 * @return 
 */   
    public boolean hasRevisor(Revisor rev){
        return this.m_rev.equals(rev);
    }
/**
 * Metodo equals
 * @param obj
 * @return 
 */
    @Override
    public boolean equals(Object obj) {
        Licitacao l = (Licitacao) obj;

        if (l.hasRevisor(m_rev)) {
            return true;
        }

        return false;
    }

}
