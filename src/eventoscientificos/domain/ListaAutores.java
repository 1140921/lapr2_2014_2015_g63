/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class ListaAutores {

    private List<Autor> m_listaAutores;
    private Autor a;

    public ListaAutores() {
        m_listaAutores = new ArrayList<Autor>();
    }

    public Autor novoAutor(String strUsername ,String strNome,String email, String strAfiliacao) {
        Autor autor = new Autor();
        autor.setUsername(strUsername);
        autor.setNome(strNome);
        autor.setStrEmail(email);
        autor.setAfiliacao(strAfiliacao);

        return autor;
    }

//    public void setAutor(String strNome, String strAfiliacao){
//        a.setNome(strNome);
//        a.setAfiliacao(strAfiliacao);
//    }
    public boolean addAutor(Autor autor) {
        if (validaAutor(autor)) {
            return getM_listaAutores().add(autor);
        } else {
            return false;
        }

    }

    public boolean hasAutor(String strUsername) {
        for (Autor autor : this.getM_listaAutores()) {
            if (autor.getStrUsername().equals(strUsername)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean validaAutor(Autor autor) {
        for (Autor a : getM_listaAutores()) {
            if (a.compareTo(autor) == 0) {
                throw new IllegalArgumentException("Autor já se encontra registado no artigo");
            }

        }
        return autor.valida();
    }

    protected List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<Autor>();

        for (Autor autor : this.getM_listaAutores()) {
            if (autor.podeSerCorrespondente()) {
                la.add(autor);
            }
        }
        return la;
    }

    /**
     * @return the m_listaAutores
     */
    public List<Autor> getM_listaAutores() {
        return m_listaAutores;
    }
}
