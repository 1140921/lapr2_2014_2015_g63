/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class RegistoTipoConflitos {

    private List<TipoConflito> m_lstTipos;

    public RegistoTipoConflitos() {
        m_lstTipos = new ArrayList<TipoConflito>();
    }

    public TipoConflito novoTipoConflito(String strDescricao, MecanismoDetecaoConflito mecanismo) {
        return new TipoConflito(strDescricao, mecanismo);
    }

    public TipoConflito novoTipoConflitoDescricao(String strDescricao) {
        return new TipoConflito(strDescricao);
    }

    public boolean registaTipoConflito(TipoConflito tpConflito) {
        if (tpConflito == null) {
            return false;
        }
        if (tpConflito.valida() && validaTipoConflito(tpConflito)) {
            return m_lstTipos.add(tpConflito);
        }
        else{
            return false;
        }
    }

    private boolean validaTipoConflito(TipoConflito tpConflito) {
        for (TipoConflito tpConflitos : m_lstTipos) {
            if (tpConflitos.getDescricao().equalsIgnoreCase(tpConflito.getDescricao())) {
                return false;
            }
        }
        return true;
    }

    private void addTipoConflito(TipoConflito tpConflito) {
        m_lstTipos.add(tpConflito);
    }

    public List<TipoConflito> getTipoConflitos() {
        return m_lstTipos;
    }

    public List<Conflito> getListaConflitos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
