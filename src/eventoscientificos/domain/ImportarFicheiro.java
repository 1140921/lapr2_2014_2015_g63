/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Andre Ribeiro
 */
public class ImportarFicheiro {

    private Empresa m_empresa;
    private Utilizador m_utilizador;
    private RegistoUtilizadores m_registoUtilizadores;
    private ArrayList<String> listaCodigo ;

    public ImportarFicheiro(Empresa empresa) {

        m_empresa = empresa;
        m_registoUtilizadores = new RegistoUtilizadores();
        listaCodigo = new ArrayList<>();

    }

    public void importarFicheiroUtilizador(String ficheiroUtilizador) {
        try {

            File fXmlFile = new File(ficheiroUtilizador);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

//            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());// Serve para confirmar se o root foi devidamente identificado
            NodeList nList = doc.getElementsByTagName("Utilizador");

            int numeroUti = Integer.parseInt(doc.getElementsByTagName("NumeroElementos").item(0).getTextContent());


            for (int i = 0; i < numeroUti; i++) {

                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
//			System.out.println("Staff id : " + eElement.getAttribute("id"));
                    String username = eElement.getElementsByTagName("Username").item(0).getTextContent();
                    String nome = eElement.getElementsByTagName("Nome").item(0).getTextContent();
                    String email = eElement.getElementsByTagName("Email").item(0).getTextContent();
                    String password = eElement.getElementsByTagName("Password").item(0).getTextContent();
                    String[] pass = password.split(";");
                    String codigo =pass[0];
                    listaCodigo.add(codigo);
                    String pw = pass[1];
                    m_empresa.getRegistoUtilizadores().registaUtilizador(new Utilizador(nome, username, pw, email));
//                    System.out.println("Utilizador adicionado");

                }
            }
        }catch(SAXException k){
            System.out.println("------------------------------------------------------------");
            System.out.println("ficheiro XML esta ilegivel , verifique o mesmo !!");
            System.out.println("------------------------------------------------------------");
        }catch(IllegalArgumentException s){
            System.out.println("-----------------------------------------------------------------------------");
            System.out.println("Ficheiro Inserido no formato não correspondido verifique se este é um .xml !!");
            System.out.println("------------------------------------------------------------------------------");
        }catch(NullPointerException a){
//            System.out.println("------------------------------------------------------------------------------------------------------------------");
//            System.out.println("Ficheiro Inserido não se encontra correcto verifique se inseriu o ficheiro correspondente ao tipo de importação !!");
//            System.out.println("--------------------------------------------------------------------------------------------------------------------");
            JOptionPane.showMessageDialog(null,"Ficheiro Inserido não se encontra correcto verifique se inseriu o ficheiro correspondente ao tipo de importação !!");
        } catch (Exception e) {
            System.out.println("-------------------------------------------------------------");
            System.out.println("ERRO :");
            System.out.println("-------------------------------------------------------------");
            e.printStackTrace();
        }
    }

    public void ImportarFicheiroLocal(File ficheiroLocal) {
// (Parcialmente desenvolvido)
//        try {
//
//            File fXmlFile = ficheiroLocal;
//            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//            Document doc = dBuilder.parse(fXmlFile);
//
//            doc.getDocumentElement().normalize();
//            
//
//            System.out.println("Root element :" + doc.getDocumentElement().getNodeName()); // Serve para confirmar se o root foi devidamente identificado
//
//            NodeList nList = doc.getElementsByTagName("Local");
//
//            System.out.println("----------------------------");
//
//            for (int temp = 0; temp < nList.getLength(); temp++) {
//
//                Node nNode = nList.item(temp);
//
//                System.out.println("\nCurrent Element :" + nNode.getNodeName());
//
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    Element eElement = (Element) nNode;
//
////			System.out.println("Staff id : " + eElement.getAttribute("id"));
//                    System.out.println("LocalID : " + eElement.getElementsByTagName("LocalID").item(0).getTextContent());
//                    System.out.println("Designacao : " + eElement.getElementsByTagName("Designacao").item(0).getTextContent());
//                    
//
//                }
//                
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    public void ImportarFicheiroEvento(File ficheiroEvento) {
        // (por desenvolver)
    }
}
