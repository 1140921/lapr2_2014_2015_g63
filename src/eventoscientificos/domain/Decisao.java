/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author iazevedo
 */
public class Decisao 
{
    private String m_strAceitacao;
    private String m_strClassificacao;
    private Submissao m_Submissao;
    private Notificacao m_Notificacao;

    
    public void setAceitacao(String a) 
    {
        m_strAceitacao=a;
    }

    
    public void setClassificacao(String c) 
    {
        m_strClassificacao=c;
    }

    
    public void setSubmissao(Submissao s) 
    {
         m_Submissao=s;
    }

    public void notifica() 
    {
        Autor ac;
        ac = m_Submissao.getAutorCorrespondente();
        setNotificacao(new Notificacao(ac));
    }

    
    private void setNotificacao(Notificacao n) 
    {
        m_Notificacao = n;
    }

    @Override
    public String toString()
    {
        return this.m_strClassificacao + "+ ..."; //Também dados de Submissao
    }
}
