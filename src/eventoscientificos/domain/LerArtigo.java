/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author 1140921
 */
public class LerArtigo {

    private Empresa m_empresa;
    private String FILE_TXT;
    private Submissao m_submissao;

    public LerArtigo(Empresa m_empresa, String File_TXT) throws FileNotFoundException {

        this.m_empresa = m_empresa;
        this.FILE_TXT = File_TXT;
        int i = 0;
        String artigo[][] = lercsv(FILE_TXT);
        for (SessaoTematica s : m_empresa.getRegistoEventos().getListEventos().get(0).getListaDeSessõesTemáticas().getListSessoes()) {
            if (artigo[i][0].equalsIgnoreCase(s.getCodigo())) {
                m_submissao = s.getListaSubmissoes().novaSubmissao();

                Artigo m_artigo = null;

                m_artigo.setTitulo(artigo[i][1]);
                m_artigo.setResumo("não tem");
                Autor a = new Autor(artigo[i][3]);
                m_artigo.setAutorCorrespondente(a);
                m_artigo.setPalavrasChave(artigo[i][4]);

                Autor a2 = new Autor(artigo[i][5]);
                a2.setAfiliacao(artigo[i][6]);
                a2.setStrEmail(artigo[i][7]);
                m_artigo.getListaAutores().addAutor(a2);
                Autor a3 = new Autor(artigo[i][8]);
                a2.setAfiliacao(artigo[i][9]);
                a2.setStrEmail(artigo[i][10]);
                m_artigo.getListaAutores().addAutor(a3);

                m_artigo = this.m_submissao.novoArtigo();

                m_submissao.setArtigo(m_artigo);
                s.getListaSubmissoes().addSubmissao(m_submissao);
                i++;
            }

        }

    }

    public String[][] lercsv(String FILE_TXT) throws FileNotFoundException {

        //String[][]ficheiroCsv = new String[200][6];
        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");
        int cont = 0;
        int cont1 = 0;
        String[][] ficheiroCsv = new String[numeroLinhasFicheiro(FILE_TXT)][numeroColunasFicheiro(FILE_TXT)];
        while (fInput.hasNextLine()) {
            String s = fInput.nextLine();
            if (cont1 != 0) {

                if (s.length() != 0) {
                    String[] temp = s.split(";");
                    ficheiroCsv[cont][0] = temp[0];
                    ficheiroCsv[cont][1] = temp[1];
                    ficheiroCsv[cont][2] = temp[2];
                    ficheiroCsv[cont][3] = temp[3];
                    ficheiroCsv[cont][4] = temp[4];
                    ficheiroCsv[cont][5] = temp[5];
                    ficheiroCsv[cont][6] = temp[6];
                    ficheiroCsv[cont][7] = temp[7];
                    ficheiroCsv[cont][8] = temp[8];
                    ficheiroCsv[cont][9] = temp[9];
                    ficheiroCsv[cont][10] = temp[10];
                    cont++;
                }
            } else {
                cont1++;
            }
        }
        fInput.close();

        return ficheiroCsv;
    }

    public static int numeroLinhasFicheiro(String FILE_TXT) throws FileNotFoundException {
        int cont = 0;
        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");
        int cont1 = 0;
        while (fInput.hasNextLine()) {
            String s = fInput.nextLine();
            if (cont1 != 0) {
                if (s.length() != 0) {
                    cont++;
                }
            } else {
                cont1++;
            }

        }
        fInput.close();

        return cont;
    }

    public static int numeroColunasFicheiro(String FILE_TXT) throws FileNotFoundException {
        int cont = 0;
        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");
        int cont1 = 0;
        while (fInput.hasNextLine()) {
            String s = fInput.nextLine();
            String[] temp = s.split(";");
            cont = temp.length;

        }
        fInput.close();

        return cont;
    }
}
