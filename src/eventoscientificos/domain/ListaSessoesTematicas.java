
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListaSessoesTematicas {
    
    private Evento m_evento;
    private List<SessaoTematica> m_listaST;
    
    public ListaSessoesTematicas(Evento e)
    {
        this.m_evento = e;
        m_listaST = new ArrayList<SessaoTematica>(); 
    }
    
    public List<CPDefinivel> getListaCPDefiniveisEmDefinicaoDoUtilizador(String strID)
    {
        List<CPDefinivel> ls = new ArrayList<CPDefinivel>();
        ls.addAll(m_listaST);        
        return ls;
    }
    
    public List<SessaoTematica> getListSessoes(){
        return m_listaST;
    }
    
    public SessaoTematica novaSessaoTematica(String cod, String desc,Date dtInicioSub, Date dtFimSub, 
            Date dtInicioDistr, Date dtLimiteRev,Date dtSubFinal){
        return new SessaoTematica(cod,desc,dtInicioSub,dtFimSub,dtInicioDistr,dtLimiteRev,dtSubFinal,m_evento);
    }
    
    public boolean registaSessaoTematica(SessaoTematica st)
    {
        if( st.valida() && validaST(st) )
            return addST(st);
        else
            return false;
    }
    
    
        //novo
        public List<Removivel> getListaEventoSessoesComSubmissaoAutor(String id) {
        List<Removivel> lst = new ArrayList();
        for(SessaoTematica s :m_listaST){
             if(!s.getListaSubmissoes().getSubmissoesDoAutor(id).isEmpty()){
                 lst.add(s);
             }          
        }
        return lst;
        }
    
    
    private boolean addST(SessaoTematica st)
    {
        if (st.setStateRegistado())
        {
            if (m_listaST.add(st))
            {
                if (this.m_evento.setStateSTDefinidas())
                    return true;
                else
                {
                    m_listaST.remove(st);
                }
            }
        }
        return false;
    }
   
    
    private boolean validaST(SessaoTematica st)
    {
          for(SessaoTematica sessao : m_listaST){
              if(st.equals(sessao)){
                  return false;
              }
          }
      return true;
    }

    public List<Decidivel> getDecisiveis(String strID) {
        List<Decidivel> ld = new ArrayList<>();
        return ld;
    }

    List<Submissivel> getListaSubmissiveisEmSubmissao()
    {
        List<Submissivel> ls = new ArrayList<Submissivel>();
        for(SessaoTematica st:this.m_listaST)
        {
            if (st.isInEmSubmissao())
                ls.add(st);
        }
        return ls;
    }

     List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String strID) {
        List<Revisavel> lr = new ArrayList<>();
        for(SessaoTematica s : m_listaST){
            if(s.isInEmRevisao() && s.hasRevisor(strID)){
                lr.add(s);
            }
            
        }
        return lr;
    }
    
     //novo
     List<Distribuivel> getDistribuiveis(String strID){
         List<Distribuivel> ld = new ArrayList<>();
         for(SessaoTematica st :m_listaST){
             if(st.isInEmDistribuicao() && st.hasProponente(strID)){
                 ld.add(st);
             }
         }
         
         return ld;
     }
     
     /**
     * UC11 
     * @return 
     */
    public List<Licitavel> getLicitacoesEmLicitacao() {
        List<Licitavel> lsLicitacoes = new ArrayList<>();
        for (SessaoTematica st : m_listaST) {
            if (st.isInEmLicitacao()) {
                lsLicitacoes.add(st);
            }
        }
        
        return lsLicitacoes;
    }
}
