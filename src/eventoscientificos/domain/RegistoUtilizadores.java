
package eventoscientificos.domain;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


public class RegistoUtilizadores 
{
    private final List<Utilizador> m_listaUtilizadores;
    
    public RegistoUtilizadores()
    {
        m_listaUtilizadores = new ArrayList<Utilizador>();
        
    }
    
    public Utilizador novoUtilizador()
    {
        
        return new Utilizador();
    }
    
    public boolean registaUtilizador(Utilizador u) throws FileNotFoundException{
        u.setPasswordCodificada(u.getM_strPassword());
        if( u.valida() && validaUtilizador(u) )
            return addUtilizador(u);
        else
            return false;
    }
    
    private boolean validaUtilizador(Utilizador u)
    {
        
        for(Utilizador utilizador: m_listaUtilizadores){
         if(u.getUsername().equals(utilizador.getUsername())){   
                  throw new IllegalArgumentException("Utilizador com username igual ja existe no sistema");
            }
            if(u.getEmail().equals(utilizador.getEmail())){
                throw new IllegalArgumentException("Utilizador com email igual ja existente no sistema");
            }
        }    
        return true;
    }    
     
    
    public boolean addUtilizador(Utilizador u)
    {
        
        return m_listaUtilizadores.add(u);
    }
    
    public Utilizador getUtilizadorByID(String strId)
    {
        for(Utilizador u:this.m_listaUtilizadores)
        {
            if (u.getUsername().equals(strId))
                return u;
        }
        return null;
    }
    
    public Utilizador getUtilizadorByPassword(String pwd){
        for(Utilizador u : this.m_listaUtilizadores){
            if(u.getUsername().equals(pwd)){
                return u;
            }
        }
      return null;
    }
    
    public List<Utilizador> getListUtilizador(){
        return m_listaUtilizadores;
    }
    
   public Utilizador getUtilizadorByEmail(String strEmail)
    {
        for(Utilizador u:this.m_listaUtilizadores)
        {
            if (u.getEmail().equals(strEmail))
                return u;
        }
        return null;
    }
     //alterei
    public boolean alteraUtilizador(Utilizador uOriginal, Utilizador uClone) throws FileNotFoundException
    {
        if (uClone.valida()){
            
            List<Utilizador> lstUtilizadores = new ArrayList<Utilizador>(m_listaUtilizadores);
            lstUtilizadores.remove(uOriginal);
            lstUtilizadores.add(uClone);
            if (validaLista(lstUtilizadores,uClone)){
                
                uOriginal.setNome(uClone.getNome());
                uOriginal.setEmail(uClone.getEmail());
                uOriginal.setUsername(uClone.getUsername());
                uOriginal.setPassword(uClone.getPwd());
                uOriginal.setPasswordCodificada(uClone.getPwd());
                return true;
            }
        }
        return false;
    }
    //alterado
    public boolean validaLista(List<Utilizador> lista,Utilizador uClone){   
        lista.remove(uClone);  
        for(Utilizador u : lista){
            if(uClone.getUsername().equals(u.getUsername())){   
                  throw new IllegalArgumentException("Utilizador com username igual ja existe no sistema");
            }
            if(uClone.getEmail().equals(u.getEmail())){
                throw new IllegalArgumentException("Utilizador com email igual ja existente no sistema");
            }  
        
        }
          
      return true;
      
    }

}
