/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author Nuno Silva
 */
public class Local
{
    private String m_strLocal;

    public Local()
    {
    }
    
    
    public void setLocal(String strLocal)
    {
        m_strLocal = strLocal;
    }
    
    public boolean valida()
    {
        System.out.println("Local: valida:" + this.toString());
        return true;
    }
    
    @Override
    public String toString(){
        return m_strLocal;
    }
    
    
}
