/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author ASUS
 */
public class ExportarFicheiro {

    private Empresa m_empresa;
    private Utilizador m_utilizador;
    private int m_contElementos;
    private int m_numEventos;
    private int m_contNumeroOrganizadores;
    private int m_contNumeroProponentes;
    private int m_contNumeroSessoesTematicas;
    private int m_contNumeroCPST;
    private int m_contNumeroSubmissoes;
    private int m_numeroAutores;
    private int m_contRevisoes;
    private int m_numeroMembroCP;
    private int m_contSubmissoesEvento;

    public ExportarFicheiro(Empresa empresa) {
        m_empresa = empresa;
        m_contElementos = 0;
        m_numeroMembroCP =0;
        m_contNumeroOrganizadores = 0;
        m_contNumeroProponentes = 0;
        m_contNumeroSessoesTematicas = 0;
        m_contNumeroCPST = 0;
        m_contNumeroSubmissoes = 0;
        m_numeroAutores = 0;
        m_numEventos = 0;
        m_contRevisoes = 0;
        m_contSubmissoesEvento = 0;
        m_utilizador = new Utilizador();
    }

    public void exportarFicheiroUtilizador(String pathficheiro) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("ListaUtilizadores");
            doc.appendChild(rootElement);

            // NumeroElementos Element
            Element numElementos = doc.createElement("NumeroElementos");
            rootElement.appendChild(numElementos);

            // staff elements
            Element utilizadores = doc.createElement("Utilizadores");
            rootElement.appendChild(utilizadores);

            for (Utilizador user : m_empresa.getRegistoUtilizadores().getListUtilizador()) {
                m_contElementos++;

                Element uti = doc.createElement("Utilizador");
                utilizadores.appendChild(uti);

                // Username Element
                Element username = doc.createElement("Username");
                username.appendChild(doc.createTextNode(user.getUsername()));
                uti.appendChild(username);

                // Nome Element
                Element nome = doc.createElement("Nome");
                nome.appendChild(doc.createTextNode(user.getNome()));
                uti.appendChild(nome);

                // Email Element
                Element email = doc.createElement("Email");
                email.appendChild(doc.createTextNode(user.getEmail()));
                uti.appendChild(email);

                // Passord Element
                Element password = doc.createElement("Password");
                String codificacao = "CA;";
                String passwordiz = user.getM_strPassword();
                String passwordFinal = codificacao + passwordiz;
                password.appendChild(doc.createTextNode(passwordFinal));
                uti.appendChild(password);

            }
            // Inserir na tag Numero de Elementos o valor correcto
            String numero = Integer.toString(m_contElementos);
            numElementos.appendChild(doc.createTextNode(numero));

            // Escrever o no ficheiroXML
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            doc.setXmlStandalone(true);
            DOMSource source = new DOMSource(doc);

            //Concatenação de Strings
            String xml = ".xml";
            String xmlFinal = pathficheiro + xml;
            StreamResult result = new StreamResult(new File(xmlFinal));

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("Ficheiro Guardado!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    public void exportarFicheiroEvento(String pathficheiro) {
        
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("ListaEventos");
            doc.appendChild(rootElement);

            Element numEventos = doc.createElement("NumeroEventos");
            rootElement.appendChild(numEventos);

            Element eventos = doc.createElement("Eventos");
            rootElement.appendChild(eventos);

            //-----------------------------------------EVENTO-------------------------------------//
            for (Evento regEvento : m_empresa.getRegistoEventos().getListEventos()) {
                m_numEventos++;
                Element evento = doc.createElement("Evento");
                eventos.appendChild(evento);

                Element titulo = doc.createElement("Titulo");
                titulo.appendChild(doc.createTextNode(regEvento.getTitulo()));
                evento.appendChild(titulo);

                Element descricao = doc.createElement("Descricao");
                descricao.appendChild(doc.createTextNode(regEvento.getM_strDescricao()));
                evento.appendChild(descricao);
                
                
                Element dataInicio = doc.createElement("DataInicio");
                dataInicio.appendChild(doc.createTextNode(fmt.format(regEvento.getM_strDataInicio())));
                evento.appendChild(dataInicio);

                Element dataFim = doc.createElement("DataFim");
                dataFim.appendChild(doc.createTextNode(fmt.format(regEvento.getM_strDataFim())));
                evento.appendChild(dataFim);

                Element dataInicioSubmissao = doc.createElement("DataInicioSubmissão");
                dataInicioSubmissao.appendChild(doc.createTextNode(fmt.format(regEvento.getDataInicioSubmissao())));
                evento.appendChild(dataInicioSubmissao);

                Element dataFimSubmissao = doc.createElement("DataFimSubmissao");
                dataFimSubmissao.appendChild(doc.createTextNode(fmt.format(regEvento.getDataFimSubmissao())));
                evento.appendChild(dataFimSubmissao);

                Element dataInicioDistribuicao = doc.createElement("DataInicioDistribuicao");
                dataInicioDistribuicao.appendChild(doc.createTextNode(fmt.format(regEvento.getDataInicioDistribuicao())));
                evento.appendChild(dataInicioDistribuicao);

                Element dataLimiteRevisao = doc.createElement("DataLimiteRevisao");
                dataLimiteRevisao.appendChild(doc.createTextNode(fmt.format(regEvento.getDataLimiteRevisao())));
                evento.appendChild(dataLimiteRevisao);

                Element dataLimiteSubmissaoFinal = doc.createElement("DataLmiteSubmissaoFinal");
                dataLimiteSubmissaoFinal.appendChild(doc.createTextNode(fmt.format(regEvento.getDataLimiteSubmissaoFinal())));
                evento.appendChild(dataLimiteSubmissaoFinal);

                Element estadoEvento = doc.createElement("EstadoEvento");
                estadoEvento.appendChild(doc.createTextNode("EventoStateEmRevisao"));
                evento.appendChild(estadoEvento);

                Element local = doc.createElement("Local");
                local.appendChild(doc.createTextNode(regEvento.getM_local().toString()));
                evento.appendChild(local);

                // ------------------------ ORGANIZADORES --------------------- //
                Element listaOrganizadores = doc.createElement("ListaOrganizadores");
                evento.appendChild(listaOrganizadores);

                Element numeroOrg = doc.createElement("NumeroOrganizadores");
                listaOrganizadores.appendChild(numeroOrg);

                Element organizadores = doc.createElement("Organizadores");
                listaOrganizadores.appendChild(organizadores);

                

                for (Organizador org : regEvento.getListaOrganizadores().getListaEventos()) {

                    
                    m_contNumeroOrganizadores++;

                    Element organizador = doc.createElement("Organizador");
                    organizador.appendChild(doc.createTextNode(org.getUsername()));
                    organizadores.appendChild(organizador);

                }
                //Adicionar numero de org do evento em questão
                String numeroOrganizadores = Integer.toString(m_contNumeroOrganizadores);
                numeroOrg.appendChild(doc.createTextNode(numeroOrganizadores));

                
                // ----------------------- CP -----------------------------------//
                Element cp = doc.createElement("CP");
                evento.appendChild(cp);

                Element cpNumeroMembros = doc.createElement("NumeroMembrosCP");
                cp.appendChild(cpNumeroMembros);

                Element membroscp = doc.createElement("MembrosCP");
                cp.appendChild(membroscp);

                for (Revisor e : regEvento.getRevisores()) {
                    m_numeroMembroCP++;
                    
                    
                    Element rev = doc.createElement("MembroCP");
                    rev.appendChild(doc.createTextNode(e.getStrUsername()));
                    membroscp.appendChild(rev);
                }
                String numeroMembroCP = Integer.toString(m_numeroMembroCP);
                cpNumeroMembros.appendChild(doc.createTextNode(numeroMembroCP));
                
                
                //------------------------Lista Sessoes Tematicas-------------//
                Element listST = doc.createElement("ListaSessoesTematicas");
                evento.appendChild(listST);

                //FALTA NUMERO
                Element numeroST = doc.createElement("NumeroSessoesTematicas");
                listST.appendChild(numeroST);

                Element st = doc.createElement("SessoesTematicas");
                listST.appendChild(st);

                for (SessaoTematica ste : regEvento.getListaDeSessõesTemáticas().getListSessoes()) {
                    
                    m_contNumeroSessoesTematicas++;
                    Element sessaoST = doc.createElement("SessaoTematica");
                    st.appendChild(sessaoST);

                    Element codigoSt = doc.createElement("CodigoST");
                    codigoSt.appendChild(doc.createTextNode(ste.getCodigo()));
                    sessaoST.appendChild(codigoSt);

                    Element descricaoSt = doc.createElement("DescricaoST");
                    descricaoSt.appendChild(doc.createTextNode(ste.getM_descricao()));
                    sessaoST.appendChild(descricaoSt);

                    Element dataInSub = doc.createElement("DataInicioSubmissao");
                    dataInSub.appendChild(doc.createTextNode(fmt.format(ste.getDataInicioSubmissao())));
                    sessaoST.appendChild(dataInSub);

                    Element dataFinSub = doc.createElement("DataFimSubmissao");
                    dataFinSub.appendChild(doc.createTextNode(fmt.format(ste.getDataFimSubmissao())));
                    sessaoST.appendChild(dataFinSub);
                    
                    Element dataLimRevisao = doc.createElement("DataLimiteRevisao");
                    dataLimRevisao.appendChild(doc.createTextNode(fmt.format(ste.getM_strDataLimiteRevisao())));
                    sessaoST.appendChild(dataLimRevisao);

                    Element dataIniDis = doc.createElement("DataInicioDistribuicao");
                    dataIniDis.appendChild(doc.createTextNode(fmt.format(ste.getDataInicioSubmissao())));
                    sessaoST.appendChild(dataIniDis);


                    Element dataLimSubF = doc.createElement("DataInicioSubmissaoFinal");
                    dataLimSubF.appendChild(doc.createTextNode(fmt.format(ste.getM_strDataLimiteSubmissaoFinal())));
                    sessaoST.appendChild(dataLimSubF);

                    Element estadoST = doc.createElement("EstadoST");
                    estadoST.appendChild(doc.createTextNode("STStateEmRevisao"));
                    sessaoST.appendChild(estadoST);

                    Element listProponentes = doc.createElement("ListaProponentes");
                    sessaoST.appendChild(listProponentes);

                    Element numProponentes = doc.createElement("NumeroProponentes");
                    listProponentes.appendChild(numProponentes);

                    Element proponentes = doc.createElement("Proponentes");
                    listProponentes.appendChild(proponentes);

                    
                    for (Proponente prop : ste.getListaProponentes().getListProponentes()) {
                        m_contNumeroProponentes++;

                        Element proponente = doc.createElement("Proponente");
                        proponente.appendChild(doc.createTextNode(prop.getNome()));
                        listProponentes.appendChild(proponente);

                    }

                    
                    String numero1 = Integer.toString(m_contNumeroProponentes);
                    numProponentes.appendChild(doc.createTextNode(numero1));

                    String numero2 = Integer.toString(m_contNumeroSessoesTematicas);
                    numeroST.appendChild(doc.createTextNode(numero2));
                    //----------------------------------------- CP SESSAO -----------------------------------//
                    Element cpSessao = doc.createElement("CPSessao");
                    sessaoST.appendChild(cpSessao);

                    Element numeroMembrosCPST = doc.createElement("NumeroMembrosCPST");
                    cpSessao.appendChild(numeroMembrosCPST);

                    Element membrosCPSessao = doc.createElement("MembrosCPSessao");
                    cpSessao.appendChild(membrosCPSessao);

                    //Revisores inseridos na CP 
                    // ste --> SessaoTematica
                    
                    for (Revisor revisorMembro : ste.getRevisores()) {
                        m_contNumeroCPST++;
                        Element membroCPSessao = doc.createElement("MembroCPSessao");
                        membroCPSessao.appendChild(doc.createTextNode(revisorMembro.getStrUsername()));
                        membrosCPSessao.appendChild(membroCPSessao);
                    }
                    String numero3 = Integer.toString(m_contNumeroCPST);
                    numeroMembrosCPST.appendChild(doc.createTextNode(numero3));
                    //---------------------------------------------------- LISTA SUBMISSOES ----------------------------------//
                    Element listSubmissoes = doc.createElement("ListaSubmissoes");
                    sessaoST.appendChild(listSubmissoes);

                    Element numeroSubmissoes = doc.createElement("NumeroSubmissoes");
                    listSubmissoes.appendChild(numeroSubmissoes);

                    Element submissoes = doc.createElement("Submissoes");
                    numeroSubmissoes.appendChild(submissoes);

                    

                    for (Submissao sub : ste.getSubmissoes()) {
                        m_contNumeroSubmissoes++;

                        Element submissao = doc.createElement("Submissao");
                        listSubmissoes.appendChild(submissao);
                        
                        Element estadoSubmissao = doc.createElement("EstadoSubmissao");
                        estadoSubmissao.appendChild(doc.createTextNode("SubmissaoStateEmRevisao"));
                        submissao.appendChild(estadoSubmissao);

                        //  // /// //// //////////////////////////////////////////////////
                        Element artigoTipo = doc.createElement("Artigo");
                        submissao.appendChild(artigoTipo);
                        
                        

                        Element subAutorCorrespondente = doc.createElement("AutorCorrespondente");
                        subAutorCorrespondente.appendChild(doc.createTextNode(sub.getArtigo().getAutorCorrespondente().getStrUsername()));
                        artigoTipo.appendChild(subAutorCorrespondente);

                        Element subAutorSubmissao = doc.createElement("AutorSubmissao");
                        subAutorSubmissao.appendChild(doc.createTextNode(sub.getArtigo().getAutorCorrespondente().getStrUsername()));
                        artigoTipo.appendChild(subAutorSubmissao);

                        /// DATE
                        Date date = new Date();
                        Element dataSubmissao = doc.createElement("DataSubmissao");
                        dataSubmissao.appendChild(doc.createTextNode(fmt.format(date)));
                        artigoTipo.appendChild(dataSubmissao);

                        Element tituloArtigo = doc.createElement("TituloArtigo");
                        tituloArtigo.appendChild(doc.createTextNode(sub.getArtigo().getM_strTitulo()));
                        artigoTipo.appendChild(tituloArtigo);
                        
                        Element resumoArtigo = doc.createElement("Resumo");
                        resumoArtigo.appendChild(doc.createTextNode(sub.getArtigo().getM_strResumo()));
                        artigoTipo.appendChild(resumoArtigo);

                        Element listaAutores = doc.createElement("ListaAutores");
                        artigoTipo.appendChild(listaAutores);

                        Element numAutores = doc.createElement("NumeroAutores");
                        listaAutores.appendChild(numAutores);

                        Element autores = doc.createElement("Autores");
                        listaAutores.appendChild(autores);

                        
                         
                        

                        for (Autor autor : sub.getArtigo().getListaAutores().getM_listaAutores()) {
                            m_numeroAutores++;

                            Element autory = doc.createElement("Autor");
                            autores.appendChild(autory);

                            Element nomeAutor = doc.createElement("NomeAutor");
                            nomeAutor.appendChild(doc.createTextNode(autor.getM_strNome()));
                            autory.appendChild(nomeAutor);

                            Element emailAutor = doc.createElement("EmailAutor");
                            emailAutor.appendChild(doc.createTextNode(autor.getStrEmail()));
                            autory.appendChild(emailAutor);

                            Element filiacaoAutor = doc.createElement("Filiacao");
                            filiacaoAutor.appendChild(doc.createTextNode(autor.getM_strAfiliacao()));
                            autory.appendChild(filiacaoAutor);

                            Element usernameAutor = doc.createElement("UsernameAutor");
                            usernameAutor.appendChild(doc.createTextNode(autor.getStrUsername()));
                            autory.appendChild(usernameAutor);

                            

                        }
                        String numero4 = Integer.toString(m_numeroAutores);
                        numAutores.appendChild(doc.createTextNode(numero4));

                        Element palavrachave = doc.createElement("PalavrasChave");
                        palavrachave.appendChild(doc.createTextNode(sub.getArtigo().getM_strPalavrasChave()));
                        artigoTipo.appendChild(palavrachave);

                        Element ficheiro = doc.createElement("Ficheiro");
                        ficheiro.appendChild(doc.createTextNode(sub.getArtigo().getM_strFicheiro()));
                        artigoTipo.appendChild(ficheiro);

                        //------------------------------------- LISTA REVISOES ------------------------------------//
                        Element listRevisoes = doc.createElement("ListaRevisoes");
                        submissao.appendChild(listRevisoes);

                        Element numRevisoes = doc.createElement("NumeroRevisoes");
                        listRevisoes.appendChild(numRevisoes);

                        Element revisoes = doc.createElement("Revisoes");
                        listRevisoes.appendChild(revisoes);

                        for (Revisao r : ste.getListaRevisoes().getM_lstRevisoes()) {
                            m_contRevisoes++;

                            Element revisao = doc.createElement("Revisao");
                            listRevisoes.appendChild(revisao);

                            Element revisor = doc.createElement("Revisor");
                            revisor.appendChild(doc.createTextNode(r.getM_revisor().getStrUsername()));
                            revisao.appendChild(revisor);

                            Element estadoRevisao = doc.createElement("EstadoRevisao");
                            estadoRevisao.appendChild(doc.createTextNode("RevisaoStateConcluida"));
                            revisao.appendChild(estadoRevisao);

                            Element confianca = doc.createElement("Confianca");
                            confianca.appendChild(doc.createTextNode(r.getM_Confianca().toString()));
                            revisao.appendChild(confianca);

                            Element adequacao = doc.createElement("Adequacao");
                            adequacao.appendChild(doc.createTextNode(r.getM_Adequacao().toString()));
                            revisao.appendChild(adequacao);

                            Element originalidade = doc.createElement("Originalidade");
                            originalidade.appendChild(doc.createTextNode(r.getM_Originalidade().toString()));
                            revisao.appendChild(originalidade);

                            Element apresentacao = doc.createElement("Apresentacao");
                            apresentacao.appendChild(doc.createTextNode(r.getM_Apresentacao().toString()));
                            revisao.appendChild(apresentacao);

                            Element recomendacao = doc.createElement("Recomendacao");
                            recomendacao.appendChild(doc.createTextNode(r.getM_Recomendacao().toString()));
                            revisao.appendChild(recomendacao);

                            Element justificacao = doc.createElement("Justificacao");
                            justificacao.appendChild(doc.createTextNode(r.getM_Justificacao()));
                            revisao.appendChild(justificacao);

                        }
                        String numero5 = Integer.toString(m_contRevisoes);
                        numRevisoes.appendChild(doc.createTextNode(numero5));

                        Element decisao = doc.createElement("Decisao");
                        submissao.appendChild(decisao);
                     //------------------------------------------------------------------------------------------//
                    }
                    String numero8 = Integer.toString(m_contNumeroSubmissoes);
                    numeroSubmissoes.appendChild(doc.createTextNode(numero8));

                }
                String numero9 = Integer.toString(m_contNumeroSessoesTematicas);
                numeroST.appendChild(doc.createTextNode(numero9));

                Element listsubEvento = doc.createElement("ListaSubmissoesEvento");
                evento.appendChild(listsubEvento);

                Element decisao = doc.createElement("NumeroSubmissoes");
                listsubEvento.appendChild(decisao);

            }
            String numeroEventos = Integer.toString(m_numEventos);
            numEventos.appendChild(doc.createTextNode(numeroEventos));

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            doc.setXmlStandalone(true);
            DOMSource source = new DOMSource(doc);

            //Concatenação de Strings
            String xml = ".xml";
            String xmlFinal = pathficheiro + xml;
            StreamResult result = new StreamResult(new File(xmlFinal));

           
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("Ficheiro Guardado!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();

        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }
}
