/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import utils.Utils;

/**
 *
 * @author iazevedo
 */
public class ProcessoDecisaoEvento implements ProcessoDecisao {

    List<Decisao> m_listaDecisoes;
    MecanismoDecisao m_mecanismoDecisao;
    
    public ProcessoDecisaoEvento()
    {
        m_listaDecisoes = new ArrayList<>();
                
    }
    
    public void setMecanismoDecisao(MecanismoDecisao m) 
    {
        m_mecanismoDecisao = m;
    }

    public void decide() 
    {
        List<Decisao> ld= m_mecanismoDecisao.decide(this);
    }

    public Decisao novaDecisao() 
    {
        return new Decisao();
    }

    public boolean notifica() 
    {
              
        for( ListIterator<Decisao> it = m_listaDecisoes.listIterator(); it.hasNext(); )
        {
            Decisao d = it.next();
            d.notifica();
        }
        return true;
    }

    
    public void setListDecisoes(List<Decisao> ld) {
        m_listaDecisoes = ld;
    }

    @Override
    public List<Decisao> getDecisoes() 
    {
        return m_listaDecisoes ;   
    }

}
