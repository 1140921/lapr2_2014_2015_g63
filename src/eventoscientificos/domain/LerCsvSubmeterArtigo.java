/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import static eventoscientificos.domain.LerCsv.numeroLinhasFicheiro;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author 1140921
 */
public class LerCsvSubmeterArtigo {

    public int numeroLinhasFicheiro(String FILE_TXT) throws FileNotFoundException {
        int cont = 0;
        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");

        while (fInput.hasNextLine()) {
            String s = fInput.nextLine();
            if (s.length() != 0) {
                cont++;
            }
        }
        fInput.close();

        return cont;
    }

    public int numeroColunasFicheiro(String FILE_TXT) throws FileNotFoundException {
        int contColunas = 0;
//        String fileName = "C:\\Users\\Loghonox\\Desktop\\Ficheiro_UC18.csv";
        File file = new File(FILE_TXT);
        try (Scanner fInput = new Scanner(file)) {
            String s = fInput.next();

            if (s.length() != 0) {
                String[] temp = s.split(";");
                contColunas = temp.length;

            }
        }

        return contColunas;
    }

    public String[][] lercsv(String FILE_TXT) throws FileNotFoundException {

        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");
        int cont = 0;
        int numeroColunas = numeroColunasFicheiro(FILE_TXT);
        int numeroLinhas = numeroLinhasFicheiro(FILE_TXT);
        String[][] ficheiroCsv = new String[numeroLinhas][numeroColunas];
        while (fInput.hasNextLine()) {
            String s = fInput.nextLine();
            if (s.length() != 0) {
                String[] temp = s.split(";");
                for (int i = 0; i < numeroLinhas; i++) {
                    for (int j = 0; j < numeroColunas; j++) {
                        ficheiroCsv[i][j]= temp[cont];
//                        ficheiroCsv[i][j] = temp[j];                
                       

                    }
                     cont++;
            }
                }
//            }

        }
        fInput.close();

        return ficheiroCsv;
    }
}
