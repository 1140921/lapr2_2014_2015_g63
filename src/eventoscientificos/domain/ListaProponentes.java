

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;


public class ListaProponentes
{
    private List<Proponente> m_listaProponentes;
    
    public ListaProponentes()
    {
        m_listaProponentes = new ArrayList<Proponente>(); 
    }
    /**
     * 
     * @return lista de proponentes em formato List
     */
    public List<Proponente> getListProponentes(){
       return m_listaProponentes; 
    }
    
    public Proponente novoProponente(Utilizador u)
    {
       if (u== null)
           return null;
       
       Proponente p = new Proponente(u);
        
       if( p.valida() && validaProponente(p) )
            return p;
        else
            return null;
    }
    
    public boolean registaProponente(Proponente prop)
    {
       
      
         if(prop.valida() && validaProponente(prop)){   
            return addProponente(prop);
         }else
            return false;
    }
    
    
    private boolean addProponente(Proponente p)
    {
        return m_listaProponentes.add(p);
    }
    /**
     * 
     * @param p
     * @return 
     */
    public boolean validaProponente(Proponente p){
        if(this.hasProponente(p.getNome())){
            throw new IllegalArgumentException("Proponente ja existe");
        }
        return true;
    }

    public boolean hasProponente(String strID)
    {
        for(Proponente prop:this.m_listaProponentes)
        {
            if (prop.getNome().equals(strID))
                return true;
        }
        return false;
    }
}
