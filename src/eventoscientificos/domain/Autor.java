/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Autor implements Comparable<Autor> {

    private String m_strNome;
    private String m_strUsername;
    private String m_strEmail;
    private String m_strAfiliacao;

    public Autor() {

    }

    public Autor(String nome) {

        setUsername(nome);
//        setAfiliacao(m_strAfiliacao);
    }

    public void setNome(String strNome) {
        this.m_strNome = strNome;
        if (this.getM_strNome() == null || this.getM_strNome().trim().isEmpty()) {
            throw new IllegalArgumentException("Nome inválido");
        }
    }
    
    public void setUsername(String strUsername){
        this.m_strUsername=strUsername;
    }
    

    public void setAfiliacao(String strAfiliacao) {
        this.m_strAfiliacao = strAfiliacao;
        if (this.getM_strAfiliacao() == null || this.getM_strAfiliacao().trim().isEmpty()) {
            throw new IllegalArgumentException("Afiliação inválida");
        }
    }

    public boolean valida() {

        return true;

    }

    boolean podeSerCorrespondente() {
        return true;
    }

    @Override
    public String toString() {
        return "Nome : " + this.getM_strNome()+"\nEmail : "+m_strEmail + "\nAfiliação : " + this.getM_strAfiliacao();
    }

    @Override
    public int compareTo(Autor outroAutor) {
        return this.getM_strNome().compareTo(outroAutor.getM_strNome());
    }

    /**
     * @return the m_strNome
     */
    public String getM_strNome() {
        return m_strNome;
    }

    /**
     * @return the m_strEmail
     */
    public String getStrEmail() {
        return m_strEmail;
    }
    
    public String getStrUsername(){
        return m_strUsername;
    }

    /**
     * @param m_strEmail the m_strEmail to set
     */
    public void setStrEmail(String m_strEmail) {
        this.m_strEmail = m_strEmail;

       
        if (m_strEmail == null || m_strEmail.trim().isEmpty()) {
            throw new IllegalArgumentException("Não introduziu nenhum Email");
        } 
    }

    /**
     * @return the m_strAfiliacao
     */
    public String getM_strAfiliacao() {
        return m_strAfiliacao;
    }

}
