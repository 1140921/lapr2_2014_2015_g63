/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.List;

/**
 *
 * @author iazevedo
 */
public interface Licitavel {
    
	ProcessoLicitacao iniciaDetecao();

	List<Revisor> getRevisores();

	List<Submissao> getSubmissoes();
	List<Submissao> getSubmissoesFinal();

	void setProcessoLicitacao(ProcessoLicitacao pl);

	ProcessoLicitacao getProcessoLicitacao();

        boolean hasRevisor(String idRev);
}
