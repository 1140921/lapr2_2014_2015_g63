/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author iazevedo
 */
public class ProcessoLicitacaoST implements ProcessoLicitacao {

    List<Licitacao> m_lstLicitacao;

    @Override
    public Licitacao novaLicitação(Revisor revisor, Submissao s) {
          Licitacao l = new Licitacao();
          l.setRevisor(revisor);
          l.setSubmissao(s);

        return l;
    }

    @Override
    public boolean addLicitacao(Licitacao l) {
        m_lstLicitacao.add(l);
        return true;

    }

    @Override
    public boolean valida() {
        throw new UnsupportedOperationException();
    }

    /**
     * Metodo responsavel por Retornar a Lista de Licitacoes do Utilizador
     * (Revisor) correspondente
     *
     * @param u Utilizador (revisor)
     * @return lista de Licitacoes da ST
     */
    @Override
    public List<Licitacao> getLicitacoesUtilizador(Utilizador u) {
        List<Licitacao> ls = new ArrayList<>();
        for (Licitacao lici : m_lstLicitacao) {
            if (lici.getRevisor().getUtilizador().equals(u)) {
                ls.add(lici);
            }
        }
        return ls;
    }

    public boolean validaLicitacao(Licitacao l) {

        for (Licitacao licitacao : m_lstLicitacao) {
            if (licitacao.equals(l)) {
                return false;
            }
        }
        return true;

    }

    @Override
    public void registaLicitacao(Licitacao l) {
        if (l.valida() && validaLicitacao(l)) {
            m_lstLicitacao.add(l);
        }

    }


}
