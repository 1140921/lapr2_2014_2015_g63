package eventoscientificos.domain;

import java.util.List;

public class GerarEstatisticasTopicos {

    private Empresa m_empresa;
    private String[] m_palavrasChave;
    private Artigo m_artigo;
    private String palavraChaveMaisFrequenteSessao;
    private String palavraChaveMaisFrequenteEvento;

    public GerarEstatisticasTopicos(Empresa empresa) {

        this.m_empresa = empresa;

    }

    public List<Evento> getListaEventos() {
        return m_empresa.getRegistoEventos().getListEventos();

    }

    public List<Submissao> getListaSubmissoesEvento() {
        return getListaEventos().get(0).getSubmissoes();
    }

    public Artigo getArtigoSubmissaoEvento() {
        return getListaSubmissoesEvento().get(0).getArtigo();
    }

    public List<SessaoTematica> getListaEventosSessoes() {
        return getListaEventos().get(0).getListaDeSessõesTemáticas().getListSessoes();
    }

    public List<Submissao> getListaSubmissoesSessao() {
        return getListaEventosSessoes().get(0).getSubmissoes();
    }

    public Artigo getArtigoSubmissaoSessao() {
        return getListaSubmissoesSessao().get(0).getArtigo();
    }

    public int numeroPalavrasChaveEvento() {
        for (int i = 0; i < getListaSubmissoesEvento().size(); i++) {
            m_artigo = getListaSubmissoesEvento().get(i).getArtigo();
            m_palavrasChave = m_artigo.getM_strPalavrasChave().split(";");

        }

        return m_palavrasChave.length;
    }

    public int numeroPalavrasChaveSessao() {
        for (int i = 0; i < getListaSubmissoesSessao().size(); i++) {
            m_artigo = getListaSubmissoesSessao().get(i).getArtigo();
            m_palavrasChave = m_artigo.getM_strPalavrasChave().split(";");

        }

        return m_palavrasChave.length;
    }

    public int numeroPalavrasChaveIguaisSessao() {
        int cont = 0;

        for (int i = 0; i < getListaSubmissoesSessao().size(); i++) {
            m_artigo = getListaSubmissoesSessao().get(i).getArtigo();
            m_palavrasChave = m_artigo.getM_strPalavrasChave().split(";");

        }

        int array[] = new int[m_palavrasChave.length];
        for (int i = 0; i < m_palavrasChave.length - 1; i++) {
            for (int j = 1; j < m_palavrasChave.length; j++) {

                if (m_palavrasChave[i].equals(m_palavrasChave[j])) {
                    cont++;
                }
            }
            array[i] = cont;
        }
        int maior = array[0];
        int k = 0;
        for (int j = 1; j < array.length; j++) {
            if (maior > array[j]) {
                maior = maior;

            } else {
                maior = array[j];
                k = j;
            }
        }
        int numeroMaiorPalavraSessao = array[k];
        return numeroMaiorPalavraSessao;
    }

    public int numeroPalavrasChaveIguaisEvento() {
        int cont = 0;

        for (int i = 0; i < getListaSubmissoesEvento().size(); i++) {
            m_artigo = getListaSubmissoesEvento().get(i).getArtigo();
            m_palavrasChave = m_artigo.getM_strPalavrasChave().split(";");

        }

        int array[] = new int[m_palavrasChave.length];

        for (int i = 0; i < m_palavrasChave.length - 1; i++) {
            for (int j = 1; j < m_palavrasChave.length; j++) {

                if (m_palavrasChave[i].equals(m_palavrasChave[j])) {
                    cont++;
                }
            }
            array[i] = cont;
        }
        int maior = array[0];
        int k = 0;
        for (int j = 1; j < array.length; j++) {
            if (maior > array[j]) {
                maior = maior;

            } else {
                maior = array[j];
                k = j;
            }
        }
        int numeroMaiorPalavraEvento = array[k];
        return numeroMaiorPalavraEvento;
    }

    public String FrequenciaPalavrasChaveEvento() {
        int cont = 0;

        for (int i = 0; i < getListaSubmissoesEvento().size(); i++) {
            m_artigo = getListaSubmissoesEvento().get(i).getArtigo();
            m_palavrasChave = m_artigo.getM_strPalavrasChave().split(";");

        }

        int array[] = new int[m_palavrasChave.length];

        for (int i = 0; i < m_palavrasChave.length - 1; i++) {
            for (int j = 1; j < m_palavrasChave.length; j++) {

                if (m_palavrasChave[i].equals(m_palavrasChave[j])) {
                    cont++;
                }
            }
            array[i] = cont;
        }
        int maior = array[0];
        int k = 0;
        for (int j = 1; j < array.length; j++) {
            if (maior > array[j]) {
                maior = maior;

            } else {
                maior = array[j];
                k = j;
            }
        }
        palavraChaveMaisFrequenteEvento = m_palavrasChave[k];

        return palavraChaveMaisFrequenteEvento;
    }

    public String FrequenciaPalavrasChaveSessao() {

        int cont = 0;

        for (int i = 0; i < getListaSubmissoesSessao().size(); i++) {
            m_artigo = getListaSubmissoesSessao().get(i).getArtigo();
            m_palavrasChave = m_artigo.getM_strPalavrasChave().split(";");

        }

        int array[] = new int[m_palavrasChave.length];
        for (int i = 0; i < m_palavrasChave.length - 1; i++) {
            for (int j = 1; j < m_palavrasChave.length; j++) {

                if (m_palavrasChave[i].equals(m_palavrasChave[j])) {
                    cont++;
                }
            }
            array[i] = cont;
        }
        int maior = array[0];
        int k = 0;
        for (int j = 1; j < array.length; j++) {
            if (maior > array[j]) {
                maior = maior;

            } else {
                maior = array[j];
                k = j;
            }
        }
        palavraChaveMaisFrequenteSessao = m_palavrasChave[k];

        return palavraChaveMaisFrequenteSessao;
    }

    public float percentagemPalavrasChaveIguaisEvento() {
        return numeroPalavrasChaveIguaisEvento() / numeroPalavrasChaveEvento();
    }

    public float percentagemPalavrasChaveIguaisSessao() {
        return numeroPalavrasChaveIguaisSessao() / numeroPalavrasChaveSessao();
    }
}
