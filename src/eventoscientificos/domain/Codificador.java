
package eventoscientificos.domain;

import java.util.Arrays;

public class Codificador {

    public static double codificar(String[][] m, String s,int coluna) {
        
        String[] charaters = new String[s.length()];

        for (int i = 0; i < charaters.length; i++) {
            charaters[i] = Character.toString(s.charAt(i));
        }
        
        String letrasNaoRepetidas[]=charatersNaoRepetidos(charaters);
        
        letrasNaoRepetidas=ordenaArray(letrasNaoRepetidas);
        
        double[]probabilidades=probabilidades(letrasNaoRepetidas, m,coluna);
        

        double[] intervalo = new double[probabilidades.length+1];
         intervalo[0]=0;
        
         //preencher intervalo
         for (int z = 1; z < intervalo.length; z++){
            //intervalo[z] = probabilidades[z-1]+intervalo[z-1];
            intervalo[z]=probabilidades[z-1];
         }
         
        int indice;
        
        for(int i=0;i<charaters.length;i++){
            indice=determinarIndice(charaters[i],letrasNaoRepetidas);  
               
            intervalo=determinarNovoIntervalo(intervalo,intervalo[indice],
                    intervalo[indice+1],probabilidades);
         
        }
    
    return intervalo[0];
    }
    
    public static double[] determinarNovoIntervalo(double[]intervalo, double primeiro,
            double fin,double probabilidades[]){
       
        intervalo[0]=primeiro;
        intervalo[intervalo.length-1]=fin;
        
        int cont=0;
        for(int i=1;i<intervalo.length-1;i++){
           intervalo[i]=(probabilidades[cont]*amplitude(intervalo))+primeiro;
        cont++;
        }
    return intervalo;
    }
    
    
    
    
    
    public static int determinarIndice(String letra,String []letrasNaoRepetidas){
        int indice=-1;   
        
        for(int i =0;i<letrasNaoRepetidas.length;i++){
             if(letra.equals(letrasNaoRepetidas[i])){
              indice=i;
             }
        }
        return indice;
    }
    
    
    
      public static String[] ordenaArray(String[] arrayLetras){  
          
        
        String aux = "";  
          
        for (int i = 0; i < arrayLetras.length; i++) {  
              
            for (int j = 0; j < arrayLetras.length; j++) {  
                String letra1 = arrayLetras[i];  
                String letra2 = arrayLetras[j];  
                  
      
                if(letra2.charAt(0) > letra1.charAt(0)){  
                    //utiliza a variável auxiliar e trocar as letras de posição no array.  
                    aux = letra1;   
                    arrayLetras[i] = letra2;  
                    arrayLetras[j] = aux;  
                  
                }  
            }  
      
        }  
  
        return arrayLetras;  
    }  

    
    

    public static double amplitude(double[] intervalo) {
        double amplitude = intervalo[intervalo.length-1]- intervalo[0];
        return amplitude;
    }

        public static String[] charatersNaoRepetidos( String charaters[] ) {
        // remover repetidos
        String[] unicos = new String[ charaters.length ];
        int qtd = 0;
        
        for( int i = 0 ; i < charaters.length ; i++ ) {
            boolean existe = false;
            for( int j = 0 ; j < qtd ; j++ ) {
                if( unicos[j].equals(charaters[ i ])) {
                    existe = true;
                    break;
                }
            }
            if( !existe ) {
                unicos[qtd++]=charaters[i];
            }
        }

        // ajuste do tamanho do vetor resultante
        unicos = Arrays.copyOf(unicos,qtd);

      
return unicos;
    }
    
        
        public static double[] probabilidades(String letrasNaoRepetidas[],String [][]m,int coluna){
                    
            double[] probabilidades = new double[letrasNaoRepetidas.length];
        
        int cont = 0;

        for (int f = 0; f < letrasNaoRepetidas.length; f++) {
            for (int j = 0; j < m.length; j++) {
                if (letrasNaoRepetidas[f].equals(m[j][0])) {
                    probabilidades[cont] = Double.parseDouble(m[j][coluna].replace(",", "."));
                    cont++;
                }
            }
        }
        
        return probabilidadesSomadas(probabilidades);
        }
        
        public static double[] probabilidadesSomadas(double probabilidades[]){
            for(int i =1;i<probabilidades.length;i++){
                probabilidades[i]=probabilidades[i]+probabilidades[i-1];
            }
            return probabilidades;
        }

}
