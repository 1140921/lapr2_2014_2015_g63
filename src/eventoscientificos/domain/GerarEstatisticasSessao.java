
package eventoscientificos.domain;


import eventoscientificos.controllers.DefinirCPController;
import java.util.ArrayList;
import java.util.List;

public class GerarEstatisticasSessao {

    private Empresa m_empresa;
    public Revisao m_revisao;
    public ListaRevisoes m_listaRevisoes;
    public List<Revisor> lstRevisores;
    
   

    public GerarEstatisticasSessao(Empresa empresa) {
        this.m_empresa = empresa;
    }
    
    public List<SessaoTematica> getListaSessoes(int s){
 
        return m_empresa.getRegistoEventos().getListEventos().get(s).getListaDeSessõesTemáticas().getListSessoes();
    }
    
    public List<Revisor> getListaRevisoresST(int indexEvento, int indexSessao){
        List<Revisor> lstRevisoresST = getListaSessoes(indexEvento).get(indexSessao).getRevisores();
    
        return lstRevisoresST;
    }
    
    public int getAdequacao(){
        return m_revisao.getM_Adequacao();
    }
    
    public int getConfianca(){
        return m_revisao.getM_Confianca();
    }
    
    public int getOriginalidade(){
        return m_revisao.getM_Originalidade();
    }
    
    public int getApresentacao(){
        return m_revisao.getM_Apresentacao();
    }
    
    public List<Revisao> getListaRevisoes(int indexEvento, int indexSessao, int indexRevisor){
         List<Revisao> lstRevisoes = getListaRevisoresST(indexEvento, indexSessao).get(indexRevisor).getLstRevisoes().getRevisoesRevisor(getListaRevisoresST(indexEvento, indexSessao).get(indexRevisor).getStrUsername());
         return lstRevisoes;
    }
    
    public int somatorioAtributosClassificacoes(int indexEvento, int indexSessao, int indexRevisor){
     
        //Alterei aqui !!
        // utilizei a revisao do for para ir buscar as classificações pq essa revisao vai ser a revisao selecionada na combo box da lista de Revisoes
        int soma = 0;
        for(Revisao revisao : getListaRevisoes(indexEvento, indexSessao, indexRevisor)){
            soma += revisao.getM_Adequacao()+revisao.getM_Apresentacao()+revisao.getM_Confianca()+revisao.getM_Originalidade();
        }
        
        return soma;
    }
    
    public float[] ClassificacaoFinalDeCadaRevisao(int indexEvento, int indexSessao, int indexRevisor){
        
        float[] RevisoesContainer = new float[getListaRevisoes(indexEvento, indexSessao, indexRevisor).size()];
        int numeroClassificacoes = 4;
        for(int i = 0; i<getListaRevisoes(indexEvento, indexSessao, indexRevisor).size(); i++){
                m_revisao=getListaRevisoes(indexEvento, indexSessao, indexRevisor).get(i);
                RevisoesContainer[i] = (m_revisao.getM_Adequacao()+m_revisao.getM_Apresentacao()+m_revisao.getM_Confianca()+m_revisao.getM_Originalidade())/numeroClassificacoes;
            }
        
        return RevisoesContainer;
    }
    
    public float mediaDoSomatorioClassificacoes(int indexEvento, int indexSessao, int indexRevisor){
        if(getListaRevisoes(indexEvento, indexSessao, indexRevisor).size()==0){
        throw new IllegalArgumentException("Impossível fazer divisão por 0."
                + "Este Revisor não fez nenhuma Revisão");
        }
        return (somatorioAtributosClassificacoes(indexEvento, indexSessao, indexRevisor))/getListaRevisoes(indexEvento, indexSessao, indexRevisor).size();
    } 
    
    public float[] CalculoParaCadaRevisao(int indexEvento, int indexSessao, int indexRevisor){
        
        float[] Di = new float[getListaRevisoes(indexEvento, indexSessao, indexRevisor).size()];
        float[] Xi = ClassificacaoFinalDeCadaRevisao(indexEvento, indexSessao, indexRevisor);
        float Xt = mediaDoSomatorioClassificacoes(indexEvento, indexSessao, indexRevisor);
        for(int i = 0; i<getListaRevisoes(indexEvento, indexSessao, indexRevisor).size(); i++){
           Di[i] = Math.abs(Xi[i] - Xt);
        }
        
        return Di;
    }
    
    public boolean ValidaTesteHipoteses(int indexEvento, int indexSessao, int indexRevisor){
        if(getListaRevisoes(indexEvento, indexSessao, indexRevisor).size() < 30){
            throw new IllegalArgumentException("Amostra menor que 30");
        }
        return true;
    }
    
    public boolean TesteHipoteses(int indexEvento, int indexSessao, int indexRevisor){
        float Zc =  1.645f;
        float Zo = zObservado(indexEvento, indexSessao, indexRevisor);
        ValidaTesteHipoteses(indexEvento, indexSessao, indexRevisor);
            if(Zo > Zc){
                return false;
            }else
                return true;
        
    }
    
    public float SomatorioDi(int indexEvento, int indexSessao, int indexRevisor){
        float[] Di = CalculoParaCadaRevisao(indexEvento, indexSessao, indexRevisor);
        float soma = 0;
        for(int i = 0; i<getListaRevisoes(indexEvento, indexSessao, indexRevisor).size(); i++){
            soma += Di[i];
        }
        return soma;
    }
    public float MediaDi(int indexEvento, int indexSessao, int indexRevisor){

        float soma = SomatorioDi(indexEvento, indexSessao, indexRevisor);
        float media;

        if(getListaRevisoes(indexEvento, indexSessao, indexRevisor).size() == 0){
        throw new IllegalArgumentException("Impossível fazer divisão por 0."
                + "Este Revisor não fez nenhuma Revisão");
        }
        media = soma / getListaRevisoes(indexEvento, indexSessao, indexRevisor).size();
        
        return media;
        
    }
    
    public float SomatorioVariancia(int indexEvento, int indexSessao, int indexRevisor){
        float soma =0;
        float[] Di = CalculoParaCadaRevisao(indexEvento, indexSessao, indexRevisor);
        
        float media = MediaDi(indexEvento, indexSessao, indexRevisor);
        
        for(int i= 0; i<getListaRevisoes(indexEvento, indexSessao, indexRevisor).size();i++){
            soma+= Math.pow(Di[i]- media, 2);
        }
        return soma;
        
        
    }
    public float Variancia(int indexEvento, int indexSessao, int indexRevisor){
        float soma = SomatorioVariancia(indexEvento, indexSessao, indexRevisor);
        float variancia;
        if((getListaRevisoes(indexEvento, indexSessao, indexRevisor).size()-1)==0){
        throw new IllegalArgumentException("Impossível fazer divisão por 0."
                + "Este Revisor fez apenas 1 Revisão.");
        }
        variancia = (1/(getListaRevisoes(indexEvento, indexSessao, indexRevisor).size()-1))*soma;
        
        return variancia;
        
    }
    
    public float DesvioPadrao(int indexEvento, int indexSessao, int indexRevisor){
        float variancia = Variancia(indexEvento, indexSessao, indexRevisor);
        float desvioPadrao;
        
        desvioPadrao = (float) Math.sqrt(variancia);
        
        return desvioPadrao;
    }
    
    public float zObservado(int indexEvento, int indexSessao, int indexRevisor){
        float Zo;
        float mediaDi = MediaDi(indexEvento, indexSessao, indexRevisor);
        float desvioPadrao = DesvioPadrao(indexEvento, indexSessao, indexRevisor);
        if(desvioPadrao == 0 || (Math.sqrt(getListaRevisoes(indexEvento, indexSessao, indexRevisor).size()))==0){
        throw new IllegalArgumentException("Impossível fazer divisão por 0."
                + "O valor do Desvio Padrão é 0.");
        }
         return (float) ((mediaDi - 1)/(desvioPadrao/(Math.sqrt(getListaRevisoes(indexEvento, indexSessao, indexRevisor).size()))));
        
         
    }

}
