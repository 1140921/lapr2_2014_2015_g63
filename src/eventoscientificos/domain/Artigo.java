/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Artigo {

    private String m_strTitulo;
    private String m_strResumo;
    private String m_strPalavrasChave;
    private ListaAutores m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_strFicheiro;
    private List<Autor> m_lstAutores;


    public Artigo() {
        this.m_listaAutores = new ListaAutores();
        this.m_lstAutores = new ArrayList<Autor>();
      
        
    }

    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
        if (this.m_strTitulo == null || this.m_strTitulo.trim().isEmpty()) {
            throw new IllegalArgumentException("Titulo não inserido");
        }
    }

    public void setResumo(String strResumo) {
        this.m_strResumo = strResumo;
        if (this.m_strResumo == null || this.m_strResumo.trim().isEmpty()) {
            throw new IllegalArgumentException("Resumo não inserido");
        }
    }
    
    public void setPalavrasChave (String m_strPalavrasChave){
        this.m_strPalavrasChave=m_strPalavrasChave;
       String palavrasChave [] = this.getM_strPalavrasChave().split(";");
       
        if(this.getM_strPalavrasChave() == null || this.getM_strPalavrasChave().trim().isEmpty() ){
            throw new IllegalArgumentException("É necessario inserir pelo menos uma palavra chave");
            
        }
        else if (palavrasChave.length>5){
             throw new IllegalArgumentException("Apenas 5 palvras chaves sao necessarias");
        }
    }
    

    public ListaAutores getListaAutores() {
        return this.m_listaAutores;
    }

    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.m_listaAutores.getPossiveisAutoresCorrespondentes();
    }

    public void setAutorCorrespondente(Autor autor) {
        this.m_autorCorrespondente = autor;
    }

    public Autor getAutorCorrespondente() {
        return this.m_autorCorrespondente;
    }

    public void setFicheiro(String strFicheiro) {
        this.m_strFicheiro = strFicheiro;
    }

    public String getInfo() {
        
        return toString()+"\nAutores : " + mostrarAutores() ;
    }

    public boolean valida() {
        System.out.println("Artigo:valida");
        return true;
    }

    /**
     * @return the m_strTitulo
     */
    public String getM_strTitulo() {
        return m_strTitulo;
    }

    /**
     * @return the m_strResumo
     */
    public String getM_strResumo() {
        return m_strResumo;
    }

    /**
     * @return the m_strFicheiro
     */
    public String getM_strFicheiro() {
        return m_strFicheiro;
    }

    @Override
    public String toString() {
        return "Titulo :" + this.m_strTitulo + "\nResumo : " + this.m_strResumo + "\nAutor Correspondente : " + this.m_autorCorrespondente.getStrUsername() + "\nFicheiro" + this.m_strFicheiro;
    }
    
   
    /**
     *
     * @return a lista de Autores
     */
    public String mostrarAutores() {

        String listAutores = "";

        for (Autor autor : m_listaAutores.getPossiveisAutoresCorrespondentes()) {
            listAutores += autor + ";";
        }
        return listAutores;

    }
    
        public Autor getAutorByUsername(String id){
        for(Autor a : getPossiveisAutoresCorrespondentes()){
           if( a.getM_strNome().equals(id)){
               return a;
           }
        }
        
        return null;
    }

    /**
     * @return the m_strPalavrasChave
     */
    public String getM_strPalavrasChave() {
        return m_strPalavrasChave;
    }
        
        
        

}
