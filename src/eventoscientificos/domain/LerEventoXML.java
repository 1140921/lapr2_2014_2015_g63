/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author 1140921
 */
import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.controllers.SubmeterArtigoController;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class LerEventoXML {
    
    private String caminho;
    private Empresa m_empresa;
    private RegistoEventos m_regEvento;
    private RegistoUtilizadores m_regUtilizadores;
    private Revisao m_revisao;
    private ListaRevisoes m_listaRevisoes;
    
    public LerEventoXML(String ficheiroEvento, Empresa empresa, String ficheiroLocal) {
        this.caminho = ficheiroEvento;
        m_regEvento = empresa.getRegistoEventos();
        m_regUtilizadores = empresa.getRegistoUtilizadores();
        m_empresa = empresa;
        
        CriarEventoController criarEvento = new CriarEventoController(m_empresa);
        DefinirCPController definirCP = new DefinirCPController(m_empresa);
        CriarSessaoTematicaController criarST = new CriarSessaoTematicaController(m_empresa);
        SubmeterArtigoController submeterArtigo = new SubmeterArtigoController(m_empresa);
        ReverArtigoController reverArtigo = new ReverArtigoController(m_empresa);
        try {
            
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            
            DefaultHandler handler = new DefaultHandler() {
                
                boolean numeroEventos = false;
                boolean evento = false;
                boolean titulo = false;
                boolean descricao = false;
                boolean dataInicio = false;
                boolean dataFim = false;
                boolean dataInicioSubmissao = false;
                boolean dataFimSubmissao = false;
                boolean dataInicioDistribuicao = false;
                boolean dataLimiteRevisao = false;
                boolean dataLmiteSubmissaoFinal = false;
                boolean estadoEvento = false;
                boolean local = false;
                boolean listaOrganizadores = false;
                boolean numeroOrganizadores = false;
                boolean organizadores = false;
                boolean organizador = false;
                boolean cp = false;
                boolean numeroMembrosCP = false;
                boolean membrosCP = false;
                boolean membroCP = false;
                boolean listaSessoesTematicas = false;
                boolean numeroSessoesTematicas = false;
                boolean sessaoTematicas = false;
                boolean codigoST = false;
                boolean descricaoST = false;
                boolean estadoST = false;
                boolean numeroProponentes = false;
                boolean proponente = false;
                boolean cpSessao = false;
                
                boolean numeroMembrosCPST = false;
                boolean membroCPSessao = false;
                boolean numeroSubmissoes = false;
                boolean estadoSubmissao = false;
                boolean artigoTipo = false;
                boolean autorCorrespondente = false;
                boolean autorSubmissao = false;
                boolean dataSubmissao = false;
                boolean tituloArtigo = false;
                boolean resumo = false;
                boolean autor = false;
                boolean emailAutor = false;
                boolean filiacao = false;
                boolean usernameAutor = false;
                boolean palavrasChave = false;
                boolean ficheiro = false;
                boolean numeroRevisoes = false;
                boolean revisao = false;
                boolean revisor = false;
                boolean estadoRevisao = false;
                boolean confianca = false;
                boolean adequacao = false;
                boolean originalidade = false;
                boolean apresentacao = false;
                boolean recomendacao = false;
                boolean justificacao = false;
                boolean nomeAutor = false;
                boolean submissao = false;
                boolean listaSubmissoesEvento = false;
                Date dInicioSubmissao = new Date();
                Date dFimSubmissao = new Date();
                Date dInicioDistribuicao = new Date();
                Date dLimiteRevisao = new Date();
                Date dLmiteSubmissaoFinal = new Date();
                String codigo = "";
                int i = 0;
                int s = 0;
                int cont = 0;
                Evento e;
                SessaoTematica st;
                CPEvento cP;
                CPSessaoTematica cPST;
                String tituloA = "";
                String nomeDeAutor = "";
                String username = "";
                String mailAutor = "";
                String afiliacao = "";
                String resumoA = "";
                Writer writer = null;
                Artigo artigo;
                String m_decisao;
                String m_Justificacao;
                Integer m_Confianca;
                Integer m_Adequacao;
                Integer m_Originalidade;
                Integer m_Apresentacao;
                Integer m_Recomendacao;
                Revisao m_revisao;
                ListaRevisoes m_listaRevisoes;
                Revisor revisorA;
                
                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {
                    
                    if (qName.equalsIgnoreCase("NUMEROEVENTOS")) {
                        numeroEventos = true;
                    }
                    if (qName.equalsIgnoreCase("evento")) {
                        evento = true;
                    }
                    
                    if (qName.equalsIgnoreCase("TITULO")) {
                        titulo = true;
                    }
                    
                    if (qName.equalsIgnoreCase("DESCRICAO")) {
                        descricao = true;
                    }
                    
                    if (qName.equalsIgnoreCase("DATAINICIO")) {
                        dataInicio = true;
                    }
                    
                    if (qName.equalsIgnoreCase("DATAFIM")) {
                        dataFim = true;
                    }
                    
                    if (qName.equalsIgnoreCase("DATAINICIOSUBMISSAO")) {
                        dataInicioSubmissao = true;
                    }
                    if (qName.equalsIgnoreCase("DATAFIMSUBMISSAO")) {
                        dataFimSubmissao = true;
                    }
                    if (qName.equalsIgnoreCase("DATAINICIODISTRIBUICAO")) {
                        dataInicioDistribuicao = true;
                    }
                    if (qName.equalsIgnoreCase("DATALIMITEREVISAO")) {
                        dataLimiteRevisao = true;
                    }
                    if (qName.equalsIgnoreCase("DataLmiteSubmissaoFinal")) {
                        dataLmiteSubmissaoFinal = true;
                    }
                    if (qName.equalsIgnoreCase("EstadoEvento")) {
                        estadoEvento = true;
                    }
                    
                    if (qName.equalsIgnoreCase("Local")) {
                        local = true;
                    }
                    if (qName.equalsIgnoreCase("ListaOrganizadores")) {
                        listaOrganizadores = true;
                    }
                    if (qName.equalsIgnoreCase("NumeroOrganizadores")) {
                        numeroOrganizadores = true;
                    }
                    if (qName.equalsIgnoreCase("Organizadores")) {
                        organizadores = true;
                    }
                    if (qName.equalsIgnoreCase("Organizador")) {
                        organizador = true;
                    }
                    if (qName.equalsIgnoreCase("CP")) {
                        cp = true;
                    }
                    if (qName.equalsIgnoreCase("NumeroMembrosCP")) {
                        numeroMembrosCP = true;
                    }
                    if (qName.equalsIgnoreCase("MembrosCP")) {
                        membrosCP = true;
                    }
                    if (qName.equalsIgnoreCase("MembroCP")) {
                        membroCP = true;
                    }
                    if (qName.equalsIgnoreCase("ListaSessoesTematicas")) {
                        listaSessoesTematicas = true;
                    }
                    if (qName.equalsIgnoreCase("ListaSubmissoesEvento")) {
                        listaSubmissoesEvento = true;
                    }
                    if (qName.equalsIgnoreCase("NumeroSessoesTematicas")) {
                        numeroSessoesTematicas = true;
                    }
                    if (qName.equalsIgnoreCase("SessaoTematica")) {
                        sessaoTematicas = true;
                    }
                    if (qName.equalsIgnoreCase("CodigoST")) {
                        codigoST = true;
                    }
                    if (qName.equalsIgnoreCase("DescricaoST")) {
                        descricaoST = true;
                    }
                    if (qName.equalsIgnoreCase("EstadoST")) {
                        estadoST = true;
                    }
                    if (qName.equalsIgnoreCase("NumeroProponentes")) {
                        numeroProponentes = true;
                    }
                    if (qName.equalsIgnoreCase("Proponente")) {
                        proponente = true;
                    }
                    if (qName.equalsIgnoreCase("CPSESSAO")) {
                        cpSessao = true;
                    }
                    
                    if (qName.equalsIgnoreCase("NumeroMembrosCPST")) {
                        numeroMembrosCPST = true;
                    }
                    if (qName.equalsIgnoreCase("MembroCPSessao")) {
                        membroCPSessao = true;
                    }
                    if (qName.equalsIgnoreCase("NumeroSubmissoes")) {
                        numeroSubmissoes = true;
                    }
                    if (qName.equalsIgnoreCase("Submissao")) {
                        submissao = true;
                    }
                    if (qName.equalsIgnoreCase("Artigo tipo = ")) {
//                         
                    }
                    
                    if (qName.equalsIgnoreCase("EstadoSubmissao")) {
                        estadoSubmissao = true;
                    }
                    if (qName.equalsIgnoreCase("AutorCorrespondente")) {
                        autorCorrespondente = true;
                    }
                    
                    if (qName.equalsIgnoreCase("AutorSubmissao")) {
                        autorSubmissao = true;
                    }
                    if (qName.equalsIgnoreCase("DataSubmissao")) {
                        dataSubmissao = true;
                    }
                    if (qName.equalsIgnoreCase("TituloArtigo")) {
                        tituloArtigo = true;
                    }
                    if (qName.equalsIgnoreCase("Resumo")) {
                        resumo = true;
                    }
                    
                    if (qName.equalsIgnoreCase("Autor")) {
                        autor = true;
                    }
                    if (qName.equalsIgnoreCase("NomeAutor")) {
                        nomeAutor = true;
                    }
                    
                    if (qName.equalsIgnoreCase("EmailAutor")) {
                        emailAutor = true;
                    }
                    if (qName.equalsIgnoreCase("Filiacao")) {
                        filiacao = true;
                    }
                    if (qName.equalsIgnoreCase("UsernameAutor")) {
                        usernameAutor = true;
                    }
                    if (qName.equalsIgnoreCase("PalavrasChave")) {
                        palavrasChave = true;
                    }
                    if (qName.equalsIgnoreCase("Ficheiro")) {
                        ficheiro = true;
                    }
                    if (qName.equalsIgnoreCase("NumeroRevisoes")) {
                        numeroRevisoes = true;
                    }
                    if (qName.equalsIgnoreCase("Revisao")) {
                        revisao = true;
                    }
                    
                    if (qName.equalsIgnoreCase("Revisor")) {
                        revisor = true;
                    }
                    if (qName.equalsIgnoreCase("EstadoRevisao")) {
                        estadoRevisao = true;
                    }
                    if (qName.equalsIgnoreCase("Confianca")) {
                        confianca = true;
                    }
                    if (qName.equalsIgnoreCase("Adequacao")) {
                        adequacao = true;
                    }
                    if (qName.equalsIgnoreCase("Originalidade")) {
                        originalidade = true;
                    }
                    if (qName.equalsIgnoreCase("Apresentacao")) {
                        apresentacao = true;
                    }
                    if (qName.equalsIgnoreCase("Recomendacao")) {
                        recomendacao = true;
                    }
                    if (qName.equalsIgnoreCase("Justificacao")) {
                        justificacao = true;
                    }
                    
                }
                
                public void endElement(String uri, String localName,
                        String qName)
                        throws SAXException {
                    if (qName.equalsIgnoreCase("ListaOrganizadores")) {
//                        criarEvento.registaEvento();
                        m_regEvento.registaEvento(e);

//                        e = m_empresa.getRegistoEventos().getListaEventosRegistadosDoUtilizador(m_empresa.getLogin().getUsername()).get(i);
//                        e = m_empresa.getRegistoEventos().getListaEventosRegistadosDoUtilizador("").get(i);
                    }
                    
                    if (qName.equalsIgnoreCase("Evento")) {
                        
                        i++;
                    }
                    if (qName.equalsIgnoreCase("SessaoTematica")) {
                        s++;
                    }
                    if (qName.equalsIgnoreCase("CP")) {
                        definirCP.registaCP();
//                        e.setCP(cP);

                    }
                    if (qName.equalsIgnoreCase("EstadoST")) {
//                        criarST.registaSessaoTematica();
                        e.getListaDeSessõesTemáticas().getListSessoes().add(st);

//                        e.getListaDeSessõesTemáticas().registaSessaoTematica(st);
                    }
                    if (qName.equalsIgnoreCase("CPSessao")) {
                        definirCP.registaCP();
                    }
                    if (qName.equalsIgnoreCase("Autor")) {
                        Autor a = submeterArtigo.novoAutor(username, nomeDeAutor, mailAutor, afiliacao);
                        submeterArtigo.addAutor(a);
                    }
//                    if (qName.equalsIgnoreCase("Submissao")) {
//
//                        submeterArtigo.registarSubmissao();
//                    }
                    if (qName.equalsIgnoreCase("Resumo")) {
                        
                        submeterArtigo.registarSubmissao();
                    }
                    if (qName.equalsIgnoreCase("Revisao")) {
//                        m_listaRevisoes = revisorA.getLstRevisoes();
//                        m_listaRevisoes.addRevisao(m_revisao);
                        revisorA.getLstRevisoes().addRevisao(m_revisao);
//                        reverArtigo.registaRevisao();
                    }
                    
                     if (qName.equalsIgnoreCase("ListaSubmissoesEvento")) {
                        listaSubmissoesEvento = false;
                    }
                    
                    
                }
                
                public void characters(char ch[], int start, int length)
                        throws SAXException {
                    
                    if (numeroEventos) {
//                        System.out.println("numero Eventos : " + new String(ch, start, length));

                        numeroEventos = false;
                    }
                    if (evento) {
//                        System.out.println("Evento : " + new String(ch, start, length));
//                        criarEvento.novoEvento();
                        e = m_regEvento.novoEvento();
                        evento = false;
                    }
                    
                    if (titulo) {
//                        System.out.println("Titulo : " + new String(ch, start, length));
//                        criarEvento.setTitulo(new String(ch, start, length));
                        e.setTitulo(new String(ch, start, length));
                        titulo = false;
                    }
                    
                    if (descricao) {
//                        System.out.println("Descricao : " + new String(ch, start, length));
//                        criarEvento.setDescricao(new String(ch, start, length));
                        e.setDescricao(new String(ch, start, length));
                        descricao = false;
                    }
                    
                    if (dataInicio) {
//                        System.out.println("Data Inicio : " + new String(ch, start, length));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        String dateInString = new String(ch, start, length);;
                        
                        try {
                            
                            Date date = formatter.parse(dateInString);
//                            criarEvento.setDataInicio(date);
                            e.setDataInicio(date);
                            
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        
                        dataInicio = false;
                    }
                    if (dataFim) {
//                        System.out.println("Data Fim : " + new String(ch, start, length));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        String dateInString = new String(ch, start, length);;
                        
                        try {
                            
                            Date date = formatter.parse(dateInString);
//                            criarEvento.setDataFim(date);
                            e.setDataFim(date);
                            
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dataFim = false;
                    }
                    
                    if (dataInicioSubmissao) {
//                        System.out.println("Data Inicio Submissao : " + new String(ch, start, length)+"<----------");
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        String dateInString = new String(ch, start, length);;
                        
                        try {
                            
                            dInicioSubmissao = formatter.parse(dateInString);
//                            criarEvento.setDataInicioSubmissao(dInicioSubmissao);
                            e.setDataInicioSubmissao(dInicioSubmissao);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        
                        dataInicioSubmissao = false;
                    }
                    if (dataFimSubmissao) {
//                        System.out.println("Data Fim Submissao : " + new String(ch, start, length));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        String dateInString = new String(ch, start, length);;
                        
                        try {
                            
                            dFimSubmissao = formatter.parse(dateInString);
//                            criarEvento.setDataFimSubmissao(dFimSubmissao);
                            e.setDataFimSubmissao(dFimSubmissao);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dataFimSubmissao = false;
                    }
                    if (dataInicioDistribuicao) {
//                        System.out.println("Data Inicio Distribuição : " + new String(ch, start, length));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        String dateInString = new String(ch, start, length);;
                        
                        try {
                            
                            dInicioDistribuicao = formatter.parse(dateInString);
//                            criarEvento.setDataInicioDistribuicao(dInicioDistribuicao);
                            e.setDataInicioDistribuicao(dInicioDistribuicao);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dataInicioDistribuicao = false;
                    }
                    if (dataLimiteRevisao) {
//                        System.out.println("Data Limite Revisao : " + new String(ch, start, length));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        String dateInString = new String(ch, start, length);;
                        
                        try {
                            
                            dLimiteRevisao = formatter.parse(dateInString);
//                            criarEvento.setDataLimiteRevisao(dLimiteRevisao);
                            e.setDataLimiteRevisao(dLimiteRevisao);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dataLimiteRevisao = false;
                    }
                    if (dataLmiteSubmissaoFinal) {
//                        System.out.println("Data Lmite Submissao Final : " + new String(ch, start, length));
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                        String dateInString = new String(ch, start, length);;
                        
                        try {
                            
                            dLmiteSubmissaoFinal = formatter.parse(dateInString);
//                            criarEvento.setDataLmiteSubmissaoFinal(dLmiteSubmissaoFinal);
                            e.setDataLmiteSubmissaoFinal(dLmiteSubmissaoFinal);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        dataLmiteSubmissaoFinal = false;
                    }
                    if (estadoEvento) {
//                        System.out.println("Estado Evento : " + new String(ch, start, length));
//                        System.err.append("--->" + new String(ch, start, length));
                        estadoEvento = false;
                    }
                    if (local) {
                        try {
                            String l[][] = m_empresa.getLocal(ficheiroLocal);
                            
                            for (int j = 0; j < l.length; j++) {
                                if (l[j][0].equalsIgnoreCase(new String(ch, start, length))) {
                                    String lclDesg = l[j][1];
//                                    criarEvento.setLocal(lclDesg);
                                    e.setLocal(lclDesg);
                                }
                                
                            }
                            
                        } catch (ParserConfigurationException ex) {
                            Logger.getLogger(LerEventoXML.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        local = false;
                        
                    }
                    
                    if (numeroOrganizadores) {
//                        System.out.println("Numero Organizadores : " + new String(ch, start, length));
                        numeroOrganizadores = false;
                    }
                    if (listaOrganizadores) {
//                        System.out.println("lista Organizadores : " + new String(ch, start, length));

                        listaOrganizadores = false;
                    }
                    if (organizadores) {
//                        System.out.println("Organizadores : " + new String(ch, start, length));

                        organizadores = false;
                    }
                    if (organizador) {
//                        System.out.println("Organizador : " + new String(ch, start, length));
//                        criarEvento.addOrganizador(new String(ch, start, length));
                        Utilizador u = m_regUtilizadores.getUtilizadorByID(new String(ch, start, length));
                        e.getListaOrganizadores().addOrganizador(u);
                        organizador = false;
                    }
                    if (cp) {

//                        System.out.println("CP : " + new String(ch, start, length));
                        definirCP.novaCP(e);

//                      cP =  (CPEvento) e.novaCP();
                        cp = false;
                    }
                    if (numeroMembrosCP) {
//                        System.out.println("Numero Membros CP : " + new String(ch, start, length));

                        numeroMembrosCP = false;
                    }
                    if (membrosCP) {
//                        System.out.println("Membros CP : " + new String(ch, start, length));
                        membrosCP = false;
                    }
                    if (membroCP) {
//                        System.out.println("Membro CP :" + new String(ch, start, length));
                        Revisor r = definirCP.novoMembroCP(new String(ch, start, length));
//                        Utilizador u = empresa.getRegistoUtilizadores().getUtilizadorByID(new String(ch, start, length));
//                        Revisor r = new Revisor(u);
//                        Revisor r =cP.novoMembroCP(u);
//                        cP.addMembroCP(r);
//                        e.setCP(cP);
                        definirCP.addMembroCP(r);
                        
                        membroCP = false;
                    }
                    if (listaSessoesTematicas) {
//                        System.out.println("Lista Sessoes Tematicas : " + new String(ch, start, length));
                        listaSessoesTematicas = false;
                    }
                    if (numeroSessoesTematicas) {
//                        System.out.println("Numero Sessoes Tematicas : " + new String(ch, start, length));
                        numeroSessoesTematicas = false;
                    }
                    if (sessaoTematicas) {
//                        System.out.println("Sessão Tematica : " + new String(ch, start, length));
//                        criarST.setEvento(e);

                        sessaoTematicas = false;
                    }
                    
                    if (codigoST) {
//                        System.out.println("Codigo ST : " + new String(ch, start, length));
                        codigo = new String(ch, start, length);
                        
                        codigoST = false;
                    }
                    if (descricaoST) {
//                        System.out.println("Descrição ST : " + new String(ch, start, length));

//                        criarST.setDados(codigo, new String(ch, start, length), dInicioSubmissao, dFimSubmissao, dInicioDistribuicao, dLimiteRevisao, dLimiteRevisao);
                        st = new SessaoTematica(codigo, new String(ch, start, length), dInicioSubmissao, dFimSubmissao, dInicioDistribuicao, dLimiteRevisao, dFimSubmissao, e);
//                        e.getListaDeSessõesTemáticas().getListSessoes().add(st);
                        descricaoST = false;
                    }
                    if (numeroProponentes) {
//                        System.out.println("Numero Proponentes : " + new String(ch, start, length));
                        numeroProponentes = false;
                    }
                    if (proponente) {
//                        System.out.println("Proponente: " + new String(ch, start, length));
                        Utilizador u = m_regUtilizadores.getUtilizadorByID(new String(ch, start, length));
                        
                        Proponente p = st.getListaProponentes().novoProponente(u);
//                        criarST.addProponente(new String(ch, start, length));
                        st.getListaProponentes().registaProponente(p);
//                        criarST.registaProponente();

                        proponente = false;
                    }
                    
                    if (cpSessao) {
                        
                        cpSessao = false;
                    }
                    
                    if (numeroMembrosCPST) {
//                        System.out.println("Numero Membros CPST: " + new String(ch, start, length));

//                        definirCP.novaCP(e);
                        st.novaCP();
                        numeroMembrosCPST = false;
                    }
                    if (membroCPSessao) {
//                        System.out.println("Membro CP Sessao: " + new String(ch, start, length));
//                        Revisor r = definirCP.novoMembroCP(new String(ch, start, length));
                        Utilizador u = m_regUtilizadores.getUtilizadorByID(new String(ch, start, length));
                        Revisor r = new Revisor(u);
                        st.get_cp().addMembroCP(r);
//                        definirCP.addMembroCP(r);

                        membroCPSessao = false;
                    }
                    if (numeroSubmissoes) {
//                        System.err.println("Numero Submissoes: " + new String(ch, start, length));
//                        System.out.println(cont+"<------");
                        numeroSubmissoes = false;
                    }
                    if (estadoSubmissao) {
                        
                        estadoSubmissao = false;
                    }
                    if (submissao) {
//                        cont++;
                        
                        submeterArtigo.selectSubmissivel(st);
                        if (listaSubmissoesEvento) {
//                            cont++;
//                              System.out.println("ENTROU" + cont);
                            submeterArtigo.selectSubmissivel(e);
                        }
//                        st.getListaSubmissoes().novaSubmissao();
                        submissao = false;
                    }
                    
                    if (artigoTipo) {
//                        System.out.println("Artigo Tipo: " + new String(ch, start, length));
//
//                        System.err.println("FALTA O TIPO SUBMISSAO" + new String(ch, start, length) + "<------");

                        artigoTipo = false;
                    }
                    if (autorCorrespondente) {
//                        System.out.println("Autor Correspondente: " + new String(ch, start, length));
                        artigo = new Artigo();
                        Autor a = new Autor(new String(ch, start, length));
                        artigo.setAutorCorrespondente(a);
                        
                        submeterArtigo.setCorrespondente(a);
                        
                        autorCorrespondente = false;
                    }
                    if (autorSubmissao) {
//                        System.out.println("Autor Submissão: " + new String(ch, start, length));
                        autorSubmissao = false;
                    }
                    if (dataSubmissao) {
//                        System.out.println("Data Submissão: " + new String(ch, start, length));
//                        submeterArtigo.s;
                        dataSubmissao = false;
                    }
                    if (tituloArtigo) {
//                        System.out.println("Titulo Artigo: " + new String(ch, start, length));
                        tituloA = (new String(ch, start, length));
                        artigo.setTitulo(tituloA);
                        try {
                            writer = new BufferedWriter(new OutputStreamWriter(
                                    new FileOutputStream("Ficheiro Erros.txt"), "utf-8"));
                            if (tituloA == null) {
                                writer.write("Titulo invalido");
                            }
                            
                            writer.close();
                            
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(LerEventoXML.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(LerEventoXML.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        tituloArtigo = false;
                    }
                    
                    if (resumo) {
//                        System.out.println("Resumo Artigo: " + new String(ch, start, length));

                        resumoA = new String(ch, start, length);
                        artigo.setResumo(resumoA);
                        resumo = false;
                    }
                    
                    if (nomeAutor) {
//                        System.out.println("Nome Autor" + new String(ch, start, length));

                        nomeDeAutor = new String(ch, start, length);
                        nomeAutor = false;
                    }
                    
                    if (emailAutor) {
//                        System.out.println("Email Autor: " + new String(ch, start, length));
                        mailAutor = new String(ch, start, length);
                        try {
                            writer = new BufferedWriter(new OutputStreamWriter(
                                    new FileOutputStream("Ficheiro Erros.txt"), "utf-8"));
                            for (Utilizador u : m_regUtilizadores.getListUtilizador()) {
                                if (u.getEmail().equalsIgnoreCase(mailAutor) == false) {
                                    writer.write("\nEste mail não se encontra registado no sistema" + mailAutor);
                                }
                            }
                            
                            writer.close();
                            
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(LerEventoXML.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(LerEventoXML.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        emailAutor = false;
                    }
                    if (filiacao) {
//                        System.out.println("filiacao Autor: " + new String(ch, start, length));
                        afiliacao = new String(ch, start, length);
                        filiacao = false;
                    }
                    if (usernameAutor) {
//                        System.out.println("Username Autor: " + new String(ch, start, length));
                        username = new String(ch, start, length);
                        Autor a = submeterArtigo.novoAutor(username, mailAutor, codigo, afiliacao);
                        artigo.getPossiveisAutoresCorrespondentes().add(a);
                        submeterArtigo.addAutor(a);
                        usernameAutor = false;
                    }
                    
                    if (palavrasChave) {
//                        System.out.println("palavras Chave: " + new String(ch, start, length));
                        submeterArtigo.setDados(tituloA, resumoA, new String(ch, start, length));
                        artigo.setPalavrasChave(new String(ch, start, length));
                        palavrasChave = false;
                    }
                    if (ficheiro) {
//                        System.out.println("palavras Chave: " + new String(ch, start, length));
                        submeterArtigo.setFicheiro(new String(ch, start, length));
                        artigo.setFicheiro(new String(ch, start, length));
                        ficheiro = false;
                    }
                    if (numeroRevisoes) {
//                        System.out.println("Numero Revisoes : " + new String(ch, start, length));
                        numeroRevisoes = false;
                    }
                    if (revisao) {
//                        System.out.println("Revisao : " + new String(ch, start, length) );
//                         reverArtigo.selecionaRevisavel(st);
                        m_revisao = new Revisao(artigo);
//                        reverArtigo.selecionaArtigo(artigo);
                        revisao = false;
                    }
                    
                    if (revisor) {
//                        System.out.println("revisor : " + new String(ch, start, length));
                        Utilizador u = m_regUtilizadores.getUtilizadorByID(new String(ch, start, length));

//                         revisorA = new Revisor(u);
                        
                        if (listaSubmissoesEvento == false) {
                        for (Revisor r : st.getRevisores()) {
                            if (r.getStrUsername().equals(new String(ch, start, length))) {
                                revisorA = r;
                                m_revisao.setRevisor(revisorA);
                            }
                        }
                        
                        }else{
                             for (Revisor r : e.getRevisores()) {
                            if (r.getStrUsername().equals(new String(ch, start, length))) {
                                revisorA = r;
                                m_revisao.setRevisor(revisorA);
                            }
                        }
                        }
                        
                        
                        
//                        reverArtigo.setRevisor(new String(ch, start, length));

//                        Utilizador u = m_regUtilizadores.getUtilizadorByID(new String(ch, start, length));
//                        Revisor r = new Revisor(u);
//                        
//                        for (Revisor i : st.getRevisores()) {
//                            if (r.getStrUsername().equals(new String(ch, start, length))) {
//                                r = i;
//                            }
//                        }
//                        m_listaRevisoes = r.getLstRevisoes();
//                        st.getRevisores().add(r);
                        revisor = false;
                    }
                    if (estadoRevisao) {
//                        System.out.println("Estado Revisao : " + new String(ch, start, length));

                        estadoRevisao = false;
                    }
                    if (confianca) {
//                        System.out.println("Confiança : " + new String(ch, start, length));
                        String conf = new String(ch, start, length);
                        m_Confianca = Integer.parseInt(conf);
                        m_revisao.setM_Confianca(m_Confianca);
//                        m_revisao = new Revisao(artigo);
                        confianca = false;
                    }
                    if (adequacao) {
//                        System.out.println("adequacao : " + new String(ch, start, length));

                        String adequacaoInt = new String(ch, start, length);
                        m_Adequacao = Integer.parseInt(adequacaoInt);
                        m_revisao.setM_Adequacao(m_Adequacao);
                        adequacao = false;
                    }
                    if (originalidade) {
//                        System.out.println("Originalidade : " + new String(ch, start, length));

                        String orgInt = new String(ch, start, length);
                        m_Originalidade = Integer.parseInt(orgInt);
                        m_revisao.setM_Originalidade(m_Originalidade);
                        originalidade = false;
                    }
                    if (apresentacao) {
//                        System.out.println("Apresentacao : " + new String(ch, start, length));
                        String aprInt = new String(ch, start, length);
                        m_Apresentacao = Integer.parseInt(aprInt);
                        m_revisao.setM_Apresentacao(m_Apresentacao);
                        apresentacao = false;
                    }
                    if (recomendacao) {
//                        System.out.println("Recomendacao : " + new String(ch, start, length));
                        String recInt = new String(ch, start, length);
                        m_Recomendacao = Integer.parseInt(recInt);
                        m_revisao.setM_Recomendacao(m_Recomendacao);
                        recomendacao = false;
                    }
                    if (justificacao) {
//                        System.out.println("Justificacao : " + new String(ch, start, length));

                        m_revisao.setJustificacao(new String(ch, start, length));
                        m_Justificacao = new String(ch, start, length);
//                        reverArtigo.setDadosRevisao(m_Justificacao, m_Justificacao, m_Confianca, m_Adequacao, m_Originalidade, m_Apresentacao, m_Recomendacao);
//                        reverArtigo.registaRevisao();
//                        m_listaRevisoes.addRevisao(m_revisao);
                        justificacao = false;
                    }
                    
                }
                
            };
            
            File file = new File(ficheiroEvento);
            InputStream inputStream = new FileInputStream(file);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");
            
            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");
            
            saxParser.parse(is, handler);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
