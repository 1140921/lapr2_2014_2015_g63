package eventoscientificos.domain;

import java.io.FileNotFoundException;

public class Utilizador implements Comparable<Utilizador> {

    private String m_strNome;
    private String m_strUsername;
    private String m_strPassword;
    private String m_strEmail;
    private double[] m_strPasswordCodificada=new double[2];
    private String[][] matrizCodificacao;
    
    public Utilizador() {

    }

    public Utilizador(String strNome, String strUsername, String strPwd, String strEmail){
        setNome(strNome);
        setEmail(strEmail);
        setUsername(strUsername);
        setPassword(strPwd);

    }

    public String getNome() {
        return this.m_strNome;
    }

    public String getPwd() {
        return this.getM_strPassword();
    }

    public String getUsername() {
        return this.m_strUsername;
    }

    public String getEmail() {
        return this.m_strEmail;
    }

    public void setNome(String strNome) {
        this.m_strNome = strNome;
        if (strNome == null || strNome.trim().isEmpty()) {
            throw new IllegalArgumentException("Não introduziu nenhum nome");
        }
    }

    public void setUsername(String strUsername) {
        m_strUsername = strUsername;
        if (strUsername == null || strUsername.trim().isEmpty()) {
            throw new IllegalArgumentException("Não introduziu nenhum username");
        }
    }

    public void setPassword(String strPassword){
        m_strPassword = strPassword;
        if (strPassword == null || strPassword.trim().isEmpty()) {
            throw new IllegalArgumentException("Não introduziu nenhuma password");
        }
        
    
    }

    public void setPasswordCodificada(String strPassword) throws FileNotFoundException{
         matrizCodificacao=LerCsv.lercsv("Tabela_CA_Pass_v01.csv");
         int coluna = 2+ (int)( 4*Math.random());   
         m_strPasswordCodificada[0]=coluna;
         m_strPasswordCodificada[1]=Codificador.codificar(matrizCodificacao,strPassword, coluna);
    }
    
    public double[] getPasswordCodificada(){
        return m_strPasswordCodificada;
    }
    
    public void setEmail(String strEmail) {

        this.m_strEmail = strEmail;
        if (strEmail == null || strEmail.trim().isEmpty()) {
            throw new IllegalArgumentException("Não introduziu nenhum Email");
        }
    }

//alterado
    public boolean valida() {
        if (this == null) {
            throw new IllegalArgumentException("Utilizador Invalido");
        }
        return true;
    }

    @Override
    public String toString() {
        String str = "Utilizador:\n";
        str += "\tNome: " + this.m_strNome + "\n";
        str += "\tUsername: " + this.m_strUsername + "\n";
        str += "\tPassword: " + this.getM_strPassword() + "\n";
        str += "\tEmail: " + this.m_strEmail + "\n";

        return str;
    }

    
    @Override
    public Utilizador clone(){
        return new Utilizador(this.getNome(), this.getUsername(), this.getPwd(), this.getEmail());
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        Utilizador outroUtilizador = (Utilizador) outroObjeto;

        return this.m_strUsername.equalsIgnoreCase(outroUtilizador.m_strUsername) && this.m_strEmail.equals(outroUtilizador.m_strEmail);
    }

    @Override
    public int compareTo(Utilizador outroUtilizador) {
        return this.m_strUsername.compareTo(outroUtilizador.m_strUsername);
    }

    /**
     * @return the m_strPassword
     */
    public String getM_strPassword() {
        return m_strPassword;
    }
}
