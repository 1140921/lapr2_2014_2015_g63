/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import eventoscientificos.state.EventoState;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Submissao 
{
    private Artigo m_artigo;
    private Date dataSubmissao;

    public Submissao()
    {
    }
    
    public Submissao (Artigo artigo)
    {
        this.m_artigo=artigo;
    }
    public Artigo novoArtigo()
    {
        return new Artigo();
    }
   
    public String getInfo()
    {
        System.out.println("Submissao:getInfo");
        return "Submissao:getInfo";
    }
    
    public Artigo getArtigo(){
        return this.m_artigo;
    }
    public void setArtigo(Artigo artigo)
    {
        this.m_artigo = artigo;
    }
    
    public boolean valida()
    {
        return true;
    }

    Autor getAutorCorrespondente() 
    {
        return m_artigo.getAutorCorrespondente();
    }
    
    
    @Override
    public Submissao clone(){
        return new Submissao(this.getArtigo());
    }
    
    public String toString(){
       return m_artigo.toString();
    }

    /**
     * @return the dataSubmissao
     */
    public Date getDataSubmissao() {
        return dataSubmissao;
    }

    /**
     * @param dataSubmissao the dataSubmissao to set
     */
    public void setDataSubmissao(Date dataSubmissao) {
        this.dataSubmissao = dataSubmissao;
    }
    
    
    
    
    
}
