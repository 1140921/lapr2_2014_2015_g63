/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.List;

/**
 *
 * @author iazevedo
 */
public interface Revisavel {
    

    
    public ProcessoDistribuicao getProcessoDistribuicao();

    public void alteraParaEmDecisão();
    
    public boolean hasRevisor(String id);
    
     public boolean isInEmRevisao();
     
     public ListaRevisoes getListaRevisoes();
    
     public ListaSubmissoes getListaSubmissoes();

    public List<Revisor> getRevisores();
}
