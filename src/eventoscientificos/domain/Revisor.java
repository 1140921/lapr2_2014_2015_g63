/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nuno Silva
 */
public class Revisor
{
    private String m_strUsername;
    private Utilizador m_utilizador;
    private ListaRevisoes lstRevisoes;
    //private List<Revisao> m_lstRevisoes;
    
    public Revisor(Utilizador u)
    {
        m_strUsername = u.getUsername();
       this.setUtilizador(u);
        this.lstRevisoes = new ListaRevisoes();

//        this.m_lstRevisoes = new ArrayList<Revisao>(); 
    }

    public boolean valida()
    {
        
        return true;
    }

    @Override
    public String toString() {
        String str = "\nUtilizador:\n";
        str += "\tNome: " + this.m_utilizador.getNome()+ "\n";
        str += "\tUsername: " + this.m_utilizador.getUsername() + "\n";
        str += "\tEmail: " + this.m_utilizador.getEmail() + "\n";

        return str;
    }

    /**
     * @return the m_strNome
     */
    public String getStrUsername() {
        return m_strUsername;
    }
    /**
     * 
     * @return Utilizador como Revisor
     */
    public Utilizador getUtilizador(){
        return this.m_utilizador;
    }

    /**
     * @return the lstRevisoes
     */
    public ListaRevisoes getLstRevisoes() {
        return lstRevisoes;
    }
//novo
    private void setUtilizador(Utilizador u) {
       this.m_utilizador=u;
    }

    /**
     * @param lstRevisoes the lstRevisoes to set
     */
    public void setLstRevisoes(ListaRevisoes lstRevisoes) {
        this.lstRevisoes = lstRevisoes;
    }
    
    

}
