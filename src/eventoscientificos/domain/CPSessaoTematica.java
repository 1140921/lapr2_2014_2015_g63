/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class CPSessaoTematica implements CP
{

    private List<Revisor> m_listaRevisor;
    
    public List<Revisor> getRevisores()
    {
        return m_listaRevisor;
    }
    
    public CPSessaoTematica()
    {
        m_listaRevisor = new ArrayList<Revisor>(); 
    }

    @Override
    public Revisor novoMembroCP(Utilizador u)
    {
        Revisor r = new Revisor(u);
        
        if( r.valida() && validaMembroCPSessao(r) )
            return r;
        else
            return null;
    }

    //adicionado
    private boolean validaMembroCPSessao(Revisor r)
    {
        
        for(Revisor re: m_listaRevisor){
            if(re.getStrUsername().equalsIgnoreCase(r.getStrUsername())){
                return false;
            }
        }
            return true;
        
    }
    
    @Override
    public boolean addMembroCP(Revisor r)
    {
        if (r== null)
           return false;
      
       

       if( r.valida() && validaMembroCPSessao(r) )
            return m_listaRevisor.add(r);
        else
            return false;
    }


    
}
