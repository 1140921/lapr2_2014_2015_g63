

package eventoscientificos.domain;

import eventoscientificos.controllers.DefinirCPController;
import java.util.ArrayList;
import java.util.List;


public class GerarEstatisticas {

    private Empresa m_empresa;
    public Revisao m_revisao;
    public ListaRevisoes m_listaRevisoes;
    public List<Revisor> lstRevisores;
    
    public GerarEstatisticas(Empresa empresa) {

        this.m_empresa = empresa;
        
    }
            
    public List<Evento> getListaEventos(){
       return m_empresa.getRegistoEventos().getListEventos();
    }
    
    public List<Revisor> getListaRevisoresEve(int s){
        List<Revisor> lstRevisoresEve = getListaEventos().get(s).getRevisores();
        return lstRevisoresEve;
    }
 
    public int getAdequacao(){
        return m_revisao.getM_Adequacao();
    }
    
    public int getConfianca(){
        return m_revisao.getM_Confianca();
    }
    
    public int getOriginalidade(){
        return m_revisao.getM_Originalidade();
    }
    
    public int getApresentacao(){
        return m_revisao.getM_Apresentacao();
    }
    
    public List<Revisao> getRevisoesRevisor(int indexEvento, int indexRevisor){
         List<Revisao> rev = getListaRevisoresEve(indexEvento).get(indexRevisor).getLstRevisoes().getRevisoesRevisor(getListaRevisoresEve(indexEvento).get(indexRevisor).getStrUsername());
         return rev;
    }
    
    public int somatorioAtributosClassificacoes(int indexEvento, int indexRevisor){
     
        int soma = 0;
        for(Revisao revisao : getRevisoesRevisor(indexEvento, indexRevisor)){
            soma += revisao.getM_Adequacao()+revisao.getM_Adequacao()+revisao.getM_Confianca()+revisao.getM_Originalidade();
        }
        
        return soma;
    }
    
    public float[] ClassificacaoFinalDeCadaRevisao(int indexEvento, int indexRevisor){
        
        float[] RevisoesContainer = new float[getRevisoesRevisor(indexEvento, indexRevisor).size()];
        int numeroClassificacoes = 4;
        for(int i = 0; i<getRevisoesRevisor(indexEvento, indexRevisor).size(); i++){
                m_revisao=getRevisoesRevisor(indexEvento, indexRevisor).get(i);
                RevisoesContainer[i] = (m_revisao.getM_Adequacao()+m_revisao.getM_Apresentacao()+m_revisao.getM_Confianca()+m_revisao.getM_Originalidade())/numeroClassificacoes;
            }
        
        return RevisoesContainer;
    }
    
    public float mediaDoSomatorioClassificacoes(int indexEvento, int indexRevisor){
        if(getRevisoesRevisor(indexEvento,indexRevisor).size()==0){
        throw new IllegalArgumentException("Impossível fazer divisão por 0."
                + "Este Revisor não fez nenhuma Revisão");
        }
        
        return (somatorioAtributosClassificacoes(indexEvento, indexRevisor))/getRevisoesRevisor(indexEvento, indexRevisor).size();
    } 
    
    public float[] CalculoParaCadaRevisao(int indexEvento, int indexRevisor){
        
        float[] Di = new float[getRevisoesRevisor(indexEvento, indexRevisor).size()];
        float[] Xi = ClassificacaoFinalDeCadaRevisao(indexEvento, indexRevisor);
        float Xt = mediaDoSomatorioClassificacoes(indexEvento, indexRevisor);
        for(int i = 0; i<getRevisoesRevisor(indexEvento, indexRevisor).size(); i++){
           Di[i] = Math.abs(Xi[i] - Xt);
        }
        
        return Di;
    }
    
        
    public boolean ValidaTesteHipoteses(int indexEvento, int indexRevisor){
        if(getRevisoesRevisor(indexEvento, indexRevisor).size() < 30){
            throw new IllegalArgumentException("Amostra menor que 30");
        }
        return true;
    }
    
    public boolean TesteHipoteses(int indexEvento, int indexRevisor){
        float Zc =  1.645f;
        float Zo = zObservado(indexEvento, indexRevisor);
        ValidaTesteHipoteses(indexEvento, indexRevisor);
            if(Zo > Zc){
                return false;
            }else
                return true;
        
    }
    
    public float SomatorioDi(int indexEvento, int indexRevisor){
        float[] Di = CalculoParaCadaRevisao(indexEvento, indexRevisor);
        float soma = 0;
        for(int i = 0; i<getRevisoesRevisor(indexEvento, indexRevisor).size(); i++){
            soma += Di[i];
        }
        return soma;
    }
    
    public float MediaDi(int indexEvento, int indexRevisor){

        float soma = SomatorioDi(indexEvento, indexRevisor);
        float media;

        if(getRevisoesRevisor(indexEvento, indexRevisor).size() == 0){
        throw new IllegalArgumentException("Impossível fazer divisão por 0."
                + "Este Revisor não fez nenhuma Revisão");
        
        }else{
             return soma / getRevisoesRevisor(indexEvento, indexRevisor).size();   
    }
    }
    
    public float SomatorioVariancia(int indexEvento, int indexRevisor){
        float soma =0;
        float[] Di = CalculoParaCadaRevisao(indexEvento, indexRevisor);
        
        float media = MediaDi(indexEvento, indexRevisor);
        
        for(int i= 0; i<getRevisoesRevisor(indexEvento, indexRevisor).size();i++){
            soma+= Math.pow(Di[i]- media, 2);
        }
        return soma;
        
        
    }
    public float Variancia(int indexEvento, int indexRevisor){
        float soma = SomatorioVariancia(indexEvento, indexRevisor);
        float variancia;
        if((getRevisoesRevisor(indexEvento, indexRevisor).size()-1)==0){
        throw new IllegalArgumentException("Impossível fazer divisão por 0."
                + "Este Revisor fez apenas 1 Revisão.");
        }
        
        return (1/(getRevisoesRevisor(indexEvento, indexRevisor).size()-1))*soma;
        
    }
    
    public float DesvioPadrao(int indexEvento, int indexRevisor){
        float variancia = Variancia(indexEvento, indexRevisor);
        float desvioPadrao;
        
        desvioPadrao = (float) Math.sqrt(variancia);
        
        return desvioPadrao;
    }
    
    public float zObservado(int indexEvento, int indexRevisor){
        float Zo;
        float mediaDi = MediaDi(indexEvento, indexRevisor);
        float desvioPadrao = DesvioPadrao(indexEvento, indexRevisor);
        if(desvioPadrao > 0){
            return (float) ((mediaDi - 1)/(desvioPadrao/(Math.sqrt(getRevisoesRevisor(indexEvento, indexRevisor).size()))));
       
        }
       
        return 0;
    }
}
