

package eventoscientificos.domain;

import java.util.List;

public class GerarEstatisticasEvento {

    private Empresa m_empresa;
    private List<Submissao> lstSub;
     
    public GerarEstatisticasEvento(Empresa empresa){
        
        this.m_empresa = empresa;
        
    }
    
    public List<Evento> getListaEventos(){
       return m_empresa.getRegistoEventos().getListEventos();
    }
    
    public List<Submissao> getSubmissoesEventos(int indexEvento, int indexSubmissao){
         List<Submissao> lstSubmissoes = getListaEventos().get(indexEvento).getSubmissoes();    
         return lstSubmissoes;
    }
    
    public float percentagemAceitaçãoSubmissao(int indexEvento, int indexSubmissao ,int SubmissoesAceites){
       
        float percentagemAceitacao = SubmissoesAceites/getNumeroSubmissoes( indexEvento,  indexSubmissao);
        
        return percentagemAceitacao;
    }
    
    public float percentagemRejeicaoSubmissao(int indexEvento, int indexSubmissao ,int SubmissoesRecusadas){
        float percentagemRejeicao = SubmissoesRecusadas/getNumeroSubmissoes( indexEvento,  indexSubmissao);
        
        return percentagemRejeicao;
    }
    
    public int getNumeroSubmissoes(int indexEvento, int indexSubmissao){
        Submissao s[] = new Submissao[100];
        int cont = 0;
        List<Submissao> lstSubmissao = getSubmissoesEventos(indexEvento, indexSubmissao);
        for(Submissao sub : getSubmissoesEventos(indexEvento, indexSubmissao)){
            lstSub.add(sub);
            cont++;
        }
      return cont;
    }
     
    
}
