/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author Paisana
 */
public class Conflito {
    private String m_strDescricao;
    
    public Conflito(String descricao){
        setDescricao(descricao);
    }
    
    public Conflito(){
        
    }

    private void setDescricao(String descricao) {
        m_strDescricao = descricao;
    }

    public String getDescricao() {
        return m_strDescricao;
    }
    
    public MecanismoDetecaoConflito getMecanismo(){
        return null;
    }
    
    private boolean valida(){
        if(m_strDescricao.equalsIgnoreCase("")){
            return false;
        }
        return true;
    }
}
