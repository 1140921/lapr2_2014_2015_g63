/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author iazevedo
 */
public class ProcessoLicitacaoEvento implements ProcessoLicitacao {

    List<Licitacao> m_lstLicitacao;

    @Override
    public Licitacao novaLicitação(Revisor revisor, Submissao s) {
          Licitacao l = new Licitacao();
        l.setRevisor(revisor);
        l.setSubmissao(s);

        return l;
    }
  

    @Override
    public boolean addLicitacao(Licitacao l) {
        this.m_lstLicitacao.add(l);
        return true;

    }

    @Override
    public boolean valida() {
        throw new UnsupportedOperationException();
    }

    /**
     * Responsavel por buscar as licitações do Evento referentes ao Revisor
     *
     * @param u Utilizador(Revisor)
     * @return lista de Licitações
     */
    @Override
    public List<Licitacao> getLicitacoesUtilizador(Utilizador u) {
        List<Licitacao> ls = new ArrayList<>();
        for (Licitacao lici : m_lstLicitacao) {
            if (lici.getRevisor().getUtilizador().equals(u)) {
                ls.add(lici);
            }
        }
        return ls;
    }

    public boolean validaLicitacao(Licitacao l) {
        for (Licitacao licitacao : m_lstLicitacao) {
            if (licitacao.equals(l)) {
                return false;
            }
        }
        return true;

    }

    @Override
    public void registaLicitacao(Licitacao licitacao) {
        if (licitacao.valida() && validaLicitacao(licitacao)) {
            m_lstLicitacao.add(licitacao);
        }        
    }


}
