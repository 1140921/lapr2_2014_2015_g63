/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author 1140921
 */
public class ImportarLocalXml {

    private String[][] local;

    public ImportarLocalXml(String ficheiroLocal) throws ParserConfigurationException, SAXException {

        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        try {

            DefaultHandler handler = new DefaultHandler() {

                boolean localID = false;
                boolean designacao = false;
                boolean numElementos = false;
                int i = 0;
                int numeroElementos;

                public void startElement(String uri, String localName,
                        String qName, Attributes attributes)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("LocalID")) {
                        localID = true;
                    }

                    if (qName.equalsIgnoreCase("Designacao")) {
                        designacao = true;
                    }
                    if (qName.equalsIgnoreCase("NumElementos")) {
                        numElementos = true;
                    }

                }

                public void endElement(String uri, String localName,
                        String qName)
                        throws SAXException {

                    if (qName.equalsIgnoreCase("LOCALID")) {

                    }

                    if (qName.equalsIgnoreCase("Designacao")) {
                        i++;

                    }
                    if (qName.equalsIgnoreCase("ListaLocais")) {
                      

                    }

                }

                public void characters(char ch[], int start, int length)
                        throws SAXException {


                    if (numElementos) {
                        numeroElementos = Integer.parseInt(new String(ch, start, length));
                        local = new String[numeroElementos][2];
                        numElementos = false;
                    }

                    if (localID) {
                        String localDentificacao = new String(ch, start, length);
                        local[i][0] = localDentificacao;
                        localID = false;
                    }

                    if (designacao) {
                        String desg = new String(ch, start, length);
                        local[i][1] = desg;
                        designacao = false;
                    }

                }

            };

            File file = new File(ficheiroLocal);
            InputStream inputStream = new FileInputStream(file);
            Reader reader = new InputStreamReader(inputStream, "UTF-8");

            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");

            saxParser.parse(is, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   

    /**
     * @return the local
     */
    public String[][] getLocal() {
        return local;
    }
    

}
