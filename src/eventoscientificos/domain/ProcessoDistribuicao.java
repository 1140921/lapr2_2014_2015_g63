/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author iazevedo
 */
public interface ProcessoDistribuicao {
    public ListaRevisoes getListaDeRevisoes();
    
    void setMecanismoDistribuicao(MecanismoDistribuicao mecanismoDistribuicao);
    
    void distribui(Distribuivel es);
     
    String getInfo();

    Revisao novaRevisao();
}
