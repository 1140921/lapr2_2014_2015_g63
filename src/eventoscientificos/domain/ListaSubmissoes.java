
package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class ListaSubmissoes implements iTOCS
{
    private Submissivel m_submissivel;
    private List<Submissao> m_lstSubmissoes;
    private List<Submissao> m_lstSubmissoesFinal;
    private Submissao s;
    private List<Submissao> m_lstSubmissoesRetiradas;
    
    public ListaSubmissoes(Submissivel est)         
    {
        this.m_submissivel = est;
        this.m_lstSubmissoes = new ArrayList<Submissao>();
        this.m_lstSubmissoesRetiradas=new ArrayList<Submissao>();
    }
    
    public Submissao novaSubmissao() {
        return new Submissao();
    }
    
    public boolean addSubmissao(Submissao submissao)
    {
        if (validaSubmissao(submissao))
        {
            return this.m_lstSubmissoes.add(submissao);
        }
        else
            return false;
    }
    public boolean addSubmissaoFinal(Submissao submissao)
    {
        if (validaSubmissao(submissao))
        {
            return this.m_lstSubmissoesFinal.add(submissao);
        }
        else
            return false;
    }
    //novo
    public boolean removerSubmissao(Submissao submissao){
         return m_lstSubmissoes.remove(submissao);
    }
    
    public List<Submissao> getSubmissoesRetiradas(){
        return m_lstSubmissoesRetiradas;
    }
    //novo
    public void addSubmissaoRetirada(Submissao s){
        m_lstSubmissoesRetiradas.add(s);
    }
    
    public List<Submissao> getSubmissoes()
    {
        return m_lstSubmissoes;
    }
    public List<Submissao> getSubmissoesFinal()
    {
        return m_lstSubmissoesFinal;
    }


    private boolean validaSubmissao(Submissao submissao) 
    {
//        for(Submissao s :m_lstSubmissoes){
//             if (submissao.getArtigo().getAutorCorrespondente().equals(s.getArtigo().getAutorCorrespondente())|| submissao.getArtigo().getM_strTitulo().equalsIgnoreCase(s.getArtigo().getM_strTitulo())) {
//                 return false;
//             }
//        }

             
        
        return submissao.valida();
    }
    
    
    //novo
    public List<Submissao> getSubmissoesDoAutor(String id){
        List<Submissao> lstSub = new ArrayList();
        for(Submissao s:m_lstSubmissoes){
            if(s.getArtigo().getListaAutores().hasAutor(id)){
                lstSub.add(s);
            }
        }
               
           return lstSub;
    }

    @Override
    public String showData() {
        return m_lstSubmissoes.toString();
    }

}
