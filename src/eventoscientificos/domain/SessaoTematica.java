package eventoscientificos.domain;

import TOCS.iTOCS;
import eventoscientificos.state.STState;
import eventoscientificos.state.STStateCriada;
import java.util.Date;
import java.util.List;

public class SessaoTematica implements CPDefinivel, Submissivel, Distribuivel, Licitavel, Revisavel, Listavel, Removivel, iTOCS {

    private String m_codigo;
    private String m_descricao;
    private Date m_strDataInicioSubmissao;
    private Date m_strDataFimSubmissao;
    private Date m_strDataInicioDistribuicao;
    private Date m_strDataLimiteRevisao;
    private Date m_strDataLimiteSubmissaoFinal;

    private ListaProponentes m_lsProp;
    private STState m_state;
    private CP m_cp;
    private ListaSubmissoes m_lstSubmissoes;
    private ListaSubmissoes m_lstSubmissoesFinal;
    private ProcessoLicitacao m_processoLicitacao;
    private ProcessoDecisao m_processoDecisao;
    private ProcessoDistribuicao m_processoDistribuicao;
    private Evento evento;

    private ListaRevisoes lstRevisoes = new ListaRevisoes();

    private SessaoTematica() {
        lstRevisoes = new ListaRevisoes();
    }

    public SessaoTematica(String codigo, String descricao, Date dtInicioSub, Date dtFimSub,
            Date dtInicioDistr, Date dtLimiteRev, Date dtSubFinal, Evento evento) {

        this.evento = evento;
        setM_codigo(codigo);
        setM_descricao(descricao);
        setM_strDataInicioSubmissao(dtInicioSub);
        setM_strDataFimSubmissao(dtFimSub);
        setM_strDataInicioDistribuicao(dtInicioDistr);
        setM_strDataLimiteRevisao(dtLimiteRev);
        setM_strDataLimiteSubmissaoFinal(dtSubFinal);

        this.m_lsProp = new ListaProponentes();
        this.m_lstSubmissoes = new ListaSubmissoes(this);
        this.m_lstSubmissoesFinal = new ListaSubmissoes(this);
        new STStateCriada(this);
    }

    public boolean isInRegistado() {
        return this.m_state.isInRegistado();
    }

    public boolean isInCPDefinida() {
        return this.m_state.isInCPDefinida();
    }

    public boolean isInEmSubmissao() {
        //return this.m_state.isInEmSubmissao();
        return m_cp != null;
    }

    public boolean isInEmDetecaoConflitos() {
        return this.m_state.isInEmDetecaoConflitos();
    }

    public boolean isInEmLicitacao() {
        return this.m_state.isInEmLicitacao();
    }

    public boolean isInEmDistribuicao() {
        return this.m_state.isInEmDistribuicao();
    }

    public boolean isInEmRevisao() {
//        return this.m_state.isInEmRevisao();
        return true;
    }

    public boolean isInEmDecisao() {
        return this.m_state.isInEmDecisao();
    }

    public boolean isInEmDecidido() {
        return this.m_state.isInEmDecidido();
    }

    public boolean setStateRegistado() {
        return this.m_state.setStateRegistado();
    }

    public boolean setStateEmSubmissao() {
        return this.m_state.setStateEmSubmissao();
    }

    public boolean setStateEmDetecaoConflitos() {
        return this.m_state.setStateEmDetecaoConflitos();
    }

    public boolean setStateEmLicitacao() {
        return this.m_state.setStateEmLicitacao();
    }

    public boolean setStateEmDistribuicao() {
        return this.m_state.setStateEmDistribuicao();
    }

    public boolean setStateEmRevisao() {
        return this.m_state.setStateEmRevisao();
    }

    public boolean setStateEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    public boolean setStateEmDecidido() {
        return this.m_state.setStateEmDecidido();
    }

    @Override
    public CP novaCP() {
        m_cp = new CPSessaoTematica();

        return m_cp;
    }

    @Override
    public boolean setCP(CP cp) {
        m_cp = cp;
        return true;
    }

    public boolean setState(STState stState) {
        this.m_state = stState;
        return true;
    }

    public boolean valida() {
        return this.m_state.valida();
    }

    public ListaProponentes getListaProponentes() {
        return this.getM_lsProp();
    }

    public Date getDataInicioSubmissao() {
        return this.m_strDataInicioSubmissao;
    }

    public Date getDataFimSubmissao() {
        return this.m_strDataFimSubmissao;
    }

    public Date getDataInicioDistribuicao() {
        return this.m_strDataInicioDistribuicao;
    }

    @Override
    public ListaSubmissoes getListaSubmissoes() {
        return this.m_lstSubmissoes;
    }
    @Override
    public ListaSubmissoes getListaSubmissoesFinal() {
        return this.m_lstSubmissoesFinal;
    }

    @Override
    public ProcessoLicitacao iniciaDetecao() {
        m_state.setStateEmDetecaoConflitos();
        return new ProcessoLicitacaoST();
    }

    @Override
    public List<Revisor> getRevisores() {
        return m_cp.getRevisores();
    }

    public Licitacao novaLicitação(Revisor r, Submissao s) {
        throw new UnsupportedOperationException();
    }

    public boolean addLicitacao(Licitacao l) {
        throw new UnsupportedOperationException();
    }

    public void setProcessoLicitacao(ProcessoLicitacao pl) {
        if (pl.valida()) {
            m_processoLicitacao = pl;
            m_state.setStateEmLicitacao();
        }
    }

    @Override
    public ProcessoLicitacao getProcessoLicitacao() {
        return m_processoLicitacao;
    }

    @Override
    public List<Submissao> getSubmissoes() {
        return m_lstSubmissoes.getSubmissoes();
    }
    @Override
    public List<Submissao> getSubmissoesFinal() {
        return m_lstSubmissoesFinal.getSubmissoesFinal();
    }

    @Override
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return m_processoDistribuicao;
    }

    @Override
    public void alteraParaEmDecisão() {
        ListaRevisoes lr = m_processoDistribuicao.getListaDeRevisoes();
        if (lr.isRevisoesConcluidas()) {
            setEmDecisao();
        }

    }

    private boolean setEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    /**
     *
     * @return String com os atributos de um objecto da classe sessao tematica
     */
    @Override
    public String toString() {
        return "Codigo:" + m_codigo + "\nDescricao: " + getM_descricao();
    }

    //novo metodo
    public String getInfo() {
        return "Codigo:" + m_codigo + "\nDescricao: " + getM_descricao()
                + "\nData de Inicio de Submissoes: " + m_strDataInicioSubmissao
                + "\nData de Fim de Submissoes: " + m_strDataFimSubmissao
                + "\nData de Inicio de Distribuicoes: " + m_strDataInicioDistribuicao
                + "\nData Limite de Revisao:" + m_strDataLimiteRevisao
                + "\nData Limite de Submissao Final:" + m_strDataLimiteSubmissaoFinal
                + "\nLista de Proponentes :" + listaProponentesStr();
    }

    /**
     *
     * @return String com lista de proponentes
     */

    private String listaProponentesStr() {
        StringBuilder s = new StringBuilder();
        for (Proponente p : getM_lsProp().getListProponentes()) {
            s.append(p.getUtilizador().getUsername() + "\n");
        }
        return s.toString();
    }

//alterado
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        SessaoTematica outraSessaoTematica = (SessaoTematica) obj;

        return this.getCodigo().equals(outraSessaoTematica.getCodigo());
    }

    /**
     * @return the m_codigo
     */
    public String getCodigo() {
        return m_codigo;
    }

    /**
     * @param m_codigo the m_codigo to set
     */
    public void setM_codigo(String m_codigo) {

        this.m_codigo = m_codigo;
        if (this.m_codigo == null || this.m_codigo.trim().isEmpty()) {
            throw new IllegalArgumentException("Codigo não  inserido");
        }
    }

    /**
     * @param m_descricao the m_descricao to set
     */
    public void setM_descricao(String m_descricao) {
        this.m_descricao = m_descricao;
        if (this.getM_descricao() == null || this.getM_descricao().trim().isEmpty()) {
            throw new IllegalArgumentException("Descricao não  inserida");
        }
    }

    /**
     * @param m_strDataInicioSubmissao the m_strDataInicioSubmissao to set
     */
    public void setM_strDataInicioSubmissao(Date m_strDataInicioSubmissao) {

        this.m_strDataInicioSubmissao = m_strDataInicioSubmissao;
        if (m_strDataInicioSubmissao.compareTo(evento.getM_strDataInicio()) < 0
                || m_strDataInicioSubmissao.compareTo(evento.getM_strDataFim()) > 0) {

            throw new IllegalArgumentException("Data inicio Submissão inválida");
        }
    }

    /**
     * @param m_strDataFimSubmissao the m_strDataFimSubmissao to set
     */
    public void setM_strDataFimSubmissao(Date m_strDataFimSubmissao) {

        this.m_strDataFimSubmissao = m_strDataFimSubmissao;
        if (m_strDataFimSubmissao.compareTo(evento.getM_strDataInicio()) < 0
                || m_strDataFimSubmissao.compareTo(evento.getM_strDataFim()) > 0
                || m_strDataFimSubmissao.compareTo(m_strDataInicioSubmissao) < 0) {

            throw new IllegalArgumentException("Data fim Submissão inválida");
        }
    }

    /**
     * @param m_strDataInicioDistribuicao the m_strDataInicioDistribuicao to set
     */
    public void setM_strDataInicioDistribuicao(Date m_strDataInicioDistribuicao) {

        this.m_strDataInicioDistribuicao = m_strDataInicioDistribuicao;
        if (m_strDataInicioDistribuicao.compareTo(evento.getM_strDataInicio()) < 0
                || m_strDataInicioDistribuicao.compareTo(evento.getM_strDataFim()) > 0
                || m_strDataInicioDistribuicao.compareTo(m_strDataFimSubmissao) < 0) {

            throw new IllegalArgumentException("Data Inicio de distribuição  inválida");
        }
    }

    /**
     * @return the m_strDataLimiteRevisao
     */
    public Date getM_strDataLimiteRevisao() {
        return m_strDataLimiteRevisao;
    }

    /**
     * @param m_strDataLimiteRevisao the m_strDataLimiteRevisao to set
     */
    public void setM_strDataLimiteRevisao(Date m_strDataLimiteRevisao) {
        this.m_strDataLimiteRevisao = m_strDataLimiteRevisao;
        if (m_strDataInicioDistribuicao.compareTo(evento.getM_strDataInicio()) < 0
                || m_strDataInicioDistribuicao.compareTo(evento.getM_strDataFim()) > 0
                || m_strDataInicioDistribuicao.compareTo(m_strDataInicioDistribuicao) < 0) {

            throw new IllegalArgumentException("Data limite de revisao  inválida");
        }
    }

    /**
     * @return the m_strDataLimiteSubmissaoFinal
     */
    public Date getM_strDataLimiteSubmissaoFinal() {
        return m_strDataLimiteSubmissaoFinal;
    }

    /**
     * @param m_strDataLimiteSubmissaoFinal the m_strDataLimiteSubmissaoFinal to
     * set
     */
    public void setM_strDataLimiteSubmissaoFinal(Date m_strDataLimiteSubmissaoFinal) {
        this.m_strDataLimiteSubmissaoFinal = m_strDataLimiteSubmissaoFinal;
        if (m_strDataInicioDistribuicao.compareTo(evento.getM_strDataInicio()) < 0
                || m_strDataInicioDistribuicao.compareTo(evento.getM_strDataFim()) > 0
                || m_strDataInicioDistribuicao.compareTo(m_strDataLimiteRevisao) < 0) {

            throw new IllegalArgumentException("Data limite de submissão  inválida");
        }
    }

//teste unitario
    boolean hasProponente(String strID) {
        return getListaProponentes().hasProponente(strID);
    }

    /**
     * Verifica se tem algum revisor na sessao tematica
     *
     * @param strID
     * @return true caso exista revisor false se não existir
     */
    @Override
    public boolean hasRevisor(String strID) {

        for (Revisor r : getRevisores()) {

            if (r.getStrUsername().equalsIgnoreCase(strID)) {

                return true;
            }
        }
        return false;
    }

    @Override
    public ProcessoDistribuicao novaDistribuicao() {
        return new ProcessoDistribuicaoST();
    }

    @Override
    public void setProcessoDistribuicao(ProcessoDistribuicao processoDistribuicao) {
        m_processoDistribuicao = processoDistribuicao;
        m_state.setStateEmRevisao();
        m_state.valida();
        this.setState(m_state);
    }

    public ListaRevisoes getListaRevisoes() {
        return this.lstRevisoes;
    }

    public CP get_cp() {
        return m_cp;
    }

    /**
     * @return the m_descricao
     */
    public String getM_descricao() {
        return m_descricao;
    }

    /**
     * @return the m_lsProp
     */
    public ListaProponentes getM_lsProp() {
        return m_lsProp;
    }

    /**
     * @param m_lsProp the m_lsProp to set
     */
    public void setM_lsProp(ListaProponentes m_lsProp) {
        this.m_lsProp = m_lsProp;
    }

    @Override
    public String showData() {

        return toString();
    }

}
