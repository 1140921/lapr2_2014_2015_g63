/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

/**
 *
 * @author Nuno Silva
 */
public class Organizador
{
    private final String m_strUsername;
    private Utilizador m_utilizador;

    public Organizador(Utilizador u )
    {
       m_strUsername = u.getUsername();
       this.setUtilizador(u);
    }

    private void setUtilizador(Utilizador u)
    {
        m_utilizador = u;
    }
    
    public Utilizador getUtilizador()
    {
        return this.m_utilizador;
    }
    
    public boolean valida()
    {
//        System.out.println("Organizador:valida: " + this.toString());
        
        return true;
    }

    /**
     * @return the m_strNome
     */
    public String getUsername() {
        return m_strUsername;
    }
    
    
    
}
