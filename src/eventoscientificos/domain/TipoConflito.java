/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.domain;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class TipoConflito
{
    private String m_strDescricao;
    private MecanismoDetecaoConflito m_mecanismo;
    
    
    public TipoConflito(){
        
    }
    
    public TipoConflito(String descricao)
    {
        setDescricao(descricao);
    }
    
    public TipoConflito(String strDescricao, MecanismoDetecaoConflito mecanismo)
    {
        setDescricao(strDescricao);
        setMecanismoDetecaoConflito(mecanismo);
    }

    public String getDescricao()
    {
        return m_strDescricao;
    }

   
    public void setDescricao(String strDescricao)
    {
        this.m_strDescricao = strDescricao;
    }

    
    public MecanismoDetecaoConflito getMecanismoDetecaoConflito()
    {
        return m_mecanismo;
    }

   
    public void setMecanismoDetecaoConflito(MecanismoDetecaoConflito m_mecanismo)
    {
        this.m_mecanismo = m_mecanismo;
    }
    
    public boolean valida()
    {
            return true;
    }
    
    @Override
    public String toString()
    {
        return getDescricao() + " - " + getMecanismoDetecaoConflito().toString();
    }   
    
}
