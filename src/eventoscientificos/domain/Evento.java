/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import eventoscientificos.state.EventoState;
import eventoscientificos.state.EventoStateCriado;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Nuno Silva && 1140921
 */
public class Evento implements CPDefinivel, Submissivel, Distribuivel, Decidivel, Licitavel, Revisavel,
        Listavel,Removivel {

    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private Date m_strDataInicio;
    private Date m_strDataFim;
    private Date m_strDataInicioSubmissao;
    private Date m_strDataFimSubmissao;
    private Date m_strDataInicioDistribuicao;
    private Date m_strDataLimiteRevisao;
    private Date m_strDataLimiteSubmissaoFinal;
    private EventoState m_state;
    private CP m_cp;
    private ListaOrganizadores m_lstOrganizadores;
    private ListaSessoesTematicas m_lstSessoesTematicas;
    private ListaSubmissoes m_lstSubmissoes;
    private ListaSubmissoes m_lstSubmissoesFinal;
    private ProcessoDecisao m_processoDecisao;
    private ProcessoLicitacao m_processoLicitacao;
    private ProcessoDistribuicao m_processoDistribuicao;
    private ListaRevisoes lstRevisoes;
   

    public Evento() {
        new EventoStateCriado(this);
        m_local = new Local();
        m_lstOrganizadores = new ListaOrganizadores();
        m_lstSessoesTematicas = new ListaSessoesTematicas(this);
        m_lstSubmissoes = new ListaSubmissoes(this);
        m_lstSubmissoesFinal = new ListaSubmissoes(this);
        lstRevisoes= new ListaRevisoes();

    }

    public boolean setState(EventoState state) {
        this.m_state = state;
        return true;
    }

    @Override
    public CP novaCP() {
        m_cp = new CPEvento();

        return get_cp();
    }

    @Override
    public boolean setCP(CP cp) {
        m_cp = cp;
        return true;
    }
  
    

    public boolean isInRegistado() {
        return this.m_state.isInRegistado();
    }

    public boolean isInSTDefinidas() {
//        return this.m_state.isInSTDefinidas();
        return true;
    }

    public boolean isInCPDefinida() {
        return this.m_state.isInCPDefinida();
    }

    public boolean isInEmSubmissao() {
//        return this.m_state.isInEmSubmissao();
        if(m_cp==null){
            return false;
        }
        return true;
    }

    public boolean isInEmDetecaoConflitos() {
        return this.m_state.isInEmDetecaoConflitos();
    }

    public boolean isInEmLicitacao() {
        return this.m_state.isInEmLicitacao();
    }

    public boolean isInEmDistribuicao() {
//        return this.m_state.isInEmDistribuicao();
        return true;
    }

    public boolean isInEmRevisao() {
//        return this.m_state.isInEmRevisao();
        return true;
    }

    public boolean isInEmDecisao() {
        return this.m_state.isInEmDecisao();
    }

    public boolean isInEmDecidido() {
        return this.m_state.isInEmDecidido();
    }

    public boolean setStateRegistado() {
        return this.m_state.setStateRegistado();
    }

    public boolean setStateSTDefinidas() {
        return this.m_state.setStateSTDefinidas();
    }

    public boolean setStateEmSubmissao() {
        return this.m_state.setStateEmSubmissao();
    }

    public boolean setStateEmDetecaoConflitos() {
        return this.m_state.setStateEmDetecaoConflitos();
    }

    public boolean setStateEmLicitacao() {
        return this.m_state.setStateEmLicitacao();
    }

    public boolean setStateEmDistribuicao() {
        return this.m_state.setStateEmDistribuicao();
    }

    public boolean setStateEmRevisao() {
        return this.m_state.setStateEmRevisao();
    }

    public boolean setStateEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }

    public boolean setStateEmDecidido() {
        return this.m_state.setStateEmDecidido();
    }

    public String getTitulo() {
        return this.m_strTitulo;
    }

    public Date getDataInicioSubmissao() {
        return this.m_strDataInicioSubmissao;
    }

    public Date getDataFimSubmissao() {
        return this.m_strDataFimSubmissao;
    }

    public Date getDataInicioDistribuicao() {
        return this.m_strDataInicioDistribuicao;
    }

    public ListaOrganizadores getListaOrganizadores() {
        return this.m_lstOrganizadores;
    }

    /**
     * Modifica o titulo do Evento. Se o titulo não for instânciado ou estiver
     * vazio, o titulo não é modificado e lança uma exceção.
     *
     * @param strTitulo o novo titulo do Evento
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;

        if (this.m_strTitulo == null || this.m_strTitulo.trim().isEmpty()) {
            throw new IllegalArgumentException("Titulo não inserido");
        }
    }

    /**
     * Modifica a descrição do Evento. Se a descrição não for instânciada ou
     * estiver vazia, a descrição não é modificada e lança uma exceção
     *
     * @param strDescricao a nova descrição do Evento
     */
    public void setDescricao(String strDescricao) {

        this.m_strDescricao = strDescricao;

        if (this.getM_strDescricao() == null || this.getM_strDescricao().trim().isEmpty()) {
            throw new IllegalArgumentException("Descricao não  inserida");
        }
    }

    public void setDataInicio(Date strDataInicio) {

        this.setM_strDataInicio(strDataInicio);
        
    }

    public void setDataFim(Date strDataFim) {
        this.m_strDataFim = strDataFim;
        if (getM_strDataFim().compareTo(getM_strDataInicio()) < 0) {

            throw new IllegalArgumentException("Data fim inválida");
        }
    }

    public void setDataInicioSubmissao(Date strDataInicioSubmissao) {
        this.m_strDataInicioSubmissao = strDataInicioSubmissao;
        if (m_strDataInicioSubmissao.compareTo(getM_strDataInicio()) < 0 || m_strDataInicioSubmissao.compareTo(getM_strDataFim()) > 0) {

            throw new IllegalArgumentException("Data inicio Submissão inválida");
        }
    }

    public void setDataFimSubmissao(Date strDataFimSubmissao) {
        this.m_strDataFimSubmissao = strDataFimSubmissao;
        if (m_strDataFimSubmissao.compareTo(getM_strDataInicio()) < 0 || m_strDataFimSubmissao.compareTo(getM_strDataFim()) > 0 || m_strDataFimSubmissao.compareTo(m_strDataInicioSubmissao) < 0) {

            throw new IllegalArgumentException("Data fim Submissão inválida");
        }
    }

    public void setDataInicioDistribuicao(Date strDataInicioDistribuicao) {
        this.m_strDataInicioDistribuicao = strDataInicioDistribuicao;
        if ( m_strDataInicioDistribuicao.compareTo(getM_strDataInicio()) < 0 || m_strDataInicioDistribuicao.compareTo(getM_strDataFim()) > 0 || m_strDataInicioDistribuicao.compareTo(m_strDataFimSubmissao) < 0 || m_strDataInicioDistribuicao.compareTo(getM_strDataFim()) > 0) {

            throw new IllegalArgumentException("Data Inicio de distribuição  inválida");
        }
    }
    
    
   
    
    

    public void setLocal(String strLocal) {
        getM_local().setLocal(strLocal);
    }

    public boolean valida() {
        return this.m_state.valida();
    }

    @Override
    public String toString() {
        return "Título : " + this.m_strTitulo + " \nDescrição : " + this.getM_strDescricao();
    }

    public String getInfo() {
        return toString() + this.getM_strDescricao() + "\nData Inicio : " + this.getM_strDataInicio() + "\nData Fim : " + this.getM_strDataFim()+"\nLocal"+m_local + "\n Data Inicio Submissão : " + this.m_strDataInicioSubmissao + "\nData Fim Submissão : " + this.m_strDataFimSubmissao + "\nData Inicio Distribuição :" + this.m_strDataInicioDistribuicao +
                "\nDataLimiteRevisao : "+this.m_strDataLimiteRevisao+"\nDataLmiteSubmissaoFinal : "+this.m_strDataLimiteSubmissaoFinal+ " \nOrganizadores :" + organizadoresInfo();
    }

    public StringBuilder organizadoresInfo() {
        StringBuilder orgInfo = new StringBuilder();
        for (Organizador mOrganizador : m_lstOrganizadores.getListaEventos()) {
            orgInfo.append(mOrganizador.getUsername());
        }
        return orgInfo;

    }

    public ListaSessoesTematicas getListaDeSessõesTemáticas() {
        return this.m_lstSessoesTematicas;
    }

    public ProcessoDecisao novoProcessoDecisao() {
        return new ProcessoDecisaoEvento();
    }

    public boolean setPD(ProcessoDecisao pd) {
        this.m_processoDecisao = pd;
        return true;
    }

    @Override
    public ListaSubmissoes getListaSubmissoes() {
        return this.m_lstSubmissoes;
    }
    @Override
    public ListaSubmissoes getListaSubmissoesFinal() {
        return this.m_lstSubmissoesFinal;
    }
    
    @Override
    public ListaRevisoes getListaRevisoes(){
        return this.lstRevisoes;
    }

    @Override
    public ProcessoLicitacao getProcessoLicitacao() {
        return m_processoLicitacao;
    }

    @Override
    public void setProcessoLicitacao(ProcessoLicitacao pl) {
        if (pl.valida()) {
            m_processoLicitacao = pl;
            m_state.setStateEmLicitacao();
        }
    }

    @Override
    public List<Submissao> getSubmissoes() {
        return m_lstSubmissoes.getSubmissoes();
    }
    @Override
    public List<Submissao> getSubmissoesFinal() {
        return m_lstSubmissoes.getSubmissoesFinal();
    }

    

    @Override
    public ProcessoLicitacao iniciaDetecao() {
        m_state.setStateEmDetecaoConflitos();
        return new ProcessoLicitacaoEvento();
    }

    @Override
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return m_processoDistribuicao;
    }

    @Override
    public boolean hasRevisor(String strID) {
     
      for(Revisor r : getRevisores()){
         
        if( r.getStrUsername().equalsIgnoreCase(strID)){
           
            return true;
        }
      }
        return false;
    }

    //teste unitario
    boolean hasOrganizador(String strID){
        return getListaOrganizadores().hasOrganizador(strID);
    }
    
    
    
    
    
    @Override
    public void alteraParaEmDecisão() {
        ListaRevisoes lr = m_processoDistribuicao.getListaDeRevisoes();
        if (lr.isRevisoesConcluidas()) {
            setEmDecisao();
        }

    }

    private boolean setEmDecisao() {
        return this.m_state.setStateEmDecisao();
    }


    /**
     * @return the m_local
     */
    public Local getM_local() {
        return m_local;
    }

    /**
     * @return the m_strDataInicio
     */
    public Date getM_strDataInicio() {
        return m_strDataInicio;
    }

    /**
     * @return the m_strDataFim
     */
    public Date getM_strDataFim() {
        return m_strDataFim;
    }

    /**
     * @return the m_cp
     */
    public CP get_cp() {
        return m_cp;
    }

    @Override
    public ProcessoDistribuicao novaDistribuicao() {
        return new ProcessoDistribuicaoEvento();
    }

    @Override
    public void setProcessoDistribuicao(ProcessoDistribuicao processoDistribuicao) {
        m_processoDistribuicao=processoDistribuicao;
        m_state.setStateEmRevisao();
        m_state.valida();
        this.setState(m_state);
    }

    /**
     * @return the m_strDataLimiteRevisao
     */
    public Date getDataLimiteRevisao() {
        return m_strDataLimiteRevisao;
    }

    /**
     * @param m_strDataLimiteRevisao the m_strDataLimiteRevisao to set
     */
    public void setDataLimiteRevisao(Date m_strDataLimiteRevisao) { 
        this.m_strDataLimiteRevisao = m_strDataLimiteRevisao;
          if (m_strDataLimiteRevisao.compareTo(getM_strDataInicio()) < 0 || m_strDataLimiteRevisao.compareTo(getM_strDataFim()) > 0 || m_strDataLimiteRevisao.compareTo(m_strDataFimSubmissao) < 0 || m_strDataLimiteRevisao.compareTo(getM_strDataFim()) > 0) {

            throw new IllegalArgumentException("Data Limite de Revisão inválida");
        }
      
    }

    /**
     * @return the m_strDataLmiteSubmissaoFinal
     */
    public Date getDataLimiteSubmissaoFinal() {
        return m_strDataLimiteSubmissaoFinal;
    }

    /**
     * @param m_strDataLimiteSubmissaoFinal the m_strDataLmiteSubmissaoFinal to set
     */
    public void setDataLmiteSubmissaoFinal(Date m_strDataLimiteSubmissaoFinal) {
        this.m_strDataLimiteSubmissaoFinal = m_strDataLimiteSubmissaoFinal;
        
          if ( m_strDataLimiteSubmissaoFinal.compareTo(getM_strDataInicio()) < 0 || m_strDataLimiteSubmissaoFinal.compareTo(getM_strDataFim()) > 0 || m_strDataLimiteSubmissaoFinal.compareTo(m_strDataFimSubmissao) < 0 || m_strDataLimiteSubmissaoFinal.compareTo(getM_strDataFim()) > 0) {

            throw new IllegalArgumentException("Data Limite de Revisão inválida");
        }
    }

    @Override
    public List<Revisor> getRevisores() {
        return m_cp.getRevisores();
    }

    /**
     * @return the m_strDescricao
     */
    public String getM_strDescricao() {
        return m_strDescricao;
    }

    /**
     * @param m_strDataInicio the m_strDataInicio to set
     */
    public void setM_strDataInicio(Date m_strDataInicio) {
        this.m_strDataInicio = m_strDataInicio;
    }


}
