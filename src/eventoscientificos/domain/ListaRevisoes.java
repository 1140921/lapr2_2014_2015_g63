/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.ArrayList;
import java.util.List;


public class ListaRevisoes {
    private List<Revisao> m_lstRevisoes;
    
    public ListaRevisoes (){
        m_lstRevisoes= new ArrayList<Revisao>();
    }
    //alterado
    public List<Revisao> getRevisoesRevisor(String id){
        List<Revisao> lr = new ArrayList<>();
     for(Revisao r : getM_lstRevisoes()){
         if(r.getRevisor().getStrUsername().equals(id)){
             lr.add(r);
         }
     }  
     return lr;
    }


    
    public boolean addRevisao(Revisao revisao){
        return getM_lstRevisoes().add(revisao);
    }
    
    boolean isRevisoesConcluidas() {
        return false;
    }

    public boolean valida(Revisao m_revisao) {
        return true;
    }
    
    public boolean isEmpty (){
    return getM_lstRevisoes().isEmpty();
           
}

    /**
     * @return the m_lstRevisoes
     */
    public List<Revisao> getM_lstRevisoes() {
        return m_lstRevisoes;
    }
    
    
    
}
