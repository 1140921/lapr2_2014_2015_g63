/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import java.util.List;

/**
 *
 * @author iazevedo
 */
public interface ProcessoLicitacao {
    
    Licitacao novaLicitação(Revisor revisor, Submissao s);

	
    boolean addLicitacao(Licitacao l);

    boolean valida();
    
    public List<Licitacao> getLicitacoesUtilizador(Utilizador u);
    
    public void registaLicitacao (Licitacao l);
    
    boolean validaLicitacao(Licitacao l);
}
