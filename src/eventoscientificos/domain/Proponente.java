
package eventoscientificos.domain;


public class Proponente
{
    private final String m_strNome;
    private Utilizador m_utilizador;

    public Proponente(Utilizador u )
    {
       m_strNome = u.getNome();
       this.setUtilizador(u);
    }

    private void setUtilizador(Utilizador u)
    {
        m_utilizador = u;
    }
    
    public Utilizador getUtilizador()
    {
        
        return this.m_utilizador;
    }
    /**
     * 
     * @return nome em formato String
     */
    public String getNome(){
        return m_strNome;
    }
    
    //testes
    public boolean valida(){
     
      return true;
     }
    }

