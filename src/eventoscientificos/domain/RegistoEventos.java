/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.domain;

import TOCS.iTOCS;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt> && 1140921
 */
public class RegistoEventos implements iTOCS {

    private List<Evento> m_listaEventos;

    public RegistoEventos() {
        m_listaEventos = new ArrayList<Evento>();
    }

    public Evento novoEvento() {
        return new Evento();
    }

    public boolean registaEvento(Evento e) {
        if (e.valida() && validaEvento(e)) {

            return addEvento(e);
        } else {
            return false;
        }

    }

    private boolean addEvento(Evento e) {
        e.setStateRegistado();

        return m_listaEventos.add(e);
    }

    //novo
    public List<Evento> getListEventos() {
        return m_listaEventos;
    }

    /**
     * Verifica se existe um evento com as mesmas caracteristicas caso este seja
     * verdade retorna true caso contrário false
     *
     * @param e
     * @return true se existir ja o mesmo evento false se não existir
     */
    private boolean validaEvento(Evento e) {
        for (Evento eventos : this.m_listaEventos) {
            if (eventos.getTitulo().equals(e.getTitulo()) || eventos.getM_strDataInicio().equals(e.getM_strDataInicio()) || eventos.getM_strDataFim().equals(e.getM_strDataFim())) {

                return false;

            }
        }
        return true;
    }

    public List<Decidivel> getDecisiveis(String strID) {
        List<Decidivel> ld = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInEmDecisao() && lorg.hasOrganizador(strID)) {
                ld.add(e);
            }

            ld.addAll(e.getListaDeSessõesTemáticas().getDecisiveis(strID));
        }
        return ld;
    }

    public List<CPDefinivel> getListaCPDefiniveisEmDefinicaoDoUtilizador(String strID) {
        List<CPDefinivel> ls = new ArrayList<CPDefinivel>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInSTDefinidas() && lorg.hasOrganizador(strID)) {
                ls.add(e);
            }

            ls.addAll(e.getListaDeSessõesTemáticas().getListaCPDefiniveisEmDefinicaoDoUtilizador(strID));
        }
        return ls;
    }

    public List<Evento> getListaEventosRegistadosDoUtilizador(String strID) {
        List<Evento> ls = new ArrayList<Evento>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (e.isInRegistado() && lorg.hasOrganizador(strID)) {
                ls.add(e);
            }
        }
        return ls;

    }
//novo

    public List<Listavel> getListaListaveisDoUtilizador(String strID) {
        List<Listavel> ls = new ArrayList<Listavel>();
        for (Evento e : this.m_listaEventos) {
            ListaOrganizadores lorg = e.getListaOrganizadores();
            if (lorg.hasOrganizador(strID)) {
                ls.add(e);
            }
            ListaSessoesTematicas lst = e.getListaDeSessõesTemáticas();
            for (SessaoTematica s : lst.getListSessoes()) {
                if (s.hasProponente(strID)) {
                    ls.add(s);
                }

            }

        }
        return ls;

    }

    //novo
    public List<Removivel> getListaEventoSessoesComSubmissaoAutor(String id) {
        List<Removivel> lst = new ArrayList();
        for (Evento e : m_listaEventos) {
            if (!e.getListaSubmissoes().getSubmissoesDoAutor(id).isEmpty()) {
                lst.add(e);
            }
            if (!e.getListaDeSessõesTemáticas().getListaEventoSessoesComSubmissaoAutor(id).isEmpty()) {
                lst.addAll(e.getListaDeSessõesTemáticas().getListaEventoSessoesComSubmissaoAutor(id));
            }
        }
        return lst;
    }

    public List<Submissivel> getListaSubmissiveisEmSubmissao() {

        List<Submissivel> ls = new ArrayList<Submissivel>();
        for (Evento e : this.m_listaEventos) {
            if (e.isInEmSubmissao()) {
                ls.add(e);

            }

            ls.addAll(e.getListaDeSessõesTemáticas().getListaSubmissiveisEmSubmissao());
        }
        return ls;
    }

//    public List<Submissivel> getListaSubmissiveis() {
//
//        List<Submissivel> ls = new ArrayList<Submissivel>();
//        for (Evento e : this.m_listaEventos) {
//            if (e.isInEmSubmissao()) {
//                ls.add((Submissivel) e.getListaSubmissoes());
//
//            }
//
//            ls.addAll(e.getListaDeSessõesTemáticas().getListSessoes());
//        }
//        return ls;
//    }
    public List<Revisavel> getRevisaveisEmRevisaoDoRevisor(String strID) {
        List<Revisavel> lr = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {

            if (e.isInEmRevisao() && e.hasRevisor(strID)) {
                lr.add(e);
            }

            lr.addAll(e.getListaDeSessõesTemáticas().getRevisaveisEmRevisaoDoRevisor(strID));
        }
        return lr;
    }

    public List<Distribuivel> getDistribuiveisEmDistribuicaoDoUtilizador(String strID) {
        List<Distribuivel> ld = new ArrayList<>();
        for (Evento e : this.m_listaEventos) {
            if (e.isInEmDistribuicao() && e.hasOrganizador(strID)) {
                ld.add(e);
            }
            ld.addAll(e.getListaDeSessõesTemáticas().getDistribuiveis(strID));
        }
        return ld;
    }

    /**
     *
     * @param idRev
     * @return
     */
    public List<Licitavel> getLicitaveisEmLicitacaoDoRevisor(String idRev) {
        List<Licitavel> licitaveisEmLicitacao = new ArrayList<>();

        for (Evento e : m_listaEventos) {
            if (e.isInEmLicitacao()) {
                licitaveisEmLicitacao.add(e);

                ListaSessoesTematicas listaS = new ListaSessoesTematicas(e);
                List<Licitavel> lstSL = listaS.getLicitacoesEmLicitacao();

                licitaveisEmLicitacao.addAll(lstSL);
            }
        }
        /**
         * Remove as Licitações cujo não sao do revisor
         */
        for (Licitavel lc : licitaveisEmLicitacao) {
            if (!lc.hasRevisor(idRev)) {
                licitaveisEmLicitacao.remove(lc);
            }
        }

        return licitaveisEmLicitacao;
    }


    @Override
    public String toString(){
        String evento = null;
        for(Evento e : m_listaEventos){
             evento = e.toString();
        }
        
       return evento;
    }
    
     @Override
    public String showData(){
         String evento = null;
        for(Evento e : m_listaEventos){
             evento = e.toString();
        }
        return evento;
    }

}
