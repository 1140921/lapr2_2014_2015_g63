/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public abstract class EventoStateImpl implements EventoState
{

    private Evento m_evento;
    public EventoStateImpl(Evento e)
    {
        m_evento = e;
    }
    
    public Evento getEvento()
    {
        return this.m_evento;
    }
    

    @Override
    public boolean isInRegistado()
    {
        return false;
    }

    @Override
    public boolean isInSTDefinidas()
    {
        return false;
    }

    @Override
    public boolean isInCPDefinida()
    {
        return false;
    }

    @Override
    public boolean isInEmSubmissao()
    {
        return false;
    }

    @Override
    public boolean isInEmDetecaoConflitos()
    {
        return false;
    }

    @Override
    public boolean isInEmLicitacao()
    {
        return false;
    }

    @Override
    public boolean isInEmDistribuicao()
    {
        return false;
    }

    @Override
    public boolean isInEmRevisao()
    {
        return false;
    }

    @Override
    public boolean isInEmDecisao()
    {
        return false;
    }

    @Override
    public boolean isInEmDecidido()
    {
        return false;
    }

    @Override
    public boolean setStateRegistado()
    {
        return false;
    }

    @Override
    public boolean setStateSTDefinidas()
    {
        return false;
    }

    @Override
    public boolean setStateCPDefinida()
    {
        return false;
    }

    @Override
    public boolean setStateEmSubmissao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDetecaoConflitos()
    {
        return false;
    }

    @Override
    public boolean setStateEmLicitacao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDistribuicao()
    {
        return false;
    }

    @Override
    public boolean setStateEmRevisao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDecisao()
    {
        return false;
    }

    @Override
    public boolean setStateEmDecidido()
    {
        return false;
    }
    
}
