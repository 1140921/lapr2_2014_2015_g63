/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author iazevedo
 */
public class EventoStateEmDecidido extends EventoStateImpl {
    
    public EventoStateEmDecidido(Evento e)
    {
        super(e);
    }

    @Override
    public boolean valida()
    {
        System.out.println("EventoStateEmDecisao: valida:" + getEvento().toString());
        return true;
    }
    

    @Override
    public boolean isInEmDecidido()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDecidido()
    {
        return true;
    }
    
    
}
