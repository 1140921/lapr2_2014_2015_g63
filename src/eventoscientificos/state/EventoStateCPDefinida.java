/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EventoStateCPDefinida extends EventoStateImpl
{

    public EventoStateCPDefinida(Evento e)
    {
       super(e);
    }

    @Override
    public boolean valida()
    {
        System.out.println("EventoStateCPDefinida: valida:" + getEvento().toString());
        return true;
    }
    

    @Override
    public boolean isInCPDefinida()
    {
        return true;
    }
    
    @Override
    public boolean setStateCPDefinida()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmSubmissao()
    {
        if (valida())
            return getEvento().setState(new EventoStateEmSubmissao(getEvento()));
        return false;
    }
    
}
