/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EventoStateSTDefinidas extends EventoStateImpl
{
    public EventoStateSTDefinidas(Evento e)
    {
       super(e);
    }

    @Override
    public boolean valida()
    {
        System.out.println("EventoStateSTDefinidas: valida:" + getEvento().toString());
        return true;
    }
    

    @Override
    public boolean isInSTDefinidas()
    {
        return true;
    }
    
    @Override
    public boolean setStateSTDefinidas()
    {
        return true;
    }
    
    @Override
    public boolean setStateCPDefinida()
    {
        if (valida())
            return getEvento().setState(new EventoStateCPDefinida(getEvento()));
        return false;
    }
    
}
