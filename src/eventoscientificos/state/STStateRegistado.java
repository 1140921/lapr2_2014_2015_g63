/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.SessaoTematica;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class STStateRegistado extends STStateImpl
{
   
    public STStateRegistado(SessaoTematica st)
    {
        super(st);
    }
    
    @Override
    public boolean valida()
    {
        System.out.println("STStateRegistado: valida:" + getSessaoTematica().toString());
        return true;
    }

    @Override
    public boolean setStateRegistado()
    {
        return true;
    }

    @Override
    public boolean isInRegistado()
    {
        return true;
    }
    
    @Override
    public boolean setStateCPDefinida()
    {
        if (valida())
            return getSessaoTematica().setState(new STStateCPDefinida(getSessaoTematica()));
        return false;
    }
}
