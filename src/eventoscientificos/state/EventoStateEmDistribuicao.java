/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EventoStateEmDistribuicao extends EventoStateImpl
{

    public EventoStateEmDistribuicao(Evento e)
    {
       super(e);
    }

    @Override
    public boolean valida()
    {
        System.out.println("EventoStateEmDistribuicao: valida:" + getEvento().toString());
        return true;
    }
    

    @Override
    public boolean isInEmDistribuicao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDistribuicao()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmRevisao()
    {
        if (valida())
            return getEvento().setState(new EventoStateEmRevisao(getEvento()));
        return false;
    }
    
}
