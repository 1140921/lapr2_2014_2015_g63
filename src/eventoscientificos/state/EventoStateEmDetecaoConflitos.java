/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EventoStateEmDetecaoConflitos extends EventoStateImpl
{

    public EventoStateEmDetecaoConflitos(Evento e)
    {
       super(e);
    }

    @Override
    public boolean valida()
    {
        System.out.println("EventoStateEmDetecaoConflitos: valida:" + getEvento().toString());
        return true;
    }
    

    @Override
    public boolean isInEmDetecaoConflitos()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmDetecaoConflitos()
    {
        return true;
    }
    
    @Override
    public boolean setStateEmLicitacao()
    {
        if (valida())
            return getEvento().setState(new EventoStateEmLicitacao(getEvento()));
        return false;
    }
    
}
