/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.state;

import eventoscientificos.domain.Evento;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class EventoStateCriado extends EventoStateImpl 
{

    public EventoStateCriado(Evento e)
    {
        super(e);
        e.setState(this);
    }

    @Override
    public boolean valida()
    {
//        System.out.println("EventoStateCriado: valida:" + getEvento().toString());
        return true;
    }
    
    @Override
    public boolean setStateRegistado()
    {
        if (valida())
            return getEvento().setState(new EventoStateRegistado(getEvento()));
        return false;
    }
    
}
