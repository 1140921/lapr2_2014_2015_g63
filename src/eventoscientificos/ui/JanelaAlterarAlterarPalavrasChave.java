/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.domain.Submissao;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author 1140921
 */
class JanelaAlterarAlterarPalavrasChave extends JDialog {
    
    private JFrame framePai;
    private AlterarSubmissaoController controller;
    private Submissao submissao;
    private JTextField txtPalavrasChaves;

    public JanelaAlterarAlterarPalavrasChave(JFrame framePai,Submissao submissao, AlterarSubmissaoController alterarSubmissaoController) {
         super(framePai, "Alterar Submissão");
        
        this.framePai = framePai;
        
        this.controller = alterarSubmissaoController;
        this.submissao=submissao;

       setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelPalavrasChaves());
         c.add(criarPainelBtn());
        
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarPainelPalavrasChaves() {

        
         JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Palavras chaves separadas por (;) :"));
        txtPalavrasChaves = new JTextField(15);
        txtPalavrasChaves.setText(submissao.getArtigo().getM_strPalavrasChave());
        painelTitulo.add(txtPalavrasChaves);

        return painelTitulo;
        
    }
    
    private JPanel criarPainelBtn() {
        JPanel painelBtn = new JPanel();

        painelBtn.add(criarBtnConfirmar());

        return painelBtn;
    }
    
       private JButton criarBtnConfirmar() {
        JButton confirmarBtn = new JButton("Confirmar");

        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
               
                   
                    String palavrasChaves = txtPalavrasChaves.getText();
                   submissao.getArtigo().setPalavrasChave(palavrasChaves);

                    dispose();
                   

              

            }
        });

        return confirmarBtn;
    }
    
}
