
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.domain.Login;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class JanelaAlterarDadosArtigo extends JDialog{

    private JFrame framePai;
    private JTextField txtTitulo;
    private JTextArea txtResumo;
    private JLabel lblTitulo, lblResumo;
    private AlterarSubmissaoController m_controllerSU;
    
    public JanelaAlterarDadosArtigo(JFrame parentFrame, AlterarSubmissaoController m_controllerSU){
        super(parentFrame, "Alterar Dados do Artigo");
        this.framePai = parentFrame;
        
        
        this.m_controllerSU = m_controllerSU; 
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelNorte(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);
//        c.add(criarPainelPassword());
//        c.add(criarPainelEmail());
//        c.add(criarPainelBotao());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
    public JPanel criarPainelNorte(){
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        lblTitulo = new JLabel("Título: ");
        lblResumo = new JLabel("Resumo: ");
        txtTitulo = new JTextField(10);
        txtResumo = new JTextArea(10,30);
        
        p.add(lblTitulo);
        p.add(txtTitulo);
        p.add(lblResumo);
        p.add(txtResumo);
        
        return p;
    }
    
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonAlterar());
        p.add(criarButtonCancelar());
        
        return p;
    }
    
    public JButton criarButtonAlterar(){
        JButton btn = new JButton("Alterar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
                String titulo = txtTitulo.getText();
                String resumo = txtResumo.getText();
                m_controllerSU.alterarDadosArtigo(titulo, resumo);
            }
        });
        return btn;
    }
    
    public JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            }
        });
        return btn;
    }
}
