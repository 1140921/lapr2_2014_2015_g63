/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.ListaSubmissoes;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class JanelaAlterarDadosSubmissao extends JDialog {

    private JFrame framePai;
    private Submissivel submissivel;
    private Artigo artigo;
    private JTextField txtTítulo;
    private JTextArea txtResumo;
    private AlterarSubmissaoController alterarSubmissaoController;
    private JComboBox<Submissao> listComboBox;
    private Empresa m_empresa;

    public JanelaAlterarDadosSubmissao(JFrame parentFrame, Submissivel submissivel, AlterarSubmissaoController alterarSubmissaoController) {
        super(parentFrame, "Alterar dados de Submissão");
        this.framePai = parentFrame;
        this.submissivel=submissivel;
        this.alterarSubmissaoController = alterarSubmissaoController;

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelAlterarDadosSubmissao(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);
//        c.add(criarPainelPassword());
//        c.add(criarPainelEmail());
//        c.add(criarPainelBotao());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    public JPanel criarPainelAlterarDadosSubmissao() {
        JPanel p = new JPanel();
        listComboBox = new JComboBox();
        List<Submissao> submissao = submissivel.getListaSubmissoes().getSubmissoes();

        for (Submissao s : submissao) {
            listComboBox.addItem(s);
        }
        p.add(listComboBox);
        return p;
    }

    public JPanel criarPainelButtons() {
        JPanel p = new JPanel();

        p.add(criarButtonAlterar());
        p.add(criarButtonCancelar());

        return p;
    }

    public JButton criarButtonAlterar() {
        JButton b = new JButton("Selecionar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            Submissao submissao = (Submissao) listComboBox.getSelectedItem();
            alterarSubmissaoController.setSubmissao(submissao);
            
                new JanelaAlterarSub(framePai, alterarSubmissaoController, m_empresa,submissao);
             
//                if (submissao == null) {
//                    JOptionPane.showMessageDialog(framePai,"Selecione a submissão que pretende alterar");
//                } else {
//                    alterarSubmissaoController.setSubmissao(submissao);
//                    dispose();
//                    
//                }
            }
        });
        return b;
    }

    public JButton criarButtonCancelar() {
        JButton b = new JButton("Cancelar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        return b;
    }
}
