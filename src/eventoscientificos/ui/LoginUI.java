package eventoscientificos.ui;

import eventoscientificos.controllers.LoginController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Login;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LoginUI extends JDialog {

    private JFrame jframePai;
    private LoginController loginController;
    private JTextField txtUsername;
    private JPasswordField password;
    private Empresa empresa;

    public LoginUI(JFrame jframePai, Empresa empresa) throws FileNotFoundException {

        super(jframePai,"Login");
        this.jframePai = jframePai;
        this.loginController = new LoginController(empresa);
        this.empresa = empresa;
        add(criarPainelIntroduzirDados(), BorderLayout.CENTER);
        add(criarPainelSul(), BorderLayout.SOUTH);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                sair();
            }
        });
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);

        pack();
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);

    }

    public JPanel criarPainelIntroduzirDados() {
        JPanel p = new JPanel();
        JLabel lblUsername = new JLabel("Username");
        txtUsername = new JTextField(10);
        JLabel lblPassword = new JLabel("Password");
        password = new JPasswordField(10);
        p.add(lblUsername);
        p.add(txtUsername);
        p.add(lblPassword);
        p.add(password);
        return p;
    }

    public JPanel criarPainelSul() {
        JPanel p = new JPanel();
        p.add(criarButtonLogin());
        p.add(criarButtonRegistar());
        p.add(criarButtonImportar());
        return p;
    }
    public JButton criarButtonImportar(){
        JButton btn = new JButton("Importar");
        btn.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                String utilizador="Utilizador";
                new ImportarFicheiroUI().run(jframePai,empresa,utilizador);
            }
        });
            
        return btn;
    }
    
    
    public JButton criarButtonLogin() {
        JButton btn = new JButton("Login");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String username = txtUsername.getText().trim();
                String pwd = password.getText();

                boolean b = loginController.validaLogin(username, pwd);

                if (b == true) {
                    loginController.setLogin(new Login(username, pwd));
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(jframePai, "Username/Password incorreto(a)", "Erro",
                            JOptionPane.ERROR_MESSAGE);
                    txtUsername.setText(null);
                    password.setText(null);
                }

            }
        });

        return btn;
    }

    public JButton criarButtonRegistar() {
        JButton btn = new JButton("Registar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new RegistarUtilizadorUI(jframePai, empresa);
            }
        });
        return btn;
    }

    public void sair() {
        int resposta = JOptionPane.showConfirmDialog(jframePai, "Pretende cancelar Login?", "Deseja cancelar?",
                JOptionPane.YES_NO_OPTION);
        if (resposta == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }
}
