
package eventoscientificos.ui;

import eventoscientificos.controllers.GerarEstatisticasController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.SessaoTematica;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class GerarEstatisticasUI extends JDialog{

    private Empresa m_empresa;
    private JFrame framePai;
    private GerarEstatisticasController m_controller;
    private JComboBox<Evento> lstEventos;
    private JComboBox<SessaoTematica> lstSessoes;
    
    public GerarEstatisticasUI(JFrame parentFrame, Empresa empresa){
     
        this.framePai = parentFrame;
        this.m_empresa = empresa;
        this.m_controller = new GerarEstatisticasController(empresa);
        
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelSelecionarEvento());
        c.add(criarPainelButtons());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
    public JPanel criarPainelSelecionarEvento(){
        
        JPanel p = new JPanel();

        lstEventos = new JComboBox();
        List<Evento> lstEve = m_controller.getListaEventos();
        
        for(Evento e : lstEve){
            lstEventos.addItem(e);
        }
        
        p.add(lstEventos);
        return p;
    }
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }
    
    public JButton criarButtonCancelar(){
        JButton b = new JButton("Cancelar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            }
        });
        return b;
    }
    
    public JButton criarButtonSelecionar(){
        JButton b = new JButton("Selecionar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            int i = JOptionPane.showConfirmDialog(GerarEstatisticasUI.this, "Deseja selecionar apenas o evento?");
                if (i == JOptionPane.YES_OPTION) {
                    dispose();
                    Evento evento = (Evento) lstEventos.getSelectedItem();
                    int s = lstEventos.getSelectedIndex();
                    
                    new JanelaGerarEstatisticasRevisoresEvento(framePai, s, evento, m_controller);
                }
                if (i == JOptionPane.NO_OPTION) {
                    dispose();
                    Evento evento = (Evento) lstEventos.getSelectedItem();
                    int s = lstEventos.getSelectedIndex();
                    
                    new JanelaEscolherSessaoT(framePai, s, evento, m_empresa);

                }
            }
        });
        return b;
    }
}
