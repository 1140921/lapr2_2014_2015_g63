/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.ListaOrganizadores;
import eventoscientificos.domain.RegistoUtilizadores;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author João Paisana
 */
public class DefinirCPUI extends JDialog {

    private Empresa m_empresa;
    private DefinirCPController m_controller;
    private JButton adicionarCP;
    private JButton cancelarCP;
    private JComboBox<CPDefinivel> listCPDefinivel;
  
    private JFrame parentFrame;

    public DefinirCPUI(JFrame parentFrame, Empresa empresa) {
        super(parentFrame, "Selecionar evento");
        this.m_empresa = empresa;
        m_controller = new DefinirCPController(empresa);
       

        //lstOrganizadores = new ListaOrganizadores();
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelSelecao());
        c.add(criarPainelBotoes());

        this.parentFrame = parentFrame;

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(parentFrame);
        setVisible(true);

    }

    public JPanel criarPainelSelecao() {
        JPanel p = new JPanel();
        listCPDefinivel = new JComboBox<>();

        for (CPDefinivel s : m_controller.getListaCPDefiniveisEmDefinicao(m_empresa.getLogin().getUsername())) {
            if (s != null) {
                listCPDefinivel.addItem(s);
            }
        }
        listCPDefinivel.setSelectedItem(0);
        if(listCPDefinivel.getItemCount()==0 ){
            
            JOptionPane.showMessageDialog(DefinirCPUI.this, "Não tem eventos ou sessões criados para este organizador/proponente, ou o utilizador que se encontra no login não é Organizador/Proponente.", "ERRO!", JOptionPane.ERROR_MESSAGE);
            
        }
        p.add(listCPDefinivel);
        return p;
    }

    public JPanel criarPainelBotoes() {
        JPanel p = new JPanel();
        p.add(criarbotaoSelecionarEventoSessao());
        p.add(criarbotaoCancelar());
        return p;
    }

    private JButton criarbotaoSelecionarEventoSessao() {
        adicionarCP = new JButton("Selecionar");
        adicionarCP.setToolTipText("Seleciona o evento/sessão");
        adicionarCP.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    m_controller.novaCP((CPDefinivel) listCPDefinivel.getSelectedItem());//Fazer validação se foi selecionado algo
                   
                    dispose();
                    new DefinirCPUITwo(parentFrame, m_controller);

                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(DefinirCPUI.this, "Tem que criar um evento e sessão e escolher um deles.", "ERRO!", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        return adicionarCP;

    }

    private JButton criarbotaoCancelar() {
        cancelarCP = new JButton("Cancelar");

        cancelarCP.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();

            }
        });
        return cancelarCP;

    }

}
