/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.controllers.SubmissaoFinalController;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author 1140921
 */
public class DadosArtigoFinal extends JDialog  {
 


    private JFrame framePai;
    private JTextField txtTitulo,txtPalavra;
    private JTextArea txtResumo;
    private SubmissaoFinalController m_controller;

    public DadosArtigoFinal(JFrame framePai, SubmissaoFinalController m_controller) {
        super();
        this.framePai = framePai;
        this.m_controller = m_controller;
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelTitulo());
      
        c.add(criarPainelResumo());
  c.add(criarPainelPalavrasChave());
        c.add(criarPainelBtn());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarPainelTitulo() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Titulo :"));
        txtTitulo = new JTextField(15);
        painelTitulo.add(txtTitulo);

        return painelTitulo;
    }
    private JPanel criarPainelPalavrasChave() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Palavras Chaves (separadas por ;):"));
        txtPalavra = new JTextField(15);
        painelTitulo.add(txtPalavra);

        return painelTitulo;
    }

    private JPanel criarPainelResumo() {
            JPanel painelResumo = new JPanel();
        painelResumo.add(new JLabel("Resumo:"));
        txtResumo = new JTextArea(10, 30);
        txtResumo.setMaximumSize(txtResumo.getPreferredSize());
        JScrollPane jScrollPane = new JScrollPane(txtResumo);

        painelResumo.add(jScrollPane);
        painelResumo.add(txtResumo);

        return painelResumo;
    }

    private JPanel criarPainelBtn() {
        JPanel painelBtn = new JPanel();

        painelBtn.add(criarBtnConfirmar());
        painelBtn.add(criarBtnCancelar());

        return painelBtn;
    }

    private JButton criarBtnConfirmar() {
        JButton confirmarBtn = new JButton("Confirmar");

        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {

                    String titulo = txtTitulo.getText();
                    String resumo = txtResumo.getText();
                    String palavrasChave = txtPalavra.getText();

                    m_controller.setDados(titulo, resumo,palavrasChave);
                    dispose();
                    
                    new DadosAutorSubmissaoFinal(framePai,m_controller);

                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(rootPane,e.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
                }
                

            }
        });

        return confirmarBtn;
    }

    private JButton criarBtnCancelar() {
        JButton confirmarBtn = new JButton("Cancelar");

        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                dispose();

            }
        });

        return confirmarBtn;
    }

}


