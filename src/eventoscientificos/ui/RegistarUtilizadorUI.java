package eventoscientificos.ui;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Utilizador;
import eventoscientificos.controllers.RegistarUtilizadorController;
import eventoscientificos.domain.RegistoUtilizadores;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import utils.*;

public class RegistarUtilizadorUI extends JDialog {

    private Empresa m_empresa;
    private JFrame framePai;
    private RegistoUtilizadores lista;
    private RegistarUtilizadorController m_controllerRU;
    protected  JTextField txtUsername, txtEmail, txtNome, txtPassword;
    private JButton btnRegistar, btnCancelar;

    public RegistarUtilizadorUI(JFrame parentFrame, Empresa empresa) {

        super(parentFrame, "Registar Utilizador");
        
        framePai = parentFrame;
        m_empresa = empresa;
        m_controllerRU = new RegistarUtilizadorController(m_empresa);
        m_controllerRU.novoUtilizador();

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelUsername());
        c.add(criarPainelNome());
        c.add(criarPainelPassword());
        c.add(criarPainelEmail());
        c.add(criarPainelBotao());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarPainelNome() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Nome :"));
        txtNome = new JTextField(15);
        painelTitulo.add(txtNome);

        return painelTitulo;
    }

    private JPanel criarPainelUsername() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Username :"));
        txtUsername = new JTextField(15);
        painelTitulo.add(txtUsername);

        return painelTitulo;
    }

    private JPanel criarPainelPassword() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Password :"));
        txtPassword = new JTextField(15);
        painelTitulo.add(txtPassword);

        return painelTitulo;
    }

    private JPanel criarPainelEmail() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Email :"));
        txtEmail = new JTextField(15);
        painelTitulo.add(txtEmail);

        return painelTitulo;
    }

    private JPanel criarPainelBotao() {
        JPanel p = new JPanel();
        p.add(criarButtonRegistar());
        p.add(criarButtonCancelar());
        return p;
    }

    private JButton criarButtonCancelar() {
        btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });

        return btnCancelar;
    }

    private JButton criarButtonRegistar() {
        JButton confirmarBtn = new JButton("Confirmar");
        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                
                    
                    String nome = txtNome.getText();
                    String userName = txtUsername.getText().trim();
                    String password = txtPassword.getText();
                    String email = txtEmail.getText();

                    try {
                    m_controllerRU.setDados(userName, password, nome, email);
                    JOptionPane.showMessageDialog(framePai,"Utilizador Registado");
                    dispose();

                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(RegistarUtilizadorUI.this, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(RegistarUtilizadorUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        return confirmarBtn;
    }

//    public RegistarUtilizadorUI( Empresa empresa ){
//        m_empresa = empresa;
//        m_controllerRU = new RegistarUtilizadorController(m_empresa);
//    }
//    public void run(){
//        novoUtilizador();
//
//        Utilizador utilizador = introduzDadosUtilizador();
//        
//        return listaUtilizadores.addUtilizador(utilizador);
//
//        apresentaUtilizador( utilizador );
//
//        apresentaUtilizador( utilizador );
//
//    }
//    private void novoUtilizador() {
//        m_controllerRU.novoUtilizador();
//    }
//      private void apresentaUtilizador( Utilizador utilizador ){
//        if(utilizador == null)
//            System.out.println("Utilizador não registado.");
//        else
//            System.out.println(utilizador.toString() );
//    }
//    private void apresentaUtilizador(Utilizador utilizador) {
//        if (utilizador == null) {
//            System.out.println("Utilizador não registado.");
//        } else {
//            System.out.println(utilizador.toString());
//        }
//    }
//      private Utilizador introduzDadosUtilizador(){
//        String strUsername = txtUsername.getText();
//        String strPassword = txtPassword.getText();
//        String strNome = txtNome.getText();
//        String strEmail = txtEmail.getText();
//
//            return m_controllerRU.setDados(strUsername, strPassword, strNome, strEmail);
//    }
}
