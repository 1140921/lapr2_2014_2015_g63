/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.controllers.SubmissaoFinalController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Submissivel;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author 1140921
 */
class SubmissaoFinal extends JDialog {

    private Empresa m_empresa;
    private SubmissaoFinalController m_controller;
    private JComboBox<Submissivel> listSubmissoes;
    private JFrame framePai;

    public SubmissaoFinal(JFrame aThis, Empresa m_empresa) {

        this.framePai = framePai;
        this.m_empresa = m_empresa;
        m_controller = new SubmissaoFinalController(m_empresa);

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelEscolherSubmissao());
        c.add(criarPainelBotoes());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    public JPanel criarPainelEscolherSubmissao() {
        JPanel p = new JPanel();
        listSubmissoes = new JComboBox<>();

        for (Submissivel s : m_controller.getListaSubmissiveisEmSubmissao()) {
            listSubmissoes.addItem(s);
        }
        listSubmissoes.setSelectedItem(0);
        p.add(listSubmissoes);
        return p;
    }

    private JPanel criarPainelBotoes() {
        JPanel painelBtn = new JPanel();

        painelBtn.add(criarButtonSelecionar());
        painelBtn.add(criarButtonCancelar());

        return painelBtn;
    }

    private JButton criarButtonCancelar() {

        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                dispose();
            }

        });
        return btn;

    }

    private JButton criarButtonSelecionar() {

        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    m_controller.selectSubmissivel((Submissivel) listSubmissoes.getSelectedItem());
                    dispose();
                    new DadosArtigoFinal(framePai, m_controller);
                } catch (NullPointerException n) {
                    JOptionPane.showMessageDialog(rootPane, "Nenhum evento selecioando", "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }

        });
        return btn;

    }
}
