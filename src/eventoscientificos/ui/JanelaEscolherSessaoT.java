
package eventoscientificos.ui;

import eventoscientificos.controllers.GerarEstatisticasController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.SessaoTematica;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class JanelaEscolherSessaoT extends JDialog{
    
    private Empresa m_empresa;
    private JFrame framePai;
    private GerarEstatisticasController m_controller;
    private JComboBox<Evento> lstEventos;
    private JComboBox<SessaoTematica> lstSessoes;
    private Evento m_evento;
    private int m_s;
    
    public JanelaEscolherSessaoT(JFrame parentFrame, int s, Evento evento, Empresa m_empresa){
     
        this.framePai = parentFrame;
        this.m_empresa = m_empresa;
        this.m_evento = evento;
        this.m_s = s;
        this.m_controller = new GerarEstatisticasController(m_empresa);
        
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelSelecionarSessao());
        c.add(criarPainelButtons());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
    public JPanel criarPainelSelecionarSessao(){
        JPanel p = new JPanel();
        lstSessoes = new JComboBox();
        List<SessaoTematica> lstST = m_controller.getListaSessoes(m_s);
        for(SessaoTematica st : lstST){
            lstSessoes.addItem(st);
        }
        
        p.add(lstSessoes);
        return p;
    }
    
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        
        return p;
    }
    
    public JButton criarButtonCancelar(){
        JButton b = new JButton("Cancelar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            }
        });
        return b;
    }
    
    public JButton criarButtonSelecionar(){
        JButton b = new JButton("Selecionar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
                    SessaoTematica sessao = (SessaoTematica) lstSessoes.getSelectedItem();
                    int j = lstSessoes.getSelectedIndex();
                    new JanelaGerarEstatisticasRevisoresSessao(framePai, m_s, j, sessao, m_controller);
            }
    });
        return b;
}
    
}
