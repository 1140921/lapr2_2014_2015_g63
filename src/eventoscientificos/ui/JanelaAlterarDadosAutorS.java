
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JanelaAlterarDadosAutorS extends JDialog{

    private JLabel lblUsernameNome, lblAfiliacao, lblEmail;
    private JTextField txtUserNome, txtAfiliacao, txtEmail;
    private JFrame framePai;
    private Autor m_autor;
    private AlterarSubmissaoController controller;
    private Empresa m_empresa;
    
    public JanelaAlterarDadosAutorS(JFrame parentFrame, AlterarSubmissaoController m_controller, Autor autor) {

        super(parentFrame, "Alterar Submissão");
        
        this.framePai = parentFrame;
        controller = m_controller;
        m_autor = autor;

        setLayout(new GridLayout(0, 1));

        Container c = getContentPane();
        c.add(criarPainelNorte(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
}
    public JPanel criarPainelNorte(){
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        lblUsernameNome = new JLabel("Nome: ");
        lblEmail = new JLabel("Email: ");
        lblAfiliacao = new JLabel("Instituição de Afiliação: ");
        txtUserNome = new JTextField(10);
        txtEmail = new JTextField(10);
        txtAfiliacao = new JTextField(10);
        
        p.add(lblUsernameNome);
        p.add(txtUserNome);
        p.add(lblEmail);
        p.add(txtEmail);
        p.add(lblAfiliacao);
        p.add(txtAfiliacao);
        
        return p;
    }
   
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        p.add(criarButtonAlterarDadosAutor());
        p.add(criarButtonCancelar());
        
        return p;
    }
    
    public JButton criarButtonAlterarDadosAutor(){
        JButton btn = new JButton("Alterar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            String nome = txtUserNome.getText();
            String afiliacao = txtAfiliacao.getText();
            String email = txtEmail.getText();
            controller.alterarDadosAutor(m_autor.getM_strNome());
            controller.setDados(nome, afiliacao, email);
                
            }
        });
        return btn;
    }
    
    public JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            
            dispose();
            }
        });
        return btn;
    }
}

