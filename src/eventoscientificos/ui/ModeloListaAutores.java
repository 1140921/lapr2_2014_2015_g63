/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.domain.Autor;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.JFrame;

/**
 *
 * @author 1140921
 */
public class ModeloListaAutores extends AbstractListModel {

    private SubmeterArtigoController controller;
    private List<Autor> lstAutor;
   

    public ModeloListaAutores( SubmeterArtigoController controller) {
        
        this.controller = controller;
        this.lstAutor = controller.getPossiveisAutoresCorrespondentes();
    }

    @Override
    public int getSize() {

        return lstAutor.size();

    }

    @Override
    public Object getElementAt(int i) {

        return lstAutor.get(i);
    }

}
