/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.domain.Evento;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author 1140921
 */
class JanelaMostrarEvento extends JDialog {

    private JFrame framePai;
    private CriarEventoController m_controller;
    private JTextArea txtDados;

    public JanelaMostrarEvento(JFrame framePai, CriarEventoController m_controller) {
        super(framePai);
        this.framePai = framePai;
        this.m_controller = m_controller;

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelConfirmação());
        c.add(criarPainelBotoes());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarPainelConfirmação() {
        JPanel p = new JPanel();
        JLabel dadosInseridos = new JLabel("Dados Inseridos");
        txtDados = new JTextArea(m_controller.getEvento().getInfo());
        p.add(dadosInseridos);
        p.add(txtDados);
        return p;

    }

    private JPanel criarPainelBotoes() {
        JPanel painelBtn = new JPanel();

        painelBtn.add(criarButtonConfirmar());
        painelBtn.add(criarButtonCancelar());

        return painelBtn;
    }

    private JButton criarButtonConfirmar() {
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (m_controller.registaEvento() == null) {
                        throw new IllegalArgumentException("Evento já existe");
                    }

                    JOptionPane.showMessageDialog(framePai, "Evento criado com Sucesso");

                } catch (IllegalArgumentException i) {
                    JOptionPane.showMessageDialog(framePai, i.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }

                dispose();
            }

        });
        return btn;

    }

    private JButton criarButtonCancelar() {

        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                JOptionPane.showMessageDialog(framePai, "Este evento não será guardado");
                dispose();
            }

        });
        return btn;

    }
}
