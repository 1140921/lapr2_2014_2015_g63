
package eventoscientificos.ui;

import eventoscientificos.controllers.GerarEstatisticasController;
import eventoscientificos.domain.Revisor;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JanelaApresentarEstatisticasRevisorSessao extends JDialog{
    
    private GerarEstatisticasController m_controller;
    private Revisor m_revisor;
    private JFrame framePai;
    private JLabel lblTesteHipoteses, lblRevisor, lblHipoteses, lblZobservado, lblEstatisticas, lblDesvioPadrao, lblZcritico;
    private JTextField txtZobservado, txtDesvio;
    private int indexEvento, indexSessao, m_s;
    
    public JanelaApresentarEstatisticasRevisorSessao(JFrame parentFrame,int indexEvento, int indexSessao, int indexRevisor, Revisor revisor, GerarEstatisticasController m_controller){
        
        super(parentFrame);
        this.framePai = parentFrame;
        this.m_controller = m_controller;
        this.indexEvento = indexEvento;
        this.indexSessao = indexSessao;
        this.m_s = indexRevisor;
        this.m_revisor = revisor;
        
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelApresentarEstatisticaRevisorSessao());
        c.add(criarPainelButtons());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
    public JPanel criarPainelApresentarEstatisticaRevisorSessao(){
        JPanel p = new JPanel();

        
        lblRevisor = new JLabel("O Revisor "+m_revisor.getStrUsername()+" é retido se o Zobservado for maior que o Zcrítico.   ");
        lblEstatisticas = new JLabel(""
                + "Algumas estatísticas:");
        lblDesvioPadrao = new JLabel("Desvio Padrão: ");
        txtDesvio = new JTextField(10); 
        txtDesvio.setText(""+m_controller.getDesvioPadraoSessao(indexEvento, indexSessao,m_s));
        txtDesvio.setEditable(false);
        lblZobservado = new JLabel("Z observado: ");
        txtZobservado = new JTextField(10);
        txtZobservado.setText(""+m_controller.getZobservadoSessao(indexEvento, indexSessao,m_s));
        txtZobservado.setEditable(false);
        lblZcritico = new JLabel("Z crítico: 1,645");
        lblTesteHipoteses = new JLabel("Zo > Zc ===> rejeita-se Ho ;");
        lblHipoteses = new JLabel("Zo < Zc ===> não se rejeita Ho;");
        p.add(lblRevisor);
        p.add(lblEstatisticas);
        p.add(lblDesvioPadrao);
        p.add(txtDesvio);
        p.add(lblZobservado);
        p.add(txtZobservado);
        p.add(lblZcritico);
        p.add(lblTesteHipoteses);
        p.add(lblHipoteses);
        
        return p;
    }
    
    public JPanel criarPainelButtons(){
        
        JPanel p = new JPanel();
        
        return p;
    }
}
