/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.controllers.GerarEstatisticasController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Revisor;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class JanelaGerarEstatisticasRevisoresEvento extends JDialog{

    private JFrame framePai;
    private GerarEstatisticasController m_controller;
    private JTextArea txtDados;
    private Evento m_evento;
    private Empresa m_empresa;
    private JComboBox<Revisor> lstRevisores;
    private int indexEvento;

    public JanelaGerarEstatisticasRevisoresEvento(JFrame framePai, int s, Evento evento, GerarEstatisticasController m_controller) {
        super(framePai);
        this.framePai = framePai;
        this.m_evento = evento;
        this.indexEvento = s;
//        this.m_empresa = m_empresa;
        this.m_controller = m_controller;

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelListaRevisores());
        c.add(criarPainelButtons());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
  
    
    public JPanel criarPainelListaRevisores(){
        JPanel p = new JPanel();
        lstRevisores = new JComboBox();
        List<Revisor> revisoresEvento = m_controller.getListaRevisoresEvento(indexEvento);
        for(Revisor r : revisoresEvento){
            lstRevisores.addItem(r);
        }
        p.add(lstRevisores);
        return p;
    }
       
    
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonSelecionarRevisor());
        p.add(criarButtonCancelar());
        
        return p;
    }
    
    public JButton criarButtonSelecionarRevisor(){
        JButton b = new JButton("Gerar Estatísticas");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            Revisor revisor = (Revisor) lstRevisores.getSelectedItem();
            int s = lstRevisores.getSelectedIndex();
            try{
            m_controller.getClassificacaoFinalDeCadaRevisao(indexEvento,s);
            m_controller.getSomatorioAtributosClassificacoes(indexEvento,s);
            m_controller.getMediaDoSomatorioClassificacoes(indexEvento,s);
            m_controller.getCalculoParaCadaRevisao(indexEvento,s);
            m_controller.getSomatorioDi(indexEvento,s);
            m_controller.getMediaDi(indexEvento,s);
            m_controller.getSomatorioVariancia(indexEvento,s);
            m_controller.getVariancia(indexEvento,s);
            m_controller.getDesvioPadrao(indexEvento,s);
            m_controller.getZobservado(indexEvento,s);
            
            m_controller.isTesteHipoteses(indexEvento,s);
            m_controller.rejeitaAceitaRevisor(indexEvento,s);
                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(JanelaGerarEstatisticasRevisoresEvento.this, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }
            new JanelaApresentarEstatisticasRevisorEvento(framePai, indexEvento, s, revisor, m_controller);
            }
        });
        return b;
    }
    
    public JButton criarButtonCancelar(){
        JButton b = new JButton("Cancelar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            }
        });
        return b;
    }
}
