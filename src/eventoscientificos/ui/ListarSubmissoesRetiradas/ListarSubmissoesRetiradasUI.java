package eventoscientificos.ui.ListarSubmissoesRetiradas;

import eventoscientificos.controllers.ListarSubmissoesRetiradasController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Listavel;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.ui.RemoverSubmissaoUI.RemoverSubmissaoUI;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ListarSubmissoesRetiradasUI extends JDialog {

    private ListarSubmissoesRetiradasController m_controller;
    private Empresa m_empresa;
    private JFrame jframePai;
    private JComboBox <Listavel>listComboBox;
    
    
    public ListarSubmissoesRetiradasUI(JFrame jframePai, Empresa empresa) {
        m_empresa = empresa;
        this.jframePai = jframePai;
        m_controller = new ListarSubmissoesRetiradasController(m_empresa);

        add(criarPainelEscolherEvento(),BorderLayout.CENTER);
        add(criarPainelButtons(),BorderLayout.SOUTH);
        
       if(listComboBox.getItemCount()==0){
        JOptionPane.showMessageDialog(jframePai,
        "Nao existem eventos registados com o utilizador "+empresa.getLogin().getUsername()+".",
        "Erro",JOptionPane.ERROR_MESSAGE);
        }else{
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
       }
    }

       public JPanel criarPainelEscolherEvento() {
        JPanel p = new JPanel();
        listComboBox = new JComboBox();
        List<Listavel> lstListavel = m_controller.getListaListaveisUtilizador(m_empresa.getLogin().getUsername());
        for (Listavel l : lstListavel) {        
            listComboBox.addItem(l);
        }
        p.add(listComboBox);

        return p;
    }
        private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }

    public JButton criarButtonSelecionar() {
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            try{
            dispose();
            Listavel listavel = (Listavel) listComboBox.getSelectedItem();
            List <Submissao> lstSubRetiradas=m_controller.selectListavel(listavel);
            
            if(!lstSubRetiradas.isEmpty()){
            new ListarSubmissoesRetiradasTwoUI(jframePai,lstSubRetiradas);
            }else
                JOptionPane.showMessageDialog(jframePai,
                        "Não existem submissões retiradas no(a) evento/sessão escolhido (a)",
                        "Erro",JOptionPane.ERROR_MESSAGE);
            
            }catch(IllegalArgumentException ex){
                JOptionPane.showMessageDialog(jframePai,ex.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
            }


            }
        });
        return btn;
    }

    public JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                dispose();

            }
        });
        return btn;
    }
    
    
}
