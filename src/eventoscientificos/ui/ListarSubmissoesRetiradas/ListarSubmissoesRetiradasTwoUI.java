
package eventoscientificos.ui.ListarSubmissoesRetiradas;

import eventoscientificos.controllers.ListarSubmissoesRetiradasController;
import eventoscientificos.domain.Listavel;
import eventoscientificos.domain.Submissao;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ListarSubmissoesRetiradasTwoUI extends JDialog{
  
    private JFrame jframePai;
    private List<Submissao> lstSubRetiradas;
    
    public ListarSubmissoesRetiradasTwoUI(JFrame jframePai,List <Submissao>lstSubRetiradas){
        super(jframePai,"Lista de Submissoes Retiradas");
        this.jframePai=jframePai;
        this.lstSubRetiradas=lstSubRetiradas; 
        
        add(criarPainelLista(),BorderLayout.CENTER);
        add(criarPainelButtons(),BorderLayout.SOUTH);
        
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }
    
    public JPanel criarPainelLista(){
        JPanel p = new JPanel();
        StringBuilder sb = new StringBuilder();
        for(Submissao s : lstSubRetiradas){
            sb.append(s+"\n"+"\n");
        }
        JTextArea txtLista=new JTextArea(sb.toString());
        txtLista.setRows(25);
        txtLista.setColumns(25);
        txtLista.setWrapStyleWord(true);
        txtLista.setEditable(false);
        JScrollPane jp = new JScrollPane(txtLista);
        p.add(jp);
        return p;
    }
        
        private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonTerminar());
        return p;
    }

    public JButton criarButtonTerminar() {
        JButton btn = new JButton("Terminar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
    
    
    
}
