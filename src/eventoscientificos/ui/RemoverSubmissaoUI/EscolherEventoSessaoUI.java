
package eventoscientificos.ui.RemoverSubmissaoUI;

import eventoscientificos.controllers.RemoverSubmissaoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Removivel;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class EscolherEventoSessaoUI extends JDialog {

    private JFrame framePai;
    private JComboBox<Removivel> listComboBox;
    private Empresa m_empresa;
    private RemoverSubmissaoController controller;

    public EscolherEventoSessaoUI(JFrame parentFrame, Empresa empresa) {

        super(parentFrame, "Escolher Evento/Sessão");
        framePai=parentFrame;
        m_empresa = empresa;
        controller = new RemoverSubmissaoController(m_empresa);

        setLayout(new GridLayout(0, 1));

        Container c = getContentPane();
        c.add(criarPainelEscolherEvento(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);
         if(listComboBox.getItemCount()==0){
        JOptionPane.showMessageDialog(framePai,
        "Nao existem eventos/sessões com submissões do autor "+m_empresa.getLogin().getUsername()+".",
        "Erro",JOptionPane.ERROR_MESSAGE);   
        dispose();
        }else{
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
         }
    }

    public JPanel criarPainelEscolherEvento() {
        JPanel p = new JPanel();
        listComboBox = new JComboBox();
        List<Removivel> lstRemovivel = 
        controller.getListaEventoSessoesComSubmissaoAutor(m_empresa.getLogin().getUsername());
        for (Removivel r : lstRemovivel) {        
            listComboBox.addItem(r);
        }
           
        p.add(listComboBox);

        return p;
    }

    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }

    public JButton criarButtonSelecionar() {
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            
            dispose();
            Removivel r = (Removivel) listComboBox.getSelectedItem();
            if(r==null){
                JOptionPane.showMessageDialog(framePai,"Nenhum Evento/Sessão Selecionado(a)",
                        "Erro",JOptionPane.ERROR_MESSAGE);
            }else
                new RemoverSubmissaoUI(framePai,r,controller);
            }
        });
        return btn;
    }

    public JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}