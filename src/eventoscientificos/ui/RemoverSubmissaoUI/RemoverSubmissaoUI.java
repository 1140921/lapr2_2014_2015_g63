
package eventoscientificos.ui.RemoverSubmissaoUI;

import eventoscientificos.controllers.RemoverSubmissaoController;
import eventoscientificos.domain.Removivel;
import eventoscientificos.domain.Submissao;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class RemoverSubmissaoUI extends JDialog {

    private JFrame framePai;
    
    private RemoverSubmissaoController m_controller;
    private JComboBox<Submissao> listComboBox;
    private Removivel removivel;

    public RemoverSubmissaoUI(JFrame parentFrame, Removivel r, 
            RemoverSubmissaoController controller) {
        super(parentFrame, "Eliminar Submissão");
        this.framePai = parentFrame;
        m_controller = controller;
        removivel=r;
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelAlterarDadosSubmissao(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);
//        c.add(criarPainelPassword());
//        c.add(criarPainelEmail());
//        c.add(criarPainelBotao());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    public JPanel criarPainelAlterarDadosSubmissao() {
        JPanel p = new JPanel();
        listComboBox = new JComboBox();
        List<Submissao> submissao = m_controller.selectRemovivel(removivel);

        for (Submissao s : submissao) {
            listComboBox.addItem(s);
        }
        p.add(listComboBox);
        return p;
    }

    public JPanel criarPainelButtons() {
        JPanel p = new JPanel();

        p.add(criarButtonRemover());
        p.add(criarButtonCancelar());

        return p;
    }

    public JButton criarButtonRemover() {
        JButton b = new JButton("Remover");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            try{
            
            Submissao submissao = (Submissao) listComboBox.getSelectedItem();
            int resposta=JOptionPane.showConfirmDialog(framePai, 
                    "Tem a certeza que pretende remover a submissão selecionada?");
            if(resposta==JOptionPane.YES_OPTION){
               m_controller.removeSubmissao(submissao);
            dispose();
            }
            }catch(IllegalArgumentException ex){
                JOptionPane.showMessageDialog(framePai,ex.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
            }
            }
        });
        return b;
    }

    public JButton criarButtonCancelar() {
        JButton b = new JButton("Cancelar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        return b;
    }
}
