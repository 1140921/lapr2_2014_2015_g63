package eventoscientificos.ui.ReverArtigo;

import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.ReverArtigoController;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class IntroduzirDadosRevisao extends JDialog {

    private JFrame jframePai;
    private JTextField txtDecisao, txtConfiancaDoRevisorNosTopicos, txtAdequacaoAoEvento,
            txtOriginalidadeDoArtigo,txtQualidadeDaApresentacao,txtRecomendacaoGlobal;
    
    private JTextArea txtJustificacao;
    private ReverArtigoController controller;

    public IntroduzirDadosRevisao(JFrame jframePai, ReverArtigoController controller) {
        super(jframePai, "Introduzir dados");
        this.jframePai = jframePai;
        this.controller = controller;
     
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelDecisao());
        c.add(criarPainelJustificacao());
        c.add(criarPainelConfiancaDoRevisorNosTopicos());
        c.add(criarPainelAdequacaoAoEvento());
        c.add(criarPainelOriginalidadeDoArtigo());
        c.add(criarPainelQualidadeDaApresentacao());
        c.add(criarPainelRecomendacaoGlobal());
        add(criarPainelButtons());
        setSize(600,600);
        //pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        //setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }

    
    
    private JPanel criarPainelDecisao() {
        JPanel p = new JPanel();
        p.add(new JLabel("Decisao :"));
        txtDecisao = new JTextField(10);
        p.add(txtDecisao);

        return p;

    }

    private JPanel criarPainelJustificacao() {
        JPanel p = new JPanel();
        p.add(new JLabel("Justificacao :"));
        txtJustificacao = new JTextArea(5, 25);
        JScrollPane jp = new JScrollPane(txtJustificacao);
        p.add(jp);

        return p;
    }

    private JPanel criarPainelConfiancaDoRevisorNosTopicos() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Confianca do Revisor nos Topicos (0-5):"));
        txtConfiancaDoRevisorNosTopicos = new JTextField(15);
        painelTitulo.add(txtConfiancaDoRevisorNosTopicos);

        return painelTitulo;
    }

    private JPanel criarPainelAdequacaoAoEvento() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Adequacão ao evento(0-5):"));
        txtAdequacaoAoEvento = new JTextField(15);
        painelTitulo.add(txtAdequacaoAoEvento);

        return painelTitulo;
    }

    private JPanel criarPainelOriginalidadeDoArtigo() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Originalidade do artigo (0-5):"));
        txtOriginalidadeDoArtigo = new JTextField(15);
        painelTitulo.add(txtOriginalidadeDoArtigo);

        return painelTitulo;
    }
    private JPanel criarPainelQualidadeDaApresentacao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Qualidade da apresentacao (0-5):"));
        txtQualidadeDaApresentacao = new JTextField(15);
        painelTitulo.add(txtQualidadeDaApresentacao);

        return painelTitulo;
    }
    private JPanel criarPainelRecomendacaoGlobal() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Recomendacao global (-2,2):"));
        txtRecomendacaoGlobal= new JTextField(15);
        painelTitulo.add(txtRecomendacaoGlobal);

        return painelTitulo;
    }
    
    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonConfirmar());
        p.add(criarButtonCancelar());
        return p;
    }

    private JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JButton criarButtonConfirmar() {
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               try{ 
                try{
                String d=txtDecisao.getText();
             String just= txtJustificacao.getText();
            int conf = Integer.parseInt(txtConfiancaDoRevisorNosTopicos.getText());
            int ade = Integer.parseInt(txtAdequacaoAoEvento.getText());
            int orig = Integer.parseInt(txtOriginalidadeDoArtigo.getText());
            int apre = Integer.parseInt(txtQualidadeDaApresentacao.getText());
            int rec = Integer.parseInt(txtRecomendacaoGlobal.getText());
            controller.setDadosRevisao(d, just, conf, ade, orig, apre, rec);
               
                 
            int resposta=JOptionPane.showConfirmDialog(jframePai, "Tem a certeza?");
            if(resposta==JOptionPane.YES_OPTION){
                controller.registaRevisao();
            JOptionPane.showMessageDialog(jframePai,"Revisão efetuada com sucesso");
                dispose();
            }
                }catch(NullPointerException ex){
                    JOptionPane.showMessageDialog(jframePai,ex.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
                }
         }catch(IllegalArgumentException ex){
                    JOptionPane.showMessageDialog(jframePai,"Introduza um valor valido","Erro",
                            JOptionPane.ERROR_MESSAGE);
                }
            
            }});
        return btn;
    }
}
