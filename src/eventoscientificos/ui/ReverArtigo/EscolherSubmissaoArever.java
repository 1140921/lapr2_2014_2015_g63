
package eventoscientificos.ui.ReverArtigo;
import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Revisavel;
import eventoscientificos.domain.Submissao;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
   
public class EscolherSubmissaoArever extends JDialog{
    private JFrame jframePai;
    private JComboBox<Submissao> listSubmissao;
    private ReverArtigoController controller; 
    private Empresa empresa;
    
        public EscolherSubmissaoArever(JFrame jframePai,ReverArtigoController controller,Empresa empresa){
        super(jframePai,"Escoher Evento/Sessão");
        this.jframePai=jframePai;
        this.controller=controller;
        this.empresa=empresa;
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelEscolherSubmissao(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER); 
          if(listSubmissao.getItemCount()==0){
        JOptionPane.showMessageDialog(jframePai,
        "Nao existem submissoes no evento/sessão escolhido(a) "+this.empresa.getLogin().getUsername()+".",
        "Erro",JOptionPane.ERROR_MESSAGE);   
        dispose();
        }else{
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }
    }
    


public JPanel criarPainelEscolherSubmissao(){
        JPanel p = new JPanel();
        listSubmissao = new JComboBox();
        List<Submissao> lst = 
                controller.getListaSubmissoesRevisavel();
        for(Submissao s : lst){
           listSubmissao.addItem(s);
        }
        p.add(listSubmissao);
        return p;
       }
    
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }
    public JButton criarButtonSelecionar(){
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            Submissao s =(Submissao) listSubmissao.getSelectedItem();    
            controller.selecionaArtigo(s.getArtigo());
            controller.setRevisor(empresa.getLogin().getUsername());
            dispose();
            new IntroduzirDadosRevisao(jframePai, controller);
            }
            }); 
        return btn;
    }
    public JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });     
return btn;
   }
}
