
package eventoscientificos.ui.ReverArtigo;

import eventoscientificos.controllers.ReverArtigoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Revisavel;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class EscolherRevisavelUI extends JDialog{
    
    
    private JFrame jframePai;
    private JComboBox listRevisavel;
    private Empresa empresa;
    private ReverArtigoController controller;
    
    public EscolherRevisavelUI(JFrame jframePai,Empresa empresa){
        super(jframePai,"Escoher Evento/Sessão");
        this.jframePai=jframePai;
        this.empresa=empresa;
        controller=new ReverArtigoController(empresa); 
                setLayout(new GridLayout(0, 1));

        Container c = getContentPane();
        c.add(criarPainelEscolherRevisavel(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);
         if(listRevisavel.getItemCount()==0){
        JOptionPane.showMessageDialog(jframePai,
        "Nao existem eventos/sessões com o revisor "+this.empresa.getLogin().getUsername()+".",
        "Erro",JOptionPane.ERROR_MESSAGE);   
        dispose();
        }else{
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }
    }
    public JPanel criarPainelEscolherRevisavel(){
        JPanel p = new JPanel();
        listRevisavel = new JComboBox();
        List<Revisavel> lst = 
                controller.getRevisaveisEmRevisaoDoRevisor(this.empresa.getLogin().getUsername());
        for(Revisavel r : lst){
            listRevisavel.addItem(r);
        }
        p.add(listRevisavel);
        return p;
       }
    
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }
    public JButton criarButtonSelecionar(){
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
            Revisavel r =(Revisavel) listRevisavel.getSelectedItem();    
            controller.selecionaRevisavel(r);
            dispose();
            new EscolherSubmissaoArever(jframePai, controller, empresa);
            }
            }); 
        return btn;
    }
    public JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });     
return btn;
   }
}