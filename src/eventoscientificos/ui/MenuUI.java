package eventoscientificos.ui;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.ImportarLocalXml;
import eventoscientificos.domain.LerEventoXML;
import eventoscientificos.ui.CriarSessaoTematica.EscolherEventoUI;
import eventoscientificos.ui.ListarSubmissoesRetiradas.ListarSubmissoesRetiradasUI;
import eventoscientificos.ui.RemoverSubmissaoUI.EscolherEventoSessaoUI;
import eventoscientificos.ui.ReverArtigo.EscolherRevisavelUI;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class MenuUI extends JFrame {

    private Empresa m_empresa;
    private JTabbedPane tabPane;
    private JFileChooser fileChooser;
    private String str_ficheiroLocal;

//    public MenuUI(Empresa empresa) {
//        m_empresa = empresa;
//    }
    public MenuUI(String titulo, Empresa m_empresa) throws FileNotFoundException {
        super(titulo);
//        m_empresa = new Empresa();
        this.m_empresa = m_empresa;
//        this.str_ficheiroLocal = str_ficheiroLocal;
        JMenuBar menuBar = criarMenuBar();
        setJMenuBar(menuBar);
        pack();
        setSize(800, 400);
        tabPane = criarSeparadores();
        add(tabPane);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setVisible(true);
        new LoginUI(MenuUI.this, m_empresa);
    }

    private JMenuBar criarMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(criarMenu());

        return menuBar;
    }

    private JMenu criarMenu() {
        JMenu menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_T);
        menu.add(criarUtilizador());
        menu.addSeparator();
        menu.add(criarCriar());
        menu.addSeparator();
        menu.add(criarDefinirCP());
        menu.addSeparator();
        menu.add(criarSubmissao());
        menu.addSeparator();
        menu.add(reverArigo());
        menu.addSeparator();
        menu.add(criarImportar());
        menu.addSeparator();
        menu.add(criarExportar());
        menu.addSeparator();
        menu.add(criarGerarEstatisticas());
        menu.addSeparator();
        menu.add(criarImportarEvento());
        menu.add(criarImportarLocal());
        fileChooser = new JFileChooser();

        return menu;
    }

    private JMenu criarUtilizador() {

        JMenu menu = new JMenu("Utilizador");
        menu.setMnemonic(KeyEvent.VK_U);
        menu.add(alterarUtilizador());

        return menu;

    }

    private JMenuItem alterarUtilizador() {

        JMenuItem menu = new JMenuItem("Alterar Utilizador");
        menu.setMnemonic(KeyEvent.VK_R);
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new AlterarUtilizadorUI(MenuUI.this, m_empresa);
            }
        });
        return menu;
    }

    private JMenu criarCriar() {
        JMenu menu = new JMenu("Criar");
        menu.setMnemonic(KeyEvent.VK_C);
        menu.add(criarEvento());
        menu.add(criarSessao());
        return menu;
    }

    private JMenu criarSubmissao() {
        JMenu menu = new JMenu("Submissão");
        menu.setMnemonic(KeyEvent.VK_C);
        menu.add(criarSubmeter());
        menu.add(criarAlterarSubmissao());
        menu.add(criarRemoverSubmissao());
        menu.add(criarListarSubmissoesRetiradas());
        menu.add(criarSubmissaoFinal());
        return menu;
    }

    private JMenuItem criarAlterarSubmissao() {

        JMenuItem menu = new JMenuItem("Alterar Submissão");
        menu.setMnemonic(KeyEvent.VK_E);
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new AlterarSubmissaoUI(MenuUI.this, m_empresa);
            }
        });

        return menu;

    }

    private JMenuItem criarRemoverSubmissao() {

        JMenuItem menu = new JMenuItem("Remover Submissão");
        menu.setMnemonic(KeyEvent.VK_R);
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new EscolherEventoSessaoUI(MenuUI.this, m_empresa);
            }
        });
        return menu;
    }

    private JMenuItem criarListarSubmissoesRetiradas() {

        JMenuItem menu = new JMenuItem("Listar Submissões Retiradas");
        menu.setMnemonic(KeyEvent.VK_R);
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new ListarSubmissoesRetiradasUI(MenuUI.this, m_empresa);
            }
        });
        return menu;
    }
    private JMenuItem criarSubmissaoFinal() {

        JMenuItem menu = new JMenuItem("Submissão final");
        menu.setMnemonic(KeyEvent.VK_R);
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new SubmissaoFinal(MenuUI.this, m_empresa);
            }
        });
        return menu;
    }

    private JMenuItem criarEvento() {

        JMenuItem menu = new JMenuItem("Criar Evento");
        menu.setMnemonic(KeyEvent.VK_E);
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new CriarEventoUI(MenuUI.this, m_empresa);
            }
        });

        return menu;

    }

    private JMenuItem criarSessao() {
        JMenuItem menu = new JMenuItem("Criar Sessão");
        menu.setMnemonic(KeyEvent.VK_S);
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new EscolherEventoUI(MenuUI.this, m_empresa);
            }
        });
        return menu;

    }

    private JMenu criarDefinirCP() {
        JMenu menu = new JMenu("Definir CP");
        menu.setMnemonic(KeyEvent.VK_D);
        menu.add(definirEventoSessaoCP());

        return menu;

    }

    private JMenuItem definirEventoSessaoCP() {
        JMenuItem menu = new JMenuItem("Definir Evento/Sessão CP");
        menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new DefinirCPUI(MenuUI.this, m_empresa);

            }
        });
        return menu;

    }

    private JMenuItem criarSubmeter() {
        JMenuItem menu = new JMenuItem("Submeter Artigo");
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new SubmeterArtigoUI(MenuUI.this, m_empresa);

            }
        });
        return menu;
    }

    private JTabbedPane criarSeparadores() {

        JTabbedPane tabPane = new JTabbedPane();
        tabPane.addTab("Início", new PainelPrincipal());

        return tabPane;

    }

    private JMenuItem criarGerarEstatisticas() {
        JMenuItem item = new JMenuItem("Gerar Estatísticas");
        item.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                new GerarEstatisticasUI(MenuUI.this, m_empresa);
            }
        });
        return item;
    }

    private JMenuItem criarImportar() {
        JMenuItem menu = new JMenuItem("Importar ficheiro");
        menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                new SelecionarImportarFicheiroUI().run(MenuUI.this, m_empresa);

            }
        });
        return menu;

    }

    private JMenu criarExportar() {
        JMenu menu = new JMenu("Exportar Ficheiro");
        menu.setMnemonic(KeyEvent.VK_0);
        menu.add(exportarUtilizador());
        menu.add(exportarEvento());
//        menu.add(exportarLocal());

        return menu;

    }

    private JMenuItem exportarUtilizador() {
        JMenuItem menu = new JMenuItem("Exportar Utilizador");
        menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String utilizador = "Utilizador";
                new ExportarFicheiroUI().run(MenuUI.this, m_empresa, utilizador);

            }
        });
        return menu;
    }

    private JMenuItem exportarEvento() {
        JMenuItem menu = new JMenuItem("Exportar Evento");
        menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String evento = "Evento";
                new ExportarFicheiroUI().run(MenuUI.this, m_empresa, evento);

            }
        });
        return menu;

    }

    private JMenuItem reverArigo() {

        JMenuItem menu = new JMenuItem("Rever Artigo");

        menu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                new EscolherRevisavelUI(MenuUI.this, m_empresa);
            }
        });
        return menu;
    }

    private JMenuItem criarImportarEvento() {
        JMenuItem item = new JMenuItem("Importar Eventos", KeyEvent.VK_I);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser = new JFileChooser();
                definirFiltroExtensaoXml(fileChooser);
                try {

                    if (str_ficheiroLocal == null) {
                        throw new IllegalArgumentException();

                    }
                } catch (IllegalArgumentException w) {
                    JOptionPane.showMessageDialog(rootPane, "Importar primeiro ficheiro Local", "Erro", JOptionPane.ERROR_MESSAGE);

                }
                int resposta = fileChooser.showOpenDialog(MenuUI.this);
                if (resposta == JFileChooser.APPROVE_OPTION && str_ficheiroLocal != null) {

                    File file = fileChooser.getSelectedFile();

                    new LerEventoXML(file.getPath(), m_empresa, str_ficheiroLocal);

                }

            }
        });

        return item;
    }

    private JMenuItem criarImportarLocal() {
        JMenuItem item = new JMenuItem("Importar Local", KeyEvent.VK_I);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser = new JFileChooser();
                definirFiltroExtensaoXml(fileChooser);

                int resposta = fileChooser.showOpenDialog(MenuUI.this);

                if (resposta == JFileChooser.APPROVE_OPTION) {

                    File file = fileChooser.getSelectedFile();

                    try {
                        new ImportarLocalXml(file.getPath());
                        str_ficheiroLocal = file.getPath();
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(MenuUI.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SAXException ex) {
                        Logger.getLogger(MenuUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        });

        return item;
    }

    private void definirFiltroExtensaoXml(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("xml");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.xml";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

}
