/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoController;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author 1140921
 */
public class DadosOrganizadorUI extends JDialog {

    private JFrame framePai;
    private CriarEventoController m_controller;
    private JTextField txtDadosOrganizador;

    public DadosOrganizadorUI(JFrame framePai, CriarEventoController m_controller) {
        super(framePai);
        this.framePai = framePai;
        this.m_controller = m_controller;

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelDadosOrganizador());
        c.add(criarPainelBotoes());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarPainelDadosOrganizador() {
        JPanel dadosOrganizador = new JPanel();
        dadosOrganizador.add(new JLabel("Inserir ID Organizador :"));
        txtDadosOrganizador = new JTextField(15);
        dadosOrganizador.add(txtDadosOrganizador);

        return dadosOrganizador;
    }

    private JPanel criarPainelBotoes() {
        JPanel painelBtn = new JPanel();

        painelBtn.add(criarBtnAdicionar());

        return painelBtn;
    }

    private JButton criarBtnAdicionar() {
        JButton confirmarBtn = new JButton("Confirmar");

        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                String iD;
                try {
                    iD = txtDadosOrganizador.getText();
                    boolean e = m_controller.addOrganizador(iD);

                    if (e == false) {
                        throw new IllegalArgumentException("Utilizador inexistente");
                    }
                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(DadosOrganizadorUI.this, e.getMessage(), "Erro", JOptionPane.YES_NO_OPTION);
                }

                txtDadosOrganizador.setText(null);
                int i = JOptionPane.showConfirmDialog(DadosOrganizadorUI.this, "Deseja inserir mais Organizadores ?");
                if (i == JOptionPane.YES_OPTION) {
                    iD = txtDadosOrganizador.getText();
                    m_controller.addOrganizador(iD);

                }
                if (i == JOptionPane.NO_OPTION && m_controller.getEvento().getListaOrganizadores().isVazio() == false) {
                    dispose();
                    new JanelaMostrarEvento(framePai, m_controller);

                }
                if (i == JOptionPane.CANCEL_OPTION) {
                    JOptionPane.showMessageDialog(DadosOrganizadorUI.this, "Evento não foi registado");
                    dispose();
                }

            }
        });

        return confirmarBtn;
    }
}
