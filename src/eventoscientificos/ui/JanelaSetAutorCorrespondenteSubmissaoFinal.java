/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.controllers.SubmissaoFinalController;
import eventoscientificos.domain.Autor;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author 1140921
 */
class JanelaSetAutorCorrespondenteSubmissaoFinal extends JDialog {

    private JFrame framePai;
    private SubmissaoFinalController m_controller;
    private JList lstAutor;
    private JButton btnPromoverAutor, btnFicheiro, btnSubmissao;

    public JanelaSetAutorCorrespondenteSubmissaoFinal(JFrame framePai, SubmissaoFinalController m_controller) {
        super();
        this.framePai = framePai;
        this.m_controller = m_controller;
        setLayout(new GridLayout(0, 1));

        add(criarListaAutores(), BorderLayout.CENTER);

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarListaAutores() {

        JPanel p = new JPanel();
        ModeloListaAutoresFinal modeloListaAutores = new ModeloListaAutoresFinal(m_controller);
        lstAutor = new JList(modeloListaAutores);
        btnPromoverAutor = criarBotaoPromoverlAutorCorrespondente();
        btnFicheiro = criarBotaoFicheiro();
        btnSubmissao = criarBotaoCriarSubmissao();
        p.add(criarPainelLista(lstAutor, btnPromoverAutor, btnFicheiro, btnSubmissao));

        return p;

    }

    private JPanel criarPainelLista(JList lstAutor, JButton btnPromoverAutor, JButton btnFicheiro, JButton btnSubmissao) {
        lstAutor.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrPane = new JScrollPane(lstAutor);

        JPanel p = new JPanel(new BorderLayout());

        p.add(scrPane, BorderLayout.CENTER);

        JPanel pBotoes = criarPainelBotoes(btnPromoverAutor, btnFicheiro, btnSubmissao);
        p.add(pBotoes, BorderLayout.SOUTH);

        return p;
    }

    private JPanel criarPainelBotoes(JButton btn1, JButton btn2, JButton btn3) {

        JPanel p = new JPanel(new GridLayout());

        p.add(btn1);
        p.add(btn2);
        p.add(btn3);

        return p;
    }

    private JButton criarBotaoPromoverlAutorCorrespondente() {
        btnPromoverAutor = new JButton("Promover autor correspondente");

        btnPromoverAutor.setEnabled(lstAutor.getModel().getSize() != 0);
        btnPromoverAutor.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                Autor autorSelecionado = (Autor) lstAutor.getSelectedValue();
                if (autorSelecionado == null) {
                    JOptionPane.showMessageDialog(rootPane,
                            "Seleccione um autor.",
                            "Promover a autor correspondente",
                            JOptionPane.WARNING_MESSAGE);

                } else {
                    String[] itens = {"Sim", "Não"};
                    int resposta = JOptionPane.showOptionDialog(rootPane,
                            "Promover\n" + autorSelecionado.toString(),
                            "Promover autor a correspondente ?",
                            0,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            itens,
                            itens[1]);
                    final int SIM = 0;
                    if (resposta == SIM) {
                        try{
                            m_controller.setCorrespondente(autorSelecionado);
                        }catch(IllegalArgumentException y){
                            JOptionPane.showMessageDialog(rootPane, y.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
                        }
                        
                    }
                }
            }
        });

        return btnPromoverAutor;
    }

    private JButton criarBotaoFicheiro() {
        btnFicheiro = new JButton("Inserir ficheiro PDF");
        btnFicheiro.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                definirFiltroExtensaoPdf(fileChooser);
                int resposta = fileChooser.showOpenDialog(rootPane);
                if (resposta == JFileChooser.APPROVE_OPTION) {

                    File file = fileChooser.getSelectedFile();
                    m_controller.setFicheiro(file.getPath());

                    JOptionPane.showMessageDialog(rootPane, "Ficheiro PDF submetido");

                }

            }
        });
        return btnFicheiro;
    }

    private void definirFiltroExtensaoPdf(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("pdf");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.pdf";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    private JButton criarBotaoCriarSubmissao() {
        btnSubmissao = new JButton("Finalizar Submissão");

        btnSubmissao.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (m_controller.isAutorCorrespondente() == false) {
                        throw new NullPointerException("Não existe autor correspondente");
                    }
                    if(m_controller.isVazioPdf()){
                        throw new NullPointerException("Ficheiro pdf não foi submetido");
                    }
                    try{
                   boolean s= m_controller.registarSubmissao();
                   if(s == false){
                       throw new IllegalArgumentException("Artigo Repetido");
                   }
                    JOptionPane.showMessageDialog(rootPane, "Submissão Final realizada com sucesso ");

                    }catch(IllegalArgumentException s){
                        JOptionPane.showMessageDialog(rootPane, s.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                    dispose();
                } catch (NullPointerException i ) {
                    JOptionPane.showMessageDialog(rootPane, i.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);

                }

            }
        }
        );

        return btnSubmissao;
    }

}
