/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.ImportarFicheiroController;
import eventoscientificos.domain.Empresa;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author ASUS
 */
public class ImportarFicheiroUI extends javax.swing.JFrame {

    private Empresa m_empresa;
    private ImportarFicheiroController m_importarFicheiroController;
    private JFrame framePai;
    private String tipoFicheiro;

    /**
     * Classe ImportarFicheiroUI ,
     *
     * @author Andre Ribeiro
     */
    public ImportarFicheiroUI() {
        super("Importar Ficheiro XML");

        initComponents();
        jFileChooser1.setAcceptAllFileFilterUsed(false);
        jFileChooser1.setMultiSelectionEnabled(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Ficheiro xml", "xml");
        jFileChooser1.setFileFilter(filter);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jFileChooser1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFileChooser1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jFileChooser1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFileChooser1ActionPerformed
        
        String comand = evt.getActionCommand();
        
        if (comand.equalsIgnoreCase(JFileChooser.CANCEL_SELECTION)) {
            dispose();
        } else if(comand.equalsIgnoreCase(JFileChooser.APPROVE_SELECTION)){

            String utilizador = "Utilizador";
            String evento = "Evento";
            String local = "Local";
            //Seleciona o ficheiro escolhido pelo utilizador
            File ficheiroSelecionado = jFileChooser1.getSelectedFile();
            // Não é necessário converter para String , mas é feito para prevenir complicações futuras.
            String nomeFich = ficheiroSelecionado.getName();

            try {

                if (utilizador.equalsIgnoreCase(tipoFicheiro)) {

                    m_importarFicheiroController.importarFicheiroUtilizador(ficheiroSelecionado.getAbsolutePath());                    
                    dispose();

                } else if (evento.equalsIgnoreCase(tipoFicheiro)) {

                    //Executar Método de Importação Relacionado com Evento
                    JOptionPane.showMessageDialog(framePai, "Evento/s registado/s com sucesso");

                } else if (local.equalsIgnoreCase(tipoFicheiro)) {

                    //Executar Método de Importação Relacionado com Local
                    JOptionPane.showMessageDialog(framePai, "Local/is registado/s com sucesso");
                } else {
                    //Caso o ficheiro introduzido não tenha o nome correcto
                    JOptionPane.showMessageDialog(framePai, "Erro , verifique se o ficheiro está com o nome correcto!");
                    dispose();
                }

            } catch (NullPointerException a) {
                JOptionPane.showMessageDialog(ImportarFicheiroUI.this, a.getMessage(), "Null Erro", JOptionPane.ERROR_MESSAGE);

            } catch (Exception e) {

                JOptionPane.showMessageDialog(ImportarFicheiroUI.this, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);

            }
        }
    }//GEN-LAST:event_jFileChooser1ActionPerformed

    public void run(JFrame parentFrame, Empresa empresa, String tipoFich) {

        tipoFicheiro = tipoFich;
        framePai = parentFrame;
        m_empresa = empresa;
        m_importarFicheiroController = new ImportarFicheiroController(empresa);

        setVisible(true);
        setLocationRelativeTo(parentFrame);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser jFileChooser1;
    // End of variables declaration//GEN-END:variables
}
