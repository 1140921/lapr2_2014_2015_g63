
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Submissao;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

public class JanelaAlterarSub extends JDialog{

    private AlterarSubmissaoController alterarSubmissaoController;
    private JFrame framePai;
    private Submissao submissao;
    
    public JanelaAlterarSub(JFrame parentFrame, AlterarSubmissaoController alterarSubmissaoController, Empresa empresa, Submissao submissao){
        super(parentFrame,"Alterar Dados submissão");
        this.framePai = parentFrame;
        this.alterarSubmissaoController = alterarSubmissaoController;
        this.submissao = submissao;

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelButtons(), BorderLayout.CENTER);
//        c.add(criarPainelPassword());
//        c.add(criarPainelEmail());
//        c.add(criarPainelBotao());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
      
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonAlterarDadosArtigo());
        p.add(criarButtonAlterarDadosAutor());
        p.add(criarButtonAlterarAutorCorrespondente());
        p.add(criarButtonAlterarFicheiroPDF());
        p.add(criarButtonAlterarPalavrasChaves());
        p.add(criarButtonCancelar());
        return p;
    }
    
    public JButton criarButtonAlterarDadosArtigo(){
        JButton btn = new JButton("Alterar Dados de Artigo");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
                new JanelaAlterarDadosArtigo(framePai, alterarSubmissaoController);
            }
        });
        return btn;
    }
    
    public JButton criarButtonAlterarDadosAutor(){
        JButton btn = new JButton("Alterar dados de Autor");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
                
                new JanelaAlterarDadosAutor(framePai, submissao, alterarSubmissaoController);
            }
        });
        return btn;
    }
   
    public JButton criarButtonAlterarAutorCorrespondente(){
        JButton btn = new JButton("Alterar Autor Correspondente");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            
            new JanelaAlterarAutorCorrespondente(framePai, alterarSubmissaoController);
            }
        });
        return btn;
    }
    public JButton criarButtonAlterarPalavrasChaves(){
        JButton btn = new JButton("Alterar Palavras Chave");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            
            new JanelaAlterarAlterarPalavrasChave(framePai,submissao ,alterarSubmissaoController);
            }
        });
        return btn;
    }
    
     public JButton criarButtonAlterarFicheiroPDF(){
        JButton btn = new JButton("Alterar Ficheiro PDF");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                JFileChooser fileChooser = new JFileChooser();
                definirFiltroExtensaoPdf(fileChooser);
                int resposta = fileChooser.showOpenDialog(rootPane);
                if (resposta == JFileChooser.APPROVE_OPTION) {

                    File file = fileChooser.getSelectedFile();
                    alterarSubmissaoController.setFicheiro(file.getPath());

                    JOptionPane.showMessageDialog(rootPane, "Ficheiro PDF submetido");

                }
            }
        });
        return btn;
    }
     
            private void definirFiltroExtensaoPdf(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("pdf");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.pdf";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    public JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            
            dispose();
            }
        });
        return btn;
    }
}
