package eventoscientificos.ui;

import eventoscientificos.controllers.CriarEventoController;
import eventoscientificos.domain.Empresa;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author 1140921
 */
public class CriarEventoUI extends JDialog {

    private Empresa m_empresa;
    private CriarEventoController m_controller;
    private JFrame framePai;
    private JTextField txtTitulo, txtDescricao, txtDataInicio, txtDataFim, txtDataInicioSubmissao, txtDataFimSubmissao, txtLocal, txtDataInicioDistribuicao,txtDataLimiteRevisao,txtDataLmiteSubmissaoFinal;

    public CriarEventoUI(JFrame framePai, Empresa empresa) {
        super(framePai);
        this.framePai = framePai;
        m_empresa = empresa;
        m_controller = new CriarEventoController(m_empresa);

//        m_controller.novoEvento();
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelTitulo());
        c.add(criarPainelDescricao());
        c.add(criarPainelLocal());
        c.add(criarPainelDataInicio());
        c.add(criarPainelDataFim());
        c.add(criarPainelDataInicioSubmissao());
        c.add(criarPainelDataFimSubmissao());
        c.add(criarPainelDataInicioDistribuicao());
        c.add(criarPainelDataLimiteRevisao());
        c.add(criarPainelDataLmiteSubmissaoFinal());
        c.add(criarPainelBtn());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarPainelTitulo() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Titulo :"));
        txtTitulo = new JTextField(15);
        painelTitulo.add(txtTitulo);

        return painelTitulo;

    }

    private JPanel criarPainelDescricao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Descrição :"));
        txtDescricao = new JTextField(15);
        painelTitulo.add(txtDescricao);

        return painelTitulo;
    }

    private JPanel criarPainelLocal() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Local :"));
        txtLocal = new JTextField(15);
        painelTitulo.add(txtLocal);

        return painelTitulo;
    }

    private JPanel criarPainelDataInicio() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data inicio (yyyy/MM/dd):"));
        txtDataInicio = new JTextField(15);
        painelTitulo.add(txtDataInicio);

        return painelTitulo;
    }

    private JPanel criarPainelDataFim() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data fim (yyyy/MM/dd) :"));
        txtDataFim = new JTextField(15);
        painelTitulo.add(txtDataFim);

        return painelTitulo;
    }

    private JPanel criarPainelDataInicioSubmissao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data inicio Submissão (yyyy/MM/dd) :"));
        txtDataInicioSubmissao = new JTextField(15);
        painelTitulo.add(txtDataInicioSubmissao);

        return painelTitulo;
    }

    private JPanel criarPainelDataFimSubmissao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data fim Submissão (yyyy/MM/dd):"));
        txtDataFimSubmissao = new JTextField(15);
        painelTitulo.add(txtDataFimSubmissao);

        return painelTitulo;
    }

    private JPanel criarPainelDataInicioDistribuicao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data inicio Distribuição (yyyy/MM/dd):"));
        txtDataInicioDistribuicao = new JTextField(15);
        painelTitulo.add(txtDataInicioDistribuicao);

        return painelTitulo;
    }
    private JPanel criarPainelDataLimiteRevisao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data Limite Revisao (yyyy/MM/dd):"));
        txtDataLimiteRevisao = new JTextField(15);
        painelTitulo.add(txtDataLimiteRevisao);

        return painelTitulo;
    }
    private JPanel criarPainelDataLmiteSubmissaoFinal() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data Lmite Submissao Final (yyyy/MM/dd):"));
        txtDataLmiteSubmissaoFinal= new JTextField(15);
        painelTitulo.add(txtDataLmiteSubmissaoFinal);

        return painelTitulo;
    }

    private JPanel criarPainelBtn() {
        JPanel painelBtn = new JPanel();

        painelBtn.add(criarBtnConfirmar());

        return painelBtn;
    }

    private JButton criarBtnConfirmar() {
        JButton confirmarBtn = new JButton("Confirmar");

        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String strTitulo = txtTitulo.getText();
                    String strDescricao = txtDescricao.getText();
                    String lcl = txtLocal.getText();
                    Date dInicio = new Date(txtDataInicio.getText());
                    Date dFim = new Date(txtDataFim.getText());
                    Date dInicioSubmissao = new Date(txtDataInicioSubmissao.getText());
                    Date dFimSubmissao = new Date(txtDataFimSubmissao.getText());
                    Date dInicioDistribuicao = new Date(txtDataInicioDistribuicao.getText());
                    Date dLimiteRevisao = new Date(txtDataInicioDistribuicao.getText());
                    Date dLmiteSubmissaoFinal = new Date(txtDataInicioDistribuicao.getText());

                    m_controller.novoEvento();
                    m_controller.setTitulo(strTitulo);
                    m_controller.setDescricao(strDescricao);
                    m_controller.setLocal(lcl);
                    m_controller.setDataInicio(dInicio);
                    m_controller.setDataFim(dFim);
                    m_controller.setDataInicioSubmissao(dInicioSubmissao);
                    m_controller.setDataFimSubmissao(dFimSubmissao);
                    m_controller.setDataInicioDistribuicao(dInicioDistribuicao);
                    m_controller.setDataLimiteRevisao(dLimiteRevisao);
                    m_controller.setDataLmiteSubmissaoFinal(dLmiteSubmissaoFinal);

                    dispose();
                    new DadosOrganizadorUI(framePai, m_controller);

                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(CriarEventoUI.this, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);

                }

            }
        });

        return confirmarBtn;
    }

//    public void run() {
////        novoEvento();
//
////        Evento evento = introduzDadosEvento();
//
////        apresentaEvento(evento);
//    }
//    private void novoEvento() {
//        m_controller.novoEvento();
//    }
//    private Evento introduzDadosEvento() {
//        String strTitulo = Utils.readLineFromConsole("Introduza Titulo: ");
//        m_controller.setTitulo(strTitulo);
//
//        String strDescricao = Utils.readLineFromConsole("Introduza Descricao: ");
//        m_controller.setDescricao(strDescricao);
//
//        String strLocal = Utils.readLineFromConsole("Introduza Local: ");
//        m_controller.setLocal(strLocal);
//
//        Date dtInicio = Utils.readDateFromConsole("Introduza Data Inicio: ");
//        m_controller.setDataInicio(dtInicio);
//
//        Date dtFim = Utils.readDateFromConsole("Introduza Data Fim: ");
//        m_controller.setDataFim(dtFim);
//
//        Date dtInicioSubmissao = Utils.readDateFromConsole("Introduza Data Inicio Submissao: ");
//        m_controller.setDataInicioSubmissao(dtInicioSubmissao);
//
//        Date dtFimSubmissao = Utils.readDateFromConsole("Introduza Data Fim Submissao: ");
//        m_controller.setDataFimSubmissao(dtFimSubmissao);
//
//        Date dtInicioDistribuicao = Utils.readDateFromConsole("Introduza Data Inicio Distribuicao: ");
//        m_controller.setDataInicioDistribuicao(dtInicioDistribuicao);
//
//        while (Utils.confirma("Pretende inserir orgaizador (s/n)?")) {
//            String strOrg = Utils.readLineFromConsole("Introduza ID Organizador: ");
//            m_controller.addOrganizador(strOrg);
//        }
//
//        apresentaEvento(m_controller.getEvento());
//
//        if (Utils.confirma("Confirma?")) {
//            return m_controller.registaEvento();
//        }
//        return null;
//    }
//    private void apresentaEvento(Evento evento) {
//        if (evento == null) {
//            System.out.println("Evento não registado.");
//        } else {
//            System.out.println(evento.toString());
//        }
//    }
}
