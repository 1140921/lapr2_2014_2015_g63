/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.domain.Autor;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author 1140921
 */
class JanelaAdicionarAutor extends JDialog {

    private JFrame framePai;
    private SubmeterArtigoController m_controller;

    private JTextField txtNomeAutor, txtAfiliacao, txtEmail,txtUsernameAutor;

    public JanelaAdicionarAutor(JFrame framePai, SubmeterArtigoController m_controller) {
        super();
        this.framePai = framePai;
        this.m_controller = m_controller;

        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelAutor());
        c.add(criarPainelUsernameAutor());
        c.add(criarPainelAutorEmail());

        c.add(criarPainelAfiliacao());

        c.add(criarPainelBtn());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);

    }

    private JPanel criarPainelAutor() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Nome :"));
        txtNomeAutor = new JTextField(15);
        painelTitulo.add(txtNomeAutor);

        return painelTitulo;
    }
    private JPanel criarPainelUsernameAutor() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Username :"));
        txtUsernameAutor = new JTextField(15);
        painelTitulo.add(txtUsernameAutor);

        return painelTitulo;
    }

    private JPanel criarPainelAutorEmail() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Email Autor :"));
        txtEmail = new JTextField(15);
        painelTitulo.add(txtEmail);

        return painelTitulo;
    }

    private JPanel criarPainelAfiliacao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Instituição Afiliação :"));
        txtAfiliacao = new JTextField(15);
        painelTitulo.add(txtAfiliacao);

        return painelTitulo;
    }

    private JPanel criarPainelBtn() {

        JPanel painelBtn = new JPanel();

        painelBtn.add(criarBtnConfirmar());
        painelBtn.add(criarBtnCancelar());

        return painelBtn;
    }

    private JButton criarBtnCancelar() {
        JButton confirmarBtn = new JButton("Cancelar");

        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();

                JOptionPane.showMessageDialog(rootPane, "Submissão Artigo não foi concluida");

            }
        });

        return confirmarBtn;
    }

    private JButton criarBtnConfirmar() {
        JButton confirmarBtn = new JButton("Confirmar");

        confirmarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                try {
                    String nomeAutor = txtNomeAutor.getText();
                    String usernameAutor = txtUsernameAutor.getText();
                    String emailAutor = txtEmail.getText();
                    String afiliacao = txtAfiliacao.getText();
                    Autor a = m_controller.novoAutor(usernameAutor,nomeAutor, emailAutor, afiliacao);
                    int resposta = JOptionPane.showConfirmDialog(rootPane, "Adicionar " + "\nAutor :" + a + " ? ", "Confirmação", JOptionPane.YES_NO_OPTION);

                    if (resposta == JOptionPane.YES_OPTION) {
                        
                        m_controller.addAutor(a);
                        txtUsernameAutor.setText(null);
                        txtNomeAutor.setText(null);
                        txtEmail.setText(null);
                        txtAfiliacao.setText(null);
                    }

                    if (resposta == JOptionPane.NO_OPTION) {
                        txtNomeAutor.setText(null);
                        txtEmail.setText(null);

                        txtAfiliacao.setText(null);
                    }
                    int resp = JOptionPane.showConfirmDialog(rootPane, "Deseja adicionar mais autores ?", "", JOptionPane.YES_NO_OPTION);
                    if (resp == JOptionPane.YES_OPTION) {
                        txtNomeAutor.setText(null);
                        txtEmail.setText(null);

                        txtAfiliacao.setText(null);
                    }
                    if (resp == JOptionPane.NO_OPTION && m_controller.isVazioAutores() == false) {
                        dispose();
                        new JanelaSetAutorCorrespondente(framePai, m_controller);
                    } else if (resp == JOptionPane.NO_OPTION && m_controller.isVazioAutores() == true) {
                        JOptionPane.showMessageDialog(rootPane, "Submissão Artigo não foi concluida");
                    }

                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

        return confirmarBtn;
    }

}
