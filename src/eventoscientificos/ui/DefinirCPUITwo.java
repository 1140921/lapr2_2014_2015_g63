/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.DefinirCPController;
import eventoscientificos.domain.CPDefinivel;
import eventoscientificos.domain.CPEvento;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.RegistoUtilizadores;
import eventoscientificos.domain.Revisor;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import utils.Utils;

/**
 *
 * @author Paisana
 */
public class DefinirCPUITwo extends JDialog {


    private DefinirCPController m_controller;
    private JButton adicionarCP;
    private JButton cancelarCP;
    private JTextField membroCP;


    public DefinirCPUITwo(JFrame parentFrame, DefinirCPController m_controller) {
        super(parentFrame, "Introdução de revisor");

        this.m_controller = m_controller;
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelInserirIDRevisor());
        c.add(criarPainelBotoes());
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(parentFrame);
        setVisible(true);

    }

    private JPanel criarPainelInserirIDRevisor() {
        JPanel p = new JPanel();
        p.add(new Label("Introduza ID Membro CP:"));
        membroCP = new JTextField(15);
        p.add(membroCP);
        return p;
    }

    private JPanel criarPainelBotoes() {
        JPanel p = new JPanel();
        p.add(criarBotaoAdicionarCp());
        p.add(criarBotaoCancelarCp());
        return p;
    }

    private JButton criarBotaoAdicionarCp() {
        adicionarCP = new JButton("Adicionar");
        adicionarCP.setToolTipText("Adiciona revisor a CP");
        adicionarCP.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    Revisor r = m_controller.novoMembroCP(membroCP.getText());
                    

                    int resposta = JOptionPane.showConfirmDialog(DefinirCPUITwo.this, "Quer tornar revisor este utilizador?" + r.toString());

                    if (resposta == JOptionPane.OK_OPTION) {
                        if(m_controller.addMembroCP(r)){
                            
                            JOptionPane.showMessageDialog(rootPane, "Revisor adicionado com sucesso");
                        }
                    

                    }else if(resposta == JOptionPane.NO_OPTION){
                        membroCP.setText(null);
                    }
                    
                    int continua = JOptionPane.showConfirmDialog(DefinirCPUITwo.this, "Deseja inserir outro revisor ?");

                    if (continua == JOptionPane.OK_OPTION) {
                        membroCP.setText(null);
                    }else if(continua == JOptionPane.NO_OPTION ){ //verificar se existe revisores adicionados
                        m_controller.registaCP();
                        
                        dispose();
                    }

                }catch (NullPointerException c) {
                    JOptionPane.showMessageDialog(DefinirCPUITwo.this, "Tem que introduzir um utilizador previamente registado, ou o utilizador que introduziu já foi definido como revisor anteriormente.","Erro" , JOptionPane.ERROR_MESSAGE);
                    
                }

            }

        });
        return adicionarCP;

    }

    private JButton criarBotaoCancelarCp() {
        cancelarCP = new JButton("Cancelar");

        cancelarCP.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();

            }
        });
        return cancelarCP;

    }

}
