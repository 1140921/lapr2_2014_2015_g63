
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.controllers.SubmeterArtigoController;
import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.ListaAutores;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

public class JanelaAlterarDadosAutor extends JDialog{
    private JFrame framePai;
    private JLabel lblSelecionar;
    private JComboBox<Autor> listComboBox; 
    private Autor m_autor;
    private ListaAutores s;
    private AlterarSubmissaoController controller;
    private Submissao submissao;
    
    private JButton btnSelecionarAutor, btnCancelar;

    public JanelaAlterarDadosAutor(JFrame parentFrame, Submissao submissao, AlterarSubmissaoController controller) {

        super(parentFrame, "Alterar Submissão");
        
        this.framePai = parentFrame;
        this.submissao= submissao;
        this.controller = controller;

        setLayout(new GridLayout(0, 1));

        Container c = getContentPane();
        c.add(criarPainelNorte(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
}
   
    public JPanel criarPainelNorte(){
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        lblSelecionar = new JLabel("Selecione o autor que pretende alterar");
        listComboBox = new JComboBox();
        List<Autor> lstAutores = controller.getListaAutores();
        for (Autor autores : lstAutores) {
         
            listComboBox.addItem(autores);
        }
        p.add(lblSelecionar);
        p.add(listComboBox);
                
        
        
        return p;
    }
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonSelecionarAutor());
        p.add(criarButtonCancelar());
        
        return p;
    }
    
    public JButton criarButtonSelecionarAutor(){
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            
            Autor autor = (Autor) listComboBox.getSelectedItem();
            controller.setAutor(autor);
            new JanelaAlterarDadosAutorS(framePai, controller, autor);

            }
        });
        return btn;
    }
    
    public JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();                
            }
        });
        return btn;
    }
}