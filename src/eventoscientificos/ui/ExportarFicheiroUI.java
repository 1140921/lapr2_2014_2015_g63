/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.ExportarFicheiroController;
import eventoscientificos.domain.Empresa;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author ASUS
 */
public class ExportarFicheiroUI extends javax.swing.JFrame {

    private Empresa m_empresa;
    private JFrame framePai;
    private ExportarFicheiroController m_exportarFicheiroController;
    private String m_tipoFicheiroExportar;

    public ExportarFicheiroUI() {
        initComponents();
        jFileChooser2.setAcceptAllFileFilterUsed(false);
        jFileChooser2.setMultiSelectionEnabled(false);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Ficheiro xml", "xml");
        jFileChooser2.setFileFilter(filter);
        jFileChooser2.setApproveButtonText("Guardar");

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser2 = new javax.swing.JFileChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jFileChooser2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFileChooser2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jFileChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jFileChooser2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jFileChooser2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFileChooser2ActionPerformed
        
        String comand = evt.getActionCommand();
        if (comand.equalsIgnoreCase(JFileChooser.APPROVE_SELECTION)) {
            String utilizador = "Utilizador";
            String evento = "Evento";
            File ficheiro;
            ficheiro = jFileChooser2.getSelectedFile();
            
           if( utilizador.equalsIgnoreCase(m_tipoFicheiroExportar)){
               
            m_exportarFicheiroController.exportarFicheiroUtilizador(ficheiro.getAbsolutePath());
            dispose();
           }else if(evento.equalsIgnoreCase(m_tipoFicheiroExportar)){
            m_exportarFicheiroController.exportarFicheiroEvento(ficheiro.getAbsolutePath());
            dispose();
           }
        } else if (comand.equalsIgnoreCase(JFileChooser.CANCEL_SELECTION)) {
            dispose();
        }
    }//GEN-LAST:event_jFileChooser2ActionPerformed

    public void run(JFrame parentFrame, Empresa empresa, String tipoFicheiro) {
        
        m_tipoFicheiroExportar=tipoFicheiro;
        framePai = parentFrame;
        m_empresa = empresa;
        m_exportarFicheiroController = new ExportarFicheiroController(empresa);

        setVisible(true);
        setLocationRelativeTo(parentFrame);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser jFileChooser2;
    // End of variables declaration//GEN-END:variables
}
