

package eventoscientificos.ui;

import eventoscientificos.controllers.GerarEstatisticasController;
import eventoscientificos.domain.Revisor;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class JanelaApresentarEstatisticasRevisorEvento extends JDialog{

    private GerarEstatisticasController m_controller;
    private Revisor m_revisor;
    private JFrame framePai;
    private JLabel lblTesteHipoteses, lblHipoteses, lblRevisor, lblZobservado, lblEstatisticas, lblDesvioPadrao, lblZcritico;
    private JTextField txtZobservado, txtDesvio;
    private int indexEvento, m_s;
    
    public JanelaApresentarEstatisticasRevisorEvento(JFrame parentFrame, int indexEvento, int indexRevisor, Revisor revisor, GerarEstatisticasController m_controller){
        
        super(parentFrame);
        this.framePai = parentFrame;
        this.m_controller = m_controller;
        this.indexEvento = indexEvento;
        this.m_s = indexRevisor;
        this.m_revisor = revisor;
        
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelApresentarEstatisticaRevisor());
        c.add(criarPainelButtons());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
    public JPanel criarPainelApresentarEstatisticaRevisor(){
        JPanel p = new JPanel();
        
        lblRevisor = new JLabel("O Revisor "+m_revisor.getStrUsername()+" é retido se o Zobservado for maior que o Zcrítico");
        lblEstatisticas = new JLabel("    Algumas estatísticas:");
        lblDesvioPadrao = new JLabel("    Desvio Padrão: ");
        txtDesvio = new JTextField(10); 
        txtDesvio.setText(""+m_controller.getDesvioPadrao(indexEvento,m_s));
        txtDesvio.setEditable(false);
        lblZobservado = new JLabel("    Z observado: ");
        txtZobservado = new JTextField(10);
        txtZobservado.setText(""+m_controller.getZobservado(indexEvento,m_s));
        txtZobservado.setEditable(false);
        lblZcritico = new JLabel("    Z crítico: 1,645");
        lblTesteHipoteses = new JLabel("    Zo > Zc ===> rejeita-se Ho");
        lblHipoteses = new JLabel("    Zo < Zc ===> não se rejeita Ho");
        p.add(lblRevisor);
        p.add(lblEstatisticas);
        p.add(lblDesvioPadrao);
        p.add(txtDesvio);
        p.add(lblZobservado);
        p.add(txtZobservado);
        p.add(lblZcritico);
        p.add(lblTesteHipoteses);
        p.add(lblHipoteses);
        
        return p;
    }
    
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonConcluirGerarEstatisticas());
        
        return p;
    }
    
    public JButton criarButtonConcluirGerarEstatisticas(){
        JButton b = new JButton("Concluir Gerar Estatísticas");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            
                dispose();
            }
        });
        return b;
    }
}
