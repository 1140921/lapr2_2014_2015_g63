
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.ListaAutores;
import eventoscientificos.domain.Submissao;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class JanelaAlterarAutorCorrespondente extends JDialog{

    private JFrame framePai;
    private JLabel lblSelecionar;
    private JComboBox<Autor> listComboBox; 
    private Autor m_autor;
    private ListaAutores s;
    private AlterarSubmissaoController controller;
    private Submissao submissao;
    
    private JButton btnSelecionarAutor, btnCancelar;

    public JanelaAlterarAutorCorrespondente(JFrame parentFrame, AlterarSubmissaoController controller) {

        super(parentFrame, "Alterar Submissão");
        
        this.framePai = parentFrame;
        this.submissao= submissao;
        this.controller = controller;

        setLayout(new GridLayout(0, 1));

        Container c = getContentPane();
        c.add(criarPainelNorte(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
}
    
    public JPanel criarPainelNorte(){
        JPanel p = new JPanel();
        lblSelecionar = new JLabel("Selecione o novo Autor Correspondente");
        listComboBox = new JComboBox();
        List<Autor> lstAutores = controller.getListaAutores();
        for (Autor autores : lstAutores) {
         
            listComboBox.addItem(autores);
        }
        p.add(listComboBox);
                
        p.add(lblSelecionar);
        
        return p;
    }
    
    public JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        
        p.add(criarButtonAlterarAutorCorrespondente());
        p.add(criarButtonCancelar());
        
        return p;
    }
    
    public JButton criarButtonAlterarAutorCorrespondente(){
        JButton btn = new JButton("Alterar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            Autor autor = (Autor) listComboBox.getSelectedItem();
            try{
            controller.alterarAutorCorrespondente(autor);
            }catch(IllegalArgumentException y){
                            JOptionPane.showMessageDialog(rootPane, y.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
                        }
            }
        });
        return btn;
    }
    
    public JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener(){
        
        @Override 
        public void actionPerformed(ActionEvent ae) {
            dispose();
        }
        });
        return btn;
        }
    
    
}
