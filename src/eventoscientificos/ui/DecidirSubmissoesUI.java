/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.DecidirSubmissoesController;
import eventoscientificos.domain.Decidivel;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Decisao;
import eventoscientificos.domain.MecanismoDecisao;
import java.util.List;
import java.util.ListIterator;
import utils.Utils;

/**
 *
 * @author nunosilva
 */
class DecidirSubmissoesUI implements UI
{
    private Empresa m_empresa;
    private DecidirSubmissoesController m_controller;

    public DecidirSubmissoesUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controller = new DecidirSubmissoesController(m_empresa);
    }
    
    public void run()
    {
        String strIdOrgProp=Utils.readLineFromConsole("Introduza id: ");
        Decidivel d;
        MecanismoDecisao m;
        
        List<Decidivel> ld = m_controller.getDecisiveis(strIdOrgProp);
        apresentaDecidiveis(strIdOrgProp,  ld);
        d=selecionaDecidivel(ld);
        m_controller.novoProcessoDecisao(d);
       
        List<MecanismoDecisao> lm = m_controller.getMecanismosDecisao();
        apresentaMecanismos(lm);
        m=selecionaMecanismo(lm);
        m_controller.setMecanismoDecisao(m);
        
        solicitaAceitacao();
        
        m_controller.notifica();
        m_controller.registaPD();
        

    }
    
    public void solicitaAceitacao()
    {
        List<Decisao> ld = m_controller.getDecisoes();
        
        for( ListIterator<Decisao> it = ld.listIterator(); it.hasNext(); )
        {
            Decisao d = it.next();
            apresentaDecisao( d );
            String a = Utils.readLineFromConsole("Introduza aceitacao: ");
            m_controller.setAceitacao(d,a);
        }
    }

    private void apresentaDecisao( Decisao d )
    {
        if(d == null)
            System.out.println("Decisao inválida.");
        else
            System.out.println(d.toString() );
    }
    
    private void apresentaDecidiveis(String strIdOrgProp, List<Decidivel> ld)
    {
        System.out.println("Eventos e sessões temáticas de " + strIdOrgProp + ":");
        
        int index = 0;
        for(Decidivel d : ld)
        {
            index++;

            System.out.println(index + ". " + d.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private Decidivel selecionaDecidivel(List<Decidivel> ld)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > ld.size());

        if( nOpcao == 0 )
            return null;
        else
            return ld.get(nOpcao - 1);
    }

    private void apresentaMecanismos(List<MecanismoDecisao> lm)
    {
        System.out.println("Mecanismos de decisão:");
        
        int index = 0;
        for(MecanismoDecisao m : lm)
        {
            index++;

            System.out.println(index + ". " + m.toString());
        }
        System.out.println("");
        System.out.println("0 - Cancelar");
    }

    private MecanismoDecisao selecionaMecanismo(List<MecanismoDecisao> lm)
    {
        String opcao;
        int nOpcao;
        do
        {
            opcao = Utils.readLineFromConsole("Introduza opção: ");
            nOpcao = new Integer(opcao);
        }
        while (nOpcao < 0 || nOpcao > lm.size());

        if( nOpcao == 0 )
            return null;
        else
            return lm.get(nOpcao - 1);
    }
            
}
