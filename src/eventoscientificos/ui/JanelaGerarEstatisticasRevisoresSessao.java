
package eventoscientificos.ui;

import eventoscientificos.controllers.GerarEstatisticasController;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.Revisor;
import eventoscientificos.domain.SessaoTematica;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class JanelaGerarEstatisticasRevisoresSessao extends JDialog{

    private JFrame framePai;
    private GerarEstatisticasController m_controller;
    private SessaoTematica m_sessao;
    private JComboBox<Revisor> lstRevisores;
    private int indexSessao, indexEvento;
    
    
    public JanelaGerarEstatisticasRevisoresSessao(JFrame framePai, int m_s, int j, SessaoTematica sessao, GerarEstatisticasController m_controller) {
        this.framePai = framePai;
        this.m_controller = m_controller;
        this.indexEvento = m_s;
        this.indexSessao = j;
        this.m_sessao = sessao;
        
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelGerarEstatisticasRevisorSessao());
        c.add(criarPainelButtons());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }
    
    public JPanel criarPainelGerarEstatisticasRevisorSessao(){
        
        JPanel p = new JPanel();
        lstRevisores = new JComboBox();
        List<Revisor> revisoresSessao = m_controller.getListaRevisoresSessao(indexEvento, indexSessao);
        for(Revisor r : revisoresSessao){
            lstRevisores.addItem(r);
        }
        p.add(lstRevisores);
        return p;
    }
    
    public JPanel criarPainelButtons(){
        
        JPanel p = new JPanel();
        
        p.add(criarButtonGerarEstatisticas());
        p.add(criarButtonCancelar());
        
        return p;
    }
    
    public JButton criarButtonGerarEstatisticas(){
        
        JButton b = new JButton("Gerar Estatisticas");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            
            Revisor revisor = (Revisor) lstRevisores.getSelectedItem();
            int s = lstRevisores.getSelectedIndex();
//            m_controller.setRevisor(revisor);
            try{
            m_controller.getClassificacaoFinalDeCadaRevisaoSessao(indexEvento, indexSessao, s);
            m_controller.getSomatorioAtributosClassificacoesSessao(indexEvento, indexSessao, s);
            m_controller.getMediaDoSomatorioClassificacoesSessao(indexEvento, indexSessao, s);
            m_controller.getCalculoParaCadaRevisaoSessao(indexEvento, indexSessao, s);
            m_controller.getSomatorioDiSessao(indexEvento, indexSessao, s);
            m_controller.getMediaDiSessao(indexEvento, indexSessao, s);
            m_controller.getSomatorioVarianciaSessao(indexEvento, indexSessao, s);
            m_controller.getVarianciaSessao(indexEvento, indexSessao, s);
            m_controller.getDesvioPadraoSessao(indexEvento, indexSessao, s);
            m_controller.getZobservadoSessao(indexEvento, indexSessao, s);
            
            m_controller.isTesteHipotesesSessao(indexEvento, indexSessao, s);
            m_controller.rejeitaAceitaRevisorSessao(indexEvento, indexSessao, s);
                } catch (IllegalArgumentException e) {
                    JOptionPane.showMessageDialog(JanelaGerarEstatisticasRevisoresSessao.this, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }
            new JanelaApresentarEstatisticasRevisorSessao(framePai, indexEvento, indexSessao, s, revisor, m_controller);
            }
        });
        return b;
    }
    
    public JButton criarButtonCancelar(){
        
        JButton b = new JButton("Cancelar");
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
            dispose();
            }
        });
        return b;
    }
}
