
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarUtilizadorController;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Login;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AlterarUtilizadorUI extends JDialog {

    private Empresa m_empresa;
    private JFrame framePai;
    private AlterarUtilizadorController m_controllerRU;
    private JLabel lblUsername, lblNome, lblPwd, lblEmail;
    private JTextField txtUsername, txtNome, txtPwd, txtEmail;
    private JButton btnAlterarUtilizador, btnCancelar;

    //private static final Dimension LABEL_TAMANHO = new JLabel("Posição:").getPreferredSize(); 
    
    public AlterarUtilizadorUI(JFrame parentFrame, Empresa empresa) {

        super(parentFrame, "Alterar Utilizador");
        framePai = parentFrame;
        m_empresa = empresa;
        m_controllerRU = new AlterarUtilizadorController(m_empresa);
//        m_controllerRU.getUtilizador();
        add(criarPainelNorte(),BorderLayout.CENTER);
        add(criarPainelButtons(),BorderLayout.SOUTH);
        
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
      }
    
    public JPanel criarPainelNorte(){
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        String username = m_controllerRU.getUtilizador().getUsername();
        String nome = m_controllerRU.getUtilizador().getNome();
        String pwd = m_controllerRU.getUtilizador().getPwd();
        String email = m_controllerRU.getUtilizador().getEmail();
        
        lblUsername = new JLabel("Username");
        txtUsername = new JTextField(10);
        txtUsername.setText(username);
        lblNome = new JLabel("Nome");
        txtNome = new JTextField(10);
        txtNome.setText(nome);
        lblPwd = new JLabel("Password");
        txtPwd = new JTextField(10);
        txtPwd.setText(pwd);
        lblEmail = new JLabel("Email");
        txtEmail = new JTextField(10);
        txtEmail.setText(email);
        
        p.add(lblUsername);
        p.add(txtUsername);
        p.add(lblNome);
        p.add(txtNome);
        p.add(lblPwd);
        p.add(txtPwd);
        p.add(lblEmail);
        p.add(txtEmail);
        

        return p;
    }
    
    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonAlterar());
        p.add(criarButtonCancelar());
        return p;
    }
    
    public JButton criarButtonCancelar(){
        btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                dispose();
            }
        });
        return btnCancelar;
    }
    public JButton criarButtonAlterar(){
        btnAlterarUtilizador = new JButton("Alterar Utilizador");
        btnAlterarUtilizador.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                    int resposta = JOptionPane.showConfirmDialog(framePai,"Tem a certeza?",
                            "Confirmacao",JOptionPane.YES_NO_OPTION);
                    
                    if(resposta==JOptionPane.YES_OPTION){
                        
                    String username = txtUsername.getText().trim();
                    String nome = txtNome.getText();
                    String password = txtPwd.getText();
                    String email = txtEmail.getText();
                    
                    try{
                    if(m_controllerRU.alteraDados(nome, username, password, email)){
                    m_controllerRU.setLogin(new Login(username,password));
                    JOptionPane.showMessageDialog(framePai,"Utilizador alterado com sucesso");                   
                    dispose();
                    }else{
                        JOptionPane.showMessageDialog(framePai,"Impossivel registar utilizador");
                    }
                    }catch(IllegalArgumentException ex){
                        JOptionPane.showMessageDialog(framePai,ex.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
                    }   catch (FileNotFoundException ex) {
                            Logger.getLogger(AlterarUtilizadorUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                 }
            }
        });
        return btnAlterarUtilizador;
    }
}
//class AlterarUtilizadorUI implements UI
//{
//    private Empresa m_empresa;
//    private AlterarUtilizadorController m_controller;
//
//    public AlterarUtilizadorUI( Empresa empresa )
//    {
//        m_empresa = empresa;
//        m_controller = new AlterarUtilizadorController(m_empresa);
//    }
//
//    @Override
//    public void run()
//    {
//        String strUser = Utils.readLineFromConsole("Introduza ID Utilizador: ");
//        Utilizador u = m_controller.getUtilizador(strUser);
//        
//        u.toString();
//        
//        String strNome = Utils.readLineFromConsole("Novo Nome:").trim();
//        if (strNome.isEmpty())
//            strNome = u.getNome();
//        String strUsername = Utils.readLineFromConsole("Novo Username:").trim();
//        if (strUsername.isEmpty())
//            strUsername = u.getUsername();
//        String strPwd = Utils.readLineFromConsole("Nova Pwd:").trim();
//        if (strPwd.isEmpty())
//            strPwd = u.getPwd();
//        String strEmail = Utils.readLineFromConsole("Novo Email:").trim();
//        if (strEmail.isEmpty())
//            strPwd = u.getEmail();
//        
//        if(m_controller.alteraDados(strNome, strUsername, strPwd, strEmail))
//            System.out.println("Utilizador alterado com sucesso");
//    }
//}
