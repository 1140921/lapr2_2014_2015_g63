package eventoscientificos.ui.CriarSessaoTematica;

import eventoscientificos.controllers.CriarSessaoTematicaController;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class IntroduzirDadosUI extends JDialog {

    private JFrame jframePai;
    private JTextField txtCodigo,txtDescricao, txtDataInicioSubmissao, txtDataFimSubmissao,
            txtDataInicioDistribuicao,txtDataLimiteRevisao,txtDataLimiteSubmissaoFinal;
    private CriarSessaoTematicaController criarSessaoController;

    public IntroduzirDadosUI(JFrame jframePai, CriarSessaoTematicaController criarSessaoController) {
        super(jframePai, "Introduzir dados");
        this.jframePai = jframePai;
        this.criarSessaoController = criarSessaoController;
       
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelCodigo());
        c.add(criarPainelDescricao());
        c.add(criarPainelDataInicioSubmissao());
        c.add(criarPainelDataFimSubmissao());
        c.add(criarPainelDataInicioDistribuicao());
        c.add(criarPainelDataLimiteRevisao());
        c.add(criarPainelDataLmiteSubmissaoFinal());
        add(criarPainelButtons());
        
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }

    
    
    private JPanel criarPainelCodigo() {
        JPanel p = new JPanel();
        p.add(new JLabel("Codigo :"));
        txtCodigo = new JTextField(15);
        p.add(txtCodigo);

        return p;

    }

    private JPanel criarPainelDescricao() {
        JPanel p = new JPanel();
        p.add(new JLabel("Descrição :"));
        txtDescricao = new JTextField(15);
        p.add(txtDescricao);

        return p;
    }

  



    private JPanel criarPainelDataInicioSubmissao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data inicio Submissão (yyyy/MM/dd) :"));
        txtDataInicioSubmissao = new JTextField(15);
        painelTitulo.add(txtDataInicioSubmissao);

        return painelTitulo;
    }

    private JPanel criarPainelDataFimSubmissao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data fim Submissão (yyyy/MM/dd):"));
        txtDataFimSubmissao = new JTextField(15);
        painelTitulo.add(txtDataFimSubmissao);

        return painelTitulo;
    }

    private JPanel criarPainelDataInicioDistribuicao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data inicio Distribuição (yyyy/MM/dd):"));
        txtDataInicioDistribuicao = new JTextField(15);
        painelTitulo.add(txtDataInicioDistribuicao);

        return painelTitulo;
    }
    private JPanel criarPainelDataLimiteRevisao() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data Limite Revisao (yyyy/MM/dd):"));
        txtDataLimiteRevisao = new JTextField(15);
        painelTitulo.add(txtDataLimiteRevisao);

        return painelTitulo;
    }
    private JPanel criarPainelDataLmiteSubmissaoFinal() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Data Limite Submissao Final (yyyy/MM/dd):"));
        txtDataLimiteSubmissaoFinal= new JTextField(15);
        painelTitulo.add(txtDataLimiteSubmissaoFinal);

        return painelTitulo;
    }
    
   

    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonConfirmar());
        p.add(criarButtonCancelar());
        return p;
    }

    private JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JButton criarButtonConfirmar() {
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Date d1 = new Date(txtDataInicioSubmissao.getText());
                    Date d2 = new Date(txtDataFimSubmissao.getText());
                    Date d3 = new Date(txtDataInicioDistribuicao.getText());
                    Date d4 = new Date(txtDataLimiteRevisao.getText());
                    Date d5 = new Date(txtDataLimiteSubmissaoFinal.getText());
                    try {
                        criarSessaoController.setDados(txtCodigo.getText(), txtDescricao.getText(), d1, d2, d3,d4,d5);

                        dispose();
                        new IntroduzirProponenteUI(jframePai, criarSessaoController);
                    } catch (IllegalArgumentException ex) {
                        JOptionPane.showMessageDialog(jframePai, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(jframePai, "Data Invalida", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        return btn;
    }
}
