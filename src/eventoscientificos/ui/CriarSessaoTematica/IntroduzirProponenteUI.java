package eventoscientificos.ui.CriarSessaoTematica;

import eventoscientificos.controllers.CriarSessaoTematicaController;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class IntroduzirProponenteUI extends JDialog {

    private JFrame jframePai;
    private CriarSessaoTematicaController criarSessaoController;
    private JTextField txtID;

    public IntroduzirProponenteUI(JFrame jframePai, CriarSessaoTematicaController criarSessaoController) {
        super(jframePai, "Introduzir Proponente");
        this.jframePai = jframePai;
        this.criarSessaoController = criarSessaoController;
        add(criarPainelIntroduzirProponente(), BorderLayout.CENTER);
        add(criarPainelButtons(), BorderLayout.SOUTH);
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }

    private JPanel criarPainelIntroduzirProponente() {
        JPanel p = new JPanel();
        JLabel lbl = new JLabel("Introduzir ID do proponente:");
        txtID = new JTextField(10);
        p.add(lbl);
        p.add(txtID);
        return p;
    }

    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonConfirmar());
        p.add(criarButtonCancelar());
        return p;
    }

    private JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

    private JButton criarButtonConfirmar() {
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                boolean b1 =criarSessaoController.addProponente(txtID.getText());
                if(b1==false){
                JOptionPane.showMessageDialog(jframePai,"Utilizador nao existe no sistema","Erro",JOptionPane.ERROR_MESSAGE);
                }else{
                int resposta = JOptionPane.showConfirmDialog(jframePai, "Tem a certeza", "Confirmação",
                        JOptionPane.YES_NO_OPTION);
                
                if (resposta == JOptionPane.YES_OPTION) {
                        
                        boolean b2 = criarSessaoController.registaProponente();

                        if (b2 == true) {
                            JOptionPane.showMessageDialog(jframePai, "Proponente: " + txtID.getText() + " registado");
                        } else {
                            JOptionPane.showMessageDialog(jframePai, "ERRO",
                                    "Proponente: " + txtID.getText() + " nao registado", JOptionPane.ERROR_MESSAGE);
                        }

                        int resposta2 = JOptionPane.showConfirmDialog(jframePai, "Deseja adicionar mais proponentes",
                                "Deseja Continuar", JOptionPane.YES_NO_OPTION);
                        if (resposta2 == JOptionPane.YES_OPTION) {
                            txtID.setText(null);
                        } else {
                        dispose();
                        new ConfirmacaoUI(jframePai, criarSessaoController);
                        }
                }
                }
                }catch(IllegalArgumentException ex){
                    JOptionPane.showMessageDialog(jframePai,ex.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
                
                }}
            
        });
        return btn;
    }
}
