
package eventoscientificos.ui.CriarSessaoTematica;

import eventoscientificos.controllers.CriarSessaoTematicaController;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class ConfirmacaoUI extends JDialog{
    
    private JFrame jframePai;
    private CriarSessaoTematicaController criarSessaoController;
    private JTextArea txtDados;
     
    ConfirmacaoUI(JFrame jframePai, CriarSessaoTematicaController criarSessaoController){
        super(jframePai,"Confirmacao de Dados");
        this.jframePai=jframePai;
        this.criarSessaoController=criarSessaoController;
        add(criarPainelConfirmacao(),BorderLayout.CENTER);   
        add(criarPainelButtons(),BorderLayout.SOUTH);
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }
    
    private JPanel criarPainelConfirmacao(){
            JPanel p = new JPanel();
            JLabel dadosInseridos= new JLabel("Dados Inseridos");
            txtDados=new JTextArea(criarSessaoController.getSessaoTematica().getInfo());
            p.add(dadosInseridos);
            p.add(txtDados);
            return p;
    }
    
    private JPanel criarPainelButtons(){
        JPanel p = new JPanel();
        p.add(criarButtonConfirmar());
        p.add(criarButtonCancelar());
        return p;
    }
    
     private JButton criarButtonCancelar(){
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
            });
     return btn;
    }
    private JButton criarButtonConfirmar(){
        JButton btn = new JButton("Confirmar");
        btn.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
               criarSessaoController.registaSessaoTematica();
               JOptionPane.showMessageDialog(jframePai,"Sessao Tematica Criada com Sucesso");
               dispose();
            }

    });
return btn;

}
}