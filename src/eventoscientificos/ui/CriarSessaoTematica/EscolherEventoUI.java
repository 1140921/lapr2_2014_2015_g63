package eventoscientificos.ui.CriarSessaoTematica;

import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class EscolherEventoUI extends JDialog {

    private JFrame jframePai;
    private JComboBox<Evento> listEventos;
    private Empresa empresa;
    private CriarSessaoTematicaController criarSessaoController;

    public EscolherEventoUI(JFrame jframePai, Empresa empresa) {
        super(jframePai, "Escolher Evento");
        this.jframePai = jframePai;
        this.empresa = empresa;
        criarSessaoController = new CriarSessaoTematicaController(empresa);
        add(criarPainelCentro(), BorderLayout.CENTER);
        add(criarPainelButtons(), BorderLayout.SOUTH);  
        if(listEventos.getItemCount()==0){
        JOptionPane.showMessageDialog(jframePai,
        "Nao existem eventos registados com o utilizador "+empresa.getLogin().getUsername()+".");   
        dispose();
        }else{
        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(jframePai);
        setVisible(true);
     }
   }

    private JPanel criarPainelCentro() {
        JPanel p = new JPanel();
        listEventos = new JComboBox();
        List<Evento> eventos=criarSessaoController.getListaEventosRegistadosDoUtilizador(empresa.getLogin().getUsername());
        
        for (Evento e : eventos){ 
            listEventos.addItem(e);
        }
        p.add(listEventos);
        
        return p;
    }

    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }

    private JButton criarButtonSelecionar() {
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Evento evento = (Evento) listEventos.getSelectedItem();
                if (evento == null) {
                    JOptionPane.showMessageDialog(jframePai,"Selecione um evento");
                }else{
                    criarSessaoController.setEvento(evento);
                    dispose();
                    new IntroduzirDadosUI(jframePai, criarSessaoController);
                }
            }
        });
        return btn;

    }

    private JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}
