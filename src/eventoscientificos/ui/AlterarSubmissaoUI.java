/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controllers.AlterarSubmissaoController;
import eventoscientificos.controllers.CriarSessaoTematicaController;
import eventoscientificos.controllers.RegistarUtilizadorController;
import eventoscientificos.domain.Artigo;
import eventoscientificos.domain.Autor;
import eventoscientificos.domain.Empresa;
import eventoscientificos.domain.Evento;
import eventoscientificos.domain.ListaSubmissoes;
import eventoscientificos.domain.Submissao;
import eventoscientificos.domain.Submissivel;
import eventoscientificos.domain.Utilizador;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class AlterarSubmissaoUI extends JDialog {

    private JFrame framePai;
    private JComboBox<Evento> listEventos;
    private JComboBox<Submissivel> listComboBox;
    private Submissao m_submissao;
    private Artigo artigo;
    private Empresa m_empresa;
    private AlterarSubmissaoController controller;
    private Autor m_autor;

    public AlterarSubmissaoUI(JFrame parentFrame, Empresa empresa) {

        super(parentFrame, "Alterar Submissão");

        m_empresa = empresa;
        controller = new AlterarSubmissaoController(m_empresa);

        setLayout(new GridLayout(0, 1));

        Container c = getContentPane();
        c.add(criarPainelEscolherEvento(), BorderLayout.NORTH);
        c.add(criarPainelButtons(), BorderLayout.CENTER);
//        c.add(criarPainelPassword());
//        c.add(criarPainelEmail());
//        c.add(criarPainelBotao());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    public JPanel criarPainelEscolherEvento() {
        JPanel p = new JPanel();
        listComboBox = new JComboBox();
        List<Submissivel> lstSubmissivel = controller.getListaSubmissoes();
        for (Submissivel submissoes : lstSubmissivel) {
         
            listComboBox.addItem(submissoes);
        }
        p.add(listComboBox);

        return p;
    }

    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }

    public JButton criarButtonSelecionar() {
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            Submissivel submissivel = (Submissivel) listComboBox.getSelectedItem();
            controller.setSubmissivel(submissivel);
             
                new JanelaAlterarDadosSubmissao(framePai,submissivel,controller);


            }
        });
        return btn;
    }

    public JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                dispose();

            }
        });
        return btn;
    }
}
//class AlterarSubmissaoUI implements UI
//{
//    private Empresa m_empresa;
//    private AlterarSubmissaoController m_controllerAlterarSubmissao;
//
//    public AlterarSubmissaoUI( Empresa empresa )
//    {
//        m_empresa = empresa;
//        m_controllerAlterarSubmissao = new AlterarSubmissaoController(m_empresa);
//    }
//
//    public void run()
//    {
//
//    }
//}
